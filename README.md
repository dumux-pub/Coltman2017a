SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

E. Coltman<br>
Numerical Investigation of Turbulent Flow Around Obstacles and Evaporating Porous Media<br>
Master's Thesis, 2017<br>
Universität Stuttgart

You can use the .bib file provided [here](Coltman2017a.bib).

Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installColtman2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Coltman2017a/raw/master/installColtman2017a.sh)
in this folder.

```bash
mkdir -p Coltman2017a && cd Coltman2017a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Coltman2017a/raw/master/installColtman2017a.sh
sh ./installColtman2017a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installColtman2017a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Coltman2017a/raw/master/installColtman2017a.sh).

In addition the following external software packages, with one of the two linear
solvers, are necessary for compiling the executables:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              |       ? | build tool        |
| suitesparse        |       ? | matrix algorithms |
| UMFPack            |       ? | linear solver     |
| SuperLU            |       ? | linear solver     |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| clang/clang++ |       ? |
| gcc/g++       |       ? |
