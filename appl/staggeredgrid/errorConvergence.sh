#!/bin/bash

if [ "$1" == "-h" -o "$1" == "--h" -o "$1" == "--help" -o "$1" == "help" -o $#  -lt 5 ]; then
  echo
  echo "Usage:"
  echo "$0 PATH_TO_RUNTEST PATH_TO_EXECUTABLE VTU_PREFIX DIMENSION NUM_START_CELL NUM_REFINEMENTS"
  echo " or "
  echo "$0 PATH_TO_EXECUTABLE VTU_PREFIX DIMENSION NUM_START_CELL NUM_REFINEMENTS"
  echo
  exit
fi

if [ $# -gt 5 ]; then
PATH_TO_RUNTEST=$1
PATH_TO_EXECUTABLE=$2
VTU_PREFIX=$3
DIMENSION=$4
NUM_START_CELLS=$5
REFINEMENTS=$6
else
PATH_TO_EXECUTABLE=$1
VTU_PREFIX=$2
DIMENSION=$3
NUM_START_CELLS=$4
REFINEMENTS=$5
fi

FILEENDING="vtp"
if [ $DIMENSION -gt 1 ]; then
  FILEENDING="vtu"
fi


CELLS=$NUM_START_CELLS
echo "# error for $1" > infile.txt
for ((REF=0; REF<$((REFINEMENTS+1)); REF++)); do
./$PATH_TO_EXECUTABLE -Grid.Refinement $REF | tee temp.txt
cp `find . -name \*-0001.$FILEENDING` temp-$REF.$FILEENDING
cp `find . -name \*-00001.$FILEENDING` tempSecondary-$REF.$FILEENDING
if [ $# -gt 5 ]; then
  if python $PATH_TO_RUNTEST --script fuzzy --command "./$PATH_TO_EXECUTABLE -Grid.Refinement $REF" --files test_references/$VTU_PREFIX"_"$CELLS.$FILEENDING $VTU_PREFIX-0001.$FILEENDING > fuzzycompare.log; then
    grep "L2 error" temp.txt | tail -n 1 >> infile.txt
    CELLS=$((CELLS*2))
  else
    echo "Comparing reference solutions failed test_references/"$VTU_PREFIX"_"$CELLS".$FILEENDING "$VTU_PREFIX"-0001.$FILEENDING"
    grep -A 1000 -B 2 "Fuzzy comparison" fuzzycompare.log
    exit 1
  fi
else
  grep "L2 error" temp.txt | tail -n 1 >> infile.txt
  CELLS=$((CELLS*2))
fi
echo "Refinement $REF of $REFINEMENTS done."
done
# cat infile.txt

echo 'reset' > temp.gnuplot
echo 'set terminal png size 800,600' >> temp.gnuplot
echo 'set output "convergence.png"' >> temp.gnuplot
echo 'set yrange [0.0001:1]' >> temp.gnuplot
echo 'set title "convergence rate for '$PATH_TO_EXECUTABLE' - numStartCells '$NUM_START_CELLS'"' >> temp.gnuplot
echo 'set xlabel "refinement"' >> temp.gnuplot
echo 'set ylabel "L2-error"' >> temp.gnuplot
echo 'set log y' >> temp.gnuplot
echo 'set arrow from 1,0.006 to 2,0.003 nohead lc rgb "gray"' >> temp.gnuplot
echo 'set arrow from 1,0.003 to 2,0.003 nohead lc rgb "gray"' >> temp.gnuplot
echo 'set arrow from 1,0.003 to 1,0.006 nohead lc rgb "gray"' >> temp.gnuplot
echo 'plot \' >> temp.gnuplot

VAR=`tail -n 1 infile.txt | awk '{ print $6 }'`
NR_VAR=`tail -n 1 infile.txt | wc -w`

echo ' "infile.txt" u :7 w lp ps 1 pt 1 t "'$VAR' L2 error" \' >> temp.gnuplot
for ((i=0; i<$((NR_VAR-7)); i+=2)); do
  VAR=`tail -n 1 infile.txt |  awk -v i=$((i+8)) '{print $i}'`
  echo ', "infile.txt" u :'$((i+9))' w lp ps 1 pt 1 t "'$VAR' L2 error" \' >> temp.gnuplot
done
echo '' >> temp.gnuplot

gnuplot temp.gnuplot
# rm temp.gnuplot temp.txt
mv infile.txt error_$VTU_PREFIX.txt
mv convergence.png convergence_$VTU_PREFIX.png
# eog convergence_$1.png &

exit 0
