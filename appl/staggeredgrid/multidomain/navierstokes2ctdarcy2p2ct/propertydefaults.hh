// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_PROPERTYDEFAULTS_HH
#define DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_PROPERTYDEFAULTS_HH

#include <dune/pdelab/gridoperator/onestep.hh>

#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>

#include <dumux/porousmediumflow/2p2c/implicit/properties.hh>
#include <dumux/porousmediumflow/2p2c/implicit/propertydefaults.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include <appl/staggeredgrid/common/fixpressureconstraints.hh>
#include <appl/staggeredgrid/common/fixvelocityconstraints.hh>
#if COUPLING_KOMEGA
#include <appl/staggeredgrid/freeflow/twoeq/komega/fixdissipationconstraints.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cniproperties.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cnipropertydefaults.hh>
#elif COUPLING_KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/fixturbulentkineticenergyconstraints.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/fixdissipationconstraints.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cniproperties.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnipropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cniproperties.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnipropertydefaults.hh>
#elif COUPLING_ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cniproperties.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cnipropertydefaults.hh>
#elif COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cniproperties.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cnipropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproperties.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/common/multidomainproperties.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainpropertydefaults.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainlocaloperator.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainnewtonmethod.hh>

#include "boundaryconditions.hh"
#include "couplingoperator.hh"
#include "indices.hh"
#include "properties.hh"

namespace Dune {
namespace PDELab {
// forward declarations
#if COUPLING_KOMEGA
template <class TypeTag> class KOmegaTwoCNIStaggeredGrid;
template <class TypeTag> class KOmegaTwoCNITransientStaggeredGrid;
#elif COUPLING_KEPSILON
template <class TypeTag> class KEpsilonTwoCNIStaggeredGrid;
template <class TypeTag> class KEpsilonTwoCNITransientStaggeredGrid;
#elif COUPLING_LOWREKEPSILON
template <class TypeTag> class LowReKEpsilonTwoCNIStaggeredGrid;
template <class TypeTag> class LowReKEpsilonTwoCNITransientStaggeredGrid;
#elif COUPLING_ONEEQ
template <class TypeTag> class SpalartAllmarasTwoCNIStaggeredGrid;
template <class TypeTag> class SpalartAllmarasTwoCNITransientStaggeredGrid;
#elif COUPLING_ZEROEQ
template <class TypeTag> class ZeroEqTwoCNIStaggeredGrid;
template <class TypeTag> class NavierStokesTwoCNITransientStaggeredGrid; // no own transient lop
#else
template <class TypeTag> class NavierStokesTwoCNIStaggeredGrid;
template <class TypeTag> class NavierStokesTwoCNITransientStaggeredGrid;
#endif
}
}

namespace Dumux
{
namespace Properties
{

// sub-domains type tags
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, StokesSubProblemTypeTag, TTAG(StokesSubProblem));
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, DarcySubProblemTypeTag, TTAG(DarcySubProblem));

// multi-domain type tag
SET_TYPE_PROP(StokesSubProblem, MultiDomainTypeTag, TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT));
SET_TYPE_PROP(DarcySubProblem, MultiDomainTypeTag, TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT));

// modified class for FVElementGeometry
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, FVElementGeometry,
              MultidomainCCFVElementGeometry<TTAG(DarcySubProblem)>);
SET_TYPE_PROP(DarcySubProblem, FVElementGeometry,
              MultidomainCCFVElementGeometry<TTAG(DarcySubProblem)>);

// single grid function spaces
SET_TYPE_PROP(StokesSubProblem, PressureConstraints,
              Dumux::FixPressureConstraints<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(StokesSubProblem, VelocityConstraints,
              Dumux::FixVelocityConstraints<TTAG(StokesSubProblem)>);
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> type;
};
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0Constrained)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using PressureConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), PressureConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, PressureConstraints, VectorBackend> type;
};

#if (COUPLING_KEPSILON)
SET_TYPE_PROP(StokesSubProblem, TurbulentKineticEnergyConstraints,
              Dumux::FixTurbulentKineticEnergyConstraints<TTAG(StokesSubProblem)>);
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using TurbulentKineticEnergyConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), TurbulentKineticEnergyConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, TurbulentKineticEnergyConstraints, VectorBackend> type;
};
#endif

#if (COUPLING_KOMEGA || COUPLING_KEPSILON)
SET_TYPE_PROP(StokesSubProblem, DissipationConstraints,
              Dumux::FixDissipationConstraints<TTAG(StokesSubProblem)>);
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0DissipationConstrained)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using DissipationConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), DissipationConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, DissipationConstraints, VectorBackend> type;
};
#endif

SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceStaggered)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), SubDomainGridView);
    using StaggeredQ0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapStaggered);
    using VelocityConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), VelocityConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, StaggeredQ0FEM, VelocityConstraints, VectorBackend> type;
};
SET_PROP(DarcySubProblem, SubDomainScalarGridFunctionSpaceP0)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(DarcySubProblem), FiniteElementMapP0);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> type;
};

// composed grid function spaces
SET_PROP(StokesSubProblem, SubDomainComposedGridFunctionSpace)
{
private:
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
    using StaggeredGFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceStaggered);
    using P0GFSConstrained = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0Constrained);
    using P0GFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0);
#if (COUPLING_KOMEGA)
    using DissipationConstrained = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#elif (COUPLING_KEPSILON)
    using TurbulentKineticEnergyConstrained = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained);
    using DissipationConstrained = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#endif
public:
    typedef Dune::PDELab::CompositeGridFunctionSpace<
        VectorBackend, Dune::PDELab::LexicographicOrderingTag,
        StaggeredGFS, P0GFSConstrained, P0GFS, P0GFS
#if COUPLING_KOMEGA
        , P0GFS, DissipationConstrained
#elif COUPLING_LOWREKEPSILON
        , P0GFS, P0GFS
#elif COUPLING_KEPSILON
        , TurbulentKineticEnergyConstrained, DissipationConstrained
#elif COUPLING_ONEEQ
        , P0GFS
#endif
        > type;
};
SET_PROP(DarcySubProblem, SubDomainComposedGridFunctionSpace)
{
private:
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
    using P0GFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0);
public:
    typedef Dune::PDELab::CompositeGridFunctionSpace<
        VectorBackend, Dune::PDELab::LexicographicOrderingTag,
        P0GFS, P0GFS, P0GFS> type;
};

// multidomain grid function space
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, MultiDomainGridFunctionSpace)
{
private:
    using MultiDomainGrid = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), MultiDomainGrid);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
    using StokesGFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainComposedGridFunctionSpace);
    using DarcyGFS = GET_PROP_TYPE(TTAG(DarcySubProblem), SubDomainComposedGridFunctionSpace);
public:
    typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
        MultiDomainGrid, VectorBackend,
        Dune::PDELab::LexicographicOrderingTag,
        StokesGFS, DarcyGFS> type;
};

// local operator for stationary part
#if COUPLING_KOMEGA
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::KOmegaTwoCNIStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_KEPSILON
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::KEpsilonTwoCNIStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_LOWREKEPSILON
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::LowReKEpsilonTwoCNIStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ONEEQ
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::SpalartAllmarasTwoCNIStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ZEROEQ
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::ZeroEqTwoCNIStaggeredGrid<TTAG(StokesSubProblem)>);
#else
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::NavierStokesTwoCNIStaggeredGrid<TTAG(StokesSubProblem)>);
#endif
SET_TYPE_PROP(DarcySubProblem, LocalOperator,
              Dumux::PDELab::PDELabLocalOperator<TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>);

// local operator for transient part
#if COUPLING_KOMEGA
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::KOmegaTwoCNITransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_KEPSILON
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::KEpsilonTwoCNITransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_LOWREKEPSILON
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::LowReKEpsilonTwoCNITransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ONEEQ
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::SpalartAllmarasTwoCNITransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ZEROEQ // NOTE: does not have an own transient lop
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTwoCNITransientStaggeredGrid<TTAG(StokesSubProblem)>);
#else
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTwoCNITransientStaggeredGrid<TTAG(StokesSubProblem)>);
#endif
SET_TYPE_PROP(DarcySubProblem, TransientLocalOperator,
              Dumux::PDELab::PDELabInstationaryLocalOperator);

// stationary PDELab sub problem
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, SubProblemStationaryStokes)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using LOpStokes = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag), LocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, LOpStokes, Condition, 0> type;
};
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, SubProblemStationaryDarcy)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using LOpDarcy = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag), LocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, LOpDarcy, Condition, 1> type;
};

// transient PDELab sub problem
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, SubProblemTransientStokes)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using TLopStokes = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag), TransientLocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, TLopStokes, Condition, 0> type;
};
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, SubProblemTransientDarcy)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using TLopDarcy = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag), TransientLocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, TLopDarcy, Condition, 1> type;
};

// coupling operator
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingOperator, Dumux::CouplingStokes2p2cniDarcy2cni<TypeTag>);

SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, Coupling)
{
private:
    using Scalar = GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), Scalar);
    using SubProblemStationaryStokes = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryStokes);
    using SubProblemStationaryDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryDarcy);
    using CouplingOperator = typename GET_PROP_TYPE(TypeTag, CouplingOperator);
public:
    typedef Dune::PDELab::MultiDomain::Coupling<
      SubProblemStationaryStokes, SubProblemStationaryDarcy, CouplingOperator> type;
};

// stationary multi domain grid operator
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, MultiDomainStationaryGridOperator)
{
private:
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using MatrixBackend = Dune::PDELab::ISTLMatrixBackend;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ConstraintsContainer = typename MultiGFS::template ConstraintsContainer<Scalar>::Type;
    using SubProblemStationaryStokes = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryStokes);
    using SubProblemStationaryDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryDarcy);
    using Coupling = typename GET_PROP_TYPE(TypeTag, Coupling);
public:
    typedef Dune::PDELab::MultiDomain::GridOperator<
        MultiGFS, MultiGFS,
        MatrixBackend, Scalar, Scalar, Scalar,
        ConstraintsContainer, ConstraintsContainer,
        SubProblemStationaryStokes,
        SubProblemStationaryDarcy,
        Coupling> type;
};

// transient multi domain grid operator
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, MultiDomainTransientGridOperator)
{
private:
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using MatrixBackend = Dune::PDELab::ISTLMatrixBackend;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ConstraintsContainer = typename MultiGFS::template ConstraintsContainer<Scalar>::Type;
    using SubProblemTransientStokes = typename GET_PROP_TYPE(TypeTag, SubProblemTransientStokes);
    using SubProblemTransientDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemTransientDarcy);
public:
    typedef Dune::PDELab::MultiDomain::GridOperator<
        MultiGFS, MultiGFS,
        MatrixBackend, Scalar, Scalar, Scalar,
        ConstraintsContainer, ConstraintsContainer,
        SubProblemTransientStokes,
        SubProblemTransientDarcy> type;
};

// full multidomain grid operator
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, MultiDomainGridOperator)
{
private:
    using StationaryGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainStationaryGridOperator);
    using TransientGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainTransientGridOperator);
public:
    typedef Dune::PDELab::OneStepGridOperator<
        StationaryGridOperator, TransientGridOperator> type;
};

// copy runtime parameters
SET_PROP(StokesSubProblem, ParameterTree)
{
private:
    using ParameterTree = typename GET_PROP(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), ParameterTree);
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }
};
SET_PROP(DarcySubProblem, ParameterTree)
{
private:
    using ParameterTree = typename GET_PROP(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), ParameterTree);
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }
};

// modified Newton method
SET_TYPE_PROP(DarcySubProblem, NewtonMethod, PDELabNewtonMethod<TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>);
SET_TYPE_PROP(DarcySubProblem, NewtonController, NewtonController<TypeTag>);

// set TimeManager for sub-problems
SET_TYPE_PROP(StokesSubProblem, TimeManager, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), TimeManager));
SET_TYPE_PROP(DarcySubProblem, TimeManager, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), TimeManager));

// some Darcy things
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, Model, GET_PROP_TYPE(TTAG(DarcySubProblem), Model));
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, SolutionVector, GET_PROP_TYPE(TTAG(DarcySubProblem), SolutionVector));
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, JacobianAssembler, GET_PROP_TYPE(TTAG(DarcySubProblem), JacobianAssembler));
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, JacobianMatrix, GET_PROP_TYPE(TTAG(DarcySubProblem), JacobianMatrix));
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, VertexMapper, GET_PROP_TYPE(TTAG(DarcySubProblem), VertexMapper));

// set the used linear solver
SET_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, LinearSolver)
{
private:
    using MultiDomainGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator);
    using MatrixBase = typename MultiDomainGridOperator::Traits::Jacobian::BaseT;
public:
    typedef Dune::UMFPack<MatrixBase> type;
};

// Set the indices used by the model
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, Indices, NavierStokesTwoCNIDarcyTwoPTwoCNIIndices<TypeTag>);

// Use Beavers Joseph as solution dependent Neumann on default (is more stable than solDependetDirichlet)
SET_BOOL_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingBeaversJosephAsSolDependentDirichlet, false);

// Minimum accepted time step size to proceed simulation
SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, NewtonAbortTimeStepSize, 1e-8);

// Factor to adapt the time step, if Newton failed
SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, NewtonTimeStepReduction, 0.5);

// Output frequency of Vtk files
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, VtkFreqOutput, 1);

// Reference pressure used for pn_rel in Vtk files
SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, VtkReferencePressure, 1e5);

// Criteria for the interface Newton
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingSolverMaxSteps, 10);
SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingSolverResidualReduction, 1e-2);
SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingSolverMaxRelativeShift, 1e-3);
SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingSlopeLimitingFactor, 1.0); // between 0< (allow all values) and 1 (allow only values from adjacent cells)

// Limit range of k and epsilon to positive values
SET_BOOL_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, NewtonKEpsilonLimiter, false);

// // Set the default episode length negative -> every time step a vtk is written
// SET_SCALAR_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, TimeManagerEpisodeLength, -1.0);

// Set the default Output frequency
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, TimeManagerOutputFreq, 1);



} // end namespace Properties
} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_PROPERTYDEFAULTS_HH
