// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_DAVARZANI_STOKES_SUBPROBLEM_HH
#define DUMUX_DAVARZANI_STOKES_SUBPROBLEM_HH

#if COUPLING_KOMEGA
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cnipropertydefaults.hh>
#elif COUPLING_KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnipropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnipropertydefaults.hh>
#elif COUPLING_ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cnipropertydefaults.hh>
#elif COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cnipropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>
#endif

#include <dumux/freeflow/turbulenceproperties.hh>
#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

namespace Dumux
{
template <class TypeTag>
class DavarzaniStokesSubProblem;

namespace Properties
{
// Do not use pressure constraints
SET_BOOL_PROP(StokesSubProblem, FixPressureConstraints, false);

// Use velocity constraints
SET_BOOL_PROP(StokesSubProblem, FixVelocityConstraints, true);

// Use complete Navier-Stokes equation
SET_BOOL_PROP(StokesSubProblem, ProblemEnableNavierStokes, true);

// Disable gravity field
SET_BOOL_PROP(StokesSubProblem, ProblemEnableGravity, false);

// Use symmetrized velocity gradient
SET_BOOL_PROP(StokesSubProblem, ProblemEnableUnsymmetrizedVelocityGradient, false);

// Use symmetry on top
NEW_PROP_TAG(FreeFlowTopIsSymmetry);
SET_BOOL_PROP(StokesSubProblem, FreeFlowTopIsSymmetry, true);

#if COUPLING_KEPSILON
// Use Pope Wall Functions on default
SET_INT_PROP(StokesSubProblem, KEpsilonWallFunctionModel, 2);
#endif
}

template <class TypeTag>
class DavarzaniStokesSubProblem
#if COUPLING_KOMEGA
  : public KOmegaTwoCNIProblem<TypeTag>
#elif COUPLING_KEPSILON
  : public KEpsilonTwoCNIProblem<TypeTag>
#elif COUPLING_LOWREKEPSILON
  : public LowReKEpsilonTwoCNIProblem<TypeTag>
#elif COUPLING_ONEEQ
  : public SpalartAllmarasTwoCNIProblem<TypeTag>
#elif COUPLING_ZEROEQ
  : public ZeroEqTwoCNIProblem<TypeTag>
#else
  : public NavierStokesTwoCNIProblem<TypeTag>
#endif
{
#if COUPLING_KOMEGA
    using ParentType = KOmegaTwoCNIProblem<TypeTag>;
#elif COUPLING_KEPSILON
    using ParentType = KEpsilonTwoCNIProblem<TypeTag>;
    using KEpsilonWallFunctions = typename GET_PROP_TYPE(TypeTag, KEpsilonWallFunctions);
#elif COUPLING_LOWREKEPSILON
    using ParentType = LowReKEpsilonTwoCNIProblem<TypeTag>;
#elif COUPLING_ONEEQ
    using ParentType = SpalartAllmarasTwoCNIProblem<TypeTag>;
#elif COUPLING_ZEROEQ
    using ParentType = ZeroEqTwoCNIProblem<TypeTag>;
#else
    using ParentType = NavierStokesTwoCNIProblem<TypeTag>;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    using MultiDomainTypeTag = typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    enum { dim = GridView::dimension };
    using DimVector = typename GET_PROP_TYPE(TypeTag, DimVector);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { phaseIdx = Indices::phaseIdx,
           transportCompIdx = Indices::transportCompIdx };

public:
    DavarzaniStokesSubProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
#if COUPLING_KEPSILON
      , kEpsilonWallFunctions_(gridView_, asImp_())
#endif
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Velocity);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Pressure);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Temperature);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassMoleFrac);
        interfacePos_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        darcyStart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyEnd_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure_);
        fluidState.setTemperature(temperature_);
        fluidState.setMassFraction(phaseIdx, transportCompIdx, massMoleFrac_);
#if COUPLING_KOMEGA
        Scalar density_ = FluidSystem::density(fluidState, phaseIdx);
        Scalar kinematicViscosity_ = FluidSystem::viscosity(fluidState, phaseIdx) / density_;
        Scalar diameter_ = (this->bBoxMax()[1] - interfacePos_)*2;
        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        dissipationRate_ = turbulenceProperties.dissipationRate(velocity_, diameter_, kinematicViscosity_, true);
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(velocity_, diameter_, kinematicViscosity_, true);
        std::cout << " diameter " << diameter_ << std::endl;;
        std::cout << " density " << density_ << std::endl;;
        std::cout << " kinematicViscosity " << kinematicViscosity_ << std::endl;;
        std::cout << " turbulentKineticEnergy " << turbulentKineticEnergy_ << std::endl;;
        std::cout << " dissipationRate " << dissipationRate_ << std::endl;;
        std::cout << " diameter " << diameter_ << std::endl;;
#endif
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        // overwrite the parent function
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    bool bcVelocityIsSymmetry(const DimVector& global) const
    {
      return (global[1] > this->bBoxMax()[1] - eps_)
            && (!bcVelocityIsCoupling(global));
    }

    bool bcVelocityIsWall(const DimVector& global) const
    {
      return (global[1] < interfacePos_ + eps_)
                && (!bcVelocityIsCoupling(global))
                && (!bcVelocityIsSymmetry(global));
    }

    bool bcVelocityIsInflow(const DimVector& global) const
    {
        return (global[0] < this->bBoxMin()[0] + eps_)
            && (global[1] > interfacePos_ - eps_)
            && (!bcVelocityIsCoupling(global))
            && (!bcVelocityIsSymmetry(global))
            && (!bcVelocityIsWall(global));
    }

    bool bcVelocityIsOutflow(const DimVector& global) const
    { return global[0] > this->bBoxMax()[0] - eps_
             && (!bcVelocityIsCoupling(global))
             && (!bcVelocityIsSymmetry(global))
             && (!bcVelocityIsWall(global))
             && (!bcVelocityIsInflow(global));
    }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    {
      return global[0] > this->bBoxMax()[0] - eps_;
    }

    bool bcPressureIsCoupling(const DimVector& global) const
    {
      return isOnCouplingFace(global) && !bcPressureIsDirichlet(global);
    }

    bool bcPressureIsOutflow(const DimVector& global) const
    {
      return (   !bcPressureIsDirichlet(global)
              && !bcPressureIsCoupling(global) );
    }

    //! \brief MassMoleFrac boundary condition types
    bool bcMassMoleFracIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcMassMoleFracIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcMassMoleFracIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcMassMoleFracIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }
    bool bcMassMoleFracIsCoupling(const DimVector& global) const
    { return bcVelocityIsCoupling(global); }

    //! \brief Temperature boundary condition types
    bool bcTemperatureIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcTemperatureIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcTemperatureIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcTemperatureIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }
    bool bcTemperatureIsCoupling(const DimVector& global) const
    { return bcVelocityIsCoupling(global); }

    //! \brief Velocity Diriclet Values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector x(0.0);

        if (bcVelocityIsWall(global))                                                             // at all, velocity is 0
            {x[0] = 0.0;}
        else
            {x[0]=velocity_;}
        return x;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    {
        return pressure_;
    }
    Scalar dirichletMassMoleFracAtPos(const DimVector& global) const
    {
        return massMoleFrac_;
    }
    Scalar dirichletTemperatureAtPos(const DimVector& global) const
    {
        return temperature_;
    }

#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
    //! \brief TurbulentKineticEnergy boundary condition types

    bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
    {
      return bcVelocityIsSymmetry(global);
    }

    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    {
      return bcVelocityIsInflow(global)
             && !bcTurbulentKineticEnergyIsSymmetry(global);
    }

    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    {
      return bcVelocityIsOutflow(global)
            && !bcTurbulentKineticEnergyIsSymmetry(global)
            && !bcTurbulentKineticEnergyIsInflow(global);
    }

    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    {
      return (bcVelocityIsWall(global) || bcVelocityIsCoupling(global))
             && !bcTurbulentKineticEnergyIsSymmetry(global)
             && !bcTurbulentKineticEnergyIsInflow(global)
             && !bcTurbulentKineticEnergyIsOutflow(global);
    }

    //! \brief TurbulentKineticEnergy boundary condition values
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
#if COUPLING_KOMEGA
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
    }
#endif
#if COUPLING_KEPSILON
        if (this->kEpsilonWallFunctions().useWallFunctionTurbulentKineticEnergy(e)
            && this->timeManager().time() > eps_)
        {
            return this->kEpsilonWallFunctions().wallFunctionTurbulentKineticEnergy(e);
        }
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, TurbulentKineticEnergy);
        }
#endif
#if !COUPLING_KEPSILON && !COUPLING_KOMEGA
        if ((bcTurbulentKineticEnergyIsInflow(global) && isOnBoundary(global))
            || this->timeManager().time() < eps_)
        {
         return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, TurbulentKineticEnergy);
        }
        return 0.0;
}
#endif

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsSymmetry(const DimVector& global) const
    {
    return bcVelocityIsSymmetry(global);
    }

    bool bcDissipationIsInflow(const DimVector& global) const
    {
    return bcVelocityIsInflow(global)
            && !bcDissipationIsSymmetry(global);
    }

    bool bcDissipationIsOutflow(const DimVector& global) const
    {
    return bcVelocityIsOutflow(global)
            && !bcDissipationIsSymmetry(global)
            && !bcDissipationIsInflow(global);
    }

    bool bcDissipationIsWall(const DimVector& global) const
    {
    return (bcVelocityIsWall(global) || bcVelocityIsCoupling(global))
           && !bcDissipationIsInflow(global)
           && !bcDissipationIsOutflow(global)
           && !bcDissipationIsSymmetry(global);
    }

    //! \brief Dissipation boundary condition values
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
#if COUPLING_KOMEGA
        Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> dofMapper(gridView_);
        Scalar BCdissipation = 0.0;

        if (bcDissipationIsInflow(global))
        {
            BCdissipation = dissipationRate_; //dissipation inflow and initial conditions
        }
        else if (this->timeManager().time() > 0.0)
        {
            BCdissipation = 6.0 * 1.57665e-5 / (0.075 * std::pow(this->wallDistance_(dofMapper.index(e)), 2)); //dissipation at boundary
        }
        else
        {
          BCdissipation = dissipationRate_; //dissipation inflow and initial conditions
        }
        return BCdissipation;
    }
#endif
#if COUPLING_KEPSILON
        if (this->kEpsilonWallFunctions().useWallFunctionDissipation(e)
            && this->timeManager().time() > eps_)
        {
          return kEpsilonWallFunctions().wallFunctionDissipation(e);
        }
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Dissipation);
    }
#endif
#if !COUPLING_KEPSILON && !COUPLING_KOMEGA
        if ((bcDissipationIsInflow(global) && isOnBoundary(global))
            || this->timeManager().time() < eps_)
        {
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Dissipation);
        }
        return 0.0;
    }
#endif

#endif

#if COUPLING_ONEEQ
    //! \brief ViscosityTilde boundary condition types
    bool bcViscosityTildeIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }
    bool bcViscosityTildeIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcViscosityTildeIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcViscosityTildeIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    //! \brief ViscosityTilde boundary condition values
    Scalar dirichletViscosityTildeAtPos(const Element& e, const DimVector& global) const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, ViscosityTilde); // [-]
    }
#endif

    //! \brief Returns whether we are on a coupling face
    const bool isOnCouplingFace(const DimVector& global) const
    {
        return (global[0] > darcyStart_ - eps_ && global[0] < + eps_) //darcy area in x direction (length)
#if DUMUX_MULTIDOMAIN_DIM == 3
                && (global[1] > darcyEnd_ - eps_ && global[1] < darcyStart_ + eps_)  //darcy area in y direction (width)
#endif
                && (global[dim - 1] < interfacePos_ + eps_); //height of interface
    }

    //! \brief Return whether a point is located on the boundary of the domain
    bool isOnBoundary(const DimVector& global) const
    {
        return global[0] < this->bBoxMin()[0] + eps_
               || global[0] > this->bBoxMax()[0] - eps_
               || global[1] > this->bBoxMax()[1] - eps_
               || global[1] < this->interfacePos_ + eps_
               || isOnCouplingFace(global);
    }

    #if COUPLING_KEPSILON
    //! \brief Returns the KEpsilonWallFunctions object used by the simulation
    const KEpsilonWallFunctions &kEpsilonWallFunctions() const
    { return kEpsilonWallFunctions_; }

    //! \brief Returns a constant value for the turbulentKineticEnergy wall function
    const Scalar constantWallFunctionTurbulentKineticEnergy() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, WallTurbulentKineticEnergy); }

    //! \brief Returns a constant value for the dissipation wall function
    const Scalar constantWallFunctionDissipation() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, WallDissipation); }
#endif

private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;
    DimVector bBoxMinFiltered_;
    bool topIsSymmetry_;

    Scalar velocity_;
    Scalar pressure_;
    Scalar temperature_;
    Scalar massMoleFrac_;
    Scalar interfacePos_;
    Scalar darcyStart_;
    Scalar darcyEnd_;

#if COUPLING_KEPSILON
    KEpsilonWallFunctions kEpsilonWallFunctions_;
#endif
#if COUPLING_KOMEGA
    Scalar dissipationRate_;
    Scalar turbulentKineticEnergy_;
#endif
};

} //end namespace

#endif // DUMUX_DAVARZANI_STOKES_SUBPROBLEM_HH
