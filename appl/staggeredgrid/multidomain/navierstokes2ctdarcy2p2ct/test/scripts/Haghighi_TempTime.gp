reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'Temperature_Temporal.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 't [s]'
set ylabel 'Temperature [K]'
set xrange [0:1000]
set yrange [291.5:296]
set xtics 0,100,1000
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set title font ",20"
set title 'Temperature over Time in Zones of Interest'
set key font ",20"
set key right reverse top samplen 1
plot \
 'test_references/HaghighiNoObs.csv'                using 1:2      w p ps 1 lc 7 pt 7 t 'Unaffected Zone (Experiment)', \
 'test_references/HaghighiDownstream.csv'           using 1:2      w p ps 1 lc 3 pt 7 t 'Downstream Zone (Experiment)', \
 'test_references/HaghighiUpstream.csv'             using 1:2      w p ps 1 lc 1 pt 7 t 'Upstream Zone (Experiment)', \
 'test_references/temp_unaffected_2_8sat.csv'       using 23:20    w l lw 3 lt 1 lc 7 t 'Unaffected Zone (Model)', \
 'test_references/temp_downstream_2_8sat.csv'       using 23:20    w l lw 3 lt 1 lc 3 t 'Downstream Zone (Model)', \
 'test_references/temp_upstream_2_8sat.csv'         using 23:20    w l lw 3 lt 1 lc 1 t 'Upstream Zone (Model)'
