reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'EvaporationRate.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 't [s]'
set ylabel  right
set ylabel 'Evaporation Rate [kg/h]'
set xrange [0:1000]
set yrange [0.10:0.30]
set xtics 0,100,1000
set ytics 0.10,0.01,0.30
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key right reverse top samplen 1
plot \
 'test_references/EvapRate/CylStorage.csv'     using 1:($2*3600*2)      w lp lw 3 lt 1 lc 7 t 'Modeled Evaporation Rate (With Cylinder)', \
 'test_references/EvapRate/NoCylStorage.csv'   using 1:($2*3600*2)      w lp lw 3 lt 1 lc 3 t 'Modeled Evaporation Rate (Without Cylinder)', \

