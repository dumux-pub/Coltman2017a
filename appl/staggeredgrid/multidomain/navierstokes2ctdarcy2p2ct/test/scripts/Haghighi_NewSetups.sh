# Haghighi/Davarzani Problems: Convergence Test
# set data separator and locate the extract line script
  DATA="."
#
# Run Haghighi Simulation Function
  runHaghighiSimulation() {
      ./haghighi_komega -ParameterFile $1 -Problem.Name "$2" | tee -a logfile.out

      mv logfile.out logfile_"$2".out
      mv staggered staggered_"$2".out
      mv storage.out storage_"$2".out
      mv *_1_Small_WCyl_6_8*.pvd *_1_Small_WCyl_6_8*.vtu *_1_Small_WCyl_6_8*out haghighiresults/1_Small_WCyl_6_8sat
      mv *_2_Large_WCyl_6_8*.pvd *_2_Large_WCyl_6_8*.vtu *_2_Large_WCyl_6_8*out haghighiresults/2_Large_WCyl_6_8sat
      mv *_3_Large_NCyl_6_8*.pvd *_3_Large_NCyl_6_8*.vtu *_3_Large_NCyl_6_8*out haghighiresults/3_Large_NCyl_6_8sat
      }
#end function
#Run the simulations with different entries:
    runHaghighiSimulation haghighi_1_6.input Haghighi_1_Small_WCyl_6_8sat
    runHaghighiSimulation haghighi_2_6.input Haghighi_2_Large_WCyl_6_8sat
    runHaghighiSimulation haghighi_3_6.input Haghighi_3_Large_NCyl_6_8sat

