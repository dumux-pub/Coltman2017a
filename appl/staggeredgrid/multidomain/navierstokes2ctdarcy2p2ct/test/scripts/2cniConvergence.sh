# Haghighi/Davarzani Problems: Convergence Test
# set data separator and locate the extract line script
  DATA="."
#Input parameters for both simulations
  INPUT_HAG=haghighi.input
  INPUT_DAV=davarzani_1224.input
#
# Run Haghighi Simulation Function
  runHaghighiSimulation() {
      ./haghighi_komega $INPUT_HAG -Grid.Cells0 "$1" -Grid.Cells1 "$2" -Grid.Cells2 "$3" -Problem.Name "$4" | tee -a logfile.out
      mv logfile.out logfile_"$4".out
      mv staggered staggered_"$4".out
      mv storage.out storage_"$4".out
#       mv *_18_10_19*.pvd *_18_10_19*.vtu *_18_10_19.out haghighiresults/18_10_19
#       mv *_22_12_19*.pvd *_22_12_19*.vtu *_22_12_19.out haghighiresults/22_12_19
#       mv *_26_14_19*.pvd *_26_14_19*.vtu *_26_14_19.out haghighiresults/26_14_19
      mv *_28_15_19*.pvd *_28_15_19*.vtu *_28_15_19.out haghighiresults/28_15_19
      mv *_30_16_19*.pvd *_30_16_19*.vtu *_30_16_19.out haghighiresults/30_16_19
      mv *_32_17_19*.pvd *_32_17_19*.vtu *_32_17_19.out haghighiresults/32_17_19
      }
#end function
#
# Run Davarzani Simulation Function
  runDavarzaniSimulation() {
      ./davarzani_komega $INPUT_DAV -Grid.Cells0 "$1" -Grid.Cells1 "$2" -Problem.Name "$3" | tee -a logfile.out
      mv logfile.out logfile_"$3".out
      mv staggered staggered_"$3".out
      mv storage.out storage_"$3".out
      mv *_050x40*.pvd *_050x40*.vtu *_050x40.out davarzaniresults/050x40
      mv *_050x50*.pvd *_050x50*.vtu *_050x50.out davarzaniresults/050x50
      mv *_050x64*.pvd *_050x64*.vtu *_050x64.out davarzaniresults/050x64
      }
#end function
#Run the simulations with different entries:
#       mkdir haghighiresults/18_10_19
#     runHaghighiSimulation '2 3 04 04 5' '06 04' '02 09 08' Haghighi_Convergence_18_10_19
#       mkdir haghighiresults/22_12_19
#     runHaghighiSimulation '2 3 06 06 5' '06 06' '02 09 08' Haghighi_Convergence_22_12_19
#       mkdir haghighiresults/26_14_19
#     runHaghighiSimulation '2 3 08 08 5' '06 08' '02 09 08' Haghighi_Convergence_26_14_19
#          mkdir davarzaniresults/050x40
#          mkdir davarzaniresults/050x50
#          mkdir davarzaniresults/050x64
#     runHaghighiSimulation '2 3 09 09 5' '06 09' '02 09 08' Haghighi_Convergence_28_15_19
#     runHaghighiSimulation '2 3 10 10 5' '06 10' '02 09 08' Haghighi_Convergence_30_16_19
#     runHaghighiSimulation '2 3 11 11 5' '06 11' '02 09 08' Haghighi_Convergence_32_17_19
#     runDavarzaniSimulation '20 25 5' '20 20' Davarzani_1224_050x40
     runDavarzaniSimulation '20 25 5' '25 25' Davarzani_1224_050x50
#     runDavarzaniSimulation '20 25 5' '32 32' Davarzani_1224_050x64
