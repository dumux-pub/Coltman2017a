// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#include <config.h>

#undef DUNE_MINIMAL_DEBUG_LEVEL
#define DUNE_MINIMAL_DEBUG_LEVEL 6

#define USE_SUPERLU

#include <iostream>
#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/start.hh>
#include "haghighiproblem.hh"

/*!
 * \brief Print a usage string for simulations.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void printUsage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::cerr << "\nUsage: "
                  << progName
                  << " [options]\n"
                  << errorMsg
                  << "\n\nThe list of optional options for this program is:\n"
                  << std::endl;
    }
}

int main(int argc, char** argv)
{
    using HaghighiMultiDomainProblemTT = TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT);
    return Dumux::start<HaghighiMultiDomainProblemTT>(argc, argv, printUsage);
}
