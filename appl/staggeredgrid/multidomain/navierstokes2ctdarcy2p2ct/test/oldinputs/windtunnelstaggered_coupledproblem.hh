// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_WINDTUNNEL_PROBLEM_HH
#define DUMUX_WINDTUNNEL_PROBLEM_HH

#if (COUPLING_KEPSILON || COUPLING_LOWREKEPSILON || COUPLING_ONEEQ || COUPLING_ZEROEQ)
#define TURBULENT 1
#endif

#include <dumux/material/fluidsystems/h2oair.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/problem.hh>

#include "windtunnelstaggered_2p2cnisubproblem.hh"
#include "windtunnelstaggered_rans2cnisubproblem.hh"

namespace Dumux
{

template <class TypeTag>
class WindtunnelProblem;

template <class TypeTag> class MultiDomainProblem;

namespace Properties
{
// problems
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, Problem,
              Dumux::WindtunnelProblem<TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>);
SET_TYPE_PROP(StokesSubProblem, Problem,
              Dumux::WindtunnelStokesSubProblem<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(DarcySubProblem, Problem,
              Dumux::WindtunnelDarcySubProblem<TTAG(DarcySubProblem)>);

// Set the fluid system to use complex relations (last argument)
SET_TYPE_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);
SET_TYPE_PROP(DarcySubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), FluidSystem));
SET_TYPE_PROP(StokesSubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), FluidSystem));

// Use a smaller FluidSystem table
NEW_PROP_TAG(ProblemUseSmallFluidSystemTable);
SET_BOOL_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, ProblemUseSmallFluidSystemTable, false);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, UseMoles, false);
SET_BOOL_PROP(DarcySubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), UseMoles));
SET_BOOL_PROP(StokesSubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT), UseMoles));

// Use the extended coupling model on default
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, CouplingMethod, 2);

// Set the output frequency
NEW_PROP_TAG(OutputFreqOutput);
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, OutputFreqOutput, 5);

// Set the default episode length
NEW_PROP_TAG(TimeManagerEpisodeLength);
SET_INT_PROP(MDSNavierStokesTwoCTDarcyTwoPTwoCT, TimeManagerEpisodeLength, 43200);
}

template <class TypeTag = TTAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT)>
class WindtunnelProblem
: public MultiDomainProblem<TypeTag>
{
    using ParentType = MultiDomainProblem<TypeTag>;

    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using SubDomainGridView = typename GET_PROP_TYPE(TypeTag, SubDomainGridView);
    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    enum { dim = MultiDomainGridView::dimension };
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;

    constexpr static unsigned int stokesSubDomainIdx = MultiDomainIndices::stokesSubDomainIdx;
    constexpr static unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief Base class for the multi domain problem
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The GridView
     */
    WindtunnelProblem(TimeManager &timeManager,
                  GridView gridView)
    : ParentType(timeManager, gridView)
    {
        // initialize the tables of the fluid system
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, UseSmallFluidSystemTable))
        {
            FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/323.15, /*numTemp=*/50,
                              /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
        }
        else
        {
            FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                              /*pMin=*/5e4, /*pMax=*/1e7, /*numP=*/995);
        }

        // spatial parameter stuff, if requested
        this->sdProblemDarcy().spatialParams().plotMaterialLaw();

        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        episodeLength_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
        this->timeManager().startNextEpisode(episodeLength_);
    }

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    { return GET_RUNTIME_PARAM(TypeTag, std::string, Output.Name) + "_staggered"; }

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    //! \copydoc Dumux::CoupledProblem::shouldWriteOutput()
    bool shouldWriteOutput() const
    {
        return this->timeManager().timeStepIndex() % freqOutput_ == 0
               || this->timeManager().episodeWillBeFinished()
               || this->timeManager().willBeFinished();
    }

    /*!
     * \brief Initialization multi-domain and the sub-domain grids
     */
    void initializeGrid()
    {
        this->mdGrid().startSubDomainMarking();

        Scalar obstacleLeftOffset = std::numeric_limits<Scalar>::lowest();
        Scalar obstacleLength = std::numeric_limits<Scalar>::lowest();
        Scalar numObstacles = std::numeric_limits<int>::lowest();
        Scalar obstacleHeight = interfaceVerticalPos();
        using ParameterTree = typename GET_PROP(TypeTag, ParameterTree);
        if ((ParameterTree::tree().hasKey("GridObstacleLeftOffset")
              || ParameterTree::tree().hasKey("Grid.ObstacleLeftOffset"))
            && (ParameterTree::tree().hasKey("GridObstacleLength")
                || ParameterTree::tree().hasKey("Grid.ObstacleLength"))
            && (ParameterTree::tree().hasKey("GridNumObstacles")
                || ParameterTree::tree().hasKey("Grid.NumObstacles"))
            && (ParameterTree::tree().hasKey("GridObstacleHeight")
                || ParameterTree::tree().hasKey("Grid.ObstacleHeight")))
        {
            obstacleLeftOffset = darcyXLeftFront()[0]
                                 + GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleLeftOffset);
            obstacleLength = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleLength);
            numObstacles = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumObstacles);
            obstacleHeight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleHeight);
        }

        for (auto eIt = this->mdGrid().template leafbegin<0>();
              eIt != this->mdGrid().template leafend<0>(); ++eIt)
        {
            auto globalPos = eIt->geometry().center();
            // only for interior entities, required for parallelization
            if (eIt->partitionType() == Dune::InteriorEntity)
            {
                if (globalPos[0] > darcyXLeftFront()[0]
                    && globalPos[0] < darcyXRightBack()[0]
                    && (globalPos[1] < interfaceVerticalPos()
                        || (floor((globalPos[0] - obstacleLeftOffset) / 2.0 / obstacleLength) < numObstacles
                            && (int)(((int)floor((globalPos[0] - obstacleLeftOffset) / obstacleLength)) % 2) == 0
                            && globalPos[1] < obstacleHeight)))
                {
                    this->mdGrid().addToSubDomain(darcySubDomainIdx, *eIt);
                }
                else if (globalPos[1] > interfaceVerticalPos())
                {
                    this->mdGrid().addToSubDomain(stokesSubDomainIdx, *eIt);
                }
            }
        }
        this->mdGrid().preUpdateSubDomains();
        this->mdGrid().updateSubDomains();
        this->mdGrid().postUpdateSubDomains();

        this->darcyElementIndices_.resize(this->sdGridViewDarcy().size(0));
        Dune::MultipleCodimMultipleGeomTypeMapper<MultiDomainGridView, Dune::MCMGElementLayout>
            multidomainDofMapper(this->mdGridView());
        Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
            subdomainDofMapper(this->sdGridViewDarcy());
        for (auto eIt = this->sdGridDarcy().template leafbegin<0>();
              eIt != this->sdGridDarcy().template leafend<0>(); ++eIt)
        {
            this->darcyElementIndices_[subdomainDofMapper.index(*eIt)] =
                multidomainDofMapper.index(this->mdGrid().multiDomainEntity(*eIt));
        }
    }

    //! \brief Returns the vertical position of the interface
    Scalar interfaceVerticalPos()
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
    }

    //! \brief Returns the left front position of the darcy domain
    GlobalPosition darcyXLeftFront()
    {
        GlobalPosition darcyXLeftFront(0.0);
        darcyXLeftFront[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
#if DUMUX_MULTIDOMAIN_DIM > 2
        darcyXLeftFront[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1);
#endif
        return darcyXLeftFront;
    }

    //! \brief Returns the right back position of the darcy domain
    GlobalPosition darcyXRightBack()
    {
        GlobalPosition darcyXRightBack(0.0);
        darcyXRightBack[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
#if DUMUX_MULTIDOMAIN_DIM > 2
        darcyXRightBack[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2);
#endif
        return darcyXRightBack;
    }

private:
    unsigned freqOutput_;
    Scalar episodeLength_;
};

} // end namespace Dumux

#endif // DUMUX_WINDTUNNEL_PROBLEM_HH
