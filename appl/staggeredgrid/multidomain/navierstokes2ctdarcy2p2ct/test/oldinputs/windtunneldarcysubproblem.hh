// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium.
 */
#ifndef DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH
#define DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstant.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

#include "windtunneldarcyspatialparams.hh"

#define ISOTHERMAL 0

namespace Dumux
{
template <class TypeTag>
class WindtunnelDarcySubProblem;

namespace Properties
{
// Enable gravity
SET_BOOL_PROP(DarcySubProblem, ProblemEnableGravity, true);

// choose pn and Sw as primary variables
SET_INT_PROP(DarcySubProblem, Formulation, TwoPTwoCFormulation::pnsw);

// the gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(DarcySubProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

#ifdef USE_SIMPLE_PHYSICS
SET_TYPE_PROP(DarcySubProblem, EffectiveDiffusivityModel, DiffusivityConstant<TypeTag>);
#endif

#ifdef USE_SIMPLE_PHYSICS
// Johansen model is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(DarcySubProblem, ThermalConductivityModel,
              ThermalConductivityJohansen<typename GET_PROP_TYPE(TypeTag, Scalar)>);
#else
// Somerton is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(DarcySubProblem, ThermalConductivityModel,
              ThermalConductivitySomerton<typename GET_PROP_TYPE(TypeTag, Scalar)>);
#endif

// Live plot of the evaporation rates
NEW_PROP_TAG(OutputPlotEvaporationRate);
NEW_PROP_TAG(OutputPlotBoxEvaporationRate);
SET_BOOL_PROP(DarcySubProblem, OutputPlotEvaporationRate, false);
SET_BOOL_PROP(DarcySubProblem, OutputPlotBoxEvaporationRate, false);

// Frequency of writing storage files
NEW_PROP_TAG(ProblemFreqMassOutput);
SET_INT_PROP(DarcySubProblem, ProblemFreqMassOutput, 1);
}


/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium. During
 *        buoyancy driven upward migration the gas passes a high
 *        temperature area.
 *
 */
template <class TypeTag >
class WindtunnelDarcySubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomainTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(MultiDomainTypeTag, SubDomainGridView) SubDomainGridView;

    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(MultiDomainTypeTag, Indices) MultiDomainIndices;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    // the equation indices
    enum { contiTotalMassIdx = Indices::contiNEqIdx,
           contiWEqIdx = Indices::contiWEqIdx,
           energyEqIdx = Indices::energyEqIdx };
    // the indices of the primary variables
    enum { pressureIdx = Indices::pressureIdx,
           switchIdx = Indices::switchIdx,
           temperatureIdx = Indices::temperatureIdx };
    // Grid and world dimension
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, LocalJacobian) LocalJacobian;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) LocalResidual;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    // property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    static const unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief The constructor.
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    WindtunnelDarcySubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , gridView_(gridView)
    {
        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = std::max(positions0.front(),darcyXLeft_);
        bBoxMax_[0] = std::min(positions0.back(),darcyXRight_);
        bBoxMin_[1] = positions1.front();
        bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);

        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Pressure);
        switch_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Switch);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Temperature);
        initialPhasePresence_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, InitialPhasePresence);
        name_ = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);

        freqMassOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, FreqMassOutput);

        storageLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        outfile.open("storage.out");
        outfile << "Time[s]" << ";"
                << "TotalMassChange[kg/(s*mDepth)]" << ";"
                << "WaterMassChange[kg/(s*mDepth))]" << ";"
                << "IntEnergyChange[J/(m^3*s*mDepth)]" << ";"
                << "WaterMass[kg/mDepth]" << ";"
                << "WaterMassLoss[kg/mDepth]" << ";"
                << "EvaporationRate[mm/s]"
                << std::endl;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \brief The destructor
    ~WindtunnelDarcySubProblem()
    {
        outfile.close();
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();
        globalStorage(storageLastTimestep_);
        initialWaterContent_ = storageLastTimestep_[contiWEqIdx];
    }

    // suppress output from DuMuX
    bool shouldWriteOutput() const
    {
        return false;
    }

    /*!
     * \brief Returns the source term at specific position in the domain.
     *
     * \param values The source values for the primary variables
     * \param globalPos The position
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    void sourceAtPos(PrimaryVariables &values,
                     const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initialAtPos(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = pressure_;
        values[switchIdx] = switch_;
        values[temperatureIdx] = temperature_;
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        return initialPhasePresence_;
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqMassOutput_ == 0
                || this->timeManager().episodeWillBeFinished())
            {
                PrimaryVariables storageChange(0.);
                storageChange = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                storageChange /= (time - lastMassOutputTime_);

                std::cout << "Time[s]: " << time
                          << " TotalMass[kg]: " << storage[contiTotalMassIdx]
                          << " WaterMass[kg]: " << storage[contiWEqIdx]
                          << " IntEnergy[J/m^3]: " << storage[energyEqIdx]
                          << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                          << std::endl;
                if (this->timeManager().time() != 0.)
                    outfile << time << ";"
                            << storageChange[contiTotalMassIdx] << ";"
                            << storageChange[contiWEqIdx] << ";"
                            << storageChange[energyEqIdx] << ";"
                            << storage[contiWEqIdx] << ";"
                            << initialWaterContent_ - storage[contiWEqIdx] << ";"
                            << storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0])
                            << std::endl;

                static double yMin = 0.0;
                static double yMax = 15.0;
                static std::vector<double> x;
                static std::vector<double> y;

                storageLastTimestep_ = storage;
                lastMassOutputTime_ = time;
                Scalar evaprate = storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0]) * 86400.0; // mm/d
                x.push_back(time / 86400.0); // d
                y.push_back(evaprate);
                yMin = std::min(yMin, evaprate);
                yMax = std::max(yMax, evaprate);

                gnuplot_.resetPlot();
                gnuplot_.setXRange(0, x[x.size()-1]);
                gnuplot_.setYRange(0, yMax*1.1);
                gnuplot_.setXlabel("time [d]");
                gnuplot_.setYlabel("evaporation rate [mm/d]");
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotBoxEvaporationRate))
                {
                    gnuplot_.addFileToPlot("box.dat", "box w l");
                }
                gnuplot_.addDataSetToPlot(x, y, "staggered");
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotEvaporationRate))
                {
                    gnuplot_.plot("0_evaprate");
                }
            }
        }
    }

    /*!
     * \brief Returns the gridview of Darcy subdomain
     */
    const SubDomainGridView sdGridViewDarcy() const
    {
        return this->gridView().grid().subDomain(darcySubDomainIdx).leafGridView();
    }

    /*!
     * \brief Compute the integral over the domain of the storage
     *        terms of all conservation quantities.
     *
     * \param storage Stores the result
     */
    void globalStorage(PrimaryVariables &storage)
    {
        localJacobian_.init(*this);
        storage = 0;

        for (const auto& element : elements(gridView_))
        {
            if(element.partitionType() == Dune::InteriorEntity
               && gridView_.indexSet().contains(darcySubDomainIdx, element))
            {
                localResidual().evalStorage(element);
                storage += localResidual().storageTerm()[0];
            }
        }

        if (gridView_.comm().size() > 1)
            storage = gridView_.comm().sum(storage);
    }

    /*!
     * \brief Reference to the local residual object
     */
    LocalResidual &localResidual()
    { return localJacobian_.localResidual(); }


private:
    static constexpr Scalar eps_ = 1e-8;

    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar darcyXLeft_;
    Scalar darcyXRight_;

    Scalar pressure_;
    Scalar switch_;
    Scalar temperature_;
    int initialPhasePresence_;
    std::string name_;

    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar initialWaterContent_;
    Scalar lastMassOutputTime_;
    std::ofstream outfile;
    Dumux::GnuplotInterface<double> gnuplot_;

    LocalJacobian localJacobian_;
    GridView gridView_;
};

} //end namespace

#endif // DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH
