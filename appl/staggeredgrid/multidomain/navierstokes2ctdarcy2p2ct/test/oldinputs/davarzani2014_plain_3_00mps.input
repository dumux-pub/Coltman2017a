###############################################################
# Configuration file for experiments published in Davarzani2014
###############################################################
[TimeManager]
DtInitial = 5e-1 # [s]
MaxTimeStepSize = 720 # [s]
TEnd = 691200 # [s]

InitTime = 0 # [s] Initialization time without coupling
EpisodeLength = 43200 # [s] # 14400s = 0.5d

[Grid]
LowerLeftX = 0.0 # [m]
UpperRightX = 1.25 # [m]
LowerLeftY = 0.0 # [m]
UpperRightY = 0.38 # [m]
# Number of elements in x-, y-direction
NumberOfCellsX = 100
NumberOfCellsY = 50
# Grading and refinement of the mesh
GradingFactorY = 1.20
RefineTopY = false

NoDarcyX1 = 0.80 # [m] # Beginning of PM below
NoDarcyX2 = 1.05 # [m] # End of PM below
RunUpDistanceX1 = 0.801 # [m] # Beginning of coupling to PM (x-coordinate)
RunUpDistanceX2 = 1.049 # [m] # End of coupling to PM (x-coordinate)
InterfacePosY = 0.25 # [m] # Vertical position of coupling interface

[Output]
NameFF = zeroeq2cni
NamePM = 2p2cni
# Frequency of restart file, flux and VTK output
FreqRestart = 50             # how often restart files are written out
FreqOutput = 5               #  10  # frequency of VTK output
FreqMassOutput = 5           #  20  # frequency of mass and evaporation rate output (Darcy)
FreqFluxOutput = 100         # 100  # frequency of detailed flux output
FreqVaporFluxOutput = 3      #   5  # frequency of summarized flux output


###############################################################
# Define conditions in the two subdomains
###############################################################
[FreeFlow]
EnableNeumannInflow = false
# the reference are 8-day means
RefVelocity = 3.013 # [m/s]
RefPressure = 1e5 # [Pa]
RefMassfrac = 0.0053 # [-]
RefTemperature = 315.6 # [K]

[BoundaryLayer]
Model = 0

[PorousMedium]
IsolatedPorousMediumBox = true
RefPressurePM = 1e5 # [Pa]
RefTemperaturePM = 300.95 # [K] # mean from first 5 data rows
RefSw = 0.98 # [-]

[SpatialParams]
# Mines
AlphaBJ = 1.0 # [-]
Permeability = 1.08e-10 # [m^2]
Porosity = 0.334 # [-]
Swr = 0.08383 # [-] -> calculated from water concent which is 0.028
Snr = 0.01 # [-]
VgAlpha = 5.8e-4 # [1/Pa]
VgN = 17.8 # [-]
ThermalConductivitySolid = 5.9 # [W/(m*K)]
PlotMaterialLaw = false


###############################################################
# Newton and Linear solver parameters
###############################################################
[Newton]
MaxRelativeShift = 1e-5
TargetSteps = 6
MaxSteps = 8
WriteConvergence = false

[LinearSolver]
Verbosity = 0


###############################################################
# Eddy Viscosity Models
# 0 = none
# 1 = Prandtl
# 2 = modified Van Driest
# 3 = Baldwin Lomax
###############################################################
# Eddy Diffusivity and Eddy Conductivity Models
# 0 = none
# 1 = Reynolds analogy
# 2 = modified Van Driest
# 3 = Deissler
# 4 = Meier and Rotta
###############################################################
[ZeroEq]
EddyViscosityModel = 3
EddyDiffusivityModel = 1
EddyConductivityModel = 1
BBoxMinSandGrainRoughness = 0.0 # [m]
BBoxMaxSandGrainRoughness = 0.0 # [m]
