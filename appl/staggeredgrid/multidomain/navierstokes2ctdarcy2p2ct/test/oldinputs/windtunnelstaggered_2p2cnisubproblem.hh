// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium.
 */
#ifndef DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH
#define DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

#include <dumux/io/readwritedatafile.hh>

#include "windtunnelstaggered_spatialparameters.hh"

#define ISOTHERMAL 0

namespace Dumux
{
template <class TypeTag>
class WindtunnelDarcySubProblem;

namespace Properties
{
// Enable gravity
SET_BOOL_PROP(DarcySubProblem, ProblemEnableGravity, true);

// choose pn and Sw as primary variables
SET_INT_PROP(DarcySubProblem, Formulation, TwoPTwoCFormulation::pnsw);

// the gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(DarcySubProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

// Use Kelvin equation to adapt the saturation vapor pressure
SET_BOOL_PROP(DarcySubProblem, UseKelvinEquation, true);

// Somerton is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(DarcySubProblem, ThermalConductivityModel,
              ThermalConductivitySomerton<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Depth in third dimension
NEW_PROP_TAG(GridExtrusionFactor);
SET_SCALAR_PROP(DarcySubProblem, GridExtrusionFactor, 1.0);

// Live plot of the evaporation rates
NEW_PROP_TAG(OutputPlotEvaporationRate);
SET_BOOL_PROP(DarcySubProblem, OutputPlotEvaporationRate, false);

// Set the output frequency
NEW_PROP_TAG(OutputFreqMassOutput);
SET_INT_PROP(DarcySubProblem, OutputFreqMassOutput, 5);
}


/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium. During
 *        buoyancy driven upward migration the gas passes a high
 *        temperature area.
 *
 */
template <class TypeTag >
class WindtunnelDarcySubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomainTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(MultiDomainTypeTag, SubDomainGridView) SubDomainGridView;

    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(MultiDomainTypeTag, Indices) MultiDomainIndices;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    // the equation indices
    enum { contiTotalMassIdx = Indices::contiNEqIdx,
           contiWEqIdx = Indices::contiWEqIdx,
           energyEqIdx = Indices::energyEqIdx };
    // the indices of the primary variables
    enum { pressureIdx = Indices::pressureIdx,
           switchIdx = Indices::switchIdx,
           temperatureIdx = Indices::temperatureIdx };
    // the phase and component indices
    enum { wPhaseIdx = Indices::wPhaseIdx,
           nPhaseIdx = Indices::nPhaseIdx };
    // Grid and world dimension
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, LocalJacobian) LocalJacobian;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) LocalResidual;
    using ThermalConductivityModel = typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel);

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    // property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    static const unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief The constructor.
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    WindtunnelDarcySubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , gridView_(gridView)
    {
        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = std::max(positions0.front(),darcyXLeft_);
        bBoxMax_[0] = std::min(positions0.back(),darcyXRight_);
        interfacePosY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        extrusionFactor_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ExtrusionFactor);

        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefPressure);
        switch_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefWaterSaturation);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperature);
        solDependentEnergySource_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, SolDependentEnergySource);
        solDependentEnergyWalls_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, SolDependentEnergyWalls);

        try {
            temperatureDataFilePM_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, PorousMedium, TemperatureDataFile);
            useTemperatureDataFilePM_ = true;
            readData(temperatureDataFilePM_, temperatureDataPM_);
        }
        catch (...) {
            useTemperatureDataFilePM_ = false;
        }

        freqMassOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);

        storageLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        std::string storageFile = name() + "-storage.out";
        outfile.open(storageFile);
        outfile << "Time[s]" << ";"
                << "TotalMassChange[kg/(s*mDepth)]" << ";"
                << "WaterMassChange[kg/(s*mDepth))]" << ";"
                << "IntEnergyChange[J/(m^3*s*mDepth)]" << ";"
                << "WaterMass[kg/mDepth]" << ";"
                << "WaterMassLoss[kg/mDepth]" << ";"
                << "EvaporationRate[mm/s]"
                << std::endl;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \brief The destructor
    ~WindtunnelDarcySubProblem()
    {
        outfile.close();
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return GET_RUNTIME_PARAM(TypeTag, std::string, Output.Name) + "_staggered"; }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();
        globalStorage(storageLastTimestep_);
        initialWaterContent_ = storageLastTimestep_[contiWEqIdx];
    }

    // suppress output from DuMuX
    bool shouldWriteOutput() const
    {
        return false;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initialAtPos(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{


    //! \copydoc Dumux::ImplicitProblem::solDependentSource()
    void solDependentSource(PrimaryVariables &values,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.;

        if (!solDependentEnergyWalls_ && !solDependentEnergySource_)
            return;

        // cell center global
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(element.geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal = element.geometry().global(insideCellCenterLocal);

        // assume thermal conduction through the plexiglass box
        Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
        Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
        Scalar thermalConductivityInside = ThermalConductivityModel::effectiveThermalConductivity(
                                              elemVolVars[scvIdx].saturation(wPhaseIdx),
                                              elemVolVars[scvIdx].fluidThermalConductivity(wPhaseIdx),
                                              elemVolVars[scvIdx].fluidThermalConductivity(nPhaseIdx),
                                              this->spatialParams().solidThermalConductivityAtPos(insideCellCenterGlobal),
                                              this->spatialParams().porosityAtPos(insideCellCenterGlobal),
                                              this->spatialParams().solidDensityAtPos(insideCellCenterGlobal));
        Scalar harmonicThermalConductivity = 2.0 * plexiglassThermalConductivity * thermalConductivityInside
                                             / (plexiglassThermalConductivity + thermalConductivityInside);
        Scalar temperatureInside = elemVolVars[scvIdx].temperature();
        Scalar temperatureRef = temperature_;
        if (useTemperatureDataFilePM_)
            temperatureRef = evaluateData(temperatureDataPM_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

        if (solDependentEnergyWalls_)
        {
            typedef typename GridView::IntersectionIterator IntersectionIterator;
            for (IntersectionIterator is = gridView_.ibegin(element);
                is != gridView_.iend(element); ++is)
            {
                // face center global
                const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
                    Dune::ReferenceElements<Scalar, dim-1>::general(is->geometry().type()).position(0, 0);
                Dune::FieldVector<Scalar, dim> faceCenterGlobal = is->geometry().global(faceCenterLocal);

                Dune::FieldVector<Scalar, dim> distance(faceCenterGlobal);
                distance -= insideCellCenterGlobal;

                if (is->boundary() && faceCenterGlobal[1] < interfacePosY_ - eps_)
                {
                    Scalar area = is->geometry().integrationElement(faceCenterLocal);
                    Scalar volume = is->inside().geometry().volume();

                    Scalar extrusionFactor = 1.0;
                    if (dim < 3)
                        extrusionFactor = extrusionFactor_; // we do not use the extrusion factor properly everywhere for our equations

                    // NOTE: - sign switches compared to DUMUX Neumann boundary conditions
                    //       - account for additional thickness dofs are not located at the boundary
                    values[energyEqIdx] -= harmonicThermalConductivity
                                           * (temperatureInside - temperatureRef)
                                           / (plexiglassThickness + distance.two_norm())
                                           * area
                                           / volume / extrusionFactor; // account for the fact that it is treated in the storage term
                }
            }
        }

        if(solDependentEnergySource_ && dim < 3)
        {
            // NOTE: - account for additional thickness dofs are not located at the boundary
            Scalar extrusionFactorForSourceTerm_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ExtrusionFactorForSourceTerm);
            values[energyEqIdx] -= 2.0 * harmonicThermalConductivity
                                   * (temperatureInside - temperatureRef)
                                   / (plexiglassThickness + extrusionFactorForSourceTerm_ / 2.0)
                                   / extrusionFactorForSourceTerm_; // we do not use the extrusion factor properly everywhere for our equations
        }
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = pressure_;
        values[switchIdx] = switch_;
        if (GET_PROP_VALUE(TypeTag, Formulation) == TwoPTwoCFormulation::pwsn
            && initialPhasePresenceAtPos(globalPos) == Indices::bothPhases)
        {
            values[switchIdx] = 1.0 - switch_;
        }
        values[temperatureIdx] = temperature_;
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        return switch_ < eps_ ? Indices::nPhaseOnly : Indices::bothPhases;
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqMassOutput_ == 0
                || this->timeManager().episodeWillBeFinished())
            {
                PrimaryVariables storageChange(0.);
                storageChange = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                storageChange /= (time - lastMassOutputTime_);

                std::cout << "Time[s]: " << time
                          << " TotalMass[kg]: " << storage[contiTotalMassIdx]
                          << " WaterMass[kg]: " << storage[contiWEqIdx]
                          << " IntEnergy[J/m^3]: " << storage[energyEqIdx]
                          << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                          << std::endl;
                GlobalPosition globalPos(0.0);
                if (this->timeManager().time() != 0.)
                    outfile << time << ";"
                            << storageChange[contiTotalMassIdx] << ";"
                            << storageChange[contiWEqIdx] << ";"
                            << storageChange[energyEqIdx] << ";"
                            << storage[contiWEqIdx] << ";"
                            << initialWaterContent_ - storage[contiWEqIdx] << ";"
                            << storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0])
                                                          / this->extrusionFactorAtPos(globalPos)
                            << std::endl;


                storageLastTimestep_ = storage;
                lastMassOutputTime_ = time;

                Scalar evaprate = storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0]) * 86400.0; // mm/d
                static double yMax = -1e100;
                static std::vector<double> x;
                static std::vector<double> y;

                x.push_back(time / 86400.0); // d
                y.push_back(evaprate);
                yMax = std::min(15.0,std::max(yMax, evaprate));

                gnuplot_.resetPlot();
                gnuplot_.setXRange(0, x[x.size()-1]);
                gnuplot_.setYRange(0, yMax);
                gnuplot_.setXlabel("time [d]");
                gnuplot_.setYlabel("evaporation rate [mm/d]");
                gnuplot_.addDataSetToPlot(x, y, "staggered");
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotEvaporationRate))
                {
                    gnuplot_.plot("0_evaprate");
                }
            }
        }
    }

    /*!
     * \brief Returns the gridview of Darcy subdomain
     */
    const SubDomainGridView sdGridViewDarcy() const
    {
        return this->gridView().grid().subDomain(darcySubDomainIdx).leafGridView();
    }

    /*!
     * \brief Compute the integral over the domain of the storage
     *        terms of all conservation quantities.
     *
     * \param storage Stores the result
     */
    void globalStorage(PrimaryVariables &storage)
    {
        localJacobian_.init(*this);
        storage = 0;

        for (const auto& element : elements(gridView_))
        {
            if(element.partitionType() == Dune::InteriorEntity
               && gridView_.indexSet().contains(darcySubDomainIdx, element))
            {
                localResidual().evalStorage(element);
                storage += localResidual().storageTerm()[0];
            }
        }

        if (gridView_.comm().size() > 1)
            storage = gridView_.comm().sum(storage);
    }

    /*!
     * \brief Reference to the local residual object
     */
    LocalResidual &localResidual()
    { return localJacobian_.localResidual(); }


private:
    static constexpr Scalar eps_ = 1e-8;

    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar interfacePosY_;
    Scalar extrusionFactor_;
    Scalar darcyXLeft_;
    Scalar darcyXRight_;

    Scalar pressure_;
    Scalar switch_;
    Scalar temperature_;
    bool solDependentEnergySource_;
    bool solDependentEnergyWalls_;
    bool useTemperatureDataFilePM_;
    std::string temperatureDataFilePM_;
    std::vector<double> temperatureDataPM_[2];

    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar initialWaterContent_;
    Scalar lastMassOutputTime_;
    std::ofstream outfile;
    Dumux::GnuplotInterface<double> gnuplot_;

    LocalJacobian localJacobian_;
    GridView gridView_;
};

} //end namespace

#endif // DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH
