// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_2P2CNI_DARCY_2CNI_HH
#define DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_2P2CNI_DARCY_2CNI_HH

#include <dune/istl/io.hh>

#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>

#include <appl/staggeredgrid/common/twocomponentfluid.hh>

#include "properties.hh"

namespace Dumux
{
namespace Properties
{
NEW_PROP_TAG(StokesSubProblemTypeTag);
NEW_PROP_TAG(DarcySubProblemTypeTag);
}

/**
 * \brief Coupling for 2p2cni Darcy and 1p2cni Stokes
 *
 * \tparam TypeTag All types wrapped in the TypeTag
 */
template<typename TypeTag>
class CouplingStokes2p2cniDarcy2cni
  : public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags
  , public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<CouplingStokes2p2cniDarcy2cni<TypeTag> >
  , public Dune::PDELab::MultiDomain::NumericalJacobianApplyCoupling<CouplingStokes2p2cniDarcy2cni<TypeTag> >
  , public Dune::PDELab::MultiDomain::FullCouplingPattern
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename GET_PROP_TYPE(TypeTag, Scalar)>
{
private:
    using StokesSubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag);
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);

    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    using StokesIndices = typename GET_PROP_TYPE(StokesSubProblemTypeTag, Indices);
    using DarcyIndices = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Indices);
    using DarcySubDomainProblem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem);
    using FVElementGeometry = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FVElementGeometry);
    using PrimaryVariables = typename GET_PROP_TYPE(DarcySubProblemTypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Scalar);
    using SpatialParams = typename GET_PROP_TYPE(DarcySubProblemTypeTag, SpatialParams);
    using VolumeVariables = typename GET_PROP_TYPE(DarcySubProblemTypeTag, VolumeVariables);

    using BaseFluid = Dumux::TwoComponentFluid<StokesSubProblemTypeTag>;
    using FluidState = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FluidState);
    using FluidSystem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FluidSystem);
    typedef Dumux::MiscibleMultiPhaseComposition<Scalar, FluidSystem> MiscibleMultiPhaseComposition;
    typedef Dumux::ComputeFromReferencePhase<Scalar, FluidSystem> ComputeFromReferencePhase;
    static const bool useConstraintSolver = GET_PROP_VALUE(DarcySubProblemTypeTag, UseConstraintSolver);

    using MapperElement = typename GET_PROP_TYPE(StokesSubProblemTypeTag, MapperElement);
    using MaterialLaw = typename GET_PROP_TYPE(DarcySubProblemTypeTag, MaterialLaw);
    using MaterialLawParams = typename GET_PROP_TYPE(DarcySubProblemTypeTag, MaterialLawParams);
    using MDGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using ThermalConductivityModel = typename GET_PROP_TYPE(DarcySubProblemTypeTag, ThermalConductivityModel);

    enum { dim = MDGridView::dimension };

    enum {
        // Darcy stuff for phases and components
        numPhases_n = GET_PROP_VALUE(DarcySubProblemTypeTag, NumPhases),
        numComponents_n = GET_PROP_VALUE(DarcySubProblemTypeTag, NumComponents),
        // indices
        wPhaseIdx_n = DarcyIndices::wPhaseIdx,
        nPhaseIdx_n = DarcyIndices::nPhaseIdx,
        wCompIdx_n = DarcyIndices::wCompIdx,
        nCompIdx_n = DarcyIndices::nCompIdx,
        // present phases
        wPhaseOnly = DarcyIndices::wPhaseOnly,
        nPhaseOnly = DarcyIndices::nPhaseOnly,
        bothPhases = DarcyIndices::bothPhases
    };

    // DoF indices
    static const unsigned int stokesVelocityIdx = StokesIndices::velocityIdx;
    static const unsigned int stokesPressureIdx = StokesIndices::pressureIdx;
    static const unsigned int stokesMassFracIdx = StokesIndices::massMoleFracIdx;
    static const unsigned int stokesTemperatureIdx = StokesIndices::temperatureIdx;
    static const unsigned int darcyPressureIdxDumux = DarcyIndices::pressureIdx;
    static const unsigned int darcySwitchIdxDumux = DarcyIndices::switchIdx;
    static const unsigned int darcyTemperatureIdxDumux = DarcyIndices::temperatureIdx;
    static const unsigned int darcyPressureIdxPDELab = MultiDomainIndices::darcyPressureIdxPDELab;
    static const unsigned int darcySwitchIdxPDELab = MultiDomainIndices::darcySwitchIdxPDELab;
    static const unsigned int darcyTemperatureIdxPDELab = MultiDomainIndices::darcyTemperatureIdxPDELab;

    // Equation indices
    static const unsigned int darcyContiWaterEqIdx = DarcyIndices::contiWEqIdx;
    static const unsigned int darcyContiTotalEqIdx = DarcyIndices::contiNEqIdx; // this is the total mass balance
    static const unsigned int darcyEnergyEqIdx = DarcyIndices::energyEqIdx;

    static const unsigned int numEqDarcy = GET_PROP_VALUE(DarcySubProblemTypeTag, NumEq);

public:
    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

  /*!
   * \brief Constructor
   */
  CouplingStokes2p2cniDarcy2cni(DarcySubDomainProblem& darcySubDomainProblem)
  : darcySubDomainProblem_(darcySubDomainProblem),
    spatialParams_(darcySubDomainProblem_.spatialParams()),
    // TODO the mapper needs the StokesGridView (in this case this is the mdGridView), so we can do it this way
    mapperElementMultiDomain_(darcySubDomainProblem_.gridView())
  {
      enableNavierStokes_ = GET_PARAM_FROM_GROUP(StokesSubProblemTypeTag, bool, Problem, EnableNavierStokes);
      enableUnsymmetrizedVelocityGradient_ = GET_PARAM_FROM_GROUP(StokesSubProblemTypeTag, bool, Problem, EnableUnsymmetrizedVelocityGradient);
      beaversJosephAsSolDependentDirichlet_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Coupling, BeaversJosephAsSolDependentDirichlet);
      newtonMaxSteps_ = GET_PARAM_FROM_GROUP(TypeTag, int, Coupling, SolverMaxSteps);
      resReduction_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Coupling, SolverResidualReduction);
      maxRelShift_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Coupling, SolverMaxRelativeShift);
      slopeLimitingFactor_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Coupling, SlopeLimitingFactor);
      if (slopeLimitingFactor_ > 1.0)
      {
          DUNE_THROW(Dune::NotImplemented, "Please choose CouplingSlopeLimitingFactor <= 1.");
      }

      enableGravityDarcy_ = GET_PARAM_FROM_GROUP(DarcySubProblemTypeTag, bool, Problem, EnableGravity);
      for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
          gravity_[dimIdx] = 0.0;
      if (enableGravityDarcy_)
      {
          gravity_[dim-1] = -9.81;
      }
      couplingMethod_ = GET_PARAM_FROM_GROUP(TypeTag, int, Coupling, Method);
      if (couplingMethod_ != 1)
      {
          std::cout << "couplingMethod_ " << couplingMethod_ << std::endl;
      }

      static_assert(!GET_PROP_VALUE(TypeTag, UseMoles),
                    "The CouplingStokes2p2cniDarcy2cni localoperator is not implemented for mole fraction formulation.");
      static_assert(GET_PROP_VALUE(StokesSubProblemTypeTag, PhaseIdx) == DarcyIndices::nPhaseIdx,
                    "The CouplingStokes2p2cniDarcy2cni localoperator is only implemented for a non-wetting free flow phase.");
      static_assert(GET_PROP_VALUE(DarcySubProblemTypeTag, ReplaceCompEqIdx) == DarcyIndices::contiNEqIdx,
                    "The CouplingStokes2p2cniDarcy2cni localoperator is only implemented when using total mass instead of gas component mass balance equation.");
  }

  /*!
   * \brief Pass turbulent variables from the StokesLocalOperator
   */
  void passAdditionalVariables(std::function<Scalar (unsigned int)> eddyKinematicViscosity,
                               std::function<Scalar (unsigned int)> eddyDiffusivity,
                               std::function<Scalar (unsigned int)> eddyThermalConductivity,
                               std::function<Dune::FieldMatrix<Scalar, dim, dim> (unsigned int)> velocityGradientTensor,
                               std::function<bool (unsigned int)> useWallFunctionMomentum)
  {
      eddyKinematicViscosity_ = eddyKinematicViscosity;
      eddyDiffusivity_ = eddyDiffusivity;
      eddyThermalConductivity_ = eddyThermalConductivity;
      velocityGradientTensor_ = velocityGradientTensor;
      useWallFunctionMomentum_ = useWallFunctionMomentum;
  }

  template<typename IG, typename LFSUStokes, typename LFSUDarcy,
           typename X, typename LFSVStokes, typename LFSVDarcy,
           typename R>
  void alpha_coupling(const IG& ig,
                      const LFSUStokes& lfsu_s, const X& x_s, const LFSVStokes& lfsv_s,
                      const LFSUDarcy& lfsu_n, const X& x_n, const LFSVDarcy& lfsv_n,
                      R& r_s, R& r_n) const
  {
    // select the velocity component from the subspaces for Navier-Stokes
    // other components are not needed
    const auto& lfsu_v_s = lfsu_s.template child<stokesVelocityIdx>();
    const auto& lfsu_p_s = lfsu_s.template child<stokesPressureIdx>();
    const auto& lfsu_x_s = lfsu_s.template child<stokesMassFracIdx>();
    const auto& lfsu_t_s = lfsu_s.template child<stokesTemperatureIdx>();
    const auto& lfsu_p_n = lfsu_n.template child<darcyPressureIdxPDELab>();
    const auto& lfsu_x_n = lfsu_n.template child<darcySwitchIdxPDELab>();
    const auto& lfsu_t_n = lfsu_n.template child<darcyTemperatureIdxPDELab>();

    // range field type for velocity
    using RangeVelocity = typename LFSUStokes::template Child<stokesVelocityIdx>::Type::
        Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType;

    // /////////////////////
    // geometry information

    static const unsigned int dim = IG::Geometry::dimension;
    auto elementInsideIdx = mapperElementMultiDomain_.index(ig.inside());

    // local position of face center and face normal
    const auto& faceCenterLocal =
        Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
    const auto& faceUnitOuterNormal = ig.centerUnitOuterNormal();

    // evaluate orientation of intersection
    unsigned int normDim = 0;
    for (unsigned int curDim = 0; curDim < dim; ++curDim)
    {
        if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
        {
            normDim = curDim;
        }
    }
    // store all tangential dimensions in array by filtering out normDim
    std::array<unsigned int, dim-1> tangDims;
    for (unsigned int curTanDim = 0; curTanDim < dim; ++curTanDim)
    {
        if (curTanDim < normDim)
        {
          tangDims[curTanDim] = curTanDim;
        }
        else if (curTanDim > normDim)
        {
          tangDims[curTanDim - 1] = curTanDim;
        }
    }

    // face midpoints of all faces
    const unsigned int numFaces =
        Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_n(numFaces);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_n(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
        faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
        faceCentersLocal_n[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.outside().geometry().type()).position(curFace, 1);
        faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        faceCentersGlobal_n[curFace] = ig.outside().geometry().global(faceCentersLocal_n[curFace]);
    }
    Dune::FieldVector<Scalar, dim> faceCenterGlobal = faceCentersGlobal_n[ig.indexInOutside()];

    // face volume for integration
    Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

    // distance between element centers
    auto elementCentersLocal =
        Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
    auto elementCentersGlobal_s = ig.inside().geometry().global(elementCentersLocal);
    auto elementCentersGlobal_n = ig.outside().geometry().global(elementCentersLocal);

    // /////////////////////
    // velocities

    // evaluate shape functions and velocities at all face midpoints
    std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
    std::vector<RangeVelocity> velocities_s(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
        // shape functions
        velocityBasis_s[curFace].resize(lfsu_v_s.size());
        lfsu_v_s.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
        // velocities
        velocities_s[curFace] = RangeVelocity(0.0);
        for (unsigned int basisIdx = 0; basisIdx < lfsu_v_s.size(); ++basisIdx)
        {
            velocities_s[curFace].axpy(x_s(lfsu_v_s, basisIdx), velocityBasis_s[curFace][basisIdx]);
        }
    }

    // /////////////////////
    // evaluation of primary variables

    // evaluate cell values for Stokes
    Scalar pressureGas_s = x_s(lfsu_p_s, 0);
    Scalar massFrac_s = x_s(lfsu_x_s, 0);
    Scalar temperature_s = x_s(lfsu_t_s, 0);

    // evaluate cell values for Darcy
    Scalar pressure_n = x_n(lfsu_p_n, 0);
    Scalar switch_n = x_n(lfsu_x_n, 0);
    Scalar temperature_n = x_n(lfsu_t_n, 0);

    // /////////////////////
    // evaluation of secondary variables, upwinding and averaging

    // Stokes
    const Scalar density_s = BaseFluid::density(pressureGas_s, temperature_s, massFrac_s);
    const Scalar effDynViscosityGas_s = BaseFluid::dynamicViscosity(pressureGas_s, temperature_s, massFrac_s)
                                        + (eddyKinematicViscosity_(elementInsideIdx) * density_s);
    const Scalar effCompDiffCoeff_s = BaseFluid::diffusionCoefficient(pressureGas_s, temperature_s, massFrac_s)
                                      + eddyDiffusivity_(elementInsideIdx);
    const Scalar molarDensityGas_s = BaseFluid::molarDensity(pressureGas_s, temperature_s, massFrac_s);
    const Scalar moleFrac_s = BaseFluid::convertToMoleFrac(massFrac_s);

    const Scalar enthalpyPhaseGas_s = BaseFluid::enthalpyPhase(pressureGas_s, temperature_s, massFrac_s);
    const Scalar enthalpyComponentAir_s =
        BaseFluid::enthalpyComponent(pressureGas_s, temperature_s, StokesIndices::phaseCompIdx);
    const Scalar enthalpyComponentWater_s =
        BaseFluid::enthalpyComponent(pressureGas_s, temperature_s, StokesIndices::transportCompIdx);
    const Scalar effThermalConductivity_s = BaseFluid::thermalConductivity(pressureGas_s, temperature_s, massFrac_s)
                                            + eddyThermalConductivity_(elementInsideIdx);

    // Darcy
    PrimaryVariables priVars;
    priVars[darcySwitchIdxDumux] = switch_n;
    priVars[darcyPressureIdxDumux] = pressure_n;
    Scalar pressureGas_n = pressure_n; // TwoPTwoCFormulation::pnsw
    if (GET_PROP_VALUE(DarcySubProblemTypeTag, Formulation) == TwoPTwoCFormulation::pwsn)
    {
        pressureGas_n += MaterialLaw::pc(spatialParams_.materialLawParamsAtPos(faceCenterGlobal), 1.0-switch_n);
    }
    priVars[darcyTemperatureIdxDumux] = temperature_n;
    FVElementGeometry fvGeometry;
    fvGeometry.update(darcySubDomainProblem_.gridView(), ig.outside());
    VolumeVariables volVars;
    volVars.update(priVars, darcySubDomainProblem_, ig.outside(), fvGeometry, 0, false);

    Scalar saturationWater = volVars.saturation(wPhaseIdx_n);
    Scalar densityGas_n = volVars.density(nPhaseIdx_n);
    Scalar densityWater_n = volVars.density(wPhaseIdx_n);
    Scalar dynamicViscosityGas_n = volVars.viscosity(nPhaseIdx_n);
    Scalar dynamicViscosityWater_n = volVars.viscosity(wPhaseIdx_n);
    Scalar diffusionCoefficientInGas_n = volVars.diffCoeff(nPhaseIdx_n);
    Scalar diffusionCoefficientInWater_n = volVars.diffCoeff(wPhaseIdx_n);
    Scalar massFracWaterInGas_n = volVars.massFraction(nPhaseIdx_n, wCompIdx_n);
    Scalar moleFracWaterInGas_n = volVars.moleFraction(nPhaseIdx_n, wCompIdx_n);
    Scalar enthalpyPhaseGas_n = volVars.enthalpy(nPhaseIdx_n);
    Scalar enthalpyPhaseWater_n = volVars.enthalpy(wPhaseIdx_n);
    Scalar enthalpyComponentAir_n = FluidSystem::componentEnthalpy(volVars.fluidState(), nPhaseIdx_n, nCompIdx_n);
    Scalar enthalpyComponentWater_n = FluidSystem::componentEnthalpy(volVars.fluidState(), nPhaseIdx_n, wCompIdx_n);

    // normal velocity and upwinding
    Scalar normalVelocityAtInterface_s = velocities_s[ig.indexInInside()][normDim]
                                         * faceUnitOuterNormal[normDim];
    Scalar densityGas_up = density_s;
    Scalar dynamicViscosityGas_up = effDynViscosityGas_s;
    Scalar massFraction_up = massFrac_s;
    Scalar enthalpyPhaseGas_up = enthalpyPhaseGas_s;
    if (normalVelocityAtInterface_s < 0)
    {
        // use values from Darcy
        densityGas_up = densityGas_n;
        dynamicViscosityGas_up = dynamicViscosityGas_n;
        massFraction_up = massFracWaterInGas_n;
        enthalpyPhaseGas_up = enthalpyPhaseGas_n;
    }

    // /////////////////////
    // calculate the momentum interface fluxes

    Scalar totalMassFlux = densityGas_up * normalVelocityAtInterface_s;
    // pressure for Darcy (continuity of normal mass fluxes)
    r_s.accumulate(lfsu_p_s, 0,
                  1.0 * totalMassFlux
                  * faceVolume);
    r_n.accumulate(lfsu_n, darcyContiTotalEqIdx,
                  -1.0 * totalMassFlux
                  * faceVolume);

    // Robin boundary condition for tangential momentum equation of Stokes
    // (Beavers-Joseph-Saffman condition)
    Scalar permeability = spatialParams_.intrinsicPermeabilityAtPos(faceCenterGlobal);
    Scalar permeabilitySquareRoot = std::sqrt(permeability);
    Scalar alphaBeaversJoseph = spatialParams_.beaversJosephCoeffAtPos(faceCenterGlobal);
    // helper variable to keep term occuring twice in formula
    Scalar beta = -1.0 * permeabilitySquareRoot / alphaBeaversJoseph
                  / (elementCentersGlobal_s[normDim] - faceCenterGlobal[normDim])
                  * faceUnitOuterNormal[normDim];

    for (auto curTangDim : tangDims)
    {
        unsigned int indexTangentialVelocity0 = curTangDim * 2;
        unsigned int indexTangentialVelocity1 = indexTangentialVelocity0 + 1;

        Scalar beaversJosephVelocity0 =
            beta * velocities_s[indexTangentialVelocity0][curTangDim] / (1.0 + beta);
        Scalar beaversJosephVelocity1 =
            beta * velocities_s[indexTangentialVelocity1][curTangDim] / (1.0 + beta);

        // (1) \b Inertia term of \b momentum balance equation
        if (enableNavierStokes_ && beaversJosephAsSolDependentDirichlet_)
        {
            r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                           0.5 * density_s
                           * beaversJosephVelocity0
                           * beaversJosephVelocity0
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
            r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                           0.5 * density_s
                           * beaversJosephVelocity1
                           * beaversJosephVelocity1
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
        }

        // NOTE: This term is the classical Beavers-Joseph conditions, thus this is
        // the only statement which will be evaluated when beaversJosephAsSolDependentDirichlet_ is false
        //(2) \b Viscous term of \b momentum balance equation
        r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                       -0.5 * effDynViscosityGas_s
                       * (velocities_s[indexTangentialVelocity0][curTangDim] - beaversJosephVelocity0)
                       / (elementCentersGlobal_s[normDim] - faceCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                       -0.5 * effDynViscosityGas_s
                       * (velocities_s[indexTangentialVelocity1][curTangDim] - beaversJosephVelocity1)
                       / (elementCentersGlobal_s[normDim] - faceCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        //(3) symmetrize velocity gradient
        if (!enableUnsymmetrizedVelocityGradient_ && beaversJosephAsSolDependentDirichlet_)
        {
          Dune::FieldMatrix<Scalar, dim, dim> velocityGradientTensor = velocityGradientTensor_(elementInsideIdx);
          r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                          -0.5 * effDynViscosityGas_s
                          * velocityGradientTensor[normDim][curTangDim]
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);
          r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                          -0.5 * effDynViscosityGas_s
                          * velocityGradientTensor[normDim][curTangDim]
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);
        }
    }

    // /////////////////////
    // calculate rest of interface fluxes

    if (couplingMethod_ == 1 || couplingMethod_ == 2 || couplingMethod_ == 3)
    {
        ////
        // normal momentum
        ////
        // Neumann boundary condition for normal momentum equation of Stokes
        // (continuity of stress in normal direction)
        r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                       pressureGas_n
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        if (couplingMethod_ == 2 || couplingMethod_ == 3)
        {

            const auto& materialParams = spatialParams_.materialLawParamsAtPos(faceCenterGlobal);
            Scalar interfaceNormalPM = -1.0 * faceUnitOuterNormal[normDim];
            Scalar mobAir = 1.0 * densityGas_up * permeability
                            * MaterialLaw::krn(materialParams, saturationWater)
                            / dynamicViscosityGas_up;

            Scalar mobWater = 0.0;
            if (volVars.phasePresence() == bothPhases && couplingMethod_ == 2)
            {
                mobWater = 1.0 * densityWater_n * permeability
                          * MaterialLaw::krw(materialParams, saturationWater)
                          / dynamicViscosityWater_n;
            }
            Scalar pressureDeltaInterface = 1.0 * totalMassFlux;
            if (enableGravityDarcy_)
            {
                pressureDeltaInterface += (mobAir * densityGas_up + mobWater * densityGas_n)
                                          * interfaceNormalPM * gravity_[normDim];
            }
            // NOTE: pressureGas_n is already accounted for above
            pressureDeltaInterface *= (faceCenterGlobal[normDim] - elementCentersGlobal_n[normDim])
                                      * interfaceNormalPM
                                      / (mobAir + mobWater);

//             std::cout << " Fluxes at " << faceCenterGlobal
//                       << " interfacePressureGas " << interfacePressureGas
//                       << " pressureGas_s " << pressureGas_s
//                       << " pressureGas_n " << pressureGas_n
//                       << std::endl;

            r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                           pressureDeltaInterface
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
        }

        ////
        // water component fluxes
        ////
        Scalar componentFlux = 0.0;

        // diffusive fluxes
        Scalar diffusiveMoleFluxWater_s =
            -1.0 * effCompDiffCoeff_s
            * molarDensityGas_s
            * (moleFracWaterInGas_n - moleFrac_s)
            / (elementCentersGlobal_n[normDim] - elementCentersGlobal_s[normDim])
            * faceUnitOuterNormal[normDim];
        Scalar diffusiveFluxesWater_s = diffusiveMoleFluxWater_s * BaseFluid::molarMassComponent(StokesIndices::transportCompIdx);
        componentFlux += diffusiveFluxesWater_s;

        // advective fluxes, upwind
        Scalar advectiveFluxesWater_s = massFraction_up * totalMassFlux;
        componentFlux += advectiveFluxesWater_s;


        r_s.accumulate(lfsu_x_s, 0,
                      1.0 * componentFlux
                      * faceVolume);
        r_n.accumulate(lfsu_n, darcyContiWaterEqIdx,
                      -1.0 * componentFlux
                      * faceVolume);

        r_s.accumulate(lfsu_p_s, 0,
                      + 1.0 * diffusiveMoleFluxWater_s
                            * BaseFluid::molarMassComponent(StokesIndices::transportCompIdx)
                            * faceVolume
                      - 1.0 * diffusiveMoleFluxWater_s
                            * BaseFluid::molarMassComponent(StokesIndices::phaseCompIdx)
                            * faceVolume);
        r_n.accumulate(lfsu_n, darcyContiTotalEqIdx,
                      - 1.0 * diffusiveMoleFluxWater_s
                            * BaseFluid::molarMassComponent(StokesIndices::transportCompIdx)
                            * faceVolume
                      + 1.0 * diffusiveMoleFluxWater_s
                            * BaseFluid::molarMassComponent(StokesIndices::phaseCompIdx)
                            * faceVolume);

        ////
        // energy (temperature)
        ////
        Scalar energyFlux = 0.0;

        // diffusive fluxes and upwinding
        // assuming binary diffusion one molar basis -> transforming the flux with the molarMass of the components
        Scalar diffusiveFluxesAir_s = -1.0 * diffusiveMoleFluxWater_s
                                      * BaseFluid::molarMassComponent(StokesIndices::phaseCompIdx);
        Scalar enthalpyComponentWater_up = enthalpyComponentWater_s;
        Scalar enthalpyComponentAir_up = enthalpyComponentAir_n;
        if (diffusiveFluxesWater_s < 0)
        {
            enthalpyComponentWater_up = enthalpyComponentWater_n;
            enthalpyComponentAir_up = enthalpyComponentAir_s;
        }
        Scalar diffusiveEnthalpyFluxAir = enthalpyComponentAir_up * diffusiveFluxesAir_s;
        Scalar diffusiveEnthalpyFluxWater = enthalpyComponentWater_up * diffusiveFluxesWater_s;
        energyFlux += diffusiveEnthalpyFluxAir;
        energyFlux += diffusiveEnthalpyFluxWater;

        // conductive fluxes
        Scalar conductiveEnergyFlux =
            - 1.0 * effThermalConductivity_s
            * (temperature_n - temperature_s)
            / (elementCentersGlobal_n[normDim] - elementCentersGlobal_s[normDim])
            * faceUnitOuterNormal[normDim];
        energyFlux += conductiveEnergyFlux;

        // advective fluxes, upwind
        Scalar advectiveEnergyFlux = enthalpyPhaseGas_up * totalMassFlux;
        energyFlux +=  advectiveEnergyFlux;

        r_s.accumulate(lfsu_t_s, 0,
                      1.0 * energyFlux
                      * faceVolume);
        r_n.accumulate(lfsu_n, darcyEnergyEqIdx,
                      -1.0 * energyFlux
                      * faceVolume);

        Dune::dgrave << " Fluxes at " << faceCenterGlobal
                      << "   totalMass " << totalMassFlux
//                       << "   momentumX-0 " <<  0.5 * alphaBeaversJoseph * fromPermeability * velocities_s[2*tangDim][tangDim] * faceVolume
                      << "   momentumY " << faceUnitOuterNormal[normDim] * pressureGas_n * faceVolume
                      << "   componentFlux " << componentFlux
                      << "   energyFlux " << energyFlux
                      << std::endl;

        Dune::dwarn  << " componentFlux " << componentFlux
                      << "   advective_g " << advectiveFluxesWater_s
                      << "   diffusive_g " << diffusiveFluxesWater_s
                      << "   advective_l " << 0.0
                      << "   diffusive_l " << 0.0
                      << std::endl;

        Dune::dwarn  << " energyFlux " << energyFlux
                      << "   diffusive^a " << diffusiveEnthalpyFluxAir
                      << "   diffusive^w " << diffusiveEnthalpyFluxWater
                      << "   conductive_g " << conductiveEnergyFlux
                      << "   advective_g " << advectiveEnergyFlux
                      << std::endl;
    }
    else if (couplingMethod_ == 4) // complex interface
    {
        if (GET_PROP_VALUE(DarcySubProblemTypeTag, Formulation) != TwoPTwoCFormulation::pnsw)
        {
            DUNE_THROW(Dune::NotImplemented, "This coupling method is not implemented for other than the pnsw formulation.");
        }

        /////////////////////////
        // Newton method
        /////////////////////////

        // Jacobian matrix
        const int numEqDarcy_temp = 3;
        typedef Dune::FieldMatrix<Scalar, numEqDarcy_temp, numEqDarcy_temp> Matrix;
        Matrix J;
        // solution
        typedef Dune::FieldVector<Scalar, numEqDarcy_temp> Vector;
        Vector deltaX;
        // right hand side
        Vector b;

        Valgrind::SetUndefined(J);
        Valgrind::SetUndefined(deltaX);
        Valgrind::SetUndefined(b);

        // initialize unknowns with cell values and set weights
        Vector initialUnknowns(0.0);
        Vector unknowns(0.0);
        Vector weights(0.0);
        // pressure
        initialUnknowns[0] = 0.5 * (pressureGas_n + pressureGas_s);
        unknowns[0] = initialUnknowns[0];
        weights[0] = initialUnknowns[0];
        // saturation or mass fraction
        initialUnknowns[1] = switch_n;
        unknowns[1] = switch_n;
        weights[1] = switch_n;
        // temperature
        initialUnknowns[2] = 0.5 * (temperature_n + temperature_s);
        unknowns[2] = initialUnknowns[2];
        weights[2] = initialUnknowns[2];

        // Initialize fixed values needed for flux calculation
        InputParamsCache pIn;
        pIn.phasePresence = volVars.phasePresence();
        pIn.normDim = normDim;
        pIn.normal_if_s = faceUnitOuterNormal[normDim];
        pIn.normal_if_n = -1.0 * faceUnitOuterNormal[normDim];
        pIn.faceCenterGlobal = faceCenterGlobal;
        pIn.coordinate_if = faceCenterGlobal[normDim];
        pIn.coordinate_s = elementCentersGlobal_s[normDim];
        pIn.coordinate_n = elementCentersGlobal_n[normDim];
        pIn.totalMassFlux = totalMassFlux;
        pIn.pressureGas_s = pressureGas_s;
        pIn.pressureGas_n = pressureGas_n;
        pIn.switch_n = switch_n;
        pIn.temperature_s = temperature_s;
        pIn.temperature_n = temperature_n;
        pIn.densityGas_s = density_s;
        pIn.densityGas_n = volVars.density(nPhaseIdx_n);
        pIn.densityWater_n = densityWater_n;
        pIn.molarDensityGas_s = molarDensityGas_s;
        pIn.molarDensityGas_n = volVars.molarDensity(nPhaseIdx_n);
        pIn.molarDensityWater_n = volVars.molarDensity(wPhaseIdx_n);
        pIn.effDynViscosityGas_s = effDynViscosityGas_s;
        pIn.dynamicViscosityGas_n = dynamicViscosityGas_n;
        pIn.dynamicViscosityWater_n = dynamicViscosityWater_n;
        pIn.massFrac_s = massFrac_s;
        pIn.massFracWaterInGas_n = massFracWaterInGas_n;
        pIn.massFracWaterInWater_n = volVars.massFraction(wPhaseIdx_n, wCompIdx_n);
        pIn.moleFrac_s = moleFrac_s;
        pIn.moleFracWaterInGas_n = moleFracWaterInGas_n;
        pIn.moleFracWaterInWater_n = volVars.moleFraction(wPhaseIdx_n, wCompIdx_n);
        pIn.effDiffCoeffInGas_s = effCompDiffCoeff_s;
        pIn.diffusionCoefficientInGas_n = diffusionCoefficientInGas_n;
        pIn.diffusionCoefficientInWater_n = diffusionCoefficientInWater_n;
        pIn.enthalpyPhaseGas_s = enthalpyPhaseGas_s;
        pIn.enthalpyComponentAir_s = enthalpyComponentAir_s;
        pIn.enthalpyComponentWater_s = enthalpyComponentWater_s;
        pIn.enthalpyPhaseGas_n = enthalpyPhaseGas_n;
        pIn.enthalpyPhaseWater_n = enthalpyPhaseWater_n;
        pIn.effThermalConductivity_s = effThermalConductivity_s;
        pIn.permeability = permeability;
        pIn.materialParams = spatialParams_.materialLawParamsAtPos(faceCenterGlobal);

        // Setup solution cache
        bool appliedSlopeLimiter = false;
        bool updatedPhasePresence = false;
        OutputParamsCache pOut;
        pOut.componentFlux = 0.0;
        pOut.energyFlux = 0.0;
        calculateDefect_(b, unknowns, pIn, pOut);
        Scalar defectInitial = b.two_norm();

        for (int nIdx = 1; nIdx < newtonMaxSteps_ + 1; ++nIdx)
        {
            Vector tmp;
            appliedSlopeLimiter = false;
            updatedPhasePresence = false;

            // global slope limiter
            Scalar maxLocal = std::max(temperature_s, temperature_n) / slopeLimitingFactor_;
            Scalar minLocal = std::min(temperature_s, temperature_n) * slopeLimitingFactor_;
            if(unknowns[2] > maxLocal || unknowns[2] < minLocal)
            {
                appliedSlopeLimiter = true;
                unknowns[2] = std::min(maxLocal,std::max(unknowns[2],minLocal));
            }

            // update phase presence
            if (pIn.phasePresence == bothPhases && unknowns[1] < 0)
            {
                updatedPhasePresence = true;
                pIn.phasePresence = nPhaseOnly;
                unknowns[1] = 0.5 * (pIn.massFrac_s + pIn.massFracWaterInGas_n);
            }

            // reset jacobian
            J = 0;

            // initialize
            Valgrind::SetUndefined(b);
            calculateDefect_(b, unknowns, pIn, pOut);
            Valgrind::CheckDefined(b);

            // assemble jacobian matrix
            for (int pvIdx = 0; pvIdx < numEqDarcy_temp; ++ pvIdx)
            {
                ////////
                // approximately calculate partial derivatives of the i-th component.
                // This is done via forward differences

                // deviate the mole fraction of the i-th component
                Vector x = unknowns;
                const Scalar eps = 1e-10 * weights[pvIdx];
                x[pvIdx] += eps;

                // compute derivative of the defect
                calculateDefect_(tmp, x, pIn, pOut);
                tmp -= b;
                tmp /= eps;
                // store derivative in jacobian matrix
                for (int eqIdx = 0; eqIdx < numEqDarcy_temp; ++eqIdx)
                    J[eqIdx][pvIdx] = tmp[eqIdx];

                // end forward differences
                ////////
            }
            Valgrind::CheckDefined(J);
            Valgrind::CheckDefined(b);

            // Solve J*x = b
            deltaX = 0;
            try { J.solve(deltaX, b); }
            catch (Dune::FMatrixError e)
            {
                throw Dumux::NumericalProblem(e.what());
            }
            Valgrind::CheckDefined(deltaX);

            Scalar relError = 0;
            Scalar maxRelShift = 0;
            for (int pvIdx = 0; pvIdx < numEqDarcy_temp; ++ pvIdx)
            {
                if (std::isnan(deltaX[pvIdx]) || std::isinf(deltaX[pvIdx]))
                {
                    Dune::printmatrix(std::cout, J, "Jacobian", "row", 10, 5);
                    Dune::printvector(std::cout, b, "b", "row", 1, 10, 5);
                    std::cerr << faceCenterGlobal << " => "
                              << " numSteps " << nIdx
                              << " defectInitial " << defectInitial
                              << " relError " << relError
                              << " relReduction " << relError/defectInitial
                              << " u[0] " << unknowns[0]
                              << " u[1] " << unknowns[1]
                              << " u[2] " << unknowns[2]
                              << std::endl;

                    // retry with smaller time step
                    std::cerr << "Defect of interface system of equation is nan or inf." << std::endl;
                    unknowns[0] = std::numeric_limits<Scalar>::quiet_NaN();
                }
                maxRelShift = std::max(maxRelShift,
                                       std::abs(deltaX[pvIdx] / std::abs(unknowns[pvIdx] - deltaX[pvIdx]) * 2.0));
            }
            relError = b.two_norm();

            // perform update
            unknowns = unknowns - deltaX;

            // criterion for aborting netwon method
            if (maxRelShift < maxRelShift_
                || relError/defectInitial < resReduction_
                || nIdx >= newtonMaxSteps_)
            {
                Dune::dgrave << faceCenterGlobal << " => "
                             << " numSteps " << nIdx
                             << " defectInitial " << defectInitial
                             << " relError " << relError
                             << " relReduction " << relError/defectInitial
                             << " maxRelShift " << maxRelShift
                             << " u[0] " << unknowns[0]
                             << " u[1] " << unknowns[1]
                             << " u[2] " << unknowns[2]
                             << std::endl;

                // check physicalness
                if (
//                    (unknowns[0] < 99500.0 || unknowns[0] > 1.5e5) ||
                       (unknowns[1] < 0.0 || unknowns[1] > 1.0)
                    || (unknowns[2] < 273 || unknowns[2] > 370)
                   )
                {
                    std::cerr << faceCenterGlobal << " => "
                              << " numSteps " << nIdx
                              << " defectInitial " << defectInitial
                              << " relError " << relError
                              << " relReduction " << relError/defectInitial
                              << " maxRelShift " << maxRelShift
                              << " u[0] " << unknowns[0]
                              << " u[1] " << unknowns[1]
                              << " u[2] " << unknowns[2]
                              << std::endl;

                    // do not accept unphysical solutions (retry with a smaller time step)
                    std::cerr << "Unphysical solutions of interface system of equation. "
                              << "Maybe this happened because the transition to a one-phase system is not implemented." << std::endl;
                    unknowns[0] = std::numeric_limits<Scalar>::quiet_NaN();
                }

                // maxNewtonSteps is reached
                if (nIdx >= newtonMaxSteps_)
                {
                    Dune::dgrave << faceCenterGlobal << " => "
                                 << " numSteps " << nIdx
                                 << " defectInitial " << defectInitial
                                 << " relError " << relError
                                 << " relReduction " << relError/defectInitial
                                 << " maxRelShift " << maxRelShift
                                 << " u[0] " << unknowns[0]
                                 << " u[1] " << unknowns[1]
                                 << " u[2] " << unknowns[2]
                                 << std::endl;

                    // accept solution, if it is not unphysical
                    if ((unknowns[0] < 99500.0 || unknowns[0] > 1.5e5)
                        || (unknowns[1] < 0.0 || unknowns[1] > 1.0)
                        || (unknowns[2] < 273 || unknowns[2] > 370))
                    {
                        std::cerr << faceCenterGlobal << " => "
                                  << " numSteps " << nIdx
                                  << " defectInitial " << defectInitial
                                  << " relError " << relError
                                  << " relReduction " << relError/defectInitial
                                  << " maxRelShift " << maxRelShift
                                  << " u[0] " << unknowns[0]
                                  << " u[1] " << unknowns[1]
                                  << " u[2] " << unknowns[2]
                                  << std::endl;

                        // retry with a smaller time step, if no convergence was reached
                        std::cerr << "No convergence in interface system of equations." << std::endl;
                        unknowns[0] = std::numeric_limits<Scalar>::quiet_NaN();
                    }
                }

                // update fluxes and finish newton
                calculateDefect_(tmp, unknowns, pIn, pOut);
                nIdx = newtonMaxSteps_ + 1;
            }
        }

        if (appliedSlopeLimiter || updatedPhasePresence)
        {
            std::cerr << '\r';
            if (appliedSlopeLimiter)
                std::cerr << " => Applied slope limiter ...";
            if (updatedPhasePresence)
                std::cerr << " => Updated phase presence ...";
        }

        /////////////////
        // Do rest of calculation

        ////
        // normal momentum
        ////
        r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                        faceUnitOuterNormal[normDim]
                        * unknowns[0]
                        * faceVolume);

        ////
        // water component fluxes
        ////
        r_s.accumulate(lfsu_x_s, 0,
                      1.0 * pOut.componentFlux
                      * faceVolume);
        r_n.accumulate(lfsu_n, darcyContiWaterEqIdx,
                      -1.0 * pOut.componentFlux
                      * faceVolume);

        r_s.accumulate(lfsu_p_s, 0,
                      + 1.0 * pOut.diffusiveMoleFlux
                            * BaseFluid::molarMassComponent(StokesIndices::transportCompIdx)
                            * faceVolume
                      - 1.0 * pOut.diffusiveMoleFlux
                            * BaseFluid::molarMassComponent(StokesIndices::phaseCompIdx)
                            * faceVolume);
        r_n.accumulate(lfsu_n, darcyContiTotalEqIdx,
                      - 1.0 * pOut.diffusiveMoleFlux
                            * BaseFluid::molarMassComponent(StokesIndices::transportCompIdx)
                            * faceVolume
                      + 1.0 * pOut.diffusiveMoleFlux
                            * BaseFluid::molarMassComponent(StokesIndices::phaseCompIdx)
                            * faceVolume);

        ////
        // energy (temperature)
        ////
        r_s.accumulate(lfsu_t_s, 0,
                      1.0 * pOut.energyFlux
                      * faceVolume);
        r_n.accumulate(lfsu_n, darcyEnergyEqIdx,
                      -1.0 * pOut.energyFlux
                      * faceVolume);
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "This coupling method is not implemented.");
    }
  }

public:
    template<typename Vector, typename InputParamsCache, typename OutputParamsCache>
    const void calculateDefect_(Vector& b, const Vector unknowns,
                                InputParamsCache pIn, OutputParamsCache& pOut) const
    {
        FluidState fluidStateInterface;

        // set the saturations of the fluid phases
        Scalar saturationAir_if = 0.0;
        Scalar saturationWater_if = 0.0;
        if (pIn.phasePresence == nPhaseOnly)
        {
            saturationWater_if = 0.0;
            saturationAir_if = 1.0;
        }
        else if (pIn.phasePresence == wPhaseOnly) {
            DUNE_THROW(Dune::NotImplemented, "This coupling method is not implemented for water-phase only system.");
        }
        else if (pIn.phasePresence == bothPhases) {
            saturationWater_if = unknowns[1];
            saturationAir_if = 1.0 - saturationWater_if;
        }
        fluidStateInterface.setSaturation(nPhaseIdx_n, saturationAir_if);
        fluidStateInterface.setSaturation(wPhaseIdx_n, saturationWater_if);

        // set the pressures of the fluid phases
        Scalar pressureGas_if = unknowns[0];
        Scalar pressureWater_if = pressureGas_if - pc(pIn.materialParams, saturationWater_if);
        fluidStateInterface.setPressure(nPhaseIdx_n, pressureGas_if);
        fluidStateInterface.setPressure(wPhaseIdx_n, pressureWater_if);

        // set the temperature
        Scalar temperature_if = unknowns[2];
        fluidStateInterface.setTemperature(temperature_if);

        // calculate the phase compositions
        typename FluidSystem::ParameterCache paramCache;
        if (pIn.phasePresence == bothPhases) {
            // both phases are present, phase compositions are a result of the nonwetting <-> wetting equilibrium.
            // This is the job of the "MiscibleMultiPhaseComposition" constraint solver
            MiscibleMultiPhaseComposition::solve(fluidStateInterface,
                                                 paramCache,
                                                 /*setViscosity=*/true,
                                                 /*setEnthalpy=*/true);
        }
        else if (pIn.phasePresence == nPhaseOnly) {
            // setMassFraction() has only to be called 1-numComponents times
            fluidStateInterface.setMassFraction(nPhaseIdx_n, wCompIdx_n, unknowns[1]);

            // calculate the composition of the remaining phases (as well as the densities of all phases).
            // This is the job of the "ComputeFromReferencePhase" constraint solver
            if (useConstraintSolver) {
                ComputeFromReferencePhase::solve(fluidStateInterface,
                                                 paramCache,
                                                 nPhaseIdx_n,
                                                 /*setViscosity=*/true,
                                                 /*setEnthalpy=*/true);
            }
        }


        /////////////////////
        // total mass balance
        b[0] = 0.0;
        // free flow side
        Scalar totalMassFlux_ff = 0.0;
        totalMassFlux_ff += pIn.totalMassFlux;
        // porous medium side
        Scalar totalMassFlux_pm = 0.0;
        Scalar pressureGradientGas = (pressureGas_if - pIn.pressureGas_n)
                                     / (pIn.coordinate_if - pIn.coordinate_n);
        if (enableGravityDarcy_)
        {
            pressureGradientGas -= fluidStateInterface.density(nPhaseIdx_n)
                                   * gravity_[pIn.normDim];
        }
        Scalar mobilityGas = pIn.densityGas_n
                             * pIn.permeability
                             * MaterialLaw::krn(pIn.materialParams, pIn.switch_n)
                             / pIn.dynamicViscosityGas_n;
        Scalar massFracWaterInGas_up = pIn.massFracWaterInGas_n;
        Scalar enthalpyPhaseGas_up = pIn.enthalpyPhaseGas_n;
        if (pressureGradientGas * pIn.normal_if_n > 0) // upwinding
        {
            mobilityGas = fluidStateInterface.density(nPhaseIdx_n)
                          * pIn.permeability
                          * MaterialLaw::krn(pIn.materialParams, pIn.switch_n)
                          / fluidStateInterface.viscosity(nPhaseIdx_n);
            massFracWaterInGas_up = fluidStateInterface.massFraction(nPhaseIdx_n, wCompIdx_n);
            enthalpyPhaseGas_up = fluidStateInterface.enthalpy(nPhaseIdx_n);
        }
        Scalar advectiveMassFluxGas = -1.0 * mobilityGas * pressureGradientGas * pIn.normal_if_n;
        totalMassFlux_pm += advectiveMassFluxGas;

        Scalar pressureGradientWater = (pressureWater_if - (pIn.pressureGas_n - pc(pIn.materialParams, pIn.switch_n)))
                                       / (pIn.coordinate_if - pIn.coordinate_n);
        if (enableGravityDarcy_)
        {
            pressureGradientWater -= fluidStateInterface.density(wPhaseIdx_n)
                                     * gravity_[pIn.normDim];
        }
        Scalar mobilityWater = pIn.densityWater_n
                               * pIn.permeability
                               * MaterialLaw::krw(pIn.materialParams, pIn.switch_n)
                               / pIn.dynamicViscosityWater_n;
        Scalar massFracWaterInWater_up = pIn.massFracWaterInWater_n;
        Scalar enthalpyPhaseWater_up = pIn.enthalpyPhaseWater_n;
        if (pressureGradientWater * pIn.normal_if_n > 0) // upwinding
        {
            mobilityWater = fluidStateInterface.density(wPhaseIdx_n)
                            * pIn.permeability
                            * MaterialLaw::krw(pIn.materialParams, pIn.switch_n)
                            / fluidStateInterface.viscosity(wPhaseIdx_n);
            massFracWaterInWater_up = fluidStateInterface.massFraction(wPhaseIdx_n, wCompIdx_n);
            enthalpyPhaseWater_up = fluidStateInterface.enthalpy(wPhaseIdx_n);
        }
        Scalar advectiveMassFluxWater = -1.0 * mobilityWater * pressureGradientWater * pIn.normal_if_n;
        if (pIn.phasePresence == nPhaseOnly)
        {
            advectiveMassFluxWater = 0.0;
        }
        totalMassFlux_pm += advectiveMassFluxWater;
        // summing up
        b[0] = totalMassFlux_ff + totalMassFlux_pm;

        /////////////////////////
        // component mass balance
        b[1] = 0.0;
        // free flow side
        Scalar componentFlux_ff = 0.0;
        componentFlux_ff += pIn.totalMassFlux * massFracWaterInGas_up;
        Scalar diffusiveMoleFluxWater_s = -1.0 * pIn.effDiffCoeffInGas_s
                                               * pIn.molarDensityGas_s
                                               * (fluidStateInterface.moleFraction(nPhaseIdx_n, wCompIdx_n) - pIn.moleFrac_s)
                                               / (pIn.coordinate_if - pIn.coordinate_s)
                                               * pIn.normal_if_s;
        Scalar diffusiveFluxWater_s = diffusiveMoleFluxWater_s
                                      * BaseFluid::molarMassComponent(StokesIndices::transportCompIdx);
        Scalar diffusiveFluxAir_s = -1.0 * diffusiveMoleFluxWater_s
                                    * BaseFluid::molarMassComponent(StokesIndices::phaseCompIdx);
        componentFlux_ff += diffusiveFluxWater_s;
        // porous medium side
        Scalar componentFlux_pm = 0.0;
        componentFlux_pm += advectiveMassFluxGas * massFracWaterInGas_up;
        componentFlux_pm += advectiveMassFluxWater * massFracWaterInWater_up;
        Scalar diffusiveMoleFluxWaterInGas_n = -1.0 * pIn.diffusionCoefficientInGas_n
                                               * pIn.molarDensityGas_n
                                               * (fluidStateInterface.moleFraction(nPhaseIdx_n, wCompIdx_n) - pIn.moleFracWaterInGas_n)
                                               / (pIn.coordinate_if - pIn.coordinate_n)
                                               * pIn.normal_if_n;
        Scalar diffusiveMoleFluxWaterInWater_n = -1.0 * pIn.diffusionCoefficientInWater_n
                                                 * pIn.molarDensityWater_n
                                                 * (fluidStateInterface.moleFraction(wPhaseIdx_n, wCompIdx_n) - pIn.moleFracWaterInWater_n)
                                                 / (pIn.coordinate_if - pIn.coordinate_n)
                                                 * pIn.normal_if_n;

        if (pIn.phasePresence == nPhaseOnly)
        {
            diffusiveMoleFluxWaterInWater_n = 0.0;
        }
        componentFlux_pm += diffusiveMoleFluxWaterInGas_n * BaseFluid::molarMassComponent(wCompIdx_n);
        componentFlux_pm += diffusiveMoleFluxWaterInWater_n * BaseFluid::molarMassComponent(wCompIdx_n);
        // summing up
        b[1] = componentFlux_ff + componentFlux_pm;
        pOut.componentFlux = componentFlux_ff;
        pOut.diffusiveMoleFlux = diffusiveMoleFluxWater_s;
        b[0] += /* free flow, gas */
               + diffusiveFluxWater_s
               + diffusiveFluxAir_s
               /* porous medium, gas */
               + diffusiveMoleFluxWaterInGas_n * BaseFluid::molarMassComponent(wCompIdx_n)
               - diffusiveMoleFluxWaterInGas_n * BaseFluid::molarMassComponent(nCompIdx_n) // negative co-current flux
               /* porous medium, water */
               + diffusiveMoleFluxWaterInWater_n * BaseFluid::molarMassComponent(wCompIdx_n)
               - diffusiveMoleFluxWaterInWater_n * BaseFluid::molarMassComponent(nCompIdx_n); // negative co-current flux

        /////////////////
        // energy balance
        b[2] = 0.0;
        // free flow side
        Scalar energyFlux_ff = 0.0;
        Scalar enthalpyComponentWater_up = pIn.enthalpyComponentWater_s;
        Scalar enthalpyComponentAir_up = FluidSystem::componentEnthalpy(fluidStateInterface, nPhaseIdx_n, nCompIdx_n);
        if (diffusiveFluxWater_s < 0) // upwinding
        {
            enthalpyComponentWater_up = FluidSystem::componentEnthalpy(fluidStateInterface, nPhaseIdx_n, wCompIdx_n);
            enthalpyComponentAir_up = pIn.enthalpyComponentAir_s;
        }
        energyFlux_ff += pIn.totalMassFlux * enthalpyPhaseGas_up;
        energyFlux_ff += diffusiveFluxWater_s * enthalpyComponentWater_up;
        energyFlux_ff += diffusiveFluxAir_s * enthalpyComponentAir_up;
        energyFlux_ff -= pIn.effThermalConductivity_s
                         * (temperature_if - pIn.temperature_s)
                         / (pIn.coordinate_if - pIn.coordinate_s)
                         * pIn.normal_if_s;
        // porous medium side
        Scalar energyFlux_pm = 0.0;
        energyFlux_pm += advectiveMassFluxGas * enthalpyPhaseGas_up;
        energyFlux_pm += advectiveMassFluxWater * enthalpyPhaseWater_up;
        Scalar effThermalConductivity_n = ThermalConductivityModel::effectiveThermalConductivity(
            saturationWater_if,
            FluidSystem::thermalConductivity(fluidStateInterface, wPhaseIdx_n),
            FluidSystem::thermalConductivity(fluidStateInterface, nPhaseIdx_n),
            spatialParams_.solidThermalConductivityAtPos(pIn.faceCenterGlobal),
            spatialParams_.porosityAtPos(pIn.faceCenterGlobal),
            spatialParams_.solidDensityAtPos(pIn.faceCenterGlobal));
        energyFlux_pm -= effThermalConductivity_n
                         * (temperature_if - pIn.temperature_n)
                         / (pIn.coordinate_if - pIn.coordinate_n)
                         * pIn.normal_if_n;
        // summing up
        b[2] = energyFlux_ff + energyFlux_pm;
        pOut.energyFlux = energyFlux_ff;

//         // OUTPUT
//         std::cout << " totalMassFlux_pm " << totalMassFlux_pm
//                   << " advectiveMassFluxGas " << advectiveMassFluxGas
//                   << " advectiveMassFluxWater " << advectiveMassFluxWater
//                   << " totalMassFlux_ff " << totalMassFlux_ff
//                   << std::endl
//                   << " componentFlux_ff " << componentFlux_ff
//                   << " diffusiveFluxWaterInGas_n " << diffusiveFluxWaterInGas_n
//                   << " diffusiveFluxWaterInWater_n " << diffusiveFluxWaterInWater_n
//                   << " componentFlux_pm " << componentFlux_pm
//                   << std::endl
//                   << " energyFlux_ff " << energyFlux_ff
//                   << " cond_pm " << (effThermalConductivity_n
//                          * (temperature_if - pIn.temperature_n)
//                          / (pIn.coordinate_if - pIn.coordinate_n)
//                          * pIn.normal_if_n)
//                   << " advEnergyGas_pm " << advectiveMassFluxGas * enthalpyPhaseGas_up
//                   << " advEnergyWater_pm " << advectiveMassFluxWater * enthalpyPhaseWater_up
//                   << " energyFlux_pm " << energyFlux_pm
//                   << std::endl;
    }

    const Scalar pc(MaterialLawParams materialParams, Scalar saturation) const
    {
        return MaterialLaw::pc(materialParams, saturation);
    }

    struct InputParamsCache
    {
        unsigned int phasePresence;
        unsigned int normDim;
        Scalar normal_if_s;
        Scalar normal_if_n;
        Dune::FieldVector<Scalar, dim> faceCenterGlobal;
        Scalar coordinate_if;
        Scalar coordinate_s;
        Scalar coordinate_n;
        Scalar totalMassFlux;
        Scalar pressureGas_s;
        Scalar pressureGas_n;
        Scalar switch_n;
        Scalar temperature_s;
        Scalar temperature_n;
        Scalar densityGas_s;
        Scalar densityGas_n;
        Scalar densityWater_n;
        Scalar molarDensityGas_s;
        Scalar molarDensityGas_n;
        Scalar molarDensityWater_n;
        Scalar effDynViscosityGas_s;
        Scalar dynamicViscosityGas_n;
        Scalar dynamicViscosityWater_n;
        Scalar massFrac_s;
        Scalar massFracWaterInGas_n;
        Scalar massFracWaterInWater_n;
        Scalar moleFrac_s;
        Scalar moleFracWaterInGas_n;
        Scalar moleFracWaterInWater_n;
        Scalar effDiffCoeffInGas_s;
        Scalar diffusionCoefficientInGas_n;
        Scalar diffusionCoefficientInWater_n;
        Scalar enthalpyPhaseGas_s;
        Scalar enthalpyComponentAir_s;
        Scalar enthalpyComponentWater_s;
        Scalar enthalpyPhaseGas_n;
        Scalar enthalpyPhaseWater_n;
        Scalar effThermalConductivity_s;
        Scalar permeability;
        MaterialLawParams materialParams;
    };

    struct OutputParamsCache
    {
        Scalar componentFlux;
        Scalar diffusiveMoleFlux;
        Scalar energyFlux;
    };

private:
    DarcySubDomainProblem& darcySubDomainProblem_;
    SpatialParams& spatialParams_;
    MapperElement mapperElementMultiDomain_;

    bool enableNavierStokes_;
    bool enableUnsymmetrizedVelocityGradient_;
    bool beaversJosephAsSolDependentDirichlet_;
    int newtonMaxSteps_;
    Scalar resReduction_;
    Scalar maxRelShift_;
    Scalar slopeLimitingFactor_;
    bool enableGravityDarcy_;
    Scalar gravity_[dim];
    int couplingMethod_;

    std::function<Scalar (unsigned int)> eddyKinematicViscosity_;
    std::function<Scalar (unsigned int)> eddyDiffusivity_;
    std::function<Scalar (unsigned int)> eddyThermalConductivity_;
    std::function<Dune::FieldMatrix<Scalar, dim, dim> (unsigned int)> velocityGradientTensor_;
    std::function<bool (unsigned int)> useWallFunctionMomentum_;
};

} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_2P2CNI_DARCY_2CNI_HH
