// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_PROPERTIES_HH
#define DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_PROPERTIES_HH

#include <dumux/porousmediumflow/2p2c/implicit/propertydefaults.hh>

#if COUPLING_KOMEGA
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cnipropertydefaults.hh>
#elif COUPLING_KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnipropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnipropertydefaults.hh>
#elif COUPLING_ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cnipropertydefaults.hh>
#elif COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cnipropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/common/multidomainpropertydefaults.hh>
#include <appl/staggeredgrid/multidomain/common/subdomainpropertydefaults.hh>

namespace Dumux
{
namespace Properties
{
NEW_TYPE_TAG(MDSNavierStokesTwoCTDarcyTwoPTwoCT, INHERITS_FROM(MultiDomainStaggered));

// The type tag for the Stokes problem
#ifdef COUPLING_KOMEGA
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridKOmega2cni, SubDomainStaggered));
#elif COUPLING_KEPSILON
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridKEpsilon2cni, SubDomainStaggered));
#elif COUPLING_LOWREKEPSILON
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridLowReKEpsilon2cni, SubDomainStaggered));
#elif COUPLING_ONEEQ
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridSpalartAllmaras2cni, SubDomainStaggered));
#elif COUPLING_ZEROEQ
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridZeroEq2cni, SubDomainStaggered));
#else
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridNavierStokes2cni, SubDomainStaggered));
#endif

// The type tags for the Darcy problem
NEW_TYPE_TAG(DarcySpatialParams);
NEW_TYPE_TAG(DarcySubProblem, INHERITS_FROM(CCTwoPTwoCNI, DarcySpatialParams, SubDomainStaggered));

// single grid function spaces
NEW_PROP_TAG(PressureConstraints);
NEW_PROP_TAG(VelocityConstraints);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0Constrained);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceStaggered);
#if COUPLING_KOMEGA
NEW_PROP_TAG(DissipationConstraints);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#elif COUPLING_KEPSILON
NEW_PROP_TAG(TurbulentKineticEnergyConstraints);
NEW_PROP_TAG(DissipationConstraints);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#endif

// composed grid function spaces
NEW_PROP_TAG(SubDomainComposedGridFunctionSpace);

// multidomain grid function space
NEW_PROP_TAG(MultiDomainGridFunctionSpace);

// local operator for stationary part (on sub domain)
NEW_PROP_TAG(LocalOperator);

// local operator for transient part (on sub domain)
NEW_PROP_TAG(TransientLocalOperator);

// stationary PDELab sub problem
NEW_PROP_TAG(SubProblemStationaryStokes);
NEW_PROP_TAG(SubProblemStationaryDarcy);

// transient PDELab sub problem
NEW_PROP_TAG(SubProblemTransientStokes);
NEW_PROP_TAG(SubProblemTransientDarcy);

// coupling operator
NEW_PROP_TAG(CouplingOperator);
NEW_PROP_TAG(Coupling);

// stationary multi domain grid operator
NEW_PROP_TAG(MultiDomainStationaryGridOperator);

// transient multi domain grid operator
NEW_PROP_TAG(MultiDomainTransientGridOperator);

// full multidomain grid operator
NEW_PROP_TAG(MultiDomainGridOperator);

// Define how the Beavers-Joseph Condition is applied
NEW_PROP_TAG(CouplingBeaversJosephAsSolDependentDirichlet);

// Minimum accepted time step size to proceed simulation
NEW_PROP_TAG(NewtonAbortTimeStepSize);

// Factor to adapt the time step, if Newton failed
NEW_PROP_TAG(NewtonTimeStepReduction);

// Output frequency of Vtk files
NEW_PROP_TAG(VtkFreqOutput);

// Reference pressure used for pn_rel in Vtk files
NEW_PROP_TAG(VtkReferencePressure);

// Criteria for the interface Newton
NEW_PROP_TAG(CouplingSolverMaxSteps);
NEW_PROP_TAG(CouplingSolverResidualReduction);
NEW_PROP_TAG(CouplingSolverMaxRelativeShift);
NEW_PROP_TAG(CouplingSlopeLimitingFactor);

// Limit range of k and epsilon to positive values
NEW_PROP_TAG(NewtonKEpsilonLimiter);

// // Set the default episode length negative -> every time step a vtk is written
// NEW_PROP_TAG(TimeManagerEpisodeLength);

// Set the default output frequency
NEW_PROP_TAG(TimeManagerOutputFreq);


} // end namespace Properties
} // end namespace Dumux



#endif // DUMUX_MULTIDOMAIN_NAVIERSTOKESTWOCT_DARCYTWOCT_PROPERTIES_HH
