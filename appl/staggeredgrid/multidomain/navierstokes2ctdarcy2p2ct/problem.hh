// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for problems which involve two sub problems
 */

#ifndef DUMUX_MULTIDOMAIN_PROBLEM_HH
#define DUMUX_MULTIDOMAIN_PROBLEM_HH

#define IS_STAGGERED_MULTIDOMAIN_MODEL 1
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON || COUPLING_ONEEQ || COUPLING_ZEROEQ)
#define TURBULENT 1
#endif

// from .cc file
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/backend/istlsolverbackend.hh>
#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/seqistlsolverbackend.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/subspace.hh>
#include <dune/pdelab/gridoperator/onestep.hh>

#include <dune/pdelab/multidomain/constraints.hh>
#include <dune/pdelab/multidomain/interpolate.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/vtk.hh>

#include <dumux/porousmediumflow/implicit/velocityoutput.hh>
#if HAVE_VALGRIND
#include <valgrind/callgrind.h>
#endif

#if COUPLING_KOMEGA
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cnistaggeredgrid.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cnitransientstaggeredgrid.hh>
#elif COUPLING_KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnistaggeredgrid.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnitransientstaggeredgrid.hh>
#elif COUPLING_LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnistaggeredgrid.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnitransientstaggeredgrid.hh>
#elif COUPLING_ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cnistaggeredgrid.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cnitransientstaggeredgrid.hh>
#elif COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cnistaggeredgrid.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnistaggeredgrid.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnitransientstaggeredgrid.hh>
#endif

#include <appl/staggeredgrid/localfunctions/staggeredq0fem.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainlocaloperator.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainnewtonmethod.hh>

#include "propertydefaults.hh"

namespace Dumux
{

// forward declarations
template<typename GV, typename RF>
class DarcyDirichletCondition;
template<typename GV>
class BCDarcyPressure;
template<typename GV>
class BCDarcyMassMoleFrac;
template<typename GV>
class BCDarcyTemperature;

/*!
 * \ingroup ImplicitBaseProblems
 * \ingroup MultidomainModel
 * \brief Base class for problems which involve two sub problems (multidomain problems)
 */
template<class TypeTag>
class MultiDomainProblem
{
private:
    // subdomain problem type tags
    using StokesSubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag);
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);

    // general types
    using Implementation = typename GET_PROP_TYPE(TypeTag, Problem);
    using Grid = typename GET_PROP_TYPE(TypeTag, Grid);
    using GridCreator = typename GET_PROP_TYPE(TypeTag, GridCreator);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using SubDomainGrid = typename GET_PROP_TYPE(TypeTag, SubDomainGrid);
    using SubDomainGridView = typename GET_PROP_TYPE(TypeTag, SubDomainGridView);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    enum { dim = MultiDomainGridView::dimension,
           dimWorld = SubDomainGridView::dimensionworld };
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    using StokesIndices = typename GET_PROP_TYPE(StokesSubProblemTypeTag, Indices);
    using DarcyIndices = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Indices);

    // types for multidomain coupling
    using CouplingOperator = typename GET_PROP_TYPE(TypeTag, CouplingOperator);
    using Coupling = typename GET_PROP_TYPE(TypeTag, Coupling);
    using MultiDomainGfs = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using SubProblemStationaryStokes = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryStokes);
    using SubProblemStationaryDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryDarcy);
    using SubProblemTransientStokes = typename GET_PROP_TYPE(TypeTag, SubProblemTransientStokes);
    using SubProblemTransientDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemTransientDarcy);
    using StationaryGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainStationaryGridOperator);
    using TransientGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainTransientGridOperator);
    using GridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator);

    using StokesSubDomainProblem = typename GET_PROP_TYPE(StokesSubProblemTypeTag, Problem);
    using DarcySubDomainProblem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem);

    // boundary conditions and source terms
    using BCStokes = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCType);
    using BCVelocity = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCVelocity);
    using DirichletVelocity = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletVelocity);
    using NeumannVelocity = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannVelocity);
    using SourceMomentumBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceMomentumBalance);
    using BCPressure = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCPressure);
    using DirichletPressure = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletPressure);
    using NeumannPressure = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannPressure);
    using SourceMassBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceMassBalance);
    using BCMassMoleFrac = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCMassMoleFrac);
    using DirichletMassMoleFrac = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletMassMoleFrac);
    using NeumannMassMoleFrac = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannMassMoleFrac);
    using SourceComponentBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceComponentBalance);
    using BCTemperature = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCTemperature);
    using DirichletTemperature = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletTemperature);
    using NeumannTemperature = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannTemperature);
    using SourceEnergyBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceEnergyBalance);
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
    using BCTurbulentKineticEnergy = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCTurbulentKineticEnergy);
    using BCDissipation = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCDissipation);
    using DirichletTurbulentKineticEnergy = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletTurbulentKineticEnergy);
    using DirichletDissipation = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletDissipation);
    using DirichletComposed = Dune::PDELab::CompositeGridFunction<
        DirichletVelocity, DirichletPressure,
        DirichletMassMoleFrac, DirichletTemperature,
        DirichletTurbulentKineticEnergy, DirichletDissipation>;
#elif (COUPLING_ONEEQ)
    using BCViscosityTilde = typename GET_PROP_TYPE(StokesSubProblemTypeTag, BCViscosityTilde);
    using DirichletViscosityTilde = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DirichletViscosityTilde);
    using DirichletComposed = Dune::PDELab::CompositeGridFunction<
        DirichletVelocity, DirichletPressure,
        DirichletMassMoleFrac, DirichletTemperature,
        DirichletViscosityTilde>;
#else
    using DirichletComposed = Dune::PDELab::CompositeGridFunction<
        DirichletVelocity, DirichletPressure, DirichletMassMoleFrac, DirichletTemperature>;
#endif
    // function spaces, operators
    using PressureConstraints = typename GET_PROP_TYPE(StokesSubProblemTypeTag, PressureConstraints);
    using VelocityConstraints = typename GET_PROP_TYPE(StokesSubProblemTypeTag, VelocityConstraints);
#if (COUPLING_KOMEGA)
    using DissipationConstraints = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DissipationConstraints);
    using stokesDissipationGfsP0 = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#elif (COUPLING_KEPSILON)
    using TurbulentKineticEnergyConstraints = typename GET_PROP_TYPE(StokesSubProblemTypeTag, TurbulentKineticEnergyConstraints);
    using DissipationConstraints = typename GET_PROP_TYPE(StokesSubProblemTypeTag, DissipationConstraints);
    using stokesTurbulentKineticEnergyGfsP0 = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained);
    using stokesDissipationGfsP0 = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#endif
    using StokesScalarGfsStaggered = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainScalarGridFunctionSpaceStaggered);
    using StokesScalarGfsP0Constrained = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainScalarGridFunctionSpaceP0Constrained);
    using StokesScalarGfsP0 = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainScalarGridFunctionSpaceP0);
    using DarcyScalarGfsP0 = typename GET_PROP_TYPE(DarcySubProblemTypeTag, SubDomainScalarGridFunctionSpaceP0);
    using StokesComposedGfs = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SubDomainComposedGridFunctionSpace);
    using DarcyComposedGfs = typename GET_PROP_TYPE(DarcySubProblemTypeTag, SubDomainComposedGridFunctionSpace);
    using StokesLocalOperator = typename GET_PROP_TYPE(StokesSubProblemTypeTag, LocalOperator);
    using DarcyLocalOperator = typename GET_PROP_TYPE(DarcySubProblemTypeTag, LocalOperator);
    using StokesTransientLocalOperator = typename GET_PROP_TYPE(StokesSubProblemTypeTag, TransientLocalOperator);
    using DarcyTransientLocalOperator = typename GET_PROP_TYPE(DarcySubProblemTypeTag, TransientLocalOperator);

    // old stuff, can maybe be replaced by decltype
    using LocalResidualDarcy = typename GET_PROP_TYPE(DarcySubProblemTypeTag, LocalResidual);
    using SolVectorDarcy = typename GET_PROP_TYPE(DarcySubProblemTypeTag, SolutionVector);
    using Model = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Model);
    using NewtonController = typename GET_PROP_TYPE(DarcySubProblemTypeTag, NewtonController);
    using SolVectorMultiDomain = typename Dune::PDELab::BackendVectorSelector<MultiDomainGfs, Scalar>::Type;

    // constants
    const unsigned int numEqDarcy = DarcyIndices::numEq;
    const unsigned int numEqStokes = StokesIndices::numEq;
    constexpr static int stokesVelocityIdx = StokesIndices::velocityIdx;
    constexpr static int stokesPressureIdx = StokesIndices::pressureIdx;
    constexpr static int stokesMassFracIdx = StokesIndices::massMoleFracIdx;
    constexpr static int stokesTemperatureIdx = StokesIndices::temperatureIdx;
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
    constexpr static int stokesTurbulentKineticEnergyIdx = StokesIndices::turbulentKineticEnergyIdx;
    constexpr static int stokesDissipationIdx = StokesIndices::dissipationIdx;
#elif (COUPLING_ONEEQ)
    constexpr static int stokesViscosityTildeIdx = StokesIndices::viscosityTildeIdx;
#endif
    constexpr static unsigned int darcyPressureIdxDumux = DarcyIndices::pressureIdx;
    constexpr static unsigned int darcySwitchIdxDumux = DarcyIndices::switchIdx;
    constexpr static unsigned int darcyTemperatureIdxDumux = DarcyIndices::temperatureIdx;
    constexpr static unsigned int darcyPressureIdxPDELab = MultiDomainIndices::darcyPressureIdxPDELab;
    constexpr static unsigned int darcySwitchIdxPDELab = MultiDomainIndices::darcySwitchIdxPDELab;
    constexpr static unsigned int darcyTemperatureIdxPDELab = MultiDomainIndices::darcyTemperatureIdxPDELab;
    constexpr static unsigned int stokesSubDomainIdx = MultiDomainIndices::stokesSubDomainIdx;
    constexpr static unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
      * \brief Base class for the multi domain problem
      *
      * \param mdGrid The multi domain grid
      * \param timeManager The TimeManager which is used by the simulation
      *
      */
    MultiDomainProblem(TimeManager &timeManager,
                       GridView gridView)
        : timeManager_(timeManager)
        , timeVector_(0.0)
        , eps_(1e-6)
        , mdGrid_(std::make_shared<MultiDomainGrid>(GridCreator::grid(), false))
        , mdGridView_(std::make_shared<MultiDomainGridView>(mdGrid_->leafGridView()))
        , sdProblemStokes_(std::make_shared<StokesSubDomainProblem>(timeManager, *mdGridView_))
        , sdProblemDarcy_(std::make_shared<DarcySubDomainProblem>(timeManager, *mdGridView_))
    {
      std::cout << "generic problem" << std::endl;
        asImp_().initializeGrid();
        maxTimeStepSize_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, MaxTimeStepSize);
        outputFreq_ = GET_PARAM_FROM_GROUP(TypeTag, int, TimeManager, OutputFreq);
//         episodeLength_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
//         if (episodeLength_ > 0)
//             this->timeManager().startNextEpisode(episodeLength_);
        writerStokes = std::make_shared<Dumux::VtkMultiWriter<SubDomainGridView>>(mdGrid_->subDomain(stokesSubDomainIdx).leafGridView(), asImp_().name() + "-ffSecondary");
        writerDarcy = std::make_shared<Dumux::VtkMultiWriter<SubDomainGridView>>(mdGrid_->subDomain(darcySubDomainIdx).leafGridView(), asImp_().name() + "-pm");
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        // initialize the sub-problems
        // the stokes init() function is not needed
        sdProblemDarcy().init();

        auto mdgv = this->mdGrid().leafGridView();

        // instantiate finite element maps
        using P0FEM = Dune::PDELab::P0LocalFiniteElementMap<Scalar, Scalar, dim>;
        P0FEM p0fem(Dune::GeometryType(Dune::GeometryType::cube, dim));
        using StaggeredQ0FEM =  Dune::PDELab::StaggeredQ0LocalFiniteElementMap<Scalar, Scalar, dim>;
        StaggeredQ0FEM staggeredq0fem;

        // set up functions defining the problem
        // Dirichlet values for Navier-Stokes
        DirichletVelocity dirichletVelocity(mdgv, this->sdProblemStokes());
        DirichletPressure dirichletPressure(mdgv, this->sdProblemStokes());
        DirichletMassMoleFrac dirichletMassMoleFrac(mdgv, this->sdProblemStokes());
        DirichletTemperature dirichletTemperature(mdgv, this->sdProblemStokes());
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
        DirichletTurbulentKineticEnergy dirichletTurbulentKineticEnergy(mdgv, this->sdProblemStokes());
        DirichletDissipation dirichletDissipation(mdgv, this->sdProblemStokes());
        DirichletComposed dirichletComposed(
            dirichletVelocity, dirichletPressure,
            dirichletMassMoleFrac, dirichletTemperature,
            dirichletTurbulentKineticEnergy, dirichletDissipation);
#elif (COUPLING_ONEEQ)
        DirichletViscosityTilde dirichletViscosityTilde(mdgv, this->sdProblemStokes());
        DirichletComposed dirichletComposed(
            dirichletVelocity, dirichletPressure,
            dirichletMassMoleFrac, dirichletTemperature,
            dirichletViscosityTilde);
#else
        DirichletComposed dirichletComposed(
            dirichletVelocity, dirichletPressure,
            dirichletMassMoleFrac, dirichletTemperature);
#endif
        BCVelocity bcVelocity(this->sdProblemStokes());
        BCPressure bcPressure(this->sdProblemStokes());
        BCMassMoleFrac bcMassMoleFrac(this->sdProblemStokes());
        BCTemperature bcTemperature(this->sdProblemStokes());
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
        BCTurbulentKineticEnergy bcTurbulentKineticEnergy(this->sdProblemStokes());
        BCDissipation bcDissipation(this->sdProblemStokes());
        BCStokes bcStokes(bcVelocity, bcPressure, bcMassMoleFrac, bcTemperature, bcTurbulentKineticEnergy, bcDissipation);
#elif (COUPLING_ONEEQ)
        BCViscosityTilde bcViscosityTilde(this->sdProblemStokes());
        BCStokes bcStokes(bcVelocity, bcPressure, bcMassMoleFrac, bcTemperature, bcViscosityTilde);
#else
        BCStokes bcStokes(bcVelocity, bcPressure, bcMassMoleFrac, bcTemperature);
#endif
        // Neumann and Source Values for Navier-Stokes
        NeumannVelocity neumannVelocity(mdgv, this->sdProblemStokes());
        NeumannPressure neumannPressure(mdgv, this->sdProblemStokes());
        NeumannMassMoleFrac neumannMassMoleFrac(mdgv, this->sdProblemStokes());
        NeumannTemperature neumannTemperature(mdgv, this->sdProblemStokes());
        SourceMomentumBalance sourceMomentumBalance(mdgv, this->sdProblemStokes());
        SourceMassBalance sourceMassBalance(mdgv, this->sdProblemStokes());
        SourceComponentBalance sourceComponentBalance(mdgv, this->sdProblemStokes());
        SourceEnergyBalance sourceEnergyBalance(mdgv, this->sdProblemStokes());
        // for Darcy
        Dumux::DarcyBoundaries<TypeTag> darcyBoundaries(this->sdProblemDarcy());

        // construct grid function space for Stokes
        VelocityConstraints velocityConstraints;
        StokesScalarGfsStaggered stokesScalarGfsStaggered(this->sdGridViewStokes(), staggeredq0fem, velocityConstraints);
        stokesScalarGfsStaggered.name("velocity");
        PressureConstraints pressureConstraints(this->mdGridView(), this->sdProblemStokes());
        StokesScalarGfsP0Constrained stokesPressureGfs(this->sdGridViewStokes(), p0fem, pressureConstraints);
        stokesPressureGfs.name("pressure");
        StokesScalarGfsP0 stokesComponentGfs(this->sdGridViewStokes(), p0fem);
        stokesComponentGfs.name("component");
        StokesScalarGfsP0 stokesTemperatureGfs(this->sdGridViewStokes(), p0fem);
        stokesTemperatureGfs.name("temperature");
        // composed function space for Stokes
#if (COUPLING_KOMEGA)
        StokesScalarGfsP0 stokesTurbulentKineticEnergyGfs(this->sdGridViewStokes(), p0fem);
        stokesTurbulentKineticEnergyGfs.name("turbulentKineticEnergy");
        DissipationConstraints dissipationConstraints(this->mdGridView(), this->sdProblemStokes());
        stokesDissipationGfsP0 stokesDissipationGfs(this->sdGridViewStokes(), p0fem, dissipationConstraints);
        stokesDissipationGfs.name("dissipation");
        StokesComposedGfs stokesComposedGfs(stokesScalarGfsStaggered, stokesPressureGfs,
                                            stokesComponentGfs, stokesTemperatureGfs,
                                            stokesTurbulentKineticEnergyGfs, stokesDissipationGfs);
#elif (COUPLING_LOWREKEPSILON)
        StokesScalarGfsP0 stokesTurbulentKineticEnergyGfs(this->sdGridViewStokes(), p0fem);
        stokesTurbulentKineticEnergyGfs.name("turbulentKineticEnergy");
        StokesScalarGfsP0 stokesDissipationGfs(this->sdGridViewStokes(), p0fem);
        stokesDissipationGfs.name("dissipation");
        StokesComposedGfs stokesComposedGfs(stokesScalarGfsStaggered, stokesPressureGfs,
                                            stokesComponentGfs, stokesTemperatureGfs,
                                            stokesTurbulentKineticEnergyGfs, stokesDissipationGfs);
#elif (COUPLING_KEPSILON)
        TurbulentKineticEnergyConstraints turbulentKineticEnergyConstraints(this->mdGridView(), this->sdProblemStokes());
        stokesTurbulentKineticEnergyGfsP0 stokesTurbulentKineticEnergyGfs(this->sdGridViewStokes(), p0fem, turbulentKineticEnergyConstraints);
        stokesTurbulentKineticEnergyGfs.name("turbulentKineticEnergy");
        DissipationConstraints dissipationConstraints(this->mdGridView(), this->sdProblemStokes());
        stokesDissipationGfsP0 stokesDissipationGfs(this->sdGridViewStokes(), p0fem, dissipationConstraints);
        stokesDissipationGfs.name("dissipation");
        StokesComposedGfs stokesComposedGfs(stokesScalarGfsStaggered, stokesPressureGfs,
                                            stokesComponentGfs, stokesTemperatureGfs,
                                            stokesTurbulentKineticEnergyGfs, stokesDissipationGfs);
#elif (COUPLING_ONEEQ)
        StokesScalarGfsP0 stokesViscosityTildeGfs(this->sdGridViewStokes(), p0fem);
        stokesViscosityTildeGfs.name("turbulentKineticEnergy");
        StokesComposedGfs stokesComposedGfs(stokesScalarGfsStaggered, stokesPressureGfs,
                                            stokesComponentGfs, stokesTemperatureGfs,
                                            stokesViscosityTildeGfs);
#else
        StokesComposedGfs stokesComposedGfs(stokesScalarGfsStaggered, stokesPressureGfs,
                                            stokesComponentGfs, stokesTemperatureGfs);
#endif
        // construct grid function space for Darcy
        DarcyScalarGfsP0 darcyPressureGfs(this->sdGridViewDarcy(), p0fem);
        darcyPressureGfs.name("pressure");
        DarcyScalarGfsP0 darcySaturationGfs(this->sdGridViewDarcy(), p0fem);
        darcySaturationGfs.name("saturation");
        DarcyScalarGfsP0 darcyTemperatureGfs(this->sdGridViewDarcy(), p0fem);
        darcyTemperatureGfs.name("temperature");
        // composed function space for Darcy
        DarcyComposedGfs darcyComposedGfs(darcyPressureGfs, darcySaturationGfs, darcyTemperatureGfs);
        // multidomain grid function space
        multidomainGfs_ =
            std::make_shared<MultiDomainGfs>(this->mdGrid(), stokesComposedGfs, darcyComposedGfs);

        // make grid function operator
        // for Stokes
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
        using NeumannTurbulentKineticEnergy = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannTurbulentKineticEnergy);
        using NeumannDissipation = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannDissipation);
        using SourceTurbulentKineticEnergyBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceTurbulentKineticEnergyBalance);
        using SourceDissipationBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceDissipationBalance);
        NeumannTurbulentKineticEnergy neumannTurbulentKineticEnergy(mdgv, this->sdProblemStokes());
        NeumannDissipation neumannDissipation(mdgv, this->sdProblemStokes());
        SourceTurbulentKineticEnergyBalance sourceTurbulentKineticEnergyBalance(mdgv, this->sdProblemStokes());
        SourceDissipationBalance sourceDissipationBalance(mdgv, this->sdProblemStokes());
        StokesLocalOperator stokesLocalOperator(
            bcStokes, sourceMomentumBalance, sourceMassBalance,
            sourceComponentBalance, sourceEnergyBalance,
            sourceTurbulentKineticEnergyBalance, sourceDissipationBalance,
            dirichletVelocity, dirichletPressure,
            dirichletMassMoleFrac, dirichletTemperature,
            dirichletTurbulentKineticEnergy, dirichletDissipation,
            neumannVelocity, neumannPressure,
            neumannMassMoleFrac, neumannTemperature,
            neumannTurbulentKineticEnergy, neumannDissipation, mdgv, this->sdProblemStokes());
#elif (COUPLING_ONEEQ)
        using NeumannViscosityTilde = typename GET_PROP_TYPE(StokesSubProblemTypeTag, NeumannViscosityTilde);
        using SourceViscosityTildeBalance = typename GET_PROP_TYPE(StokesSubProblemTypeTag, SourceViscosityTildeBalance);
        NeumannViscosityTilde neumannViscosityTilde(mdgv, this->sdProblemStokes());
        SourceViscosityTildeBalance sourceViscosityTildeBalance(mdgv, this->sdProblemStokes());
        StokesLocalOperator stokesLocalOperator(
            bcStokes, sourceMomentumBalance, sourceMassBalance,
            sourceComponentBalance, sourceEnergyBalance,
            sourceViscosityTildeBalance,
            dirichletVelocity, dirichletPressure,
            dirichletMassMoleFrac, dirichletTemperature,
            dirichletViscosityTilde,
            neumannVelocity, neumannPressure,
            neumannMassMoleFrac, neumannTemperature,
            neumannViscosityTilde, mdgv, this->sdProblemStokes());
#else
        StokesLocalOperator stokesLocalOperator(
              bcStokes, sourceMomentumBalance, sourceMassBalance,
              sourceComponentBalance, sourceEnergyBalance,
              dirichletVelocity, dirichletPressure,
              dirichletMassMoleFrac, dirichletTemperature,
              neumannVelocity, neumannPressure,
              neumannMassMoleFrac, neumannTemperature, mdgv);
#endif
        StokesTransientLocalOperator stokesTransientLocalOperator(mdgv);

        // for Darcy
        DarcyLocalOperator darcyLocalOperator(this->sdProblemDarcy().model());
        DarcyTransientLocalOperator darcyTransientLocalOperator;

        using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
        Condition conditionStokes(stokesSubDomainIdx);
        Condition conditionDarcy(darcySubDomainIdx);

        SubProblemStationaryStokes upper_sp(stokesLocalOperator, conditionStokes);
        SubProblemTransientStokes upper_sp_dt(stokesTransientLocalOperator, conditionStokes);

        SubProblemStationaryDarcy lower_sp(darcyLocalOperator, conditionDarcy);
        SubProblemTransientDarcy lower_sp_dt(darcyTransientLocalOperator, conditionDarcy);

        CouplingOperator couplingOperator(sdProblemDarcy());
        Coupling coupling(upper_sp, lower_sp, couplingOperator);

#if COUPLING_KOMEGA
        // pass additional functions to coupling local operator
        auto wallDistance = [&](unsigned int elementIdx) -> Scalar
        {
            return stokesLocalOperator.storedDistanceToWall[elementIdx];
        };
#endif
#if COUPLING_KOMEGA
        // pass additional functions to coupling local operator
        auto elementHasWall = [&](unsigned int elementIdx) -> Scalar
        {

            return stokesLocalOperator.storedElementHasWall[elementIdx];
        };
#endif

#if COUPLING_KOMEGA
        sdProblemStokes().passAdditionalVariables(wallDistance, elementHasWall);
#endif
        // pass additional functions to coupling local operator
        auto eddyKinematicViscosity = [&](unsigned int elementIdx) -> Scalar
        {
#if TURBULENT
            return stokesLocalOperator.storedKinematicEddyViscosity[elementIdx];
#else
            return 0.0;
#endif
        };
        auto eddyDiffusivity = [&](unsigned int elementIdx) -> Scalar
        {
#if TURBULENT
            return stokesLocalOperator.storedEddyDiffusivity[elementIdx];
#else
            return 0.0;
#endif
        };
        auto eddyThermalConductivity = [&](unsigned int elementIdx) -> Scalar
        {
#if TURBULENT
            return stokesLocalOperator.storedThermalEddyConductivity[elementIdx];
#else
            return 0.0;
#endif
        };
        auto velocityGradientTensor = [&](unsigned int elementIdx) -> Dune::FieldMatrix<Scalar, dim, dim>
        {
#if TURBULENT
            return stokesLocalOperator.storedVelocityGradientTensor[elementIdx];
#else
            return Dune::FieldMatrix<Scalar, dim, dim>(0.0);
#endif
        };
        auto useWallFunctionMomentum = [&](unsigned int elementIdx) -> bool
        {
#if TURBULENT
            return stokesLocalOperator.useWallFunctionMomentum(elementIdx);
#else
            return false;
#endif
        };
        couplingOperator.passAdditionalVariables(
            eddyKinematicViscosity, eddyDiffusivity, eddyThermalConductivity, velocityGradientTensor, useWallFunctionMomentum);

        auto constraints = Dune::PDELab::MultiDomain::constraints<Scalar>(*multidomainGfs_,
          Dune::PDELab::MultiDomain::constrainSubProblem(upper_sp, bcStokes),
          Dune::PDELab::MultiDomain::constrainSubProblem(lower_sp, darcyBoundaries.boundaryType));

        using ConstraintsContainer = typename MultiDomainGfs::template ConstraintsContainer<Scalar>::Type;
        ConstraintsContainer constraintsContainer;
        constraints.assemble(constraintsContainer);

        // make coefficent Vector and initialize it from a function
        SolVectorMultiDomain x(*multidomainGfs_);
        Dune::PDELab::MultiDomain::interpolateOnTrialSpace(*multidomainGfs_, x,
            dirichletComposed, upper_sp, darcyBoundaries.boundaryValue, lower_sp);

        std::cout << "interpolation: " << x.N()
          << " dof total, " << constraintsContainer.size()
          << " dof constrained" << std::endl;

        StationaryGridOperator stationaryGridOperator(
            *multidomainGfs_, *multidomainGfs_,
            constraintsContainer, constraintsContainer,
            upper_sp, lower_sp, coupling);
        TransientGridOperator transientGridOperator(
            *multidomainGfs_, *multidomainGfs_,
            constraintsContainer, constraintsContainer,
            upper_sp_dt, lower_sp_dt);
        std::shared_ptr<GridOperator> gridoperator =
            std::make_shared<GridOperator>(stationaryGridOperator, transientGridOperator);

        // lambda functions to copy PDELab into DuMuX vectors
        auto copyVectorPdelabToDumux = [&](SolVectorMultiDomain& md, SolVectorDarcy& darcy, bool reverse)
        {
            // nota bene: Solution vector is too large, it contains DoFs for all
            // MultiDomainGrid entities.
            unsigned int numDofs = md.base().size();
            unsigned int numElements = this->sdGridViewDarcy().size(0);

            if (reverse)
            {
                // only needed for catching the variable switch correctly
                for (unsigned int i = 0; i < numElements; ++i)
                {
                    (md.base())[numDofs - (numEqDarcy - darcySwitchIdxPDELab) * numElements + i]
                        = darcy[darcyElementIndices_[i]][darcySwitchIdxDumux];
                }
            }
            else
            {
                for (unsigned int i = 0; i < numElements; ++i)
                {
                    darcy[darcyElementIndices_[i]][darcyPressureIdxDumux] =
                        (md.base())[numDofs - (numEqDarcy - darcyPressureIdxPDELab) * numElements + i];
                    darcy[darcyElementIndices_[i]][darcySwitchIdxDumux] =
                        (md.base())[numDofs - (numEqDarcy - darcySwitchIdxPDELab) * numElements + i];
                    darcy[darcyElementIndices_[i]][darcyTemperatureIdxDumux] =
                        (md.base())[numDofs - (numEqDarcy - darcyTemperatureIdxPDELab) * numElements + i];
                }
            }
        };

        // update the constrainted values
        auto updateConstraints = [&](SolVectorMultiDomain& md)
        {
            // copy current solution
            SolVectorMultiDomain constrainedSolution(*multidomainGfs_);

            // update which dofs should be constrained
            constraintsContainer.clear();
            constraints = Dune::PDELab::MultiDomain::constraints<Scalar>(*multidomainGfs_,
                              Dune::PDELab::MultiDomain::constrainSubProblem(upper_sp, bcStokes),
                              Dune::PDELab::MultiDomain::constrainSubProblem(lower_sp, darcyBoundaries.boundaryType));
            constraints.assemble(constraintsContainer);

            // update all values
            Dune::PDELab::MultiDomain::interpolateOnTrialSpace(*multidomainGfs_, constrainedSolution,
                dirichletComposed, upper_sp, darcyBoundaries.boundaryValue, lower_sp);
            Dune::PDELab::copy_constrained_dofs(constraintsContainer, constrainedSolution, md);
            std::cout << "interpolation: " << md.N()
              << " dof total, " << constraintsContainer.size()
              << " dof constrained" << std::endl;
        };

        // lambda function to update the stored variables and to accept only
        // non-negative solutions for k, dissipation, nuTildefor turbulence models
        auto updateStoredValues = [&](SolVectorMultiDomain& md, bool& acceptSolution, bool doUpdate)
        {
            // NOTE: Solution vector is too large, it contains DoFs for all
            // MultiDomainGrid entities. Going reverse is easier than jumping over
            // the velocity dofs
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
            unsigned int numElementsStokes = this->sdGridViewStokes().size(0);
            unsigned int darcyOffset = md.base().size() - numEqDarcy * this->sdGridViewDarcy().size(0);

            bool applySlopeLimiter = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, KEpsilonLimiter);
            for (unsigned int i = 1; i < numElementsStokes + 1 && applySlopeLimiter; ++i)
            {
                if ((md.base())[darcyOffset - numElementsStokes * (numEqStokes - stokesTurbulentKineticEnergyIdx - 1) - i] < 0.0)
                {
                    (md.base())[darcyOffset - numElementsStokes * (numEqStokes - stokesTurbulentKineticEnergyIdx - 1) - i] = 1e-8;
                    acceptSolution = false;
                }
                if ((md.base())[darcyOffset - numElementsStokes * (numEqStokes - stokesDissipationIdx - 1) - i] < 0.0)
                {
                    (md.base())[darcyOffset - numElementsStokes * (numEqStokes - stokesDissipationIdx - 1) - i] = 1e-8;
                    acceptSolution = false;
                }
            }
#endif
#if TURBULENT
            if (doUpdate)
            {
                stokesLocalOperator.template updateStoredValues
                                    <SubDomainGridView, MultiDomainGfs, SolVectorMultiDomain, stokesSubDomainIdx>
                                    (this->sdGridViewStokes(), *multidomainGfs_, md);
            }
#endif
        };

        bool dummy = true;
        updateStoredValues(x, dummy, true);

        // lambda function to write Vtk output
        auto writeVtkOutput = [&](SolVectorMultiDomain& solution, unsigned int step, bool writeTimestep)
        {
            if (asImp_().shouldWriteOutput() || step == 0)
            {
                writeVtkOutputStokes(stokesLocalOperator, solution, step, writeTimestep);
                if (writeTimestep)
                {
                    writeVtkOutputTimestepDarcy(solution);
                }
                else
                {
                    writeVtkOutputNewtonstepDarcy(solution, step);
                }
            }
        };

        // pass gridoperator to Newton method
        this->sdProblemDarcy().newtonMethod().passAdditionalVariables(
            gridoperator, multidomainGfs_, copyVectorPdelabToDumux, updateConstraints, updateStoredValues, writeVtkOutput, x);

        // write VTK for initial condition
        writeVtkOutput(x, 0, true);

        // start a new callgrind dump to separate tabluraization of material laws
#if HAVE_VALGRIND
        CALLGRIND_DUMP_STATS;
#endif

        using ParameterTree = typename GET_PROP(TypeTag, ParameterTree);
        if ((ParameterTree::tree().hasKey("PrintProperties")
             || ParameterTree::tree().hasKey("TimeManager.PrintProperties"))
            && GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, TimeManager, PrintProperties))
        {
            Dumux::Properties::print<TypeTag>();
            std::cout << std::endl;
            Dumux::Properties::print<StokesSubProblemTypeTag>();
            std::cout << std::endl;
            Dumux::Properties::print<DarcySubProblemTypeTag>();
            std::cout << std::endl;
        }
        timeManager().run();
    }


//     /*!
//      * \brief Initialization multi-domain and the sub-domain grids
//      */
//     void initializeGrid()
//     {
//         this->mdGrid().startSubDomainMarking();
//
//         Scalar interfaceVerticalPos = asImp_().interfaceVerticalPos();
//         Scalar obstacleVerticalPos = interfaceVerticalPos;
//         Scalar obstacleXLeft = std::numeric_limits<Scalar>::lowest();
//         Scalar obstacleXRight = std::numeric_limits<Scalar>::lowest();
//         Scalar obstacleXLeftTop = std::numeric_limits<Scalar>::lowest();
//         Scalar obstacleXRightTop = std::numeric_limits<Scalar>::lowest();
//         Scalar gradientLeft = 0.0;
//         Scalar gradientRight = 0.0;
//         using ParameterTree = typename GET_PROP(TypeTag, ParameterTree);
//         if ((ParameterTree::tree().hasKey("GridObstacleXLeft")
//               || ParameterTree::tree().hasKey("Grid.ObstacleXLeft"))
//             && (ParameterTree::tree().hasKey("GridObstacleXRight")
//                 || ParameterTree::tree().hasKey("Grid.ObstacleXRight"))
//             && (ParameterTree::tree().hasKey("GridObstacleVerticalPos")
//                 || ParameterTree::tree().hasKey("Grid.ObstacleVerticalPos")))
//         {
//             obstacleVerticalPos = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleVerticalPos);
//             obstacleXLeft = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleXLeft);
//             obstacleXRight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleXRight);
//             obstacleXLeftTop = obstacleXLeft;
//             obstacleXRightTop = obstacleXRight;
//             if ((ParameterTree::tree().hasKey("GridObstacleXLeftTop")
//                   || ParameterTree::tree().hasKey("Grid.ObstacleXLeftTop"))
//                 && (ParameterTree::tree().hasKey("GridObstacleXRightTop")
//                     || ParameterTree::tree().hasKey("Grid.ObstacleXRightTop")))
//             {
//                 obstacleXLeftTop = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleXLeftTop);
//                 obstacleXRightTop = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleXRightTop);
//                 gradientLeft = (obstacleVerticalPos - interfaceVerticalPos)
//                                / (obstacleXLeftTop - obstacleXLeft);
//                 gradientRight = (obstacleVerticalPos - interfaceVerticalPos)
//                                 / (obstacleXRightTop - obstacleXRight);
//             }
//         }
//
//         for (auto eIt = this->mdGrid().template leafbegin<0>();
//               eIt != this->mdGrid().template leafend<0>(); ++eIt)
//         {
//             auto globalPos = eIt->geometry().center();
//             // only for interior entities, required for parallelization
//             if (eIt->partitionType() == Dune::InteriorEntity)
//             {
//             if (
// #if DUMUX_MULTIDOMAIN_DIM > 1
//                 (globalPos[0] < obstacleXLeft
//                         && globalPos[dim - 1] > interfaceVerticalPos)
//                     || (globalPos[0] >= obstacleXLeft && globalPos[0] < obstacleXLeftTop
//                           && globalPos[dim - 1] > interfaceVerticalPos + gradientLeft * (globalPos[0] - obstacleXLeft) - eps_)
//                     || (globalPos[0] >= obstacleXLeftTop && globalPos[0] < obstacleXRightTop
//                           && globalPos[dim - 1] > obstacleVerticalPos)
//                     || (globalPos[0] >= obstacleXRightTop && globalPos[0] < obstacleXRight
//                           && globalPos[dim - 1] > obstacleVerticalPos + gradientRight * (globalPos[0] - obstacleXRightTop) - eps_)
//                     || (globalPos[0] >= obstacleXRight &&
// #endif
//                           globalPos[dim - 1] > interfaceVerticalPos)
// #if DUMUX_MULTIDOMAIN_DIM > 1
//                     )
// #endif
//             {
//                 this->mdGrid().addToSubDomain(stokesSubDomainIdx, *eIt);
//             }
//             else
// #if DUMUX_MULTIDOMAIN_DIM > 1
//                  if (globalPos[0] > asImp_().darcyXLeftFront()[0]
//                      && globalPos[0] < asImp_().darcyXRightBack()[0]
// #if DUMUX_MULTIDOMAIN_DIM > 2
//                      && globalPos[1] > asImp_().darcyXLeftFront()[1]
//                      && globalPos[1] < asImp_().darcyXRightBack()[1]
// #endif
//                     )
// #endif
//             {
//                 this->mdGrid().addToSubDomain(darcySubDomainIdx, *eIt);
//             }
//             }
//         }
//         this->mdGrid().preUpdateSubDomains();
//         this->mdGrid().updateSubDomains();
//         this->mdGrid().postUpdateSubDomains();
//
//         darcyElementIndices_.resize(this->sdGridViewDarcy().size(0));
//         Dune::MultipleCodimMultipleGeomTypeMapper<MultiDomainGridView, Dune::MCMGElementLayout>
//             multidomainDofMapper(this->mdGridView());
//         Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
//             subdomainDofMapper(this->sdGridViewDarcy());
//         for (auto eIt = this->sdGridDarcy().template leafbegin<0>();
//               eIt != this->sdGridDarcy().template leafend<0>(); ++eIt)
//         {
//             darcyElementIndices_[subdomainDofMapper.index(*eIt)] =
//                 multidomainDofMapper.index(this->mdGrid().multiDomainEntity(*eIt));
//         }
//     }
//
//     //! \brief Returns the vertical position of the interface
//     Scalar interfaceVerticalPos()
//     {
//         return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);
//     }
//
//     //! \brief Returns the left front position of the darcy domain
//     GlobalPosition darcyXLeftFront()
//     {
//         GlobalPosition darcyXLeftFront(0.0);
//         darcyXLeftFront[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
// #if DUMUX_MULTIDOMAIN_DIM > 2
//         darcyXLeftFront[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyYFront);
// #endif
//         return darcyXLeftFront;
//     }
//
//     //! \brief Returns the right back position of the darcy domain
//     GlobalPosition darcyXRightBack()
//     {
//         GlobalPosition darcyXRightBack(0.0);
//         darcyXRightBack[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);
// #if DUMUX_MULTIDOMAIN_DIM > 2
//         darcyXRightBack[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyYBack);
// #endif
//         return darcyXRightBack;
//     }

    /*! \brief write VTK output for Stokes subdomain
     *
     * \param lop The Stokes localoperator
     * \param solution Solution vector for coupled problem
     * \param step Current number of time or Newton step
     * \param writeTimestep Whether time step is written - and not the Newton step
     */
    void writeVtkOutputStokes(StokesLocalOperator lop, SolVectorMultiDomain& solution,
                              unsigned int step, bool writeTimestep)
    {
        // grid function sub spaces
        using SubGfsStokesVelocity = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesVelocityIdx> >;
        using SubGfsStokesPressure = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesPressureIdx> >;
        using SubGfsStokesMassMoleFrac = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesMassFracIdx> >;
        using SubGfsStokesTemperature = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesTemperatureIdx> >;
        SubGfsStokesVelocity subGfsStokesVelocity(*multidomainGfs_);
        SubGfsStokesPressure subGfsStokesPressure(*multidomainGfs_);
        SubGfsStokesMassMoleFrac subGfsStokesMassMoleFrac(*multidomainGfs_);
        SubGfsStokesTemperature subGfsStokesTemperature(*multidomainGfs_);

        // discrete function objects
        using DgfStokesVelocity = Dune::PDELab::DiscreteGridFunction<SubGfsStokesVelocity, SolVectorMultiDomain>;
        using DgfStokesPressure = Dune::PDELab::DiscreteGridFunction<SubGfsStokesPressure, SolVectorMultiDomain>;
        using DgfStokesMassMoleFrac = Dune::PDELab::DiscreteGridFunction<SubGfsStokesMassMoleFrac, SolVectorMultiDomain>;
        using DgfStokesTemperature = Dune::PDELab::DiscreteGridFunction<SubGfsStokesTemperature, SolVectorMultiDomain>;
        DgfStokesVelocity dgfStokesVelocity(subGfsStokesVelocity, solution);
        DgfStokesPressure dgfStokesPressure(subGfsStokesPressure, solution);
        DgfStokesMassMoleFrac dgfStokesMassMoleFrac(subGfsStokesMassMoleFrac, solution);
        DgfStokesTemperature dgfStokesTemperature(subGfsStokesTemperature, solution);

        // add to vtk writer
        Dune::SubsamplingVTKWriter<SubDomainGridView> vtkWriterStokes(
            this->sdGridViewStokes(), 1);
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesVelocity>>(dgfStokesVelocity, "velocityN"));
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesPressure>>(dgfStokesPressure, "pn"));
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesMassMoleFrac>>(dgfStokesMassMoleFrac, "X_gas^H2O"));
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesTemperature>>(dgfStokesTemperature, "temperature"));
#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
        using SubGfsStokesTurbulentKineticEnergy = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesTurbulentKineticEnergyIdx> >;
        using SubGfsStokesDissipation = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesDissipationIdx> >;
        SubGfsStokesTurbulentKineticEnergy subGfsStokesTurbulentKineticEnergy(*multidomainGfs_);
        SubGfsStokesDissipation subGfsStokesDissipation(*multidomainGfs_);
        using DgfStokesTurbulentKineticEnergy = Dune::PDELab::DiscreteGridFunction<SubGfsStokesTurbulentKineticEnergy, SolVectorMultiDomain>;
        using DgfStokesDissipation = Dune::PDELab::DiscreteGridFunction<SubGfsStokesDissipation, SolVectorMultiDomain>;
        DgfStokesTurbulentKineticEnergy dgfStokesTurbulentKineticEnergy(subGfsStokesTurbulentKineticEnergy, solution);
        DgfStokesDissipation dgfStokesDissipation(subGfsStokesDissipation, solution);
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesTurbulentKineticEnergy>>(dgfStokesTurbulentKineticEnergy, "turbulentKineticEnergy"));
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesDissipation>>(dgfStokesDissipation, "dissipation"));
#elif (COUPLING_ONEEQ)
        using SubGfsStokesViscosityTilde = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<stokesSubDomainIdx, stokesViscosityTildeIdx> >;
        SubGfsStokesViscosityTilde subGfsStokesViscosityTilde(*multidomainGfs_);
        using DgfStokesViscosityTilde = Dune::PDELab::DiscreteGridFunction<SubGfsStokesViscosityTilde, SolVectorMultiDomain>;
        DgfStokesViscosityTilde dgfStokesViscosityTilde(subGfsStokesViscosityTilde, solution);
        vtkWriterStokes.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfStokesViscosityTilde>>(dgfStokesViscosityTilde, "viscosityTilde"));
#endif
        char fname[255];
        std::string filenameFormat = "";
        if (writeTimestep)
        {
            timeVector_.push_back(timeManager().time()+timeManager().timeStepSize());
            timeVector_[0] = 0.0;
            filenameFormat = "%s-ff-%05d";
            sprintf(fname, filenameFormat.c_str(), asImp_().name().c_str(), timeVector_.size()-1);
            std::cout << "Writing result file \"" << asImp_().name() + std::string("-ff") << "\"\n";
            vtkWriterStokes.write(fname, Dune::VTK::ascii);
        }
        else
        {
            filenameFormat = "%s-ff-newton-%05d-%05d";
            sprintf(fname, filenameFormat.c_str(), asImp_().name().c_str(), timeManager().timeStepIndex(), step);
            vtkWriterStokes.write(fname, Dune::VTK::ascii);
            return;
        }

        // write stokes pvd file
        sprintf(fname, "%s-ff.pvd", asImp_().name().c_str());
        std::string pvdname = fname;
        std::ofstream pvd(pvdname.c_str());
        assert(pvd.is_open());
        pvd << std::setprecision(16);
        pvd << "<?xml version=\"1.0\"?>\n"
            << "<VTKFile type=\"Collection\"\n"
            << "         version=\"0.1\"\n"
            << "         byte_order=\"LittleEndian\"\n"
            << "         compressor=\"vtkZLibDataCompressor\">\n"
            << "<Collection>\n";
        for (unsigned int i = 0; i < timeVector_.size(); ++i)
        {
            sprintf(fname, filenameFormat.c_str(), asImp_().name().c_str(), i);
            pvd << "  <DataSet"
                << " index=\"" << i << "\""
                << " timestep=\"" << timeVector_[i] << "\""
                << " file=\"" << fname;
            if (dim > 1)
                pvd << ".vtu\"/>\n";
            else
                pvd << ".vtp\"/>\n";
        }
        pvd << "</Collection>\n"
            << "</VTKFile>\n";
        pvd.close();

#if TURBULENT
        // Write output for secondary variables of the Stokes domain
        std::cout << "Writing result file \"" << asImp_().name() + std::string("-ffSecondary") << "\"\n";
        using IntField = Dune::BlockVector<Dune::FieldVector<int, 1> >;
        using ScalarField = Dune::BlockVector<Dune::FieldVector<Scalar, 1> >;
        using VectorField = Dune::BlockVector<Dune::FieldVector<Scalar, dimWorld> >;
        unsigned numDofs = this->sdGridViewStokes().size(0);

        if (timeVector_.size() == 1)
        {
            writerStokes->beginWrite(timeManager().time());
        }
        else
        {
            writerStokes->beginWrite(timeManager().time()+timeManager().timeStepSize());
        }

        // create the required scalar fields
        ScalarField *isInStokesDomain = writerStokes->allocateManagedBuffer(numDofs);
        VectorField *velocity = writerStokes->template allocateManagedBuffer<Scalar, dimWorld>(numDofs);
        VectorField *velocityGradients0 = writerStokes->template allocateManagedBuffer<Scalar, dimWorld>(numDofs);
        VectorField *velocityGradients1 = writerStokes->template allocateManagedBuffer<Scalar, dimWorld>(numDofs);
        VectorField *velocityGradients2 = writerStokes->template allocateManagedBuffer<Scalar, dimWorld>(numDofs);
        ScalarField *pressure = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *relPressure = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *massMoleFrac = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *temperature = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *density = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *viscosity = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *diffusivity = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *conductivity = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *eddyViscosity = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *eddyDiffusivity = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *eddyConductivity = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *distanceToWall = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *uPlus = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *yPlus = writerStokes->allocateManagedBuffer(numDofs);
        IntField *hasWall = writerStokes->template allocateManagedBuffer<int, 1>(numDofs);
#if COUPLING_KOMEGA
        ScalarField *k = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *omega = writerStokes->allocateManagedBuffer(numDofs);
#elif COUPLING_KEPSILON
        IntField *inWallFunctionRegion = writerStokes->template allocateManagedBuffer<int, 1>(numDofs);
        IntField *useWallFunctionV = writerStokes->template allocateManagedBuffer<int, 1>(numDofs);
        IntField *useWallFunctionK = writerStokes->template allocateManagedBuffer<int, 1>(numDofs);
        IntField *useWallFunctionE = writerStokes->template allocateManagedBuffer<int, 1>(numDofs);
        IntField *isMatchingPoint = writerStokes->template allocateManagedBuffer<int, 1>(numDofs);
        ScalarField *k = writerStokes->allocateManagedBuffer(numDofs);
        ScalarField *epsilon = writerStokes->allocateManagedBuffer(numDofs);
#endif

        // use the
        Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
            sdDofMapper(this->sdGridViewStokes());
        Dune::MultipleCodimMultipleGeomTypeMapper<MultiDomainGridView, Dune::MCMGElementLayout>
            mdDofMapper(this->mdGridView());
        for (const auto& element : Dune::elements(this->sdGridViewStokes()))
        {
            int sdDofIdx = sdDofMapper.index(element);
            int mdElementIdx = mdDofMapper.index(mdGrid().multiDomainEntityPointer(element));

            (*isInStokesDomain)[sdDofIdx] = (Scalar) lop.storedValueIsInStokesDomain[mdElementIdx];

            (*velocity)[sdDofIdx] = lop.storedVelocitiesAtElementCenter[mdElementIdx];
            (*velocityGradients0)[sdDofIdx] = lop.storedVelocityGradientTensor[mdElementIdx][0];
            if (dim > 1)
                (*velocityGradients1)[sdDofIdx] = lop.storedVelocityGradientTensor[mdElementIdx][1];
            if (dim > 2)
                (*velocityGradients2)[sdDofIdx] = lop.storedVelocityGradientTensor[mdElementIdx][2];
            (*pressure)[sdDofIdx] = lop.storedPressure[mdElementIdx];
            (*relPressure)[sdDofIdx] = lop.storedPressure[mdElementIdx] - 1e5; //referencepressure
            (*massMoleFrac)[sdDofIdx] = lop.storedMassMoleFrac[mdElementIdx];
            (*temperature)[sdDofIdx] = lop.storedTemperature[mdElementIdx];
            (*density)[sdDofIdx] = lop.storedDensity[mdElementIdx];
            (*viscosity)[sdDofIdx] = lop.storedKinematicViscosity[mdElementIdx];
            (*diffusivity)[sdDofIdx] = lop.storedDiffusionCoefficient[mdElementIdx];
            (*conductivity)[sdDofIdx] = lop.storedThermalConductivity[mdElementIdx];
            (*eddyViscosity)[sdDofIdx] = lop.storedKinematicEddyViscosity[mdElementIdx];
            (*eddyDiffusivity)[sdDofIdx] = lop.storedEddyDiffusivity[mdElementIdx];
            (*eddyConductivity)[sdDofIdx] = lop.storedThermalEddyConductivity[mdElementIdx];
            (*distanceToWall)[sdDofIdx] = lop.storedDistanceToWall[mdElementIdx];
            (*uPlus)[sdDofIdx] = lop.storedVelocityInWallCoordinates[mdElementIdx];
            (*yPlus)[sdDofIdx] = lop.storedDistanceInWallCoordinates[mdElementIdx];
            (*hasWall)[sdDofIdx] = lop.storedElementHasWall[mdElementIdx];
#if COUPLING_KOMEGA
            (*k)[sdDofIdx] = lop.storedTurbulentKineticEnergy[mdElementIdx];
            (*omega)[sdDofIdx] = lop.storedDissipation[mdElementIdx];
#elif COUPLING_KEPSILON
            auto mdElement = mdGrid().multiDomainEntityPointer(element);
            (*isMatchingPoint)[sdDofIdx] = lop.storedElementIsMatchingPoint[mdElementIdx];
            (*inWallFunctionRegion)[sdDofIdx] = asImp_().sdProblemStokes().kEpsilonWallFunctions().inWallFunctionRegion(mdElement);
            (*useWallFunctionV)[sdDofIdx] = lop.useWallFunctionMomentum(mdElementIdx);
            (*useWallFunctionK)[sdDofIdx] = asImp_().sdProblemStokes().kEpsilonWallFunctions().useWallFunctionTurbulentKineticEnergy(mdElement);
            (*useWallFunctionE)[sdDofIdx] = asImp_().sdProblemStokes().kEpsilonWallFunctions().useWallFunctionDissipation(mdElement);
            (*k)[sdDofIdx] = lop.storedTurbulentKineticEnergy[mdElementIdx];
            (*epsilon)[sdDofIdx] = lop.storedDissipation[mdElementIdx];
#endif

        } // loop over elements

        writerStokes->attachDofData(*isInStokesDomain, "isInStokesDomain", false);
        writerStokes->attachDofData(*velocity, "velocityAtElementCenters", false, dim);
        writerStokes->attachDofData(*velocityGradients0, "velocityGradients0", false, dim);
        if (dim > 1)
            writerStokes->attachDofData(*velocityGradients1, "velocityGradients1", false, dim);
        if (dim > 2)
            writerStokes->attachDofData(*velocityGradients2, "velocityGradients2", false, dim);
        writerStokes->attachDofData(*pressure, "pressure", false);
        writerStokes->attachDofData(*relPressure, "pn_rel", false);
        writerStokes->attachDofData(*massMoleFrac, "massMoleFrac", false);
        writerStokes->attachDofData(*temperature, "temperature", false);
        writerStokes->attachDofData(*density, "density", false);
        writerStokes->attachDofData(*viscosity, "viscosity", false);
        writerStokes->attachDofData(*diffusivity, "diffusivity", false);
        writerStokes->attachDofData(*conductivity, "conductivity", false);
        writerStokes->attachDofData(*eddyViscosity, "eddyViscosity", false);
        writerStokes->attachDofData(*eddyDiffusivity, "eddyDiffusivity", false);
        writerStokes->attachDofData(*eddyConductivity, "eddyConductivity", false);
        writerStokes->attachDofData(*distanceToWall, "distanceToWall", false);
        writerStokes->attachDofData(*uPlus, "u^+", false);
        writerStokes->attachDofData(*yPlus, "y^+", false);
        writerStokes->attachDofData(*hasWall, "hasWall", false);
#if COUPLING_KOMEGA
        writerStokes->attachDofData(*k, "k", false);
        writerStokes->attachDofData(*omega, "omega", false);
#elif COUPLING_KEPSILON
        writerStokes->attachDofData(*isMatchingPoint, "isMatchingPoint", false);
        writerStokes->attachDofData(*inWallFunctionRegion, "inWallFunctionRegion", false);
        writerStokes->attachDofData(*useWallFunctionV, "useWallFunctionV", false);
        writerStokes->attachDofData(*useWallFunctionK, "useWallFunctionK", false);
        writerStokes->attachDofData(*useWallFunctionE, "useWallFunctionE", false);
        writerStokes->attachDofData(*k, "k", false);
        writerStokes->attachDofData(*epsilon, "epsilon", false);
#endif

        writerStokes->endWrite();
#endif
    }

    /*! \brief write VTK output for Darcy subdomain, used for Newton steps convergence writer
     *
     * \param solution Solution vector for coupled problem
     * \param newtonStep Current number of Newton step
     */
    void writeVtkOutputNewtonstepDarcy(SolVectorMultiDomain& solution, unsigned int newtonStep)
    {
        // grid function sub spaces
        using SubGfsDarcyPressure = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<darcySubDomainIdx, darcyPressureIdxPDELab> >;
        using SubGfsDarcyMassMoleFrac = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<darcySubDomainIdx, darcySwitchIdxPDELab> >;
        using SubGfsDarcyTemperature = Dune::PDELab::GridFunctionSubSpace
            <MultiDomainGfs, Dune::TypeTree::TreePath<darcySubDomainIdx, darcyTemperatureIdxPDELab> >;
        SubGfsDarcyPressure subGfsDarcyPressure(*multidomainGfs_);
        SubGfsDarcyMassMoleFrac subGfsDarcyMassMoleFrac(*multidomainGfs_);
        SubGfsDarcyTemperature subGfsDarcyTemperature(*multidomainGfs_);

        // discrete function objects
        using DgfDarcyPressure = Dune::PDELab::DiscreteGridFunction<SubGfsDarcyPressure, SolVectorMultiDomain>;
        using DgfDarcyMassMoleFrac = Dune::PDELab::DiscreteGridFunction<SubGfsDarcyMassMoleFrac, SolVectorMultiDomain>;
        using DgfDarcyTemperature = Dune::PDELab::DiscreteGridFunction<SubGfsDarcyTemperature, SolVectorMultiDomain>;
        DgfDarcyPressure dgfDarcyPressure(subGfsDarcyPressure, solution);
        DgfDarcyMassMoleFrac dgfDarcyMassMoleFrac(subGfsDarcyMassMoleFrac, solution);
        DgfDarcyTemperature dgfDarcyTemperature(subGfsDarcyTemperature, solution);

        // add to vtk writer
        Dune::VTKWriter<SubDomainGridView> vtkWriterDarcy(
            this->sdGridViewDarcy(), Dune::VTK::nonconforming);
        vtkWriterDarcy.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfDarcyPressure>>(dgfDarcyPressure, "pn"));
        vtkWriterDarcy.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfDarcyMassMoleFrac>>(dgfDarcyMassMoleFrac, "switch"));
        vtkWriterDarcy.addCellData(
            std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<DgfDarcyTemperature>>(dgfDarcyTemperature, "temperature"));
        std::string filenameFormat = "%s-pm-newton-%05d-%05d";
        char fname[255];
        sprintf(fname, filenameFormat.c_str(), asImp_().name().c_str(), timeManager().timeStepIndex(), newtonStep);
        vtkWriterDarcy.write(fname, Dune::VTK::ascii);
    }

    /*! \brief write VTK output for Darcy subdomain, used for time steps
     *
     * \param solution Solution vector for coupled problem
     */
    void writeVtkOutputTimestepDarcy(SolVectorMultiDomain& solution)
    {
        // add to vtk writer
        std::cout << "Writing result file \"" << asImp_().name() + std::string("-pm") << "\"\n";
        using FluidSystem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FluidSystem);
        const unsigned int numComponents = GET_PROP_VALUE(DarcySubProblemTypeTag, NumComponents);
        const unsigned int numPhases = GET_PROP_VALUE(DarcySubProblemTypeTag, NumPhases);
        using ElementVolumeVariables = typename GET_PROP_TYPE(DarcySubProblemTypeTag, ElementVolumeVariables);
        using FVElementGeometry = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FVElementGeometry);
        const unsigned int nPhaseIdx = DarcyIndices::nPhaseIdx;
        const unsigned int wPhaseIdx = DarcyIndices::wPhaseIdx;
        using ScalarField = Dune::BlockVector<Dune::FieldVector<Scalar, 1> >;
        using VectorField = Dune::BlockVector<Dune::FieldVector<Scalar, dimWorld> >;

        // get the number of degrees of freedom
        unsigned numDofs = this->sdGridViewDarcy().size(0);

        if (timeVector_.size() == 1)
        {
            writerDarcy->beginWrite(0);
        }
        else
        {
            writerDarcy->beginWrite(timeManager().time()+timeManager().timeStepSize());
        }
        // create the required scalar fields
        ScalarField *sN    = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *sW    = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *pn    = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *pnRel = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *pw    = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *pc    = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *rhoW  = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *rhoN  = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *mobW  = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *mobN  = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *phasePresence = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *massFrac[numPhases][numComponents];
        for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
                massFrac[phaseIdx][compIdx] = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *moleFrac[numPhases][numComponents];
        for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
              for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
                  moleFrac[phaseIdx][compIdx] = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *temperature = writerDarcy->allocateManagedBuffer(numDofs);
        ScalarField *poro = writerDarcy->allocateManagedBuffer(numDofs);
        VectorField *velocityN = writerDarcy->template allocateManagedBuffer<Scalar, dimWorld>(numDofs);
        VectorField *velocityW = writerDarcy->template allocateManagedBuffer<Scalar, dimWorld>(numDofs);
        ImplicitVelocityOutput<DarcySubProblemTypeTag> velocityOutput(this->sdProblemDarcy());

        if (velocityOutput.enableOutput())
        {
            // initialize velocity fields
            for (unsigned int i = 0; i < numDofs; ++i)
            {
                (*velocityN)[i] = 0;
                (*velocityW)[i] = 0;
            }
        }

        Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
            dofMapper(this->sdGridViewDarcy());
        for (const auto& element : Dune::elements(this->sdGridViewDarcy()))
        {
            if (element.partitionType() == Dune::InteriorEntity)
            {
                FVElementGeometry fvGeometry;
                fvGeometry.update(this->mdGridView(), this->mdGrid().multiDomainEntity(element));

                ElementVolumeVariables elemVolVars;
                elemVolVars.update(this->sdProblemDarcy(),
                                    this->mdGrid().multiDomainEntity(element),
                                    fvGeometry,
                                    false /* oldSol? */);

                int dofIdxGlobal = dofMapper.index(element);

                (*sN)[dofIdxGlobal]    = elemVolVars[0].saturation(nPhaseIdx);
                (*sW)[dofIdxGlobal]    = elemVolVars[0].saturation(wPhaseIdx);
                (*pn)[dofIdxGlobal]    = elemVolVars[0].pressure(nPhaseIdx);
                (*pnRel)[dofIdxGlobal] = elemVolVars[0].pressure(nPhaseIdx) - 1e5; //or referencepressure
                (*pw)[dofIdxGlobal]    = elemVolVars[0].pressure(wPhaseIdx);
                (*pc)[dofIdxGlobal]    = elemVolVars[0].capillaryPressure();
                (*rhoW)[dofIdxGlobal]  = elemVolVars[0].density(wPhaseIdx);
                (*rhoN)[dofIdxGlobal]  = elemVolVars[0].density(nPhaseIdx);
                (*mobW)[dofIdxGlobal]  = elemVolVars[0].mobility(wPhaseIdx);
                (*mobN)[dofIdxGlobal]  = elemVolVars[0].mobility(nPhaseIdx);
                for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                    for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
                    {
                        (*massFrac[phaseIdx][compIdx])[dofIdxGlobal]
                            = elemVolVars[0].massFraction(phaseIdx, compIdx);

                        Valgrind::CheckDefined((*massFrac[phaseIdx][compIdx])[dofIdxGlobal][0]);
                    }
                for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                    for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
                    {
                        (*moleFrac[phaseIdx][compIdx])[dofIdxGlobal]
                            = elemVolVars[0].moleFraction(phaseIdx, compIdx);

                        Valgrind::CheckDefined((*moleFrac[phaseIdx][compIdx])[dofIdxGlobal][0]);
                    }
                (*poro)[dofIdxGlobal]  = elemVolVars[0].porosity();
                (*temperature)[dofIdxGlobal] = elemVolVars[0].temperature();
                (*phasePresence)[dofIdxGlobal] = elemVolVars[0].phasePresence();

                // velocity output
                velocityOutput.calculateVelocity(*velocityW, elemVolVars, fvGeometry,
                    this->mdGrid().multiDomainEntity(element), wPhaseIdx);
                velocityOutput.calculateVelocity(*velocityN, elemVolVars, fvGeometry,
                    this->mdGrid().multiDomainEntity(element), nPhaseIdx);
            }
        } // loop over elements

        writerDarcy->attachDofData(*sN,     "Sn", false);
        writerDarcy->attachDofData(*sW,     "Sw", false);
        writerDarcy->attachDofData(*pn,     "pn", false);
        writerDarcy->attachDofData(*pnRel,  "pn_rel", false);
        writerDarcy->attachDofData(*pw,     "pw", false);
        writerDarcy->attachDofData(*pc,     "pc", false);
        writerDarcy->attachDofData(*rhoW,   "rhoW", false);
        writerDarcy->attachDofData(*rhoN,   "rhoN", false);
        writerDarcy->attachDofData(*mobW,   "mobW", false);
        writerDarcy->attachDofData(*mobN,   "mobN", false);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                std::ostringstream oss;
                oss << "X_" << FluidSystem::phaseName(phaseIdx) << "^" << FluidSystem::componentName(compIdx);
                writerDarcy->attachDofData(*massFrac[phaseIdx][compIdx], oss.str(), false);
            }
        }
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                std::ostringstream oss;
                oss << "x_" << FluidSystem::phaseName(phaseIdx) << "^" << FluidSystem::componentName(compIdx);
                writerDarcy->attachDofData(*moleFrac[phaseIdx][compIdx], oss.str(), false);
            }
        }
        writerDarcy->attachDofData(*poro, "porosity", false);
        writerDarcy->attachDofData(*temperature, "temperature", false);
        writerDarcy->attachDofData(*phasePresence,  "phase presence", false);

        if (velocityOutput.enableOutput())
        {
            writerDarcy->attachDofData(*velocityW, "velocityW", false, dim);
            writerDarcy->attachDofData(*velocityN, "velocityN", false, dim);
        }
        writerDarcy->endWrite();
    }

    /*!
     * \name Simulation control
     */
    // \{

    /*!
     * \brief Called by the time manager before the time integration. Calls preTimeStep()
     *        of the subproblems.
     */
    void preTimeStep()
    {
        asImp_().sdProblemStokes().preTimeStep();
        asImp_().sdProblemDarcy().preTimeStep();
    }

    //! \copydoc Dumux::ImplicitProblem::timeIntegration()
    void timeIntegration()
    {
        const int maxFails = GET_PARAM_FROM_GROUP(TypeTag, int, Newton, MaxTimeStepDivisions);
        for (int i = 0; i < maxFails; ++i)
        {
            if (model().update(sdProblemDarcy().newtonMethod(), sdProblemDarcy().newtonController()))
                return;

            // update failed
            Scalar dt = timeManager().timeStepSize();
            Scalar nextDt = dt * GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, TimeStepReduction);
            timeManager().setTimeStepSize(nextDt);

            if (nextDt < GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, AbortTimeStepSize))
            {
                DUNE_THROW(Dune::MathError,
                           "Newton solver did not converge with a reasonable time step size dt="
                           << dt << ".");
            }
            std::cout << "Newton solver did not converge. Retrying with time step of "
                      << timeManager().timeStepSize() << "sec\n";
        }

        DUNE_THROW(Dune::MathError,
                   "Newton solver didn't converge after "
                   << maxFails
                   << " timestep divisions. dt="
                   << timeManager().timeStepSize());
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution. Calls postTimeStep()
     *        of the subproblems.
     */
    void postTimeStep()
    {
        // the stokes postTimeStep() function is not needed
        asImp_().sdProblemDarcy().postTimeStep();
    }

    //! \copydoc Dumux::ImplicitProblem::maxTimeStepSize()
    Scalar maxTimeStepSize() const
    {
        return maxTimeStepSize_;
    }

    //! \copydoc Dumux::ImplicitProblem::nextTimeStepSize()
    Scalar nextTimeStepSize(const Scalar dt)
    {
        return newtonController().suggestTimeStepSize(dt);
    }

    /*!
     * \brief This method is called by the model if the update to the
     *        next time step failed completely.
     */
    void updateSuccessful()
    {
        model().updateSuccessful();
    }
//     //! \copydoc Dumux::ImplicitProblem::shouldWriteOutput()
//     bool shouldWriteOutput() const
//     {
//         return (    (this->timeManager().episodeWillBeFinished())
//                  || (this->timeManager().willBeFinished())
//                  || (this->timeManager().time() < (dtInitial_*1.1))
//                  || (episodeLength_ < 0)
//                );
//     }

    //! \copydoc Dumux::ImplicitProblem::shouldWriteOutput()
    bool shouldWriteOutput() const
    {
        return (   ((this->timeManager().timeStepIndex() + 1) % (outputFreq_) == 0)
                || (this->timeManager().willBeFinished())
               );
    }

    //! \copydoc Dumux::ImplicitProblem::shouldWriteRestartFile()
    bool shouldWriteRestartFile() const
    { return false; }

    //! \copydoc Dumux::ImplicitProblem::episodeEnd()
    void episodeEnd()
    {
        std::cerr << "The end of an episode is reached, but the problem "
                  << "does not override the episodeEnd() method. "
                  << "Doing nothing!\n";
    }

    //! \copydoc Dumux::ImplicitProblem::advanceTimeLevel()
    void advanceTimeLevel()
    {
        asImp_().sdProblemStokes().advanceTimeLevel();
        asImp_().sdProblemDarcy().advanceTimeLevel();

        model().advanceTimeLevel();
    }

    //! \copydoc Dumux::ImplicitProblem::writeOutput()
    void writeOutput()
    { }

    void restart(Scalar tRestart)
    {}

    // \}

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name); }

    //! \copydoc Dumux::ImplicitProblem::setName()
    static void setName(const char *newName)
    { simname_ = newName; }

    //! \copydoc Dumux::ImplicitProblem::timeManager()
    TimeManager &timeManager()
    { return timeManager_; }

    //! \copydoc Dumux::ImplicitProblem::timeManager()
    const TimeManager &timeManager() const
    { return timeManager_; }

    //! \copydoc Dumux::ImplicitProblem::newtonController()
    NewtonController &newtonController()
    { return sdProblemDarcy().newtonController(); }

    //! \copydoc Dumux::ImplicitProblem::newtonController()
    const NewtonController &newtonController() const
    { return sdProblemDarcy().newtonController(); }

    //! \copydoc Dumux::ImplicitProblem::model()
    Model &model()
    { return sdProblemDarcy().model(); }

    //! \copydoc Dumux::ImplicitProblem::model()
    const Model &model() const
    { return sdProblemDarcy().model(); }
    // \}

    //! \copydoc Dumux::ImplicitProblem::serialize()
    void serialize()
    {
      // not yet supported
    }

    /*!
     * \brief Returns a reference to Stokes subproblem
     */
    StokesSubDomainProblem& sdProblemStokes()
    {
        return *sdProblemStokes_;
    }

    /*!
     * \brief Returns a const reference to Stokes subproblem
     */
    const StokesSubDomainProblem& sdProblemStokes() const
    {
        return *sdProblemStokes_;
    }

    /*!
     * \brief Returns a reference to Darcy subproblem
     */
    DarcySubDomainProblem& sdProblemDarcy()
    {
        return *sdProblemDarcy_;
    }

    /*!
     * \brief Returns a const reference to Darcy subproblem
     */
    const DarcySubDomainProblem& sdProblemDarcy() const
    {
        return *sdProblemDarcy_;
    }

    // LocalResidualStokes& localResidualStokes()
    // does not exist on purpose as it is not provided by PDELab

    /*!
     * \brief Returns a reference to the Darcy localresidual
     */
    LocalResidualDarcy& localResidualDarcy()
    {
        return sdProblemDarcy().model().localResidual();
    }

    /*!
     * \brief Returns a reference to the multidomain grid
     */
    MultiDomainGrid& mdGrid()
    {
        return *mdGrid_;
    }

    /*!
     * \brief Returns a const reference to the multidomain grid
     */
    const MultiDomainGrid& mdGrid() const
    {
        return *mdGrid_;
    }

    /*!
     * \brief Returns the multidomain gridview
     */
    const MultiDomainGridView mdGridView() const
    {
        return mdGrid().leafGridView();
    }

    /*!
     * \brief Returns a const reference to the grid of the Stokes subdomain
     */
    const SubDomainGrid& sdGridStokes() const
    {
        return mdGrid()->subDomain(stokesSubDomainIdx);
    }

    /*!
     * \brief Returns the gridview of Stokes subdomain
     */
    const SubDomainGridView sdGridViewStokes() const
    {
        return mdGrid().subDomain(stokesSubDomainIdx).leafGridView();
    }

    /*!
     * \brief Returns a const reference to grid of the Darcy subdomain
     */
    const SubDomainGrid& sdGridDarcy() const
    {
        return mdGrid().subDomain(darcySubDomainIdx);
    }

    /*!
     * \brief Returns the gridview of Darcy subdomain
     */
    const SubDomainGridView sdGridViewDarcy() const
    {
        return mdGrid().subDomain(darcySubDomainIdx).leafGridView();
    }

protected:
    Implementation &asImp_()
    {
        return *static_cast<Implementation *>(this);
    }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    {
        return *static_cast<const Implementation *>(this);
    }

    mutable std::vector<unsigned int> darcyElementIndices_;

private:
    // a string for the name of the current simulation, which could be
    // set by means of an program argument, for example.
    static std::string simname_;
    TimeManager &timeManager_;
    Scalar maxTimeStepSize_;
    int outputFreq_;

    std::vector<Scalar> timeVector_;
    Scalar eps_;

    std::shared_ptr<MultiDomainGrid> mdGrid_;
    std::shared_ptr<MultiDomainGridView> mdGridView_;

    std::shared_ptr<StokesSubDomainProblem> sdProblemStokes_;
    std::shared_ptr<DarcySubDomainProblem> sdProblemDarcy_;
    std::shared_ptr<MultiDomainGfs> multidomainGfs_;

    std::shared_ptr<Dumux::VtkMultiWriter<SubDomainGridView>> writerDarcy;
    std::shared_ptr<Dumux::VtkMultiWriter<SubDomainGridView>> writerStokes;
};

} // namespace Dumux

#endif // DUMUX_MULTIDOMAIN_PROBLEM_HH
