// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_NAVIERSTOKES_DARCYTWOP_PROPERTIES_HH
#define DUMUX_MULTIDOMAIN_NAVIERSTOKES_DARCYTWOP_PROPERTIES_HH

#include <dumux/porousmediumflow/2p2c/implicit/propertydefaults.hh>

#if COUPLING_KOMEGA                             //use K Omega Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegapropertydefaults.hh>
#elif COUPLING_KEPSILON                         //use K Epsilon Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonpropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON                    //use K LowReEpsilon Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonpropertydefaults.hh>
#elif COUPLING_ONEEQ                            //use one-EQ Model for free flow
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmaraspropertydefaults.hh>
#elif COUPLING_ZEROEQ                           //use K Zero-EQ Model for free flow
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqpropertydefaults.hh>
#else                                           //Otherwise, only NS without turbulence
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokespropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/common/multidomainpropertydefaults.hh>
#include <appl/staggeredgrid/multidomain/common/subdomainpropertydefaults.hh>

namespace Dumux
{
namespace Properties
{
NEW_TYPE_TAG(MDSNavierStokesDarcyTwoP, INHERITS_FROM(MultiDomainStaggered));

// The type tag for the Stokes problem
#ifdef COUPLING_KOMEGA
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridKOmega, SubDomainStaggered));
#elif COUPLING_KEPSILON
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridKEpsilon, SubDomainStaggered));
#elif COUPLING_LOWREKEPSILON
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridLowReKEpsilon, SubDomainStaggered));
#elif COUPLING_ONEEQ
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridSpalartAllmaras, SubDomainStaggered));
#elif COUPLING_ZEROEQ
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridZeroEq, SubDomainStaggered));
#else
NEW_TYPE_TAG(StokesSubProblem, INHERITS_FROM(StaggeredGridNavierStokes, SubDomainStaggered));
#endif

// The type tags for the Darcy problem
NEW_TYPE_TAG(DarcySpatialParams);
NEW_TYPE_TAG(DarcySubProblem, INHERITS_FROM(CCTwoP, DarcySpatialParams, SubDomainStaggered));

// single grid function spaces
NEW_PROP_TAG(PressureConstraints);
NEW_PROP_TAG(VelocityConstraints);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0Constrained);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceStaggered);
#if (COUPLING_KOMEGA)
NEW_PROP_TAG(DissipationConstraints);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#elif (COUPLING_KEPSILON)
NEW_PROP_TAG(TurbulentKineticEnergyConstraints);
NEW_PROP_TAG(DissipationConstraints);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained);
NEW_PROP_TAG(SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#endif


// composed grid function spaces
NEW_PROP_TAG(SubDomainComposedGridFunctionSpace);

// multidomain grid function space
NEW_PROP_TAG(MultiDomainGridFunctionSpace);

// local operator for stationary part (on sub domain)
NEW_PROP_TAG(LocalOperator);

// local operator for transient part (on sub domain)
NEW_PROP_TAG(TransientLocalOperator);

// stationary PDELab sub problem
NEW_PROP_TAG(SubProblemStationaryStokes);
NEW_PROP_TAG(SubProblemStationaryDarcy);

// transient PDELab sub problem
NEW_PROP_TAG(SubProblemTransientStokes);
NEW_PROP_TAG(SubProblemTransientDarcy);

// coupling operator
NEW_PROP_TAG(CouplingOperator);
NEW_PROP_TAG(Coupling);

// stationary multi domain grid operator
NEW_PROP_TAG(MultiDomainStationaryGridOperator);

// transient multi domain grid operator
NEW_PROP_TAG(MultiDomainTransientGridOperator);

// full multidomain grid operator
NEW_PROP_TAG(MultiDomainGridOperator);

// Define how the Beavers-Joseph Condition is applied
NEW_PROP_TAG(CouplingBeaversJosephAsSolDependentDirichlet);

// Minimum accepted time step size to proceed simulation
NEW_PROP_TAG(NewtonAbortTimeStepSize);

// Factor to adapt the time step, if Newton failed
NEW_PROP_TAG(NewtonTimeStepReduction);

// Limit range of k and epsilon to positive values
NEW_PROP_TAG(NewtonKEpsilonLimiter);
} // end namespace Properties
} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_NAVIERSTOKES_DARCYTWOP_PROPERTIES_HH
