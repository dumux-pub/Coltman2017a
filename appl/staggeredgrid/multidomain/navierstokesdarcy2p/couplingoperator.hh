// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_DARCY2P_HH
#define DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_DARCY2P_HH

#include <dune/istl/io.hh>

#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>

#include <appl/staggeredgrid/common/onecomponentfluid.hh>

#include "properties.hh"

namespace Dumux
{
namespace Properties
{
NEW_PROP_TAG(StokesSubProblemTypeTag);
NEW_PROP_TAG(DarcySubProblemTypeTag);
}

/**
 * \brief Coupling for 2pDarcy and 1pStokes
 *
 * \tparam TypeTag All types wrapped in the TypeTag
 */
template<typename TypeTag>
class CouplingStokesDarcy2p
  : public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags
  , public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<CouplingStokesDarcy2p<TypeTag> >
  , public Dune::PDELab::MultiDomain::NumericalJacobianApplyCoupling<CouplingStokesDarcy2p<TypeTag> >
  , public Dune::PDELab::MultiDomain::FullCouplingPattern
  , public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename GET_PROP_TYPE(TypeTag, Scalar)>
{
private:
    using StokesSubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag);
    using DarcySubProblemTypeTag = typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag);

    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    using StokesIndices = typename GET_PROP_TYPE(StokesSubProblemTypeTag, Indices);
    using DarcyIndices = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Indices);
    using DarcySubDomainProblem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Problem);
    using StokesSubDomainProblem = typename GET_PROP_TYPE(StokesSubProblemTypeTag, Problem);
    using FVElementGeometry = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FVElementGeometry);
    using PrimaryVariables = typename GET_PROP_TYPE(DarcySubProblemTypeTag, PrimaryVariables);
    using Scalar = typename GET_PROP_TYPE(DarcySubProblemTypeTag, Scalar);
    using SpatialParams = typename GET_PROP_TYPE(DarcySubProblemTypeTag, SpatialParams);
    using VolumeVariables = typename GET_PROP_TYPE(DarcySubProblemTypeTag, VolumeVariables);

    using BaseFluid = Dumux::OneComponentFluid<StokesSubProblemTypeTag>;
    using FluidState = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FluidState);
    using FluidSystem = typename GET_PROP_TYPE(DarcySubProblemTypeTag, FluidSystem);
    using MapperElement = typename GET_PROP_TYPE(StokesSubProblemTypeTag, MapperElement);
    using MaterialLaw = typename GET_PROP_TYPE(DarcySubProblemTypeTag, MaterialLaw);
    using MaterialLawParams = typename GET_PROP_TYPE(DarcySubProblemTypeTag, MaterialLawParams);
    using MDGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using ThermalConductivityModel = typename GET_PROP_TYPE(DarcySubProblemTypeTag, ThermalConductivityModel);

    enum { dim = MDGridView::dimension };

    enum {
        // Darcy stuff for phases and components
        numPhases_n = GET_PROP_VALUE(DarcySubProblemTypeTag, NumPhases),
        // indices
        wPhaseIdx_n = DarcyIndices::wPhaseIdx,
        nPhaseIdx_n = DarcyIndices::nPhaseIdx,
    };

    // DoF indices
    static const unsigned int stokesVelocityIdx = StokesIndices::velocityIdx;
    static const unsigned int stokesPressureIdx = StokesIndices::pressureIdx;
    static const unsigned int darcyPressureIdxDumux = DarcyIndices::pressureIdx;
    static const unsigned int darcySaturationIdxDumux = DarcyIndices::saturationIdx;
    static const unsigned int darcyPressureIdxPDELab = MultiDomainIndices::darcyPressureIdxPDELab;
    static const unsigned int darcySaturationIdxPDELab = MultiDomainIndices::darcySaturationIdxPDELab;

    // Equation indices
    static const unsigned int darcyContiWaterEqIdx = DarcyIndices::contiWEqIdx;
    static const unsigned int darcyContiTotalEqIdx = DarcyIndices::contiNEqIdx;

    static const unsigned int numEqDarcy = GET_PROP_VALUE(DarcySubProblemTypeTag, NumEq);
    using Matrix = Dune::FieldMatrix<Scalar, numEqDarcy, numEqDarcy>;
    using Vector = Dune::FieldVector<Scalar, numEqDarcy>;

public:
    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

  /*!
   * \brief Constructor
   */
  CouplingStokesDarcy2p(DarcySubDomainProblem& darcySubDomainProblem,
                        StokesSubDomainProblem& stokesSubDomainProblem)
  : darcySubDomainProblem_(darcySubDomainProblem),
    stokesSubDomainProblem_(stokesSubDomainProblem),
    spatialParams_(darcySubDomainProblem_.spatialParams()),
    // TODO the mapper needs the StokesGridView (in this case this is the mdGridView), so we can do it this way
    mapperElementMultiDomain_(darcySubDomainProblem_.gridView())
  {
      enableNavierStokes_ = GET_PARAM_FROM_GROUP(StokesSubProblemTypeTag, bool, Problem, EnableNavierStokes);
      enableUnsymmetrizedVelocityGradient_ = GET_PARAM_FROM_GROUP(StokesSubProblemTypeTag, bool, Problem, EnableUnsymmetrizedVelocityGradient);
      beaversJosephAsSolDependentDirichlet_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Coupling, BeaversJosephAsSolDependentDirichlet);

      enableGravityDarcy_ = GET_PARAM_FROM_GROUP(DarcySubProblemTypeTag, bool, Problem, EnableGravity);
      for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
          gravity_[dimIdx] = 0.0;
      if (enableGravityDarcy_)
      {
          gravity_[dim-1] = -9.81;
      }

      couplingMethod_ = GET_PARAM_FROM_GROUP(TypeTag, int, Coupling, Method);
      if (couplingMethod_ != 1)
      {
          std::cout << "couplingMethod_ " << couplingMethod_ << std::endl;
      }

      static_assert(!GET_PROP_VALUE(TypeTag, UseMoles),
                    "The CouplingStokesDarcy2p localoperator is not implemented for mole fraction formulation.");
      static_assert(GET_PROP_VALUE(DarcySubProblemTypeTag, Formulation) == TwoPFormulation::pnsw,
                    "The CouplingStokesDarcy2p localoperator is only implemented for a pnsw formulation.");
      static_assert(GET_PROP_VALUE(StokesSubProblemTypeTag, PhaseIdx) == DarcyIndices::nPhaseIdx,
                    "The CouplingStokesDarcy2p localoperator is only implemented for a non-wetting free flow phase.");
//       TODO: check whether it is necessary to use the total mass balance in this case
//       static_assert(GET_PROP_VALUE(DarcySubProblemTypeTag, ReplaceCompEqIdx) == DarcyIndices::contiNEqIdx,
//                     "The CouplingStokes2p2cniDarcy2cni localoperator is only implemented when using total mass instead of gas component mass balance equation.");
  }

  /*!
   * \brief Pass turbulent variables from the StokesLocalOperator
   */
  void passAdditionalVariables(std::function<Scalar (unsigned int)> eddyKinematicViscosity,
                               std::function<Dune::FieldMatrix<Scalar, dim, dim> (unsigned int)> velocityGradientTensor,
                               std::function<bool (unsigned int)> useWallFunctionMomentum)
  {
      eddyKinematicViscosity_ = eddyKinematicViscosity;
      velocityGradientTensor_ = velocityGradientTensor;
      useWallFunctionMomentum_ = useWallFunctionMomentum;
  }

  template<typename IG, typename LFSUStokes, typename LFSUDarcy,
           typename X, typename LFSVStokes, typename LFSVDarcy,
           typename R>
  void alpha_coupling(const IG& ig,
                      const LFSUStokes& lfsu_s, const X& x_s, const LFSVStokes& lfsv_s,
                      const LFSUDarcy& lfsu_n, const X& x_n, const LFSVDarcy& lfsv_n,
                      R& r_s, R& r_n) const
  {
    // select the velocity component from the subspaces for Navier-Stokes
    // other components are not needed
    const auto& lfsu_v_s = lfsu_s.template child<stokesVelocityIdx>();
    const auto& lfsu_p_s = lfsu_s.template child<stokesPressureIdx>();
    const auto& lfsu_p_n = lfsu_n.template child<darcyPressureIdxPDELab>();
    const auto& lfsu_x_n = lfsu_n.template child<darcySaturationIdxPDELab>();

    // range field type for velocity
    using RangeVelocity = typename LFSUStokes::template Child<stokesVelocityIdx>::Type::
        Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType;

    // /////////////////////
    // geometry information

    static const unsigned int dim = IG::Geometry::dimension;
    auto elementInsideIdx = mapperElementMultiDomain_.index(ig.inside());

    // local position of face center and face normal
    const auto& faceCenterLocal =
        Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
    const auto& faceUnitOuterNormal = ig.centerUnitOuterNormal();

    // evaluate orientation of intersection
    unsigned int normDim = 0;
    for (unsigned int curDim = 0; curDim < dim; ++curDim)
    {
        if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
        {
            normDim = curDim;
        }
    }
    // store all tangential dimensions in array by filtering out normDim
    std::array<unsigned int, dim-1> tangDims;
    for (unsigned int curTanDim = 0; curTanDim < dim; ++curTanDim)
    {
        if (curTanDim < normDim)
        {
          tangDims[curTanDim] = curTanDim;
        }
        else if (curTanDim > normDim)
        {
          tangDims[curTanDim - 1] = curTanDim;
        }
    }

    // face midpoints of all faces
    const unsigned int numFaces =
        Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_n(numFaces);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
    std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_n(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
        faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
        faceCentersLocal_n[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.outside().geometry().type()).position(curFace, 1);
        faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        faceCentersGlobal_n[curFace] = ig.outside().geometry().global(faceCentersLocal_n[curFace]);
    }
    Dune::FieldVector<Scalar, dim> faceCenterGlobal = faceCentersGlobal_n[ig.indexInOutside()];

    // face volume for integration
    Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

    // distance between element centers
    auto elementCentersLocal =
        Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
    auto elementCentersGlobal_s = ig.inside().geometry().global(elementCentersLocal);
    auto elementCentersGlobal_n = ig.outside().geometry().global(elementCentersLocal);

    // /////////////////////
    // velocities

    // evaluate shape functions and velocities at all face midpoints
    std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
    std::vector<RangeVelocity> velocities_s(numFaces);
    for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
    {
        // shape functions
        velocityBasis_s[curFace].resize(lfsu_v_s.size());
        lfsu_v_s.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
        // velocities
        velocities_s[curFace] = RangeVelocity(0.0);
        for (unsigned int basisIdx = 0; basisIdx < lfsu_v_s.size(); ++basisIdx)
        {
            velocities_s[curFace].axpy(x_s(lfsu_v_s, basisIdx), velocityBasis_s[curFace][basisIdx]);
        }
    }

    // /////////////////////
    // evaluation of primary variables

    // evaluate cell values for Stokes
    Scalar pressure_s = x_s(lfsu_p_s, 0);
    Scalar massFrac_s = stokesSubDomainProblem_.massMoleFracAtPos(faceCenterGlobal);
    Scalar temperature_s = stokesSubDomainProblem_.temperatureAtPos(faceCenterGlobal);

    // evaluate cell values for Darcy
    Scalar pressure_n = x_n(lfsu_p_n, 0);
    Scalar switch_n = x_n(lfsu_x_n, 0);

    // /////////////////////
    // evaluation of secondary variables, upwinding and averaging

    // Stokes
    const Scalar density_s = BaseFluid::density(pressure_s, temperature_s, massFrac_s);
    const Scalar effDynViscosityGas_s = BaseFluid::dynamicViscosity(pressure_s, temperature_s, massFrac_s)
                                        + (eddyKinematicViscosity_(elementInsideIdx) * density_s);

    // Darcy
    PrimaryVariables priVars;
    priVars[darcyPressureIdxDumux] = pressure_n;
    priVars[darcySaturationIdxDumux] = switch_n;
    FVElementGeometry fvGeometry;
    fvGeometry.update(darcySubDomainProblem_.gridView(), ig.outside());
    VolumeVariables volVars;
    volVars.update(priVars, darcySubDomainProblem_, ig.outside(), fvGeometry, 0, false);

    Scalar saturationWater = volVars.saturation(wPhaseIdx_n);
    Scalar densityGas_n = volVars.density(nPhaseIdx_n);
    Scalar densityWater_n = volVars.density(wPhaseIdx_n);
    Scalar dynamicViscosityGas_n = volVars.viscosity(nPhaseIdx_n);
    Scalar dynamicViscosityWater_n = volVars.viscosity(wPhaseIdx_n);

    // normal velocity and upwinding
    Scalar normalVelocityAtInterface_s = velocities_s[ig.indexInInside()][normDim]
                                         * faceUnitOuterNormal[normDim];
    Scalar densityGas_up = density_s;
    Scalar dynamicViscosityGas_up = effDynViscosityGas_s;
    if (normalVelocityAtInterface_s < 0)
    {
        // use values from Darcy
        densityGas_up = densityGas_n;
        dynamicViscosityGas_up = dynamicViscosityGas_n;
    }

    // /////////////////////
    // calculate the momentum interface fluxes

    Scalar totalMassFlux = densityGas_up * normalVelocityAtInterface_s;
    // pressure for Darcy (continuity of normal mass fluxes)
    r_s.accumulate(lfsu_p_s, 0,
                  1.0 * totalMassFlux
                  * faceVolume);
    r_n.accumulate(lfsu_n, darcyContiTotalEqIdx,
                  -1.0 * totalMassFlux
                  * faceVolume);

    // Robin boundary condition for tangential momentum equation of Stokes
    // (Beavers-Joseph-Saffman condition)
    Scalar permeability = spatialParams_.intrinsicPermeabilityAtPos(faceCenterGlobal);
    Scalar permeabilitySquareRoot = std::sqrt(permeability);
    Scalar alphaBeaversJoseph = spatialParams_.beaversJosephCoeffAtPos(faceCenterGlobal);
    // helper variable to keep term occuring twice in formula
    Scalar beta = -1.0 * permeabilitySquareRoot / alphaBeaversJoseph
                  / (elementCentersGlobal_s[normDim] - faceCenterGlobal[normDim])
                  * faceUnitOuterNormal[normDim];

    for (auto curTangDim : tangDims)
    {
        unsigned int indexTangentialVelocity0 = curTangDim * 2;
        unsigned int indexTangentialVelocity1 = indexTangentialVelocity0 + 1;

        Scalar beaversJosephVelocity0 =
            beta * velocities_s[indexTangentialVelocity0][curTangDim] / (1.0 + beta);
        Scalar beaversJosephVelocity1 =
            beta * velocities_s[indexTangentialVelocity1][curTangDim] / (1.0 + beta);

        // (1) \b Inertia term of \b momentum balance equation
        if (enableNavierStokes_ && beaversJosephAsSolDependentDirichlet_)
        {
            r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                           0.5 * density_s
                           * beaversJosephVelocity0
                           * beaversJosephVelocity0
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
            r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                           0.5 * density_s
                           * beaversJosephVelocity1
                           * beaversJosephVelocity1
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
        }

        // NOTE: This term is the classical Beavers-Joseph conditions, thus this is
        // the only statement which will be evaluated when beaversJosephAsSolDependentDirichlet_ is false
        //(2) \b Viscous term of \b momentum balance equation
        r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                       -0.5 * effDynViscosityGas_s
                       * (velocities_s[indexTangentialVelocity0][curTangDim] - beaversJosephVelocity0)
                       / (elementCentersGlobal_s[normDim] - faceCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                       -0.5 * effDynViscosityGas_s
                       * (velocities_s[indexTangentialVelocity1][curTangDim] - beaversJosephVelocity1)
                       / (elementCentersGlobal_s[normDim] - faceCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        //(3) symmetrize velocity gradient
        if (!enableUnsymmetrizedVelocityGradient_ && beaversJosephAsSolDependentDirichlet_)
        {
          Dune::FieldMatrix<Scalar, dim, dim> velocityGradientTensor = velocityGradientTensor_(elementInsideIdx);
          r_s.accumulate(lfsu_v_s, indexTangentialVelocity0,
                          -0.5 * effDynViscosityGas_s
                          * velocityGradientTensor[normDim][curTangDim]
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);
          r_s.accumulate(lfsu_v_s, indexTangentialVelocity1,
                          -0.5 * effDynViscosityGas_s
                          * velocityGradientTensor[normDim][curTangDim]
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);
        }
    }

    // /////////////////////
    // calculate rest of interface fluxes

    if (couplingMethod_ == 1 || couplingMethod_ == 2 || couplingMethod_ == 3)
    {
        ////
        // normal momentum
        ////
        // Neumann boundary condition for normal momentum equation of Stokes
        // (continuity of stress in normal direction)
        r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                       pressure_n
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        if (couplingMethod_ == 2 || couplingMethod_ == 3)
        {

            const auto& materialParams = spatialParams_.materialLawParamsAtPos(faceCenterGlobal);
            Scalar interfaceNormalPM = -1.0 * faceUnitOuterNormal[normDim];
            Scalar mobAir = 1.0 * densityGas_up * permeability
                            * MaterialLaw::krn(materialParams, saturationWater)
                            / dynamicViscosityGas_up;

            Scalar mobWater = 0.0;
            if (volVars.saturation(wPhaseIdx_n) > 0.0
                && volVars.saturation(nPhaseIdx_n) > 0.0
                && couplingMethod_ == 2)
            {
                mobWater = 1.0 * densityWater_n * permeability
                          * MaterialLaw::krw(materialParams, saturationWater)
                          / dynamicViscosityWater_n;
            }
            Scalar pressureDeltaInterface = 1.0 * totalMassFlux;
            if (enableGravityDarcy_)
            {
                pressureDeltaInterface += (mobAir * densityGas_up + mobWater * densityGas_n)
                                          * interfaceNormalPM * gravity_[normDim];
            }
            // NOTE: pressureGas_n is already accounted for above
            pressureDeltaInterface *= (faceCenterGlobal[normDim] - elementCentersGlobal_n[normDim])
                                      * interfaceNormalPM
                                      / (mobAir + mobWater);

//             std::cout << " Fluxes at " << faceCenterGlobal
//                       << " interfacePressureGas " << interfacePressureGas
//                       << " pressure_s " << pressure_s
//                       << " pressure_n " << pressure_n
//                       << std::endl;

            r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                           pressureDeltaInterface
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
        }

        Dune::dgrave << " Fluxes at " << faceCenterGlobal
                      << "   totalMass " << totalMassFlux
//                       << "   momentumX-0 " <<  0.5 * alphaBeaversJoseph * fromPermeability * velocities_s[2*tangDim][tangDim] * faceVolume
                      << "   momentumY " << faceUnitOuterNormal[normDim] * pressure_n * faceVolume
                      << std::endl;
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "This coupling method is not implemented.");
    }
  }

private:
    DarcySubDomainProblem& darcySubDomainProblem_;
    StokesSubDomainProblem& stokesSubDomainProblem_;
    SpatialParams& spatialParams_;
    MapperElement mapperElementMultiDomain_;

    bool enableNavierStokes_;
    bool enableUnsymmetrizedVelocityGradient_;
    bool beaversJosephAsSolDependentDirichlet_;
    bool enableGravityDarcy_;
    Scalar gravity_[dim];
    int couplingMethod_;

    std::function<Scalar (unsigned int)> eddyKinematicViscosity_;
    std::function<Dune::FieldMatrix<Scalar, dim, dim> (unsigned int)> velocityGradientTensor_;
    std::function<bool (unsigned int)> useWallFunctionMomentum_;
};

} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_PDELAB_COUPLING_STOKES_DARCY2P_HH
