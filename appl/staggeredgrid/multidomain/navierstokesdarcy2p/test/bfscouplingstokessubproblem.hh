// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_BFS_COUPLING_STOKES_SUBPROBLEM_HH
#define DUMUX_BFS_COUPLING_STOKES_SUBPROBLEM_HH

#include<dumux/freeflow/turbulenceproperties.hh>

#if COUPLING_KOMEGA                             //use K Omega Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegaproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegapropertydefaults.hh>
#elif COUPLING_KEPSILON                         //use K Epsilon Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonpropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON                    //use K LowReEpsilon Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonpropertydefaults.hh>
#elif COUPLING_ONEEQ                            //use one-EQ Model for free flow
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmarasproblem.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmaraspropertydefaults.hh>
#elif COUPLING_ZEROEQ                           //use K Zero-EQ Model for free flow
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqproblem.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqpropertydefaults.hh>
#else                                           //Otherwise, only NS without turbulence
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokespropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/navierstokesdarcy2p/properties.hh>

namespace Dumux
{
template <class TypeTag>
class CouplingStokesSubProblem;

namespace Properties
{
// Use pressure constraints
SET_BOOL_PROP(StokesSubProblem, FixPressureConstraints, false);

// Use complete Navier-Stokes equation
SET_BOOL_PROP(StokesSubProblem, ProblemEnableNavierStokes, true);

// Disable gravity field
SET_BOOL_PROP(StokesSubProblem, ProblemEnableGravity, false);

// Use either velocity or Reynolds number to define the flow
NEW_PROP_TAG(FreeFlowVelocity);
NEW_PROP_TAG(FreeFlowReynoldsNumber);
SET_SCALAR_PROP(StokesSubProblem, FreeFlowVelocity, 0.0);
SET_SCALAR_PROP(StokesSubProblem, FreeFlowReynoldsNumber, 0.0);

// Use either velocity or Reynolds number to define the flow
NEW_PROP_TAG(FreeFlowProfileType);
SET_STRING_PROP(StokesSubProblem, FreeFlowProfileType, "parabola");
}

template <class TypeTag>
class CouplingStokesSubProblem
#if COUPLING_KOMEGA
  : public KOmegaProblem<TypeTag>
#elif COUPLING_KEPSILON
  : public KEpsilonProblem<TypeTag>
#elif COUPLING_LOWREKEPSILON
  : public LowReKEpsilonProblem<TypeTag>
#elif COUPLING_ONEEQ
  : public OneEqProblem<TypeTag>
#elif COUPLING_ZEROEQ
  : public ZeroEqProblem<TypeTag>
#else
  : public NavierStokesProblem<TypeTag>
#endif
{
#if COUPLING_KOMEGA
    using ParentType = KOmegaProblem<TypeTag>;
#elif COUPLING_KEPSILON
    using ParentType = KEpsilonProblem<TypeTag>;
#elif COUPLING_LOWREKEPSILON
    using ParentType = LowReKEpsilonProblem<TypeTag>;
#elif COUPLING_OneEQ
    using ParentType = OneEqProblem<TypeTag>;
#elif COUPLING_ZEROEQ
    using ParentType = ZeroEqProblem<TypeTag>;
#else
    using ParentType = NavierStokesProblem<TypeTag>;
#endif
    using MultiDomainTypeTag = typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using DimVector = typename GET_PROP_TYPE(TypeTag, DimVector);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    enum { dim = GridView::dimension };
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { phaseIdx = Indices::phaseIdx };

public:
    CouplingStokesSubProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Velocity);
        velocityProfileType_ = GET_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, ProfileType);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Pressure);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Temperature);

        Scalar reynoldsNumber = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, ReynoldsNumber);
        if (reynoldsNumber > eps_ && std::abs(velocity_) > 1e-10)
        {
            std::cout << "Please specify either the Reynolds number or the velocity" << std::endl;
            exit(2300);
        }

        darcyLowerLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyLowerLeft);
        darcyUpperRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyUpperRight);

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure_);
        fluidState.setTemperature(temperature_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        kinematicViscosity_ = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        Scalar DiameterUpStep = std::max(0.0, this->bBoxMax()[1] - this->darcyUpperRight_[1]);
        if (reynoldsNumber > eps_)
        {
            velocity_ = reynoldsNumber * kinematicViscosity_ / DiameterUpStep;
        }
        Scalar velocityUpStep =   std::abs(velocity_);
        std::cout << "  Upstep Params - "
                  << "    d: " << DiameterUpStep
                  << "    v: " << velocityUpStep
                  << "    Re: " << DiameterUpStep * velocityUpStep / kinematicViscosity_
                  << "    density: " <<  density
                  << " Top Y: " << this->bBoxMax()[1]
                  << " Right X: " << this->bBoxMax()[0]
                  << std::endl;
        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        dissipationRate_ = turbulenceProperties.dissipationRate(velocityUpStep, DiameterUpStep, kinematicViscosity_, true);
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(velocityUpStep, DiameterUpStep, kinematicViscosity_, true);
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Returns whether we are on a coupling face
    const bool isOnCouplingFace(const DimVector& global) const
    {
        bool isOnCouplingFace = false;
        //Returns the 4 boundaries bounding the Darcy subdomain rectangle.
        for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
        {
            if (   (    (global[dimIdx] < (darcyLowerLeft_[dimIdx] + eps_) && global[dimIdx] > (darcyLowerLeft_[dimIdx] - eps_))
                     || (global[dimIdx] < (darcyUpperRight_[dimIdx] + eps_) && global[dimIdx] > (darcyUpperRight_[dimIdx] - eps_))
                   )
                   && global[1-dimIdx] > (darcyLowerLeft_[1-dimIdx] - eps_)
                   && global[1-dimIdx] < (darcyUpperRight_[1-dimIdx] + eps_)
               )
            {
                isOnCouplingFace = true;
            }
        }
        return isOnCouplingFace;
    }

    //! \brief Return whether a point is located on the boundary of the domain
    bool isOnBoundary(const DimVector& global) const
    {
        return    (global[0] < (this->bBoxMin()[0] + eps_))     // bottom
               || (global[0] > (this->bBoxMax()[0] - eps_))     // top
               || (global[1] < (this->bBoxMin()[1] + eps_))     // left
               || (global[1] > (this->bBoxMax()[1] - eps_))     // right
               #if DUMUX_MULTIDOMAIN_DIM == 3
               || (global[2] < (this->bBoxMin()[2] + eps_ ))        // z deep
               || (global[2] > (this->bBoxMax()[2] - eps_ ))        // z front
#endif
               || isOnCouplingFace(global);                     // coupling
    }

    //! \brief Velocity boundary condition types and Locations
    bool bcVelocityIsInflow(const DimVector& global) const              //INFLOW Location Velocity
    {
        return   (global[0] < this->bBoxMin()[0] + eps_                 //left wall
               && global[1] > darcyUpperRight_[1] - eps_)               //above coupling domain
                  && (!isOnCouplingFace(global)) ;                      //not on coupling domain
    }
    bool bcVelocityIsOutflow(const DimVector& global) const                     //OUTFLOW Velocity
    {
        return (global[0] > this->bBoxMax()[0] - eps_)                  //right wall
               && (!isOnCouplingFace(global));                          // not couplingface
    }
    bool bcVelocityIsCoupling(const DimVector& global) const                    //Coupling Velocity
    { return isOnCouplingFace(global);                                  //coupling face
    }
    bool bcVelocityIsSymmetry(const DimVector& global) const                    //Symmetry Velocity
    {
      return (   (global[1] < (this->bBoxMin()[1] + eps_))                    //bottom wall
              && (global[0] < (this->darcyUpperRight_[0]+ eps_))
              && (!isOnCouplingFace(global))   )           //before darcy domain
     #if DUMUX_MULTIDOMAIN_DIM == 3
               || (    ((global[2] < (this->bBoxMin()[2] + eps_)) ||
                        (global[2] > (this->bBoxMax()[2] - eps_)) )
                    && (!isOnCouplingFace(global))   )                // z front
#endif
               ;
    }
    bool bcVelocityIsWall(const DimVector& global) const                        //Wall Velocity
    {                                                                   //Wall is a:
        return isOnBoundary(global)                                     // boundary
               && !bcVelocityIsInflow(global)                           // not inflow
               && !bcVelocityIsOutflow(global)                          // not outflow
               && !bcVelocityIsCoupling(global)                         // not coupling
               && !bcVelocityIsSymmetry(global);                        // not symmetry
    }

    //! \brief Velocity Diriclet Values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector x(0.0);
        if (std::strcmp(velocityProfileType_.c_str(), "parabola") == 0)
        {
            x[0] = 4.0 * velocity_ * global[1] * (this->bBoxMax()[1] - global[1])           // Parabola inflow
                   / (this->bBoxMax()[1] - this->darcyUpperRight_[1])
                   / (this->bBoxMax()[1] - this->darcyUpperRight_[1]);
        }
        else if (std::strcmp(velocityProfileType_.c_str(), "half-parabola") == 0)
        {
            x[0] = 4.0 * velocity_ * (global[1] / 2) * (this->bBoxMax()[1] - (global[1] / 2))    // halfparabola inflow
                   / (this->bBoxMax()[1] - this->darcyUpperRight_[1])
                   / (this->bBoxMax()[1] - this->darcyUpperRight_[1]);
        }
        else if (std::strcmp(velocityProfileType_.c_str(), "block") == 0)
        {                                                                                         //block inflow
            x[0] = velocity_;
        }
        else
        {
            std::cout << "Velocity profile type is unkown" << std::endl;
            exit(2301);
        }
        if (bcVelocityIsWall(global))                                                             // at all, velocity is 0
            x[0] = 0.0;
        return x;
    }

#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
    //! \brief TurbulentKineticEnergy boundary condition types

    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }

    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }

    bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }

    //! \brief TurbulentKineticEnergy boundary condition values
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
    }

    //! \brief Dissipation boundary condition types

    bool bcDissipationIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }

    bool bcDissipationIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }

    bool bcDissipationIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    bool bcDissipationIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }

    //! \brief Dissipation boundary condition values
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
        Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> dofMapper(gridView_);
        Scalar BCdissipation = 0.0;

        if (bcDissipationIsInflow(global))
        {
            BCdissipation = dissipationRate_; //dissipation inflow and initial conditions
        }
        else if (this->timeManager().time() > 0.0)
        {
            BCdissipation = 6.0 * kinematicViscosity_ / (0.075 * std::pow(this->wallDistance_(dofMapper.index(e)), 2)); //dissipation at boundary
        }
        else
        {
          BCdissipation = dissipationRate_; //dissipation inflow and initial conditions
        }
        return BCdissipation;
    }
#endif

    //! \brief Pressure boundary condition types

    bool bcPressureIsDirichlet(const DimVector& global) const              //Pressure Dirichlet locations (outflow)
    { return bcVelocityIsOutflow(global); }

    bool bcPressureIsCoupling(const DimVector& global) const               //Pressure Coupling Locations
    { return isOnCouplingFace(global); }

    bool bcPressureIsOutflow(const DimVector& global) const                //Pressure OUTFLOW location, All diriclet, except coupling
    { return !bcPressureIsDirichlet(global) && !bcPressureIsCoupling(global); }

    // Pressure Boundary conditions
    Scalar dirichletPressureAtPos(const DimVector& global) const
    {
        return pressure_;
    }
    // Mole Fraction Boundary conditions
    Scalar massMoleFracAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    // Temperature Boundary conditions
    Scalar temperatureAtPos(const DimVector& global) const
    {
        return temperature_;
    }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition darcyLowerLeft_;
    GlobalPosition darcyUpperRight_;

    Scalar velocity_;
    std::string velocityProfileType_;
    Scalar pressure_;
    Scalar temperature_;
    Scalar dissipationRate_;
    Scalar kinematicViscosity_;
    Scalar turbulentKineticEnergy_;
};

} //end namespace

#endif // DUMUX_BFS_COUPLING_STOKES_SUBPROBLEM_HH
