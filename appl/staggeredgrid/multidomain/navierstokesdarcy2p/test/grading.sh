#calculate the size of the largest grid
# When running this, enter  Length, Cells, and Grading as positional params. (in this order)"

length=$1
cells=$2
grading=$3

count=0
sum=0
while [ $count -lt $cells ]
do
    term=`echo "1 / ( $grading ^ $count )" | bc -l`
    sum=`echo $sum + $term | bc -l`
    count=`expr $count + 1`
done

largestgridsize=`echo "scale=8; $length / $sum" | bc -l`
echo "The Largest cell is $largestgridsize m"
smallestgridsize=`echo "scale=8; $largestgridsize / ( $grading ^ ( $cells - 1 ) ) " | bc -l`
echo "The Smallest cell is $smallestgridsize m"
