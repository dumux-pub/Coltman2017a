#ifndef DUMUX_COUPLING_PROBLEM_HH
#define DUMUX_COUPLING_PROBLEM_HH

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include <appl/staggeredgrid/multidomain/navierstokesdarcy2p/problem.hh>

#include "couplingdarcysubproblem.hh"
#include "couplingstokessubproblem.hh"

namespace Dumux
{

template <class TypeTag>
class CouplingProblem;

template <class TypeTag> class MultiDomainProblem;

namespace Properties
{
// problems
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, Problem,
              Dumux::CouplingProblem<TTAG(MDSNavierStokesDarcyTwoP)>);
SET_TYPE_PROP(StokesSubProblem, Problem,
              Dumux::CouplingStokesSubProblem<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(DarcySubProblem, Problem,
              Dumux::CouplingDarcySubProblem<TTAG(DarcySubProblem)>);

// Set the fluid system to use complex relations (last argument)
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);
SET_TYPE_PROP(DarcySubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), FluidSystem));
SET_TYPE_PROP(StokesSubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), FluidSystem));

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(MDSNavierStokesDarcyTwoP, UseMoles, false);
SET_BOOL_PROP(DarcySubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesDarcyTwoP), UseMoles));
SET_BOOL_PROP(StokesSubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesDarcyTwoP), UseMoles));
}

template <class TypeTag = TTAG(MDSNavierStokesDarcyTwoP)>
class CouplingProblem
: public MultiDomainProblem<TypeTag>
{
    using Implementation = typename GET_PROP_TYPE(TypeTag, Problem);
    using ParentType = MultiDomainProblem<TypeTag>;

    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using SubDomainGridView = typename GET_PROP_TYPE(TypeTag, SubDomainGridView);
    enum { dim = MultiDomainGridView::dimension };
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    constexpr static unsigned int stokesSubDomainIdx = MultiDomainIndices::stokesSubDomainIdx;
    constexpr static unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief Base class for the multi domain problem
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The GridView
     */
    CouplingProblem(TimeManager &timeManager,
                     GridView gridView)
    : ParentType(timeManager, gridView)
    {
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
    }

    /*!
     * \brief Initialization multi-domain and the sub-domain grids
     */
    void initializeGrid()
    {
        this->mdGrid().startSubDomainMarking();

        for (auto eIt = this->mdGrid().template leafbegin<0>();
              eIt != this->mdGrid().template leafend<0>(); ++eIt)
        {
            auto globalPos = eIt->geometry().center();
            // only for interior entities, required for parallelization
            if (eIt->partitionType() == Dune::InteriorEntity)
            {
                if (inDarcyDomain(globalPos))
                {
                    this->mdGrid().addToSubDomain(darcySubDomainIdx, *eIt);
                }
                else
                {
                    this->mdGrid().addToSubDomain(stokesSubDomainIdx, *eIt);
                }
            }
        }
        this->mdGrid().preUpdateSubDomains();
        this->mdGrid().updateSubDomains();
        this->mdGrid().postUpdateSubDomains();

        this->darcyElementIndices_.resize(this->sdGridViewDarcy().size(0));
        Dune::MultipleCodimMultipleGeomTypeMapper<MultiDomainGridView, Dune::MCMGElementLayout>
            multidomainDofMapper(this->mdGridView());
        Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
            subdomainDofMapper(this->sdGridViewDarcy());
        for (auto eIt = this->sdGridDarcy().template leafbegin<0>();
              eIt != this->sdGridDarcy().template leafend<0>(); ++eIt)
        {
            this->darcyElementIndices_[subdomainDofMapper.index(*eIt)] =
                multidomainDofMapper.index(this->mdGrid().multiDomainEntity(*eIt));
        }
    }

    /*!
     * \brief Function to decide whether one point is inside the Darcy domain
     */
    bool inDarcyDomain(GlobalPosition globalPos)
    {
        GlobalPosition darcyLowerLeft = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyLowerLeft);
        GlobalPosition darcyUpperRight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyUpperRight);
        bool inDarcyDomain = true;

        for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            if (globalPos[dimIdx] < darcyLowerLeft[dimIdx]
                || globalPos[dimIdx] > darcyUpperRight[dimIdx])
                    inDarcyDomain &= false;

        return inDarcyDomain;
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution. Calls postTimeStep()
     *        of the subproblems.
     */
    void postTimeStep()
    {
        // call stuff from the parent type
        ParentType::postTimeStep();

#if HAVE_PVPYTHON
        if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotVelocityProfiles))
        {
            char fileName[255];
            std::string fileNameFormat = "%s-ff-%05d";
            sprintf(fileName, fileNameFormat.c_str(), asImp_().name().c_str(), asImp_().timeVector_.size()-1);
            std::string csvFileName = std::string(fileName) + ".csv";
            std::string vtuFileName = std::string(fileName) + ".vtu";
            std::string script = std::string(DUMUX_INCLUDE_DIRS) + "/bin/postprocessing/extractlinedata.py";
            std::string syscom;

            // the coordinates of interest
            GlobalPosition positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, Positions0);
            GlobalPosition positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, Positions1);
            GlobalPosition darcyLowerLeft = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyLowerLeft);
            GlobalPosition darcyUpperRight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyUpperRight);
            std::ostringstream bBoxMin0; bBoxMin0 << positions0[0] << " ";
            std::ostringstream bBoxMin1; bBoxMin1 << positions1[0] << " ";
            std::ostringstream bBoxMax0; bBoxMax0 << positions0[1] << " ";
            std::ostringstream bBoxMax1; bBoxMax1 << positions1[1] << " ";
            std::ostringstream darcyMin0; darcyMin0 << darcyLowerLeft[0] << " ";
            std::ostringstream darcyMax0; darcyMax0 << darcyUpperRight[0] << " ";
            std::ostringstream darcyMin1; darcyMin1 << darcyLowerLeft[1] << " ";
            std::ostringstream darcyMax1; darcyMax1 << darcyUpperRight[1] << " ";

            // execute the pvpython script
            std::string command = std::string(PVPYTHON_EXECUTABLE) + " " + script + " -f " + vtuFileName + " -v 1 -r 1000";
            syscom =  command + " -p1 " + bBoxMin0.str() + bBoxMax1.str() + " 0.0"
                              + " -p2 " + bBoxMax0.str() + bBoxMax1.str() + " 0.0"
                              + " -of " + std::string(fileName) + "_inflow\n";
            syscom += command + " -p1 " + bBoxMin0.str() + darcyMax1.str() + " 0.0"
                              + " -p2 " + bBoxMax0.str() + darcyMax1.str() + " 0.0"
                              + " -of " + std::string(fileName) + "_before\n";
            syscom += command + " -p1 " + bBoxMin0.str() + darcyMin1.str() + " 0.0"
                              + " -p2 " + bBoxMax0.str() + darcyMin1.str() + " 0.0"
                              + " -of " + std::string(fileName) + "_after\n";
            syscom += command + " -p1 " + bBoxMin0.str() + bBoxMin1.str() + " 0.0"
                              + " -p2 " + bBoxMax0.str() + bBoxMin1.str() + " 0.0"
                              + " -of " + std::string(fileName) + "_outflow\n";
            system(syscom.c_str());


            char gnuplotFileName[255];
            sprintf(gnuplotFileName, fileNameFormat.c_str(), "velProfiles", asImp_().timeVector_.size()-1);
            gnuplot_.setDatafileSeparator(',');
            gnuplot_.resetPlot();
            gnuplot_.setXlabel("x [m]");
            gnuplot_.setYlabel("v_y [m/s]");
            std::ostringstream time; time << asImp_().timeVector_.back();
            gnuplot_.setOption("set title \'velocity profiles at t: " + time.str() + "s\'");
            gnuplot_.setOption("set arrow from " + darcyMin0.str() + ", graph 0 to " + darcyMin0.str() + ", graph 1 nohead ls 2 lc rgb \'gray\'");
            gnuplot_.setOption("set label \'porous medium\' at 1.05*" + darcyMin0.str() + ", graph 0 tc rgb \'gray\' rotate by 90");
            gnuplot_.setOption("set arrow from " + darcyMin0.str() + ", graph 0 to " + darcyMin0.str() + ", graph 1 nohead ls 2 lc rgb \'gray\'");
            gnuplot_.setOption("set label \'porous medium\' at 0.98*" + darcyMax0.str() + ", graph 0 tc rgb \'gray\' rotate by 90");
            gnuplot_.addFileToPlot(std::string(fileName) + "_inflow.csv", "u 9:3 w l lt 1 t 'inflow, y=" + bBoxMax1.str() + "'");
            gnuplot_.addFileToPlot(std::string(fileName) + "_after.csv", "u 9:3 w l lt 2 t 'before, y=" + darcyMax1.str() + "'");
            gnuplot_.addFileToPlot(std::string(fileName) + "_before.csv", "u 9:3 w l lt 3 t 'after, y=" + darcyMin1.str() + "'");
            gnuplot_.addFileToPlot(std::string(fileName) + "_outflow.csv", "u 9:3 w l lt 4 t 'outflow, y=" + bBoxMin1.str() + "'");
            gnuplot_.plot(std::string(gnuplotFileName));
        }
#endif
    }

protected:
    Implementation &asImp_()
    {
        return *static_cast<Implementation *>(this);
    }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    {
        return *static_cast<const Implementation *>(this);
    }

private:
    Dumux::GnuplotInterface<double> gnuplot_;
};

} // end namespace Dumux

#endif // DUMUX_COUPLING_PROBLEM_HH
