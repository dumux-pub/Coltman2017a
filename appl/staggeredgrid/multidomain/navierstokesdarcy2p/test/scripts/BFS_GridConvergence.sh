# BACKWARD FACING STEP: Convergence Test

# set data separator and locate the extract line script
  DATA="."
  VTU2CSV="/var/run/media/nedc/workspace10/MastersThesis/dumux/dumux/bin/postprocessing/extractlinedata.py"

# Run simulation Function
  runSimulation() {
# run bfs test. Input= input file.  append all to outputfile.
    ./test_bfscoupling_komega $INPUT -Grid.Grading1 "$1" -Problem.Name "$2" | tee -a logfile.out

# searches output files for the last .vtu file. when located, stores file name in "input"
    input=`ls -ltr *Secondary*vtu | tail -n 1 | awk '{print $9}'`
    #ls -ltr sorts directory for files with Secondary in them and vtu at the end. Most recent files are printed at the bottom.
    #tail -n 1 selects the last file (at the bottom), awk '{print $9}' prints the 9th parameter in this line.
    # $1 is permissions, $2 is "1", $3 is "user", $4 is "systemname", $5 is size of file, $6$7$8 are date and time,
    # $9 is file name
    echo "$input" -> "$2" | tee -a logfile.out
    #
      mv logfile.out logfile_"$2".out
      mv *storage.out storage_"$2".out
      mv drainage drainage_"$2".out

#define locations of lines for vtu -> csv data collection. "step" is x coordinate of step.
    step=110 # x-Coordinate of Step
#5 data lines, x/H = -4, +1, +4, +6, +10
    stepmin04=`expr $step - 04`
    steppls01=`expr $step + 01`
    steppls04=`expr $step + 04`
    steppls06=`expr $step + 06`
    steppls10=`expr $step + 10`
    steppls36=`expr $step + 36`
#Create x,y,z coordinates for linedata extraction
    P1_mins04="$stepmin04 0.0 0.0"
    P2_mins04="$stepmin04 9.0 0.0"
    P1_plus01="$steppls01 0.0 0.0"
    P2_plus01="$steppls01 9.0 0.0"
    P1_plus04="$steppls04 0.0 0.0"
    P2_plus04="$steppls04 9.0 0.0"
    P1_plus06="$steppls06 0.0 0.0"
    P2_plus06="$steppls06 9.0 0.0"
    P1_plus10="$steppls10 0.0 0.0"
    P2_plus10="$steppls10 9.0 0.0"
    P1_Wall00="$stepmin04 0.0 0.0"
    P2_Wall00="$steppls36 0.0 0.0"

# extract csv files from VTU using extractlinedata.py
  # -f=vtu file,        -o=outputdirectory,     -of=outputfile  basename,
  # -p1=point1(x y z),  -p2=point2(x y z),
  # -v=verbosity        -r resolution
  # "| tee -a" appends all output to an external logfile
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$2""_mins04" -p1 $P1_mins04 -p2 $P2_mins04 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$2""_plus01" -p1 $P1_plus01 -p2 $P2_plus01 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$2""_plus04" -p1 $P1_plus04 -p2 $P2_plus04 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$2""_plus06" -p1 $P1_plus06 -p2 $P2_plus06 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$2""_plus10" -p1 $P1_plus10 -p2 $P2_plus10 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$2""_Wall00" -p1 $P1_Wall00 -p2 $P2_Wall00 -v 2 -r 10000 | tee -a logfile.out

mv *10_20*.vtu *10_20*.pvd *10_20*.csv *10_20*.out results_10_20/
mv *10_30*.vtu *10_30*.pvd *10_30*.csv *10_30*.out results_10_30/

mv *20_20*.vtu *20_20*.pvd *20_20*.csv *20_20*.out results_20_20/
mv *20_30*.vtu *20_30*.pvd *20_30*.csv *20_30*.out results_20_30/

}
#end function

#run Backward Facing Step Problem
  INPUT=test_backwardfacingstep.input

#Grid Parameters test
   runSimulation '1.10 -1.10 1.20 -1.20' BFS_convergence_10_20
   runSimulation '1.10 -1.10 1.30 -1.30' BFS_convergence_10_30 # USE THIS
#
   runSimulation '1.20 -1.20 1.20 -1.20' BFS_convergence_20_20
   runSimulation '1.20 -1.20 1.30 -1.30' BFS_convergence_20_30
