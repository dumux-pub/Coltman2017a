reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'CoefficientOfFriction_Poro.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'Distance from step x [m]'
set ylabel 'Coefficient of Friction C_{f}'
set xrange [0:30]
set yrange [-0.003:0.003]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key right reverse center samplen 1
plot \
 'test_references/EXP_FrictionCoefficient.csv' using 1:2                                  w p ps 2 pt 2 lc 8 t              'Experimental Data', \
 'test_references/wilcoxmodel_Cf.csv'          using 2:3                                  w l lw 3 lt 0 lc 8 t              'Reference Model', \
 'test_references/Model_wall00.csv'         using ($25-110):($14)*($6+$8)/(0.5*0.11*0.11) w l lw 3 lt 7 lc 8 t              'Impermeable Model', \
 'test_references/ParameterStudy/Phi/1/Model_wall00.csv' using ($25-110):($14)*($6+$8)/(0.5*0.11*0.11) w l lw 3 lt 7 lc 6 t 'Porosity = 0.1', \
 'test_references/ParameterStudy/Phi/3/Model_wall00.csv' using ($25-110):($14)*($6+$8)/(0.5*0.11*0.11) w l lw 3 lt 7 lc 7 t 'Porosity = 0.3', \
 'test_references/ParameterStudy/Phi/5/Model_wall00.csv' using ($25-110):($14)*($6+$8)/(0.5*0.11*0.11) w l lw 3 lt 7 lc 9 t 'Porosity = 0.5'


# Coefficient of Friction is calculated via ShearStress_wall/(0.5*Density*ReferenceVelocity^2)
# In this case the ShearStress_wall is calculated via the equations supplied in the K-Omega Model.
# ShearStress_wall is calculated via (density*K/Omega)*(frac{del{U_i}}{del{X_j}}+frac{del{U_j}}{del{X_i}})
# $6    velocityGradients0:1 ---> (frac{del{U_i}}{del{X_j}})
# $8    velocityGradients1:0 ---> (frac{del{U_j}}{del{X_i}})
# $21   k
# $22   omega
# $14   viscosity
# $15   eddy viscosity
# But, as omega and k should be infinte and zero at the wall, we use the kinematic viscosity instead
#
# Total Numerator = ($14)*($6+$8)
#
# Density is stored in column 13, but is not required as it is found in both the numerator and the denominator
# ReferenceVelocity must be guessed at originally, and corrected for after implementation is through (0.11 [m/s])
# Total Denominator = (0.5*0.11*0.11)

#Total Arguement = ($14)*($6+$8)/(0.5*0.11*0.11)
