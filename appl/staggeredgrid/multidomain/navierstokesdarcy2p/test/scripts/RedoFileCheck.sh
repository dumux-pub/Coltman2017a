# BACKWARD FACING STEP TEST

# set data separator and locate the extract line script
  DATA="."
  VTU2CSV="/var/run/media/nedc/workspace10/MastersThesis/dumux/dumux/bin/postprocessing/extractlinedata.py"

  step=110 # x-Coordinate of Step
#5 data lines, x/H = -4, +1, +4, +6, +10
    stepmin04=`expr $step - 04`
    steppls01=`expr $step + 01`
    steppls04=`expr $step + 04`
    steppls06=`expr $step + 06`
    steppls10=`expr $step + 10`
    steppls36=`expr $step + 36`
#Create x,y,z coordinates for linedata extraction
    P1_mins04="$stepmin04 0.0 0.0"
    P2_mins04="$stepmin04 9.0 0.0"
    P1_plus01="$steppls01 0.0 0.0"
    P2_plus01="$steppls01 9.0 0.0"
    P1_plus04="$steppls04 0.0 0.0"
    P2_plus04="$steppls04 9.0 0.0"
    P1_plus06="$steppls06 0.0 0.0"
    P2_plus06="$steppls06 9.0 0.0"
    P1_plus10="$steppls10 0.0 0.0"
    P2_plus10="$steppls10 9.0 0.0"
    P1_Wall00="$stepmin04 0.0 0.0"
    P2_Wall00="$steppls36 0.0 0.0"

# Run simulation Function
  runSimulation() {
#define locations of lines for vtu -> csv data collection. "step" is x coordinate of step.
# extract csv files from VTU using extractlinedata.py
  # -f=vtu file,        -o=outputdirectory,     -of=outputfile  basename,
  # -p1=point1(x y z),  -p2=point2(x y z),
  # -v=verbosity        -r resolution
  # "| tee -a" appends all output to an external logfile
    pvpython $VTU2CSV -f $1 -o "$DATA" -of "$2""_mins04" -p1 $P1_mins04 -p2 $P2_mins04 -v 2 -r 1000000
    pvpython $VTU2CSV -f $1 -o "$DATA" -of "$2""_plus01" -p1 $P1_plus01 -p2 $P2_plus01 -v 2 -r 1000000
    pvpython $VTU2CSV -f $1 -o "$DATA" -of "$2""_plus04" -p1 $P1_plus04 -p2 $P2_plus04 -v 2 -r 1000000
    pvpython $VTU2CSV -f $1 -o "$DATA" -of "$2""_plus06" -p1 $P1_plus06 -p2 $P2_plus06 -v 2 -r 1000000
    pvpython $VTU2CSV -f $1 -o "$DATA" -of "$2""_plus10" -p1 $P1_plus10 -p2 $P2_plus10 -v 2 -r 1000000
    pvpython $VTU2CSV -f $1 -o "$DATA" -of "$2""_Wall00" -p1 $P1_Wall00 -p2 $P2_Wall00 -v 2 -r 1000000
}
#end function
#run Backward Facing Step Problemresults_BJ/001
  runSimulation results_BJ/0p1/bfscoupling_BJ_0p1-ffSecondary-00079.vtu BJ_0p1
  runSimulation results_BJ/001/bfscoupling_BJ_001-ffSecondary-00078.vtu BJ_001
  runSimulation results_BJ/050/bfscoupling_BJ_050-ffSecondary-00070.vtu BJ_050
