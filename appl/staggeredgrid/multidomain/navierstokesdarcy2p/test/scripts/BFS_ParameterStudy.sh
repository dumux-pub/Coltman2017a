# BACKWARD FACING STEP TEST

# set data separator and locate the extract line script
  DATA="."
  VTU2CSV="/var/run/media/nedc/workspace10/MastersThesis/dumux/dumux/bin/postprocessing/extractlinedata.py"

# Run simulation Function
  runSimulation() {
# run bfs test. Input= input file. Porosity= 1st PosPar ($1). Permeability= 2nd PosPar ($2). append all to outputfile.
    ./test_bfscoupling_komega $INPUT -SpatialParams.Porosity "$1" -SpatialParams.Permeability "$2" -SpatialParams.AlphaBJ "$3" -Problem.Name "$4" | tee -a logfile.out

# searches output files for the last .vtu file. when located, stores file name in "input"
    input=`ls -ltr *Secondary*vtu | tail -n 1 | awk '{print $9}'`
    #ls -ltr sorts directory for files with Secondary in them and vtu at the end. Most recent files are printed at the bottom.
    #tail -n 1 selects the last file (at the bottom), awk '{print $9}' prints the 9th parameter in this line.
    # $1 is permissions, $2 is "1", $3 is "user", $4 is "systemname", $5 is size of file, $6$7$8 are date and time,
    # $9 is file name

#define locations of lines for vtu -> csv data collection. "step" is x coordinate of step.
    step=110 # x-Coordinate of Step
#5 data lines, x/H = -4, +1, +4, +6, +10
    stepmin04=`expr $step - 04`
    steppls01=`expr $step + 01`
    steppls04=`expr $step + 04`
    steppls06=`expr $step + 06`
    steppls10=`expr $step + 10`
    steppls36=`expr $step + 36`
#Create x,y,z coordinates for linedata extraction
    P1_mins04="$stepmin04 0.0 0.0"
    P2_mins04="$stepmin04 9.0 0.0"
    P1_plus01="$steppls01 0.0 0.0"
    P2_plus01="$steppls01 9.0 0.0"
    P1_plus04="$steppls04 0.0 0.0"
    P2_plus04="$steppls04 9.0 0.0"
    P1_plus06="$steppls06 0.0 0.0"
    P2_plus06="$steppls06 9.0 0.0"
    P1_plus10="$steppls10 0.0 0.0"
    P2_plus10="$steppls10 9.0 0.0"
    P1_Wall00="$stepmin04 0.0 0.0"
    P2_Wall00="$steppls36 0.0 0.0"

# extract csv files from VTU using extractlinedata.py
  # -f=vtu file,        -o=outputdirectory,     -of=outputfile  basename,
  # -p1=point1(x y z),  -p2=point2(x y z),
  # -v=verbosity        -r resolution
  # "| tee -a" appends all output to an external logfile
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_mins04" -p1 $P1_mins04 -p2 $P2_mins04 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus01" -p1 $P1_plus01 -p2 $P2_plus01 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus04" -p1 $P1_plus04 -p2 $P2_plus04 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus06" -p1 $P1_plus06 -p2 $P2_plus06 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus10" -p1 $P1_plus10 -p2 $P2_plus10 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_Wall00" -p1 $P1_Wall00 -p2 $P2_Wall00 -v 2 -r 10000 | tee -a logfile.out

 mv logfile.out logfile_"$4".out
 mv *storage.out storage_"$4".out
 mv drainage drainage_"$4".out

#  mv *impermeable* results_impermeable

#  mv *K_15*.vtu *K_15*.pvd *K_15*.csv *K_15*out results_perm/15
#  mv *K_10*.vtu *K_10*.pvd *K_10*.csv *K_10*out results_perm/10
#  mv *K_09*.vtu *K_09*.pvd *K_09*.csv *K_09*out results_perm/09
#  mv *K_08*.vtu *K_08*.pvd *K_08*.csv *K_08*out results_perm/08
#  mv *K_07*.vtu *K_07*.pvd *K_07*.csv *K_07*out results_perm/07
#  mv *K_06*.vtu *K_06*.pvd *K_06*.csv *K_06*out results_perm/06
#  mv *K_05*.vtu *K_05*.pvd *K_05*.csv *K_05*out results_perm/05

#  mv *BJ_0p001*.vtu *BJ_0p001*.pvd *BJ_0p001*.csv *BJ_0p001*out results_BJ/0p001
#  mv *BJ_0p010*.vtu *BJ_0p010*.pvd *BJ_0p010*.csv *BJ_0p010*out results_BJ/0p010
#  mv *BJ_0p100*.vtu *BJ_0p100*.pvd *BJ_0p100*.csv *BJ_0p100*out results_BJ/0p100
#  mv *BJ_1p000*.vtu *BJ_1p000*.pvd *BJ_1p000*.csv *BJ_1p000*out results_BJ/1p000
#  mv *BJ_10p00*.vtu *BJ_10p00*.pvd *BJ_10p00*.csv *BJ_10p00*out results_BJ/10p00
#  mv *BJ_100p0*.vtu *BJ_100p0*.pvd *BJ_100p0*.csv *BJ_100p0*out results_BJ/100p0

#  mv *phi_5*.vtu *phi_5*.pvd *phi_5*.csv *phi_5*out results_poro/5
#  mv *phi_3*.vtu *phi_3*.pvd *phi_3*.csv *phi_3*out results_poro/3
#  mv *phi_1*.vtu *phi_1*.pvd *phi_1*.csv *phi_1*out results_poro/1

}
#end function
#run Backward Facing Step Problem
  INPUT=test_backwardfacingstep.input
    #Impermeable test
#    runSimulation 0.05 1e-30 1 bfscoupling_impermeable
    #permeability test
#    runSimulation 0.3 1e-10 1 bfscoupling_K_10
#    runSimulation 0.3 1e-09 1 bfscoupling_K_09
#    runSimulation 0.3 1e-08 1 bfscoupling_K_08
#    runSimulation 0.3 1e-07 1 bfscoupling_K_07
#    runSimulation 0.3 1e-06 1 bfscoupling_K_06
#    runSimulation 0.3 1e-05 1 bfscoupling_K_05
    #BJ test
#    runSimulation 0.3 1e-15 0.001 bfscoupling_BJ_0p001
#    runSimulation 0.3 1e-15 0.010 bfscoupling_BJ_0p010
#    runSimulation 0.3 1e-15 0.100 bfscoupling_BJ_0p100
#    runSimulation 0.3 1e-15 1.000 bfscoupling_BJ_1p000
#    runSimulation 0.3 1e-15 10.00 bfscoupling_BJ_10p00
#    runSimulation 0.3 1e-15 100.0 bfscoupling_BJ_100p0
    #Porosity test
#    runSimulation 0.5 1e-15 1 bfscoupling_phi_5
#    runSimulation 0.3 1e-15 1 bfscoupling_phi_3
#    runSimulation 0.1 1e-15 1 bfscoupling_phi_1
