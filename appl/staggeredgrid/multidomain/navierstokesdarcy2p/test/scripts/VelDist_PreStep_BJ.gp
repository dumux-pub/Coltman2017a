reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'VelDist_PreStep_BJ.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max}'
set ylabel 'y [m]'
set xrange [0:1.02]
set yrange [1:1.5]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse center samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'        using 8:7               w p ps 2 pt 2 lc 8 t 'Driver Experimental Data x/H=-4', \
  'test_references/wilcoxmodel_Prestep.csv'    using 2:3               w l lw 3 lt 0 lc 8 t 'Wilcox Reference Model x/H=-4', \
  'test_references/Model_mins04.csv'           using ($2/0.0737):26    w l lw 3 lt 7 lc 8 t 'K-Omega Model Impermeable', \
  'test_references/ParameterStudy/BJ/0p010/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 4 t 'K-Omega Model Alpha = 0.01', \
  'test_references/ParameterStudy/BJ/1p000/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 6 t 'K-Omega Model Alpha = 1.0', \
  'test_references/ParameterStudy/BJ/10p00/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 7 t 'K-Omega Model Alpha = 10', \
  'test_references/ParameterStudy/BJ/100p0/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 9 t 'K-Omega Model Alpha = 100'


reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'VelDist_BJ_combo.png'
set xlabel font ",20"
set ylabel font ",20"
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set multiplot layout 1,2 rowsfirst
set xlabel 'v_x/v_{max} X/H = -4'
set ylabel 'y [m]'
set xrange [0:1.02]
set yrange [1:1.3]
set key font ",20"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'        using 8:7                                w p ps 2 pt 2 lc 8 t 'Exp Data ', \
  'test_references/wilcoxmodel_Prestep.csv'    using 2:3                                w l lw 3 lt 0 lc 8 t 'Ref Model', \
  'test_references/Model_mins04.csv'           using ($2/0.0737):26                     w l lw 3 lt 7 lc 8 t 'Impermeable Model', \
  'test_references/ParameterStudy/BJ/100p0/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 9 t 'Alpha = 100', \
  'test_references/ParameterStudy/BJ/1p000/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 6 t 'Alpha = 1.0', \
  'test_references/ParameterStudy/BJ/0p010/Model_mins04.csv'    using ($2/0.0737):26    w l lw 3 lt 7 lc 4 t 'Alpha = 0.01'
set xlabel 'v_x/v_{max} at X/H = 1'
set ylabel 'y [m]'
set xrange [-0.1:0.05]
set yrange [0:0.3]
unset key
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
plot \
  'test_references/EXP_Prepoststep.csv'        using ($3):2                             w p ps 2 pt 2 lc 8 t 'Exp Data ', \
  'test_references/wilcoxmodel_Poststep.csv'   using ($3):2                             w l lw 3 lt 0 lc 8 t 'Ref Model', \
  'test_references/Model_plus01.csv'           using (($2/0.0737)):26                   w l lw 3 lt 7 lc 8 t 'Impermeable Model', \
  'test_references/ParameterStudy/BJ/100p0/Model_plus01.csv'    using (($2/0.0737)):26  w l lw 3 lt 7 lc 9 t 'Alpha = 100', \
  'test_references/ParameterStudy/BJ/1p000/Model_plus01.csv'    using (($2/0.0737)):26  w l lw 3 lt 7 lc 6 t 'Alpha = 1.0', \
  'test_references/ParameterStudy/BJ/0p010/Model_plus01.csv'    using (($2/0.0737)):26  w l lw 3 lt 7 lc 4 t 'Alpha = 0.01'
unset multiplot
