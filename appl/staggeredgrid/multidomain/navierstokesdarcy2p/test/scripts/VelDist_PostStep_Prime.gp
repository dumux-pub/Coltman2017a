reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'VelDist_PostStep01_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max} + X'
set ylabel 'y [m]'
set xrange [0.75:2.05]
set yrange [0:3]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($3+1):2         w p ps 2 pt 2 lc 8 t 'Driver Experimental Data x/H=+1', \
  'test_references/wilcoxmodel_Poststep.csv' using ($3+1):2         w l lw 3 lt 0 lc 8 t 'Wilcox Reference Model x/H=+1', \
  'test_references/Model_plus01.csv'      using (($2/0.0737)+1):26  w l lw 3 lt 1 lc 7 t 'K-Omega Model  x/H=+1'

reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'VelDist_PostStep04_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max} + X'
set ylabel 'y [m]'
set xrange [3.75:5.05]
set yrange [0:3]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($4+4):2          w p ps 2 pt 2 lc 8 t 'Driver Experimental Data x/H=+4', \
  'test_references/wilcoxmodel_Poststep.csv' using ($5+4):4          w l lw 3 lt 0 lc 8 t 'Wilcox Reference Model x/H=+4', \
  'test_references/Model_plus04.csv'       using (($2/0.0737)+4):26  w l lw 3 lt 2 lc 7 t 'K-Omega Model  x/H=+4'


reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'VelDist_PostStep06_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max} + X'
set ylabel 'y [m]'
set xrange [5.75:7.05]
set yrange [0:3]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($5+6):2          w p ps 2 pt 2 lc 8 t 'Driver Experimental Data x/H=+6', \
  'test_references/wilcoxmodel_Poststep.csv' using ($7+6):6          w l lw 3 lt 0 lc 8 t 'Wilcox Reference Model x/H=+6', \
  'test_references/Model_plus06.csv'       using (($2/0.0737)+6):26  w l lw 3 lt 3 lc 7 t 'K-Omega Model  x/H=+6'


reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'VelDist_PostStep10_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max} + X'
set ylabel 'y [m]'
set xrange [9.75:11.05]
set yrange [0:3]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($6+10):2          w p ps 2 pt 2 lc 8 t 'Driver Experimental Data x/H=+10', \
  'test_references/wilcoxmodel_Poststep.csv' using ($9+10):8          w l lw 3 lt 0 lc 8 t 'Wilcox Reference Model x/H=+10', \
  'test_references/Model_plus10.csv'       using (($2/0.0737)+10):26  w l lw 3 lt 4 lc 7 t 'K-Omega Model x/H=+10'

reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'
set output 'VelDist_PostStepCombo_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set multiplot layout 1,3 rowsfirst
set xrange [-0.25:1]
set yrange [0:3]
set ylabel 'y [m]'
set xlabel 'v_x/v_{max} at X/H=4'
set key font ",16"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($4):2           w p ps 2 pt 2 lc 1 t 'Exp Data', \
  'test_references/wilcoxmodel_Poststep.csv' using ($5):4           w l lw 3 lt 0 lc 1 t 'Ref Model', \
  'test_references/Model_plus04.csv'       using (($2/0.0737)):26   w l lw 3 lt 2 lc 1 t 'K-Omega Model'
set xrange [-0.15:1]
set yrange [0:3]
set ylabel ''
set xlabel 'v_x/v_{max} at X/H=6'
set key font ",16"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($5):2           w p ps 2 pt 2 lc 2 t 'Exp Data', \
  'test_references/wilcoxmodel_Poststep.csv' using ($7):6           w l lw 3 lt 0 lc 2 t 'Ref Model', \
  'test_references/Model_plus06.csv'       using (($2/0.0737)):26   w l lw 3 lt 3 lc 2 t 'K-Omega Model'
set xrange [-0.05:1]
set yrange [0:3]
set ylabel ''
set xlabel 'v_x/v_{max} at X/H=10'
set key font ",16"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using ($6):2           w p ps 2 pt 2 lc 3 t 'Exp Data', \
  'test_references/wilcoxmodel_Poststep.csv' using ($9):8           w l lw 3 lt 0 lc 3 t 'Ref Model', \
  'test_references/Model_plus10.csv'       using (($2/0.0737)):26   w l lw 3 lt 4 lc 3 t 'K-Omega Model'
unset multiplot
