gnuplot scripts/VelDist_PostStep_Prime.gp
gnuplot scripts/VelDist_PreStep_Prime.gp
gnuplot scripts/FricCof_BaseWall_Prime.gp

gnuplot scripts/VelDist_PostStep_BJ.gp
gnuplot scripts/VelDist_PreStep_BJ.gp
gnuplot scripts/FricCof_BaseWall_BJ.gp

gnuplot scripts/VelDist_PostStep_Perm.gp
gnuplot scripts/VelDist_PreStep_Perm.gp
gnuplot scripts/FricCof_BaseWall_Perm.gp

gnuplot scripts/VelDist_PostStep_Poro.gp
gnuplot scripts/VelDist_PreStep_Poro.gp
gnuplot scripts/FricCof_BaseWall_Poro.gp
