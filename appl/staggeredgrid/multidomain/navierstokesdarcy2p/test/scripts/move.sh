#move impermeable Results
cp results_impermeable/bfscoupling_impermeable_mins04.csv test_references/Model_mins04.csv
cp results_impermeable/bfscoupling_impermeable_plus01.csv test_references/Model_plus01.csv
cp results_impermeable/bfscoupling_impermeable_plus04.csv test_references/Model_plus04.csv
cp results_impermeable/bfscoupling_impermeable_plus06.csv test_references/Model_plus06.csv
cp results_impermeable/bfscoupling_impermeable_plus10.csv test_references/Model_plus10.csv
cp results_impermeable/bfscoupling_impermeable_Wall00.csv test_references/Model_wall00.csv

#move Permeability Results#############################################################################################
cp results_perm/15/bfscoupling_K_15_mins04.csv test_references/ParameterStudy/K/15/Model_mins04.csv
cp results_perm/15/bfscoupling_K_15_plus01.csv test_references/ParameterStudy/K/15/Model_plus01.csv
cp results_perm/15/bfscoupling_K_15_plus04.csv test_references/ParameterStudy/K/15/Model_plus04.csv
cp results_perm/15/bfscoupling_K_15_plus06.csv test_references/ParameterStudy/K/15/Model_plus06.csv
cp results_perm/15/bfscoupling_K_15_plus10.csv test_references/ParameterStudy/K/15/Model_plus10.csv
cp results_perm/15/bfscoupling_K_15_Wall00.csv test_references/ParameterStudy/K/15/Model_wall00.csv

cp results_perm/10/bfscoupling_K_10_mins04.csv test_references/ParameterStudy/K/10/Model_mins04.csv
cp results_perm/10/bfscoupling_K_10_plus01.csv test_references/ParameterStudy/K/10/Model_plus01.csv
cp results_perm/10/bfscoupling_K_10_plus04.csv test_references/ParameterStudy/K/10/Model_plus04.csv
cp results_perm/10/bfscoupling_K_10_plus06.csv test_references/ParameterStudy/K/10/Model_plus06.csv
cp results_perm/10/bfscoupling_K_10_plus10.csv test_references/ParameterStudy/K/10/Model_plus10.csv
cp results_perm/10/bfscoupling_K_10_Wall00.csv test_references/ParameterStudy/K/10/Model_wall00.csv

cp results_perm/09/bfscoupling_K_09_mins04.csv test_references/ParameterStudy/K/09/Model_mins04.csv
cp results_perm/09/bfscoupling_K_09_plus01.csv test_references/ParameterStudy/K/09/Model_plus01.csv
cp results_perm/09/bfscoupling_K_09_plus04.csv test_references/ParameterStudy/K/09/Model_plus04.csv
cp results_perm/09/bfscoupling_K_09_plus06.csv test_references/ParameterStudy/K/09/Model_plus06.csv
cp results_perm/09/bfscoupling_K_09_plus10.csv test_references/ParameterStudy/K/09/Model_plus10.csv
cp results_perm/09/bfscoupling_K_09_Wall00.csv test_references/ParameterStudy/K/09/Model_wall00.csv

cp results_perm/08/bfscoupling_K_08_mins04.csv test_references/ParameterStudy/K/08/Model_mins04.csv
cp results_perm/08/bfscoupling_K_08_plus01.csv test_references/ParameterStudy/K/08/Model_plus01.csv
cp results_perm/08/bfscoupling_K_08_plus04.csv test_references/ParameterStudy/K/08/Model_plus04.csv
cp results_perm/08/bfscoupling_K_08_plus06.csv test_references/ParameterStudy/K/08/Model_plus06.csv
cp results_perm/08/bfscoupling_K_08_plus10.csv test_references/ParameterStudy/K/08/Model_plus10.csv
cp results_perm/08/bfscoupling_K_08_Wall00.csv test_references/ParameterStudy/K/08/Model_wall00.csv

cp results_perm/07/bfscoupling_K_07_mins04.csv test_references/ParameterStudy/K/07/Model_mins04.csv
cp results_perm/07/bfscoupling_K_07_plus01.csv test_references/ParameterStudy/K/07/Model_plus01.csv
cp results_perm/07/bfscoupling_K_07_plus04.csv test_references/ParameterStudy/K/07/Model_plus04.csv
cp results_perm/07/bfscoupling_K_07_plus06.csv test_references/ParameterStudy/K/07/Model_plus06.csv
cp results_perm/07/bfscoupling_K_07_plus10.csv test_references/ParameterStudy/K/07/Model_plus10.csv
cp results_perm/07/bfscoupling_K_07_Wall00.csv test_references/ParameterStudy/K/07/Model_wall00.csv

cp results_perm/06/bfscoupling_K_06_mins04.csv test_references/ParameterStudy/K/06/Model_mins04.csv
cp results_perm/06/bfscoupling_K_06_plus01.csv test_references/ParameterStudy/K/06/Model_plus01.csv
cp results_perm/06/bfscoupling_K_06_plus04.csv test_references/ParameterStudy/K/06/Model_plus04.csv
cp results_perm/06/bfscoupling_K_06_plus06.csv test_references/ParameterStudy/K/06/Model_plus06.csv
cp results_perm/06/bfscoupling_K_06_plus10.csv test_references/ParameterStudy/K/06/Model_plus10.csv
cp results_perm/06/bfscoupling_K_06_Wall00.csv test_references/ParameterStudy/K/06/Model_wall00.csv

cp results_perm/05/bfscoupling_K_05_mins04.csv test_references/ParameterStudy/K/05/Model_mins04.csv
cp results_perm/05/bfscoupling_K_05_plus01.csv test_references/ParameterStudy/K/05/Model_plus01.csv
cp results_perm/05/bfscoupling_K_05_plus04.csv test_references/ParameterStudy/K/05/Model_plus04.csv
cp results_perm/05/bfscoupling_K_05_plus06.csv test_references/ParameterStudy/K/05/Model_plus06.csv
cp results_perm/05/bfscoupling_K_05_plus10.csv test_references/ParameterStudy/K/05/Model_plus10.csv
cp results_perm/05/bfscoupling_K_05_Wall00.csv test_references/ParameterStudy/K/05/Model_wall00.csv

#move Beavers-Joesph Results#############################################################################################
cp results_BJ/100p0/bfscoupling_BJ_100p0_mins04.csv test_references/ParameterStudy/BJ/100p0/Model_mins04.csv
cp results_BJ/100p0/bfscoupling_BJ_100p0_plus01.csv test_references/ParameterStudy/BJ/100p0/Model_plus01.csv
cp results_BJ/100p0/bfscoupling_BJ_100p0_plus04.csv test_references/ParameterStudy/BJ/100p0/Model_plus04.csv
cp results_BJ/100p0/bfscoupling_BJ_100p0_plus06.csv test_references/ParameterStudy/BJ/100p0/Model_plus06.csv
cp results_BJ/100p0/bfscoupling_BJ_100p0_plus10.csv test_references/ParameterStudy/BJ/100p0/Model_plus10.csv
cp results_BJ/100p0/bfscoupling_BJ_100p0_Wall00.csv test_references/ParameterStudy/BJ/100p0/Model_wall00.csv

cp results_BJ/10p00/bfscoupling_BJ_10p00_mins04.csv test_references/ParameterStudy/BJ/10p00/Model_mins04.csv
cp results_BJ/10p00/bfscoupling_BJ_10p00_plus01.csv test_references/ParameterStudy/BJ/10p00/Model_plus01.csv
cp results_BJ/10p00/bfscoupling_BJ_10p00_plus04.csv test_references/ParameterStudy/BJ/10p00/Model_plus04.csv
cp results_BJ/10p00/bfscoupling_BJ_10p00_plus06.csv test_references/ParameterStudy/BJ/10p00/Model_plus06.csv
cp results_BJ/10p00/bfscoupling_BJ_10p00_plus10.csv test_references/ParameterStudy/BJ/10p00/Model_plus10.csv
cp results_BJ/10p00/bfscoupling_BJ_10p00_Wall00.csv test_references/ParameterStudy/BJ/10p00/Model_wall00.csv

cp results_BJ/1p000/bfscoupling_BJ_1p000_mins04.csv test_references/ParameterStudy/BJ/1p000/Model_mins04.csv
cp results_BJ/1p000/bfscoupling_BJ_1p000_plus01.csv test_references/ParameterStudy/BJ/1p000/Model_plus01.csv
cp results_BJ/1p000/bfscoupling_BJ_1p000_plus04.csv test_references/ParameterStudy/BJ/1p000/Model_plus04.csv
cp results_BJ/1p000/bfscoupling_BJ_1p000_plus06.csv test_references/ParameterStudy/BJ/1p000/Model_plus06.csv
cp results_BJ/1p000/bfscoupling_BJ_1p000_plus10.csv test_references/ParameterStudy/BJ/1p000/Model_plus10.csv
cp results_BJ/1p000/bfscoupling_BJ_1p000_Wall00.csv test_references/ParameterStudy/BJ/1p000/Model_wall00.csv

cp results_BJ/0p100/bfscoupling_BJ_0p100_mins04.csv test_references/ParameterStudy/BJ/0p100/Model_mins04.csv
cp results_BJ/0p100/bfscoupling_BJ_0p100_plus01.csv test_references/ParameterStudy/BJ/0p100/Model_plus01.csv
cp results_BJ/0p100/bfscoupling_BJ_0p100_plus04.csv test_references/ParameterStudy/BJ/0p100/Model_plus04.csv
cp results_BJ/0p100/bfscoupling_BJ_0p100_plus06.csv test_references/ParameterStudy/BJ/0p100/Model_plus06.csv
cp results_BJ/0p100/bfscoupling_BJ_0p100_plus10.csv test_references/ParameterStudy/BJ/0p100/Model_plus10.csv
cp results_BJ/0p100/bfscoupling_BJ_0p100_Wall00.csv test_references/ParameterStudy/BJ/0p100/Model_wall00.csv

cp results_BJ/0p010/bfscoupling_BJ_0p010_mins04.csv test_references/ParameterStudy/BJ/0p010/Model_mins04.csv
cp results_BJ/0p010/bfscoupling_BJ_0p010_plus01.csv test_references/ParameterStudy/BJ/0p010/Model_plus01.csv
cp results_BJ/0p010/bfscoupling_BJ_0p010_plus04.csv test_references/ParameterStudy/BJ/0p010/Model_plus04.csv
cp results_BJ/0p010/bfscoupling_BJ_0p010_plus06.csv test_references/ParameterStudy/BJ/0p010/Model_plus06.csv
cp results_BJ/0p010/bfscoupling_BJ_0p010_plus10.csv test_references/ParameterStudy/BJ/0p010/Model_plus10.csv
cp results_BJ/0p010/bfscoupling_BJ_0p010_Wall00.csv test_references/ParameterStudy/BJ/0p010/Model_wall00.csv

cp results_BJ/0p001/bfscoupling_BJ_0p001_mins04.csv test_references/ParameterStudy/BJ/0p001/Model_mins04.csv
cp results_BJ/0p001/bfscoupling_BJ_0p001_plus01.csv test_references/ParameterStudy/BJ/0p001/Model_plus01.csv
cp results_BJ/0p001/bfscoupling_BJ_0p001_plus04.csv test_references/ParameterStudy/BJ/0p001/Model_plus04.csv
cp results_BJ/0p001/bfscoupling_BJ_0p001_plus06.csv test_references/ParameterStudy/BJ/0p001/Model_plus06.csv
cp results_BJ/0p001/bfscoupling_BJ_0p001_plus10.csv test_references/ParameterStudy/BJ/0p001/Model_plus10.csv
cp results_BJ/0p001/bfscoupling_BJ_0p001_Wall00.csv test_references/ParameterStudy/BJ/0p001/Model_wall00.csv

#move Porosity Results#############################################################################################
cp results_poro/1/bfscoupling_phi_1_mins04.csv test_references/ParameterStudy/Phi/1/Model_mins04.csv
cp results_poro/1/bfscoupling_phi_1_plus01.csv test_references/ParameterStudy/Phi/1/Model_plus01.csv
cp results_poro/1/bfscoupling_phi_1_plus04.csv test_references/ParameterStudy/Phi/1/Model_plus04.csv
cp results_poro/1/bfscoupling_phi_1_plus06.csv test_references/ParameterStudy/Phi/1/Model_plus06.csv
cp results_poro/1/bfscoupling_phi_1_plus10.csv test_references/ParameterStudy/Phi/1/Model_plus10.csv
cp results_poro/1/bfscoupling_phi_1_Wall00.csv test_references/ParameterStudy/Phi/1/Model_wall00.csv

cp results_poro/3/bfscoupling_phi_3_mins04.csv test_references/ParameterStudy/Phi/3/Model_mins04.csv
cp results_poro/3/bfscoupling_phi_3_plus01.csv test_references/ParameterStudy/Phi/3/Model_plus01.csv
cp results_poro/3/bfscoupling_phi_3_plus04.csv test_references/ParameterStudy/Phi/3/Model_plus04.csv
cp results_poro/3/bfscoupling_phi_3_plus06.csv test_references/ParameterStudy/Phi/3/Model_plus06.csv
cp results_poro/3/bfscoupling_phi_3_plus10.csv test_references/ParameterStudy/Phi/3/Model_plus10.csv
cp results_poro/3/bfscoupling_phi_3_Wall00.csv test_references/ParameterStudy/Phi/3/Model_wall00.csv

cp results_poro/5/bfscoupling_phi_5_mins04.csv test_references/ParameterStudy/Phi/5/Model_mins04.csv
cp results_poro/5/bfscoupling_phi_5_plus01.csv test_references/ParameterStudy/Phi/5/Model_plus01.csv
cp results_poro/5/bfscoupling_phi_5_plus04.csv test_references/ParameterStudy/Phi/5/Model_plus04.csv
cp results_poro/5/bfscoupling_phi_5_plus06.csv test_references/ParameterStudy/Phi/5/Model_plus06.csv
cp results_poro/5/bfscoupling_phi_5_plus10.csv test_references/ParameterStudy/Phi/5/Model_plus10.csv
cp results_poro/5/bfscoupling_phi_5_Wall00.csv test_references/ParameterStudy/Phi/5/Model_wall00.csv
