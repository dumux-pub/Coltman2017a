reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'VelDist_PreStep_Poro.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max}'
set ylabel 'y [m]'
set xrange [0:1.02]
set yrange [1:3.5]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse center samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'       using 8:7                            w p      pt 2 lc 8 t 'Experimental Data', \
  'test_references/wilcoxmodel_Prestep.csv'   using 2:3                            w l lw 3 lt 0 lc 8 t 'Reference Model', \
  'test_references/Model_mins04.csv'          using ($2/0.0737):26                 w l lw 1 lt 7 lc 8 t 'Impermeable Model', \
  'test_references/ParameterStudy/Phi/1/Model_mins04.csv'  using ($2/0.0737):26    w l lw 3 lt 7 lc 6 t 'Porosity = 0.1', \
  'test_references/ParameterStudy/Phi/3/Model_mins04.csv'  using ($2/0.0737):26    w l lw 3 lt 7 lc 7 t 'Porosity = 0.3', \
  'test_references/ParameterStudy/Phi/5/Model_mins04.csv'  using ($2/0.0737):26    w l lw 3 lt 7 lc 9 t 'Porosity = 0.5'

reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'VelDist_Poro_combo.png'
set xlabel font ",20"
set ylabel font ",20"
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set multiplot layout 1,2 rowsfirst
set xrange [-0.25:1.05]
set yrange [1:5]
set ylabel 'y [m]'
set xlabel 'v_x/v_{max} at X/H=-4'
set key font ",16"
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'                    using 8:7               w p      pt 2 lc 8 t 'Experimental Data', \
  'test_references/wilcoxmodel_Prestep.csv'                using 2:3               w l lw 3 lt 0 lc 8 t 'Reference Model', \
  'test_references/Model_mins04.csv'                       using ($2/0.0737):26    w l lw 1 lt 7 lc 8 t 'Impermeable Model', \
  'test_references/ParameterStudy/Phi/1/Model_mins04.csv'  using ($2/0.0737):26    w l lw 3 lt 7 lc 6 t 'Porosity = 0.1', \
  'test_references/ParameterStudy/Phi/3/Model_mins04.csv'  using ($2/0.0737):26    w l lw 3 lt 7 lc 7 t 'Porosity = 0.3', \
  'test_references/ParameterStudy/Phi/5/Model_mins04.csv'  using ($2/0.0737):26    w l lw 3 lt 7 lc 9 t 'Porosity = 0.5'
set xrange [-0.25:1.05]
set yrange [0:3]
set ylabel 'y [m]'
set xlabel 'v_x/v_{max} at X/H=1'
unset key
plot \
  'test_references/EXP_Prepoststep.csv'                   using 3:2               w p ps 2 pt 2 lc 8 t 'Experimental Data', \
  'test_references/wilcoxmodel_Poststep.csv'              using 3:2               w l lw 3 lt 0 lc 8 t 'Reference Model', \
  'test_references/Model_plus01.csv'                      using (($2/0.0737)):26  w l lw 3 lt 7 lc 8 t 'Impermeable Model', \
  'test_references/ParameterStudy/Phi/1/Model_plus01.csv' using (($2/0.0737)):26  w l lw 3 lt 7 lc 6 t 'Porosity = 0.1', \
  'test_references/ParameterStudy/Phi/3/Model_plus01.csv' using (($2/0.0737)):26  w l lw 3 lt 7 lc 7 t 'Porosity = 0.3', \
  'test_references/ParameterStudy/Phi/5/Model_plus01.csv' using (($2/0.0737)):26  w l lw 3 lt 7 lc 9 t 'Porosity = 0.5'
unset multiplot
