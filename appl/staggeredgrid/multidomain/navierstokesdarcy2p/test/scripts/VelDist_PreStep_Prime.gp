reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'VelDist_PreStep_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'v_x/v_{max}'
set ylabel 'y [m]'
set xrange [0:1.02]
set yrange [1:3.5]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set key font ",20"
set key left Left reverse center samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'         using 8:7               w p ps 2 pt 2 lc 8 t 'Driver Experimental Data x/H=-4', \
  'test_references/wilcoxmodel_Prestep.csv'     using 2:3               w l lw 3 lt 0 lc 8 t 'Wilcox Reference Model x/H=-4', \
  'test_references/Model_mins04.csv'            using ($2/0.0737):26    w l lw 3 lt 7 lc 7 t 'K-Omega Model X/H = -4'
