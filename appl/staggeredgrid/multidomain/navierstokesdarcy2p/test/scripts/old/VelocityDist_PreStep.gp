reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'test_references/VelocityProfiles_PreStep.png'
set xlabel '$v_x/v_{max}$'
set ylabel '$y [m]$'
set xrange [0:1.02]
set yrange [1:3.5]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set title 'Velocity Profile at X/H=-4 (Before Step)'
set key left Left reverse center samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'         using 3:2               w p      pt 2 lc 7 t 'Experimental Data x/H=-4', \
  'test_references/wilcoxmodel_Prestep.csv'     using 2:3               w l lw 3 lt 0 lc 7 t 'Wilcox Reference Model x/H=-4', \
  'test_references/Mod_PreStep04.csv'           using ($2/0.0737):26    w l lw 1 lt 7 lc 7 t 'K-Omega Model X/H = -4'
