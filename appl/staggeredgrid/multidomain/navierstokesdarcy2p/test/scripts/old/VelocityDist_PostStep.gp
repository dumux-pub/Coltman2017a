reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'test_references/VelocityProfiles_PostStep.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.6:1.05]
set yrange [0:3]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set title 'Velocity Profile at X/H=1 (After Step)'
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using 3:2             w p      pt 2 lc 1 t 'Experimental Data x/H=+1', \
  'test_references/wilcoxmodel_Poststep.csv' using 3:2             w l lw 3 lt 0 lc 1 t 'Wilcox Reference Model x/H=+1', \
  'test_references/Mod_PostStep01.csv'       using ($2/0.0737):26  w l lw 1 lt 1 lc 1 t 'K-W Model Results  x/H=+1', \
\
  'test_references/EXP_Prepoststep.csv'      using 4:2             w p      pt 2 lc 2 t 'Experimental Data x/H=+4', \
  'test_references/wilcoxmodel_Poststep.csv' using 5:4             w l lw 3 lt 0 lc 2 t 'Wilcox Reference Model x/H=+4', \
  'test_references/Mod_PostStep04.csv'       using ($2/0.0737):26  w l lw 1 lt 2 lc 2 t 'K-W Model Results  x/H=+4', \
\
  'test_references/EXP_Prepoststep.csv'      using 5:2             w p      pt 2 lc 3 t 'Experimental Data x/H=+6', \
  'test_references/wilcoxmodel_Poststep.csv' using 7:6             w l lw 3 lt 0 lc 3 t 'Wilcox Reference Model x/H=+6', \
  'test_references/Mod_PostStep06.csv'       using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-W Model Results  x/H=+6', \
\
  'test_references/EXP_Prepoststep.csv'      using 6:2             w p      pt 2 lc 4 t 'Experimental Data x/H=+10', \
  'test_references/wilcoxmodel_Poststep.csv' using 9:8             w l lw 3 lt 0 lc 4 t 'Wilcox Reference Model x/H=+10', \
  'test_references/Mod_PostStep10.csv'       using ($2/0.0737):26  w l lw 1 lt 4 lc 4 t 'K-W Model Results x/H=+10'
