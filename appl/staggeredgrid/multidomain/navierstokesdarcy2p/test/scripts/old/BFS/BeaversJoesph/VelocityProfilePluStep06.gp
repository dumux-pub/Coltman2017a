reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'BJ_VelocityProfilePluStep06.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.2:1.1]
set yrange [0:3]
set title 'Velocity Profile at X/H=6 (After Step)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 5:2 w l ls 7 t 'Reference x/H=+6', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_0p1_plus06.csv' u ($2/0.078):26 w l lt 1 t 'BJ Coefficient= 0.1', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_001_plus06.csv' u ($2/0.078):26 w l lt 3 t 'BJ Coefficient= 1.0', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_050_plus06.csv' u ($2/0.078):26 w l lt 2 t 'BJ Coefficient= 50', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_100_plus06.csv' u ($2/0.078):26 w l lt 4 t 'BJ Coefficient= 100'

