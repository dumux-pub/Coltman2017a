reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'Poro_VelocityProfileMinStep04.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [0:1.1]
set yrange [1:5]
set title 'Velocity Profile at X/H=-4 (Before Step)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 8:7   w l ls 7 t 'Reference x/H=-4', \
  DATA.'BFS_results/poro/bfscoupling_phi05_mins04.csv' u ($2/$19):26 w l lt 1 t 'Porosity = 0.5', \
  DATA.'BFS_results/poro/bfscoupling_phi04_mins04.csv' u ($2/$19):26 w l lt 2 t 'Porosity = 0.4', \
  DATA.'BFS_results/poro/bfscoupling_phi03_mins04.csv' u ($2/$19):26 w l lt 5 t 'Porosity = 0.3', \
  DATA.'BFS_results/poro/bfscoupling_phi02_mins04.csv' u ($2/$19):26 w l lt 3 t 'Porosity = 0.2', \
  DATA.'BFS_results/poro/bfscoupling_phi01_mins04.csv' u ($2/$19):26 w l lt 4 t 'Porosity = 0.1'






