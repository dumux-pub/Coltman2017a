reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'BJ_VelocityProfileMinStep04.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [0:1.1]
set yrange [1:5]
set title 'Velocity Profile at X/H=-4 (Before Step)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 8:7   w l ls 7 t 'Reference x/H=-4', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_0p1_mins04.csv' u ($2/$19):26 w l lt 1 t 'BJ Coefficient= 0.1', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_001_mins04.csv' u ($2/$19):26 w l lt 3 t 'BJ Coefficient= 1.0', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_050_mins04.csv' u ($2/$19):26 w l lt 2 t 'BJ Coefficient= 50', \
  DATA.'BFS_results/BJ/bfscoupling_BJ_100_mins04.csv' u ($2/$19):26 w l lt 4 t 'BJ Coefficient= 100'









