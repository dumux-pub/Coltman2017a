reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'K_VelocityProfilePluStep06.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.2:1.1]
set yrange [0:3]
set title 'Velocity Profile at X/H=6 (After Step)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 5:2 w l ls 7 t 'Reference x/H=6', \
  DATA.'BFS_results/perm/bfscoupling_K_20_plus06.csv' u ($2/0.078):26 w l lt 1 t 'Permeability = 1e-20', \
  DATA.'BFS_results/perm/bfscoupling_K_15_plus06.csv' u ($2/0.078):26 w l lt 3 t 'Permeability = 1e-15', \
  DATA.'BFS_results/perm/bfscoupling_K_10_plus06.csv' u ($2/0.078):26 w l lt 2 t 'Permeability = 1e-10', \
  DATA.'BFS_results/perm/bfscoupling_K_05_plus06.csv' u ($2/0.065):26 w l lt 4 t 'Permeability = 1e-05'
