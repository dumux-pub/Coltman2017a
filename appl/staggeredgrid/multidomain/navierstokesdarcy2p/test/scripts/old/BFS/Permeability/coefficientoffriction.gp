reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'K_CoefficientOfFriction.png'
set xlabel '$x [m]$'
set ylabel '$Coefficient of Friction C_{f}$'
set xrange [-4:30]
set yrange [-0.001:0.0035]
set title 'C_f'
set key left Left reverse center samplen 1
plot \
  'test_references/FrictionCoefficient.csv' using 1:2 w l ls 7 t 'Reference Model', \
  DATA.'BFS_results/perm/bfscoupling_K_20_Wall00.csv' u ($25-110):((($14)*($6+$8))/(0.5*0.075*0.075)) w l lt 1 t 'Permeability = 1e-20', \
  DATA.'BFS_results/perm/bfscoupling_K_15_Wall00.csv' u ($25-110):((($14)*($6+$8))/(0.5*0.075*0.075)) w l lt 3 t 'Permeability = 1e-15', \
  DATA.'BFS_results/perm/bfscoupling_K_10_Wall00.csv' u ($25-110):((($14)*($6+$8))/(0.5*0.075*0.075)) w l lt 2 t 'Permeability = 1e-10', \
  DATA.'BFS_results/perm/bfscoupling_K_05_Wall00.csv' u ($25-110):((($14)*($6+$8))/(0.5*0.065*0.065)) w l lt 4 t 'Permeability = 1e-05'


# Coefficient of Friction is calculated via ShearStress_wall/(0.5*Density*ReferenceVelocity^2)
# In this case the ShearStress_wall is calculated via the equations supplied in the K-Omega Model.
# ShearStress_wall is calculated via (density*K/Omega)*(frac{del{U_i}}{del{X_j}}+frac{del{U_j}}{del{X_i}})
# $6    velocityGradients0:1 ---> (frac{del{U_i}}{del{X_j}})
# $8    velocityGradients1:0 ---> (frac{del{U_j}}{del{X_i}})
# $21   k
# $22   omega
# But, as omega should be infinte at the wall, we use the kinematic viscosity instead
#
# Total Numerator = ($14)*($6+$8)
#
# Density is stored in column 13, but is not required as it is found in both the numerator and the denominator
# ReferenceVelocity must be guessed at originally, and corrected for after implementation is through (0.075 [m/s])
# Total Denominator = (0.5*0.198*0.198)

#Total Arguement = ($14)*($6+$8)/(0.5*0.075*0.075)
