reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'test_references/Convergence/frictioncoeff.png'
set xlabel '$x [m]$'
set ylabel '$Coefficient of Friction C_{f}$'
set xrange [0:40]
set yrange [-0.00135:0.0042]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set title 'C_f'
set key left Left reverse center samplen 1
plot \
 'test_references/EXP_FrictionCoefficient.csv' using 1:2             w p      pt 2 lc 7 t 'Experimental Data', \
 'test_references/wilcoxmodel_Cf.csv'          using 2:3             w l lw 3 lt 0 lc 7 t 'Wilcox Reference Model', \
#results from convergence test
 'results_00_20/BFS_convergence_00_20_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.00 & 1.20', \
 'results_00_25/BFS_convergence_00_25_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.00 & 1.25', \
 'results_00_30/BFS_convergence_00_30_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.00 & 1.30', \
 'results_10_20/BFS_convergence_10_20_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.10 & 1.20', \
 'results_10_25/BFS_convergence_10_25_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.10 & 1.25', \
 'results_10_30/BFS_convergence_10_30_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.10 & 1.30', \
 'results_20_20/BFS_convergence_20_20_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.20 & 1.20', \
 'results_20_25/BFS_convergence_20_25_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.20 & 1.25', \
 'results_20_30/BFS_convergence_20_30_Wall00.csv' using ($14)*($6+$8)/(-0.5*0.0737*.0737)  w l lw 1 lt 1 lc 7 t 'K-Omega Grading 1.20 & 1.30'

# Coefficient of Friction is calculated via ShearStress_wall/(0.5*Density*ReferenceVelocity^2)
# In this case the ShearStress_wall is calculated via the equations supplied in the K-Omega Model.
# ShearStress_wall is calculated via (density*K/Omega)*(frac{del{U_i}}{del{X_j}}+frac{del{U_j}}{del{X_i}})
# $6    velocityGradients0:1 ---> (frac{del{U_i}}{del{X_j}})
# $8    velocityGradients1:0 ---> (frac{del{U_j}}{del{X_i}})
# $21   k
# $22   omega
# $14   viscosity
# $15   eddy viscosity
# But, as omega should be infinte at the wall, we use the kinematic viscosity instead
#
# Total Numerator = ($14)*($6+$8)
#
# Density is stored in column 13, but is not required as it is found in both the numerator and the denominator
# ReferenceVelocity must be guessed at originally, and corrected for after implementation is through (0.075 [m/s])
# Total Denominator = (0.5*0.198*0.198)

#Total Arguement = ($14)*($6+$8)/(0.5*0.075*0.075)
