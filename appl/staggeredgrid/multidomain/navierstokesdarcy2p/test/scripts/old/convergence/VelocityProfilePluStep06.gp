reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'test_references/Convergence/V_step_plus06.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.6:1.05]
set yrange [0:3]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set title 'Velocity Profile at X/H=1 (After Step)'
set key left Left reverse top samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'      using 5:2             w p      pt 2 lc 3 t 'Experimental Data x/H=+6', \
  'test_references/wilcoxmodel_Poststep.csv' using 7:6             w l lw 3 lt 0 lc 3 t 'Wilcox Reference Model x/H=+6', \
# results from convergence test
  'results_00_20/BFS_convergence_00_20_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.00 & 1.20', \
  'results_00_25/BFS_convergence_00_25_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.00 & 1.25', \
  'results_00_30/BFS_convergence_00_30_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.00 & 1.30', \
  'results_10_20/BFS_convergence_10_20_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.10 & 1.20', \
  'results_10_25/BFS_convergence_10_25_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.10 & 1.25', \
  'results_10_30/BFS_convergence_10_30_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.10 & 1.30', \
  'results_20_20/BFS_convergence_20_20_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.20 & 1.20', \
  'results_20_25/BFS_convergence_20_25_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.20 & 1.25', \
  'results_20_30/BFS_convergence_20_30_plus06.csv' using ($2/0.0737):26  w l lw 1 lt 3 lc 3 t 'K-Omega Grading 1.20 & 1.30'
