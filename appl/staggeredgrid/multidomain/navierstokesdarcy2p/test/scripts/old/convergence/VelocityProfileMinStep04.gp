reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'test_references/Convergence/V_step_mins04.png'
set xlabel '$v_x/v_{max}$'
set ylabel '$y [m]$'
set xrange [0:1.02]
set yrange [1:3.5]
set grid ytics lc 9 lw 1 lt 0
set grid xtics lc 9 lw 1 lt 0
set title 'Velocity Profile at X/H=-4 (Before Step)'
set key left Left reverse center samplen 1
plot \
  'test_references/EXP_Prepoststep.csv'     using 3:2             w p      pt 2 lc 7 t 'Experimental Data x/H=-4', \
  'test_references/wilcoxmodel_Prestep.csv' using 2:3             w l lw 3 lt 0 lc 7 t 'Wilcox Reference Model x/H=-4', \
#results from convergence test
 'results_00_20/BFS_convergence_00_20_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.00 & 1.20', \
 'results_00_25/BFS_convergence_00_25_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.00 & 1.25', \
 'results_00_30/BFS_convergence_00_30_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.00 & 1.30', \
 'results_10_20/BFS_convergence_10_20_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.10 & 1.20', \
 'results_10_25/BFS_convergence_10_25_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.10 & 1.25', \
 'results_10_30/BFS_convergence_10_30_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.10 & 1.30', \
 'results_20_20/BFS_convergence_20_20_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.20 & 1.20', \
 'results_20_25/BFS_convergence_20_25_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.20 & 1.25', \
 'results_20_30/BFS_convergence_20_30_mins04.csv' using ($2/0.0737):26  w l lw 1 lt 7 lc 7 t 'K-Omega Grading 1.20 & 1.30'
