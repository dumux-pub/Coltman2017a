#pass results to create plots

#results for Porosity Parameter Review
gnuplot scripts/BFS/Porosity/VelocityProfileMinStep20.gp
gnuplot scripts/BFS/Porosity/VelocityProfilePluStep01.gp
gnuplot scripts/BFS/Porosity/VelocityProfilePluStep04.gp
gnuplot scripts/BFS/Porosity/VelocityProfilePluStep06.gp
gnuplot scripts/BFS/Porosity/VelocityProfilePluStep10.gp
gnuplot scripts/BFS/Porosity/coefficientoffriction.gp

#results for Permeability Parameter Review
gnuplot scripts/BFS/Permeability/VelocityProfileMinStep20.gp
gnuplot scripts/BFS/Permeability/VelocityProfilePluStep01.gp
gnuplot scripts/BFS/Permeability/VelocityProfilePluStep04.gp
gnuplot scripts/BFS/Permeability/VelocityProfilePluStep06.gp
gnuplot scripts/BFS/Permeability/VelocityProfilePluStep10.gp
gnuplot scripts/BFS/Permeability/coefficientoffriction.gp

#results for BeaversJoesph
gnuplot scripts/BFS/BeaversJoesph/VelocityProfileMinStep20.gp
gnuplot scripts/BFS/BeaversJoesph/VelocityProfilePluStep01.gp
gnuplot scripts/BFS/BeaversJoesph/VelocityProfilePluStep04.gp
gnuplot scripts/BFS/BeaversJoesph/VelocityProfilePluStep06.gp
gnuplot scripts/BFS/BeaversJoesph/VelocityProfilePluStep10.gp
gnuplot scripts/BFS/BeaversJoesph/coefficientoffriction.gp
