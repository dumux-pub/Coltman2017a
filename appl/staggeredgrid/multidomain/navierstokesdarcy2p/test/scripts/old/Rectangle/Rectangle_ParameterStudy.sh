# BACKWARD FACING STEP TEST

# set data separator and locate the extract line script
  DATA="."
  VTU2CSV="/var/run/media/nedc/workspace10/MastersThesis/dumux/dumux/bin/postprocessing/extractlinedata.py"

# Run simulation Function
  runSimulation() {
# run bfs test. Input= input file. Porosity= 1st PosPar ($1). Permeability= 2nd PosPar ($2). append all to outputfile.
    ./test_rectanglecoupling_komega $INPUT -SpatialParams.Porosity "$1" -SpatialParams.Permeability "$2" -SpatialParams.AlphaBJ "$3" -Problem.Name "$4" | tee -a logfile.out

# searches output files for the last .vtu file. when located, stores file name in "input"
    input=`ls -ltr *Secondary*vtu | tail -n 1 | awk '{print $9}'`
    #ls -ltr sorts directory for files with Secondary in them and vtu at the end. Most recent files are printed at the bottom.
    #tail -n 1 selects the last file (at the bottom), awk '{print $9}' prints the 9th parameter in this line.
    # $1 is permissions, $2 is "1", $3 is "user", $4 is "systemname", $5 is size of file, $6$7$8 are date and time,
    # $9 is file name
    echo "$input" -> "$2" | tee -a logfile.out
    #


#define locations of lines for vtu -> csv data collection. "step" is x coordinate of step.
    step=0.18 # x-Coordinate of Step
#5 data lines, x/H = -4, +1, +4, +6, +10
    stepmins100=`echo "scale=8; $step - 0.10" | bc -l`
    stepmins080=`echo "scale=8; $step - 0.08" | bc -l`
    stepmins040=`echo "scale=8; $step - 0.04" | bc -l`
    stepplus010=`echo "scale=8; $step + 0.01" | bc -l`
    stepplus040=`echo "scale=8; $step + 0.04" | bc -l`
    stepplus080=`echo "scale=8; $step + 0.08" | bc -l`

#Create x,y,z coordinates for linedata extraction
   $P1_mins100="$stepmins100 0.0 0.0"
   $P2_mins100="$stepmins100 0.4 0.0"
   $P1_mins080="$stepmins080 0.0 0.0"
   $P2_mins080="$stepmins080 0.4 0.0"
   $P1_mins040="$stepmins040 0.0 0.0"
   $P2_mins040="$stepmins040 0.4 0.0"
   $P1_plus010="$stepplus010 0.0 0.0"
   $P2_plus010="$stepplus010 0.4 0.0"
   $P1_plus040="$stepplus040 0.0 0.0"
   $P2_plus040="$stepplus040 0.4 0.0"
   $P1_plus080="$stepplus080 0.0 0.0"
   $P2_plus080="$stepplus080 0.4 0.0"

# extract csv files from VTU using extractlinedata.py
  # -f=vtu file,        -o=outputdirectory,     -of=outputfile  basename,
  # -p1=point1(x y z),  -p2=point2(x y z),
  # -v=verbosity        -r resolution
  # "| tee -a" appends all output to an external logfile
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_mins100" -p1 $P1_mins100 -p2 $P2_mins100 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_mins080" -p1 $P1_mins080 -p2 $P2_mins080 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_mins040" -p1 $P1_mins040 -p2 $P2_mins040 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus010" -p1 $P1_plus010 -p2 $P2_plus010 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus040" -p1 $P1_plus040 -p2 $P2_plus040 -v 2 -r 10000 | tee -a logfile.out
    pvpython $VTU2CSV -f $input -o "$DATA" -of "$4""_plus080" -p1 $P1_plus080 -p2 $P2_plus080 -v 2 -r 10000 | tee -a logfile.out

mv *_phi_*.vtu *_phi_*.pvd *_phi_*.csv Rectangle_results/poro/
mv *_K_*.vtu *_K_*.pvd *_K_*.csv Rectangle_results/perm/
mv *_BJ_*.vtu *_BJ_*.pvd *_BJ_*.csv Rectangle_results/perm/

#pass results to create plots

#results for Porosity Parameter Review
gnuplot scripts/Rectangle/Porosity/VelocityProfileminstep100.gp
gnuplot scripts/Rectangle/Porosity/VelocityProfileminstep080.gp
gnuplot scripts/Rectangle/Porosity/VelocityProfileminstep040.gp
gnuplot scripts/Rectangle/Porosity/VelocityProfileplustep010.gp
gnuplot scripts/Rectangle/Porosity/VelocityProfileplustep040.gp
gnuplot scripts/Rectangle/Porosity/VelocityProfileplustep080.gp
#results for Permeability Parameter Review
gnuplot scripts/Rectangle/Permeability/VelocityProfileminstep100.gp
gnuplot scripts/Rectangle/Permeability/VelocityProfileminstep080.gp
gnuplot scripts/Rectangle/Permeability/VelocityProfileminstep040.gp
gnuplot scripts/Rectangle/Permeability/VelocityProfileplustep010.gp
gnuplot scripts/Rectangle/Permeability/VelocityProfileplustep040.gp
gnuplot scripts/Rectangle/Permeability/VelocityProfileplustep080.gp
#results for BeaversJoesph
gnuplot scripts/Rectangle/BeaversJoesph/VelocityProfileminstep100.gp
gnuplot scripts/Rectangle/BeaversJoesph/VelocityProfileminstep080.gp
gnuplot scripts/Rectangle/BeaversJoesph/VelocityProfileminstep040.gp
gnuplot scripts/Rectangle/BeaversJoesph/VelocityProfileplustep010.gp
gnuplot scripts/Rectangle/BeaversJoesph/VelocityProfileplustep040.gp
gnuplot scripts/Rectangle/BeaversJoesph/VelocityProfileplustep080.gp

    }
#end function

#run Backward Facing Step Problem
  INPUT=test_rectanglecoupling.input
    #porosity test
      runSimulation 0.5 1e-20 1 Rectangle_phi_05
      runSimulation 0.4 1e-20 1 Rectangle_phi_04
      runSimulation 0.3 1e-20 1 Rectangle_phi_03
      runSimulation 0.2 1e-20 1 Rectangle_phi_02
      runSimulation 0.1 1e-20 1 Rectangle_phi_01
    #permeability test
      runSimulation 0.4 1e-20 1 Rectangle_K_20
      runSimulation 0.4 1e-15 1 Rectangle_K_15
      runSimulation 0.4 1e-10 1 Rectangle_K_10
      runSimulation 0.4 1e-05 1 Rectangle_K_05

    #permeability test
      runSimulation 0.4 1e-20 0.100 Rectangle_BJ_0p1
      runSimulation 0.4 1e-20 1.000 Rectangle_BJ_001
      runSimulation 0.4 1e-20 50.00 Rectangle_BJ_050
      runSimulation 0.4 1e-20 100.0 Rectangle_BJ_100


















