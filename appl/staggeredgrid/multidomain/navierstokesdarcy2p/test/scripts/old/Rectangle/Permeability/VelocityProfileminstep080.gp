reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'K_VelocityProfilePluStep01.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.2:1.1]
set yrange [0:3]
set title 'Velocity Profile at X/H=1 (After Block)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 3:2   w l ls 2 t 'Reference x/H=1', \
  DATA.'bfscoupling_K_20_plus01.csv' u ($2/0.078):26 w l lt 1 t 'K=1e-20', \
  DATA.'bfscoupling_K_15_plus01.csv' u ($2/0.078):26 w l lt 2 t 'K=1e-15', \
  DATA.'bfscoupling_K_10_plus01.csv' u ($2/0.078):26 w l lt 3 t 'K=1e-10', \
  DATA.'bfscoupling_K_05_plus01.csv' u ($2/0.065):26 w l lt 3 t 'K=1e-15'