reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'Poro_VelocityProfilePluStep04.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.2:1.1]
set yrange [0:3]
set title 'Velocity Profile at X/H=4 (After Block)'
set key left Left reverse center samplen 1
plot \
  DATA.'bfscoupling_phi05_mins100.csv' u 2:26 w l lt 1 t 'Porosity = 0.5', \
  DATA.'bfscoupling_phi04_mins100.csv' u 2:26 w l lt 2 t 'Porosity = 0.4', \
  DATA.'bfscoupling_phi03_mins100.csv' u 2:26 w l lt 3 t 'Porosity = 0.3', \
  DATA.'bfscoupling_phi02_mins100.csv' u 2:26 w l lt 2 t 'Porosity = 0.2', \
  DATA.'bfscoupling_phi01_mins100.csv' u 2:26 w l lt 3 t 'Porosity = 0.1', \
