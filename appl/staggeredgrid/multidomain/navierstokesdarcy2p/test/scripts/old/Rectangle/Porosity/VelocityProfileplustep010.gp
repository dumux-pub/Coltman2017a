reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'Poro_VelocityProfilePluStep06.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [-0.2:1.1]
set yrange [0:3]
set title 'Velocity Profile at X/H=6 (After Block)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 5:2 w l ls 6 t 'Reference x/H=6', \
  DATA.'bfscoupling_phi05_plus06.csv' u ($2/0.078):26 w l lt 1 t 'Porosity = 0.5' , \
  DATA.'bfscoupling_phi04_plus06.csv' u ($2/0.078):26 w l lt 2 t 'Porosity = 0.4', \
  DATA.'bfscoupling_phi03_plus06.csv' u ($2/0.078):26 w l lt 3 t 'Porosity = 0.3', \
  DATA.'bfscoupling_phi02_plus06.csv' u ($2/0.078):26 w l lt 2 t 'Porosity = 0.2', \
  DATA.'bfscoupling_phi01_plus06.csv' u ($2/0.078):26 w l lt 3 t 'Porosity = 0.1'