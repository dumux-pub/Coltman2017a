reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'K_VelocityProfileMinStep04.png'
set xlabel '$v_x/v_max$'
set ylabel '$y [m]$'
set xrange [0:1.1]
set yrange [1:5]
set title 'Velocity Profile at X/H=-4 (Before Block)'
set key left Left reverse center samplen 1
plot \
  'test_references/U_y_prepoststep.csv' using 8:7   w l ls 5 t 'Reference x/H=-4', \
  DATA.'bfscoupling_K_20_mins04.csv' u ($2/$19):26 w l lt 1 t 'K=1e-20', \
  DATA.'bfscoupling_K_15_mins04.csv' u ($2/$19):26 w l lt 2 t 'K=1e-15', \
  DATA.'bfscoupling_K_10_mins04.csv' u ($2/$19):26 w l lt 3 t 'K=1e-10', \
  DATA.'bfscoupling_K_05_mins04.csv' u ($2/$19):26 w l lt 3 t 'K=1e-15'








