// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_CONSERVATION_STOKES_SUBPROBLEM_HH
#define DUMUX_CONSERVATION_STOKES_SUBPROBLEM_HH

#if COUPLING_KOMEGA                             //use K Omega Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegaproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegapropertydefaults.hh>
#elif COUPLING_KEPSILON                         //use K Epsilon Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonpropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON                    //use K LowReEpsilon Model for free flow
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonpropertydefaults.hh>
#elif COUPLING_ONEEQ                            //use one-EQ Model for free flow
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmarasproblem.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmaraspropertydefaults.hh>
#elif COUPLING_ZEROEQ                           //use K Zero-EQ Model for free flow
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqproblem.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqpropertydefaults.hh>
#else                                           //Otherwise, only NS without turbulence
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokespropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/navierstokesdarcy2p/properties.hh>

namespace Dumux
{
template <class TypeTag>
class ConservationStokesSubProblem;

namespace Properties
{
// Use pressure constraints
SET_BOOL_PROP(StokesSubProblem, FixPressureConstraints, false);

// Use complete Navier-Stokes equation
SET_BOOL_PROP(StokesSubProblem, ProblemEnableNavierStokes, false);

// Disable gravity field
SET_BOOL_PROP(StokesSubProblem, ProblemEnableGravity, false);
}

template <class TypeTag>
class ConservationStokesSubProblem
#if COUPLING_KOMEGA
  : public KOmegaProblem<TypeTag>
#elif COUPLING_KEPSILON
  : public KEpsilonProblem<TypeTag>
#elif COUPLING_LOWREKEPSILON
  : public LowReKEpsilonProblem<TypeTag>
#elif COUPLING_ONEEQ
  : public OneEqProblem<TypeTag>
#elif COUPLING_ZEROEQ
  : public ZeroEqProblem<TypeTag>
#else
  : public NavierStokesProblem<TypeTag>
#endif
{
#if COUPLING_KOMEGA
    using ParentType = KOmegaProblem<TypeTag>;
#elif COUPLING_KEPSILON
    using ParentType = KEpsilonProblem<TypeTag>;
#elif COUPLING_LOWREKEPSILON
    using ParentType = LowReKEpsilonProblem<TypeTag>;
#elif COUPLING_ONEEQ
    using ParentType = OneEqProblem<TypeTag>;
#elif COUPLING_ZEROEQ
    using ParentType = ZeroEqProblem<TypeTag>;
#else
    using ParentType = NavierStokesProblem<TypeTag>;
#endif
    using MultiDomainTypeTag = typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using DimVector = typename GET_PROP_TYPE(TypeTag, DimVector);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    enum { dim = GridView::dimension };

public:
    ConservationStokesSubProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Velocity);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Pressure);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Temperature);

        // exclude Darcy domain from bounding box
        bBoxMinFiltered_ = ParentType::bBoxMin();
        bBoxMinFiltered_[dim-1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);
        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    {
        return !bcVelocityIsInflow(global)
               && !bcVelocityIsCoupling(global);
    }
    bool bcVelocityIsInflow(const DimVector& global) const
    {
        return (global[dim-1] > this->bBoxMax()[dim-1] - eps_)
               && !isOnCouplingFace(global);
    }
    bool bcVelocityIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Pressure boundary condition types
    bool bcPressureIsOutflow(const DimVector& global) const
    {
        return !bcPressureIsCoupling(global);
    }
    bool bcPressureIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (dim == 1)
            y[0] = velocity_;
        else // dim == 2
            y[1] = 4.0 * velocity_ * global[0] * (this->bBoxMax()[0] - global[0])
                   / (this->bBoxMax()[0] - this->bBoxMin()[0])
                   / (this->bBoxMax()[0] - this->bBoxMin()[0]);
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    {
        return pressure_;
    }
    Scalar massMoleFracAtPos(const DimVector& global) const
    {
        return 0.0;
    }
    Scalar temperatureAtPos(const DimVector& global) const
    {
        return temperature_;
    }

#if (COUPLING_KOMEGA || COUPLING_KEPSILON || COUPLING_LOWREKEPSILON)
    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }

    //! \brief TurbulentKineticEnergy boundary condition values
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
        if ((bcTurbulentKineticEnergyIsInflow(global) && isOnBoundary(global))
            || this->timeManager().time() < eps_)
        {
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, TurbulentKineticEnergy);
        }
        return 0.0;
    }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }

    //! \brief Dissipation boundary condition values
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
        if ((bcDissipationIsInflow(global) && isOnBoundary(global))
            || this->timeManager().time() < eps_)
        {
            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Dissipation);
        }
        return 0.0;
    }
#endif

    /*!
     * \brief The coordinate of the corner of the GridView's bounding
     *        box with the smallest values.
     *
     * Filter out Darcy part
     */
    const DimVector &bBoxMin() const
    {
        return bBoxMinFiltered_;
    }

    //! \brief Returns whether we are on a coupling face
    const bool isOnCouplingFace(const DimVector& global) const
    {
        if (dim == 1)
            return global[0] < bBoxMinFiltered_[0] + eps_;
        else // dim == 2
            return global[dim-1] < bBoxMinFiltered_[dim-1] + eps_
                  && global[0] > darcyXLeft_
                  && global[0] < darcyXRight_;
    }

    //! \brief Return whether a point is located on the boundary of the domain
    bool isOnBoundary(const DimVector& global) const
    {
        return global[0] < this->bBoxMin()[0] + eps_
               || global[0] > this->bBoxMax()[0] - eps_
               || global[1] < this->bBoxMin()[1] + eps_
               || global[1] > this->bBoxMax()[1] - eps_
               || isOnCouplingFace(global);
    }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;
    DimVector bBoxMinFiltered_;
    Scalar darcyXLeft_;
    Scalar darcyXRight_;

    Scalar velocity_;
    Scalar pressure_;
    Scalar temperature_;
};

} //end namespace

#endif // DUMUX_CONSERVATION_STOKES_SUBPROBLEM_HH
