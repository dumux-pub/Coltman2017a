#include <config.h>

#undef DUNE_MINIMAL_DEBUG_LEVEL
#define DUNE_MINIMAL_DEBUG_LEVEL 6

#define USE_SUPERLU

#include <iostream>
#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/start.hh>

#include "backwardfacingstepproblem.hh"

/*!
 * \brief Print a usage string for simulations.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void printUsage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
        errorMessageOut += progName;
        errorMessageOut += " [options]\n";
        errorMessageOut += errorMsg;
        errorMessageOut += "\n\nThe list of optional options for this program is:\n"
                           "\n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv)
{
    using BackwardFacingStepProblemTT = TTAG(MDSNavierStokesDarcyTwoP);
    return Dumux::start<BackwardFacingStepProblemTT>(argc, argv, printUsage);
}
