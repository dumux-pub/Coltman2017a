#ifndef DUMUX_RECTANGLE_COUPLING_PROBLEM_HH
#define DUMUX_RECTANGLE_COUPLING_PROBLEM_HH

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include <appl/staggeredgrid/multidomain/navierstokesdarcy2p/problem.hh>

#include "rectanglecouplingdarcysubproblem.hh"
#include "rectanglecouplingstokessubproblem.hh"

namespace Dumux
{

template <class TypeTag>
class RectangleCouplingProblem;

template <class TypeTag> class MultiDomainProblem;

namespace Properties
{
// problems
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, Problem,
              Dumux::RectangleCouplingProblem<TTAG(MDSNavierStokesDarcyTwoP)>);
SET_TYPE_PROP(StokesSubProblem, Problem,
              Dumux::CouplingStokesSubProblem<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(DarcySubProblem, Problem,
              Dumux::CouplingDarcySubProblem<TTAG(DarcySubProblem)>);

// Set the fluid system to use complex relations (last argument)
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);
SET_TYPE_PROP(DarcySubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), FluidSystem));
SET_TYPE_PROP(StokesSubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), FluidSystem));

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(MDSNavierStokesDarcyTwoP, UseMoles, false);
SET_BOOL_PROP(DarcySubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesDarcyTwoP), UseMoles));
SET_BOOL_PROP(StokesSubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesDarcyTwoP), UseMoles));
}

template <class TypeTag = TTAG(MDSNavierStokesDarcyTwoP)>
class RectangleCouplingProblem
: public MultiDomainProblem<TypeTag>
{
    using Implementation = typename GET_PROP_TYPE(TypeTag, Problem);
    using ParentType = MultiDomainProblem<TypeTag>;

    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using MultiDomainGridView = typename GET_PROP_TYPE(TypeTag, MultiDomainGridView);
    using SubDomainGridView = typename GET_PROP_TYPE(TypeTag, SubDomainGridView);
    enum { dim = MultiDomainGridView::dimension };
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    using MultiDomainIndices = typename GET_PROP_TYPE(TypeTag, Indices);
    constexpr static unsigned int stokesSubDomainIdx = MultiDomainIndices::stokesSubDomainIdx;
    constexpr static unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief Base class for the multi domain problem
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The GridView
     */
    RectangleCouplingProblem(TimeManager &timeManager,
                     GridView gridView)
    : ParentType(timeManager, gridView)
    {
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
    }

    /*!
     * \brief Initialization multi-domain and the sub-domain grids
     */
    void initializeGrid()
    {
        this->mdGrid().startSubDomainMarking();

        for (auto eIt = this->mdGrid().template leafbegin<0>();
              eIt != this->mdGrid().template leafend<0>(); ++eIt)
        {
            auto globalPos = eIt->geometry().center();
            // only for interior entities, required for parallelization
            if (eIt->partitionType() == Dune::InteriorEntity)
            {
                if (inDarcyDomain(globalPos))
                {
                    this->mdGrid().addToSubDomain(darcySubDomainIdx, *eIt);
                }
                else
                {
                    this->mdGrid().addToSubDomain(stokesSubDomainIdx, *eIt);
                }
            }
        }
        this->mdGrid().preUpdateSubDomains();
        this->mdGrid().updateSubDomains();
        this->mdGrid().postUpdateSubDomains();

        this->darcyElementIndices_.resize(this->sdGridViewDarcy().size(0));
        Dune::MultipleCodimMultipleGeomTypeMapper<MultiDomainGridView, Dune::MCMGElementLayout>
            multidomainDofMapper(this->mdGridView());
        Dune::MultipleCodimMultipleGeomTypeMapper<SubDomainGridView, Dune::MCMGElementLayout>
            subdomainDofMapper(this->sdGridViewDarcy());
        for (auto eIt = this->sdGridDarcy().template leafbegin<0>();
              eIt != this->sdGridDarcy().template leafend<0>(); ++eIt)
        {
            this->darcyElementIndices_[subdomainDofMapper.index(*eIt)] =
                multidomainDofMapper.index(this->mdGrid().multiDomainEntity(*eIt));
        }
    }

    /*!
     * \brief Function to decide whether one point is inside the Darcy domain
     */
    bool inDarcyDomain(GlobalPosition globalPos)
    {
        GlobalPosition darcyLowerLeft = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyLowerLeft);
        GlobalPosition darcyUpperRight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyUpperRight);
        bool inDarcyDomain = true;

        for (unsigned int dimIdx = 0; dimIdx < 2; ++dimIdx)
            if (globalPos[dimIdx] < darcyLowerLeft[dimIdx]
                || globalPos[dimIdx] > darcyUpperRight[dimIdx])
                    inDarcyDomain &= false;

        return inDarcyDomain;
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution. Calls postTimeStep()
     *        of the subproblems.
     */
    void postTimeStep()
    {
        // call stuff from the parent type
        ParentType::postTimeStep();
    }

protected:
    Implementation &asImp_()
    {
        return *static_cast<Implementation *>(this);
    }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    {
        return *static_cast<const Implementation *>(this);
    }

private:
    Dumux::GnuplotInterface<double> gnuplot_;
};

} // end namespace Dumux

#endif // DUMUX_RECTANGLE_COUPLING_PROBLEM_HH
