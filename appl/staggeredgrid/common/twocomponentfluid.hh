// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Functions required by the Navier-Stokes staggered grid local operator
 *        for a fluid with two components.
 */
#ifndef DUMUX_TWO_COMPONENT_FLUID_HH
#define DUMUX_TWO_COMPONENT_FLUID_HH

// TODO: Replace the twocomponentfluid.hh with functionality form dumux-stable

#include<cmath>
#include<dune/common/stdstreams.hh>

//! \todo should be replaced by common staggered grid properties
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproperties.hh>

namespace Dumux
{

/*!
 * \brief Functions required by the Navier-Stokes staggered grid local operator
 *        for a fluid with two components.
 */
template <class TypeTag>
class TwoComponentFluid
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
  typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

  //! \brief flow and transport properties
  typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
  enum { numComponents = Indices::numComponents,
         phaseIdx = Indices::phaseIdx,
         transportCompIdx = Indices::transportCompIdx,
         phaseCompIdx = Indices::phaseCompIdx };

  /**
    * \brief Returns the density [kg/m^3]
    */
  static const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
#ifdef USE_SIMPLE_PHYSICS
#if FUEL_CELL_PROPERTIES
    pressure = 2.013e5;
    massMoleFrac = 0.1;
#else // FUEL_CELL_PROPERTIES
    pressure = 1e5;
    massMoleFrac = 0.0;
#endif // FUEL_CELL_PROPERTIES
#endif
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);

    Scalar variable = FluidSystem::density(fluidState, phaseIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::density(fluidState, phaseIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the density [mol/m^3]
    */
  static const Scalar molarDensity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    return density(pressure, temperature, massMoleFrac)
           / molarMassPhase(massMoleFrac);
  }

  /**
    * \brief Returns the dynamic viscosity [kg/(m s)]
    */
  static const Scalar dynamicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
#ifdef USE_SIMPLE_PHYSICS
#if FUEL_CELL_PROPERTIES
    pressure = 2.013e5;
    temperature = 343.15;
#else // FUEL_CELL_PROPERTIES
    pressure = 1e5;
    temperature = 293.15;
#endif // FUEL_CELL_PROPERTIES
#endif
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);

    Scalar variable = FluidSystem::viscosity(fluidState, phaseIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::viscosity(fluidState, phaseIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the kinematic viscosity [m^2/s]
    */
  static const Scalar kinematicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
#ifdef FIXED_VISCOSITY
    return FIXED_VISCOSITY;
#else
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    return dynamicViscosity(pressure, temperature, massMoleFrac)
           / density(pressure, temperature, massMoleFrac);
#endif
  }

  /**
    * \brief Returns the binary diffusion coefficient [m^2/s]
    */
  static const Scalar diffusionCoefficient(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
#ifdef USE_SIMPLE_PHYSICS
#if FUEL_CELL_PROPERTIES
    pressure = 2.013e5;
#else // FUEL_CELL_PROPERTIES
    pressure = 1e5;
#endif // FUEL_CELL_PROPERTIES
#endif
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);

    typename FluidSystem::ParameterCache paramCache;
    paramCache.updateAll(fluidState);

    Scalar variable = FluidSystem::binaryDiffusionCoefficient(fluidState, paramCache, phaseIdx, transportCompIdx, phaseCompIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::binaryDiffusionCoefficient: " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the thermal conductivity [W/(m*K)]
    */
  static const Scalar thermalConductivity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);

    Scalar variable = FluidSystem::thermalConductivity(fluidState, phaseIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::thermalConductivity(fluidState, phaseIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the heat capacity [J/(kg*K)]
    */
  static const Scalar heatCapacity(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);

    Scalar variable = FluidSystem::heatCapacity(fluidState, phaseIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::heatCapacity(fluidState, phaseIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the internal energy [J/kg]
    */
  static const Scalar internalEnergy(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);
    // the next two are really important!!
    fluidState.setDensity(phaseIdx, density(pressure, temperature, massMoleFrac));
    fluidState.setEnthalpy(phaseIdx, enthalpyPhase(pressure, temperature, massMoleFrac));

    // there is no internalEnergy in the fluid system
    Scalar variable = fluidState.internalEnergy(phaseIdx);
    if (std::isnan(variable) || std::isinf(variable)/* || variable < 1e-12*/)
      Dune::dinfo << "fluidState.internalEnergy(phaseIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << "enthalpyPhase(pressure, temperature, massMoleFrac): " << enthalpyPhase(pressure, temperature, massMoleFrac)
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the enthalpy of the phase [J/kg]
    */
  static const Scalar enthalpyPhase(Scalar pressure, Scalar temperature, Scalar massMoleFrac)
  {
    checkPressure(pressure);
    checkTemperature(temperature);
    checkMassMoleFrac(massMoleFrac);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);
    Scalar moleFrac = convertToMoleFrac(massMoleFrac);
    fluidState.setMoleFraction(phaseIdx, transportCompIdx, moleFrac);
    fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1.0-moleFrac);

    Scalar variable = FluidSystem::enthalpy(fluidState, phaseIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::enthalpy(fluidState, phaseIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << "massMoleFrac: " << massMoleFrac
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the enthalpy of a component [J/kg]
    */
  static const Scalar enthalpyComponent(Scalar pressure, Scalar temperature, unsigned int compIdx)
  {
    checkPressure(pressure);
    checkTemperature(temperature);
    checkCompIdx(compIdx);
    FluidState fluidState;
    fluidState.setPressure(phaseIdx, pressure);
    fluidState.setTemperature(temperature);

    Scalar variable = FluidSystem::componentEnthalpy(fluidState, phaseIdx, compIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::componentEnthalpy(fluidState, phaseIdx, compIdx): " << variable
                  << "pressure: " << pressure
                  << "temperature: " << temperature
                  << std::endl;

    return variable;
  }

  /**
    * \brief Returns the mole fraction of the transported component [-]
    */
  static const Scalar convertToMoleFrac(Scalar massMoleFrac)
  {
    checkMassMoleFrac(massMoleFrac);
    if(GET_PROP_VALUE(TypeTag, UseMoles) != true) //mass-fraction formulation
    {
      Scalar avgMolarMass = molarMassPhase(massMoleFrac);
      Scalar M1 = molarMassComponent(transportCompIdx);
      return massMoleFrac*avgMolarMass/M1;
    }
    else
    {
      return massMoleFrac;
    }
  }

  /**
    * \brief Returns the molar mass of the phase [kg/mol]
    */
  static const Scalar molarMassPhase(Scalar massMoleFrac)
  {
    checkMassMoleFrac(massMoleFrac);
    if(GET_PROP_VALUE(TypeTag, UseMoles) != true) //mass-fraction formulation
    {
      // calculate average molar mass of the gas phase
      Scalar M1 = molarMassComponent(transportCompIdx);
      Scalar M2 = molarMassComponent(phaseCompIdx);
      Scalar X2 = 1.0-massMoleFrac;
      return M1*M2/(M2 + X2*(M1 - M2)); // avgMolarMass
    }
    else
    {
      DUNE_THROW(Dune::NotImplemented, "If someone wants to use a mole-fraction formulation, please update and revise the code.");
    }
  }

  /**
    * \brief Returns the mass of the component [kg/mol]
    */
  static const Scalar molarMassComponent(unsigned int compIdx)
  {
    checkCompIdx(compIdx);
    Scalar variable = FluidSystem::molarMass(compIdx);
    if (std::isnan(variable) || std::isinf(variable) || variable < 1e-12)
      Dune::dinfo << "FluidSystem::molarMass(compIdx): " << variable << std::endl;

    return variable;
  }

  //!\brief check pressure (just to check implementation)
  static const void checkPressure(Scalar pressure)
  {
    if (pressure > 2e5 || pressure < 9e4)
      Dune::dinfo << "pressure out of bounds: " << pressure << std::endl;
  }

  //!\brief check temperature (just to check implementation)
  static const void checkTemperature(Scalar temperature)
  {
    if (temperature > 400 || temperature < 273)
      Dune::dinfo << "temperature out of bounds: " << temperature << std::endl;
  }

  //!\brief check massMoleFrac (just to check implementation)
  static const void checkMassMoleFrac(Scalar massMoleFrac)
  {
    if (massMoleFrac > 0.99|| massMoleFrac < 1e-6)
      Dune::dinfo << "massMoleFrac out of bounds: " << massMoleFrac << std::endl;
  }

  //!\brief check compIdx (just to check implementation)
  static const void checkCompIdx(unsigned int compIdx)
  {
    if (compIdx != phaseCompIdx && compIdx != transportCompIdx)
      Dune::dinfo << "compIdx out of bounds: " << compIdx << std::endl;
  }
};
} // end namespace

#endif // DUMUX_TWO_COMPONENT_FLUID_HH
