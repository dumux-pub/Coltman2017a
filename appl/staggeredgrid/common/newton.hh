/** \file
 *  \brief \note This file is only necessary to adapt the PDELab Newton to:
 *         (1) include the maxRelShift criterion
 *         (2) allow to update the stored (wall) variables.
*/
#ifndef DUMUX_STAGGEREDGRID_NEWTON_HH
#define DUMUX_STAGGEREDGRID_NEWTON_HH

#include <iostream>
#include <iomanip>
#include <cmath>

#include <math.h>

#include <dune/common/exceptions.hh>
#include <dune/common/ios_state.hh>
#include <dune/common/timer.hh>
#include <dune/common/parametertree.hh>

#include <dune/pdelab/backend/solver.hh>
#include <dune/pdelab/newton/newton.hh>

namespace Dune
{
  namespace PDELab
  {
    template<class GOS, class S, class TrlV, class TstV>
    class StaggeredGridNewtonSolver : virtual NewtonBase<GOS,TrlV,TstV>
    {
      typedef S Solver;
      typedef GOS GridOperator;
      typedef TrlV TrialVector;
      typedef TstV TestVector;

      typedef typename TestVector::ElementType RFType;
      typedef typename GOS::Traits::Jacobian Matrix;

    public:
      typedef NewtonResult<RFType> Result;

      StaggeredGridNewtonSolver(GridOperator& go, TrialVector& u_, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , solver(solver_)
        , result_valid(false)
        , maxRelShift_(0)
        , updateStoredVariables_(nullptr)
        , enableUpdateStoredVariables_(false)
      {}

      StaggeredGridNewtonSolver(GridOperator& go, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , solver(solver_)
        , result_valid(false)
        , maxRelShift_(0)
        , updateStoredVariables_(nullptr)
        , enableUpdateStoredVariables_(false)
      {}

      void apply();

      void apply(TrialVector& u_);

      const Result& result() const
      {
        if (!result_valid)
          DUNE_THROW(NewtonError,
                     "StaggeredGridNewtonSolver::result() called before StaggeredGridNewtonSolver::solve()");
        return this->res;
      }

      /*!
      * \brief Pass the updateStoredValues routine
      */
      void setUpdateStoredVariablesFunction(std::function<void (TrialVector&)> updateStoredVariables)
      {
        updateStoredVariables_ = updateStoredVariables;
        enableUpdateStoredVariables_ = true;
      }

      //! Set maximum relative shift
      void setMaxRelShift(double maxRelShift)
      { maxRelShift_ = maxRelShift; }

    protected:
      virtual void defect(TestVector& r)
      {
        r = 0.0;                                        // TODO: vector interface
        this->gridoperator.residual(*this->u, r);
        this->res.defect = this->solver.norm(r);                    // TODO: solver interface
        if (!std::isfinite(this->res.defect))
        {
          printvector(std::cout, r.base(), "r", "row", 10, 2);
          DUNE_THROW(NewtonDefectError,
                     "StaggererGridNewtonSolver::defect(): Non-linear defect is NaN or Inf");
        }
      }


    private:
      void linearSolve(Matrix& A, TrialVector& z, TestVector& r) const
      {
//         printmatrix(std::cout, A.base(), "matrix", "row", 10, 2);
//         printvector(std::cout, z.base(), "z", "row", 5, 2);
//         printvector(std::cout, r.base(), "r", "row", 5, 2);
        if (this->verbosity_level >= 4)
          std::cout << "      Solving linear system..." << std::endl;
        z = 0.0;                                        // TODO: vector interface
        this->solver.apply(A, z, r, this->linear_reduction);        // TODO: solver interface

        ios_base_all_saver restorer(std::cout); // store old ios flags

        if (!this->solver.result().converged)                 // TODO: solver interface
          DUNE_THROW(NewtonLinearSolverError,
                     "StaggeredGridNewtonSolver::linearSolve(): Linear solver did not converge "
                     "in " << this->solver.result().iterations << " iterations");
        if (this->verbosity_level >= 4)
          std::cout << "          linear solver iterations:     "
                    << std::setw(12) << solver.result().iterations << std::endl
                    << "          linear defect reduction:      "
                    << std::setw(12) << std::setprecision(4) << std::scientific
                    << solver.result().reduction << std::endl;
      }

      Solver& solver;
      bool result_valid;
      double maxRelShift_;
      std::function<void (TrialVector&)> updateStoredVariables_;
      bool enableUpdateStoredVariables_;
    };

    template<class GOS, class S, class TrlV, class TstV>
    void StaggeredGridNewtonSolver<GOS,S,TrlV,TstV>::apply(TrialVector& u_)
    {
      this->u = &u_;
      apply();
    }

    template<class GOS, class S, class TrlV, class TstV>
    void StaggeredGridNewtonSolver<GOS,S,TrlV,TstV>::apply()
    {
      this->res.iterations = 0;
      this->res.converged = false;
      this->res.reduction = 1.0;
      this->res.conv_rate = 1.0;
      this->res.elapsed = 0.0;
      this->res.assembler_time = 0.0;
      this->res.linear_solver_time = 0.0;
      this->res.linear_solver_iterations = 0;
      result_valid = true;
      Timer timer;

      try
        {
          TestVector r(this->gridoperator.testGridFunctionSpace());
          this->defect(r);
          this->res.first_defect = this->res.defect;
          this->prev_defect = this->res.defect;

          if (this->verbosity_level >= 2)
            {
              // store old ios flags
              ios_base_all_saver restorer(std::cout);
              std::cout << "  Initial defect: "
                        << std::setw(12) << std::setprecision(4) << std::scientific
                        << this->res.defect << std::endl;
            }

          Matrix A(this->gridoperator);
          TrialVector z(this->gridoperator.trialGridFunctionSpace());

          double maxRelShift = 1.0;
          auto oldSol = *this->u;
          while (!(this->terminate() || maxRelShift < maxRelShift_))
            {
              if (this->verbosity_level >= 3)
                std::cout << "  Newton iteration " << this->res.iterations
                          << " --------------------------------" << std::endl;

              Timer assembler_timer;
              try
                {
                  if (enableUpdateStoredVariables_)
                    updateStoredVariables_(*this->u);
//               updateStoredVariables_(u);
                  this->prepare_step(A,r);
                }
              catch (...)
                {
                  this->res.assembler_time += assembler_timer.elapsed();
                  throw;
                }
              double assembler_time = assembler_timer.elapsed();
              this->res.assembler_time += assembler_time;
              if (this->verbosity_level >= 3)
                std::cout << "      matrix assembly time:             "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << assembler_time << std::endl;

              Timer linear_solver_timer;
              try
                {
                  this->linearSolve(A, z, r);
                }
              catch (...)
                {
                  this->res.linear_solver_time += linear_solver_timer.elapsed();
                  this->res.linear_solver_iterations += this->solver.result().iterations;
                  throw;
                }
              double linear_solver_time = linear_solver_timer.elapsed();
              this->res.linear_solver_time += linear_solver_time;
              this->res.linear_solver_iterations += this->solver.result().iterations;

              try
                {
                  this->line_search(z, r);
                }
              catch (NewtonLineSearchError)
                {
                  if (this->reassembled)
                    throw;
                  if (this->verbosity_level >= 3)
                    std::cout << "      line search failed - trying again with reassembled matrix" << std::endl;
                  continue;
                }

              this->res.reduction = this->res.defect/this->res.first_defect;
              this->res.iterations++;
              this->res.conv_rate = std::pow(this->res.reduction, 1.0/this->res.iterations);

              maxRelShift = 0.0;
              for (unsigned int i = 0; i < this->u->N(); ++i)
              {
                  double eqErr = std::abs(this->u->block(i) - oldSol.block(i));
                  eqErr /= std::max<double>(1.0, std::abs(this->u->block(i) + oldSol.block(i))/2);
                  maxRelShift = std::max(maxRelShift, eqErr);
              }
              oldSol = *this->u;

              // store old ios flags
              ios_base_all_saver restorer(std::cout);

              if (this->verbosity_level == 2)
                std::cout << "  Newton iteration " << std::setw(2) << this->res.iterations
                          << ".  New defect: "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res.defect
                          << ".  Reduction (this): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res.defect/this->prev_defect
                          << ".  Reduction (total): "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << this->res.reduction
                          << ".  MaxRelShift: "
                          << std::setw(12) << std::setprecision(4) << std::scientific
                          << maxRelShift
                          << std::endl;
            }
        }
      catch(...)
        {
          this->res.elapsed = timer.elapsed();
          throw;
        }
      this->res.elapsed = timer.elapsed();

      ios_base_all_saver restorer(std::cout); // store old ios flags

      if (this->verbosity_level == 1)
        std::cout << "  Newton converged after " << std::setw(2) << this->res.iterations
                  << " iterations.  Reduction: "
                  << std::setw(12) << std::setprecision(4) << std::scientific
                  << this->res.reduction
                  << "   (" << std::setprecision(4) << this->res.elapsed << "s)"
                  << std::endl;
    }

    template<class GOS, class S, class TrlV, class TstV = TrlV>
    class StaggeredGridNewton : public StaggeredGridNewtonSolver<GOS,S,TrlV,TstV>
                              , public NewtonTerminate<GOS,TrlV,TstV>
                              , public NewtonLineSearch<GOS,TrlV,TstV>
                              , public NewtonPrepareStep<GOS,TrlV,TstV>
    {
      typedef GOS GridOperator;
      typedef S Solver;
      typedef TrlV TrialVector;

    public:
      StaggeredGridNewton(GridOperator& go, TrialVector& u_, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go,u_)
        , StaggeredGridNewtonSolver<GOS,S,TrlV,TstV>(go,u_,solver_)
        , NewtonTerminate<GOS,TrlV,TstV>(go,u_)
        , NewtonLineSearch<GOS,TrlV,TstV>(go,u_)
        , NewtonPrepareStep<GOS,TrlV,TstV>(go,u_)
      {}
      StaggeredGridNewton(GridOperator& go, Solver& solver_)
        : NewtonBase<GOS,TrlV,TstV>(go)
        , StaggeredGridNewtonSolver<GOS,S,TrlV,TstV>(go,solver_)
        , NewtonTerminate<GOS,TrlV,TstV>(go)
        , NewtonLineSearch<GOS,TrlV,TstV>(go)
        , NewtonPrepareStep<GOS,TrlV,TstV>(go)
      {}
      //! interpret a parameter tree as a set of options for the newton solver
      /**

         example configuration:

         \code
         [NewtonParameters]

         ReassembleThreshold = 0.1
         LineSearchMaxIterations = 10
         MaxIterations = 7
         AbsoluteLimit = 1e-6
         Reduction = 1e-4
         LinearReduction = 1e-3
         LineSearchDamping  = 0.9
         \endcode

         and invocation in the code:
         \code
         newton.setParameters(param.sub("NewtonParameters"));
         \endcode
      */
      void setParameters(ParameterTree & param)
      {
        typedef typename TstV::ElementType RFType;
        if (param.hasKey("VerbosityLevel"))
          this->setVerbosityLevel(
            param.get<unsigned int>("VerbosityLevel"));
        if (param.hasKey("VerbosityLevel"))
          this->setVerbosityLevel(
            param.get<unsigned int>("VerbosityLevel"));
        if (param.hasKey("Reduction"))
          this->setReduction(
            param.get<RFType>("Reduction"));
        if (param.hasKey("MaxIterations"))
          this->setMaxIterations(
            param.get<unsigned int>("MaxIterations"));
        if (param.hasKey("ForceIteration"))
          this->setForceIteration(
            param.get<bool>("ForceIteration"));
        if (param.hasKey("AbsoluteLimit"))
          this->setAbsoluteLimit(
            param.get<RFType>("AbsoluteLimit"));
        if (param.hasKey("MinLinearReduction"))
          this->setMinLinearReduction(
            param.get<RFType>("MinLinearReduction"));
        if (param.hasKey("FixedLinearReduction"))
          this->setFixedLinearReduction(
            param.get<bool>("FixedLinearReduction"));
        if (param.hasKey("ReassembleThreshold"))
          this->setReassembleThreshold(
            param.get<RFType>("ReassembleThreshold"));
        if (param.hasKey("LineSearchStrategy"))
          this->setLineSearchStrategy(
            param.get<std::string>("LineSearchStrategy"));
        if (param.hasKey("LineSearchMaxIterations"))
          this->setLineSearchMaxIterations(
            param.get<unsigned int>("LineSearchMaxIterations"));
        if (param.hasKey("LineSearchDampingFactor"))
          this->setLineSearchDampingFactor(
            param.get<RFType>("LineSearchDampingFactor"));
      }
    };
  }
}

#endif // DUMUX_STAGGEREDGRID_NEWTON_HH
