/** \file
 *  \ingroup StaggeredModel
 *
 *  \brief Constraints fixing the velocity values at the boundary.
*/

#ifndef DUMUX_NAVIER_STOKES_FIXVELOCITYCONSTRAINTS_HH
#define DUMUX_NAVIER_STOKES_FIXVELOCITYCONSTRAINTS_HH

#include <cstddef>
#include <algorithm>

#include <dune/common/exceptions.hh>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include <dune/grid/common/grid.hh>

#include <dune/localfunctions/common/interfaceswitch.hh>

#include <dune/typetree/typetree.hh>

#include <dune/pdelab/common/geometrywrapper.hh>
#include <dune/pdelab/constraints/common/constraintsparameters.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspacetags.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproperties.hh>

namespace Dumux {

/**
  * \brief Sets fix values for the specified domain.
  *
  * \tparam TypeTag TypeTag of the problem
  */
template<class TypeTag>
class FixVelocityConstraints
{
public:
    enum { doBoundary = true };
    enum { doProcessor = false };
    enum { doSkeleton = false };
    enum { doVolume = false };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    enum { dim = GridView::dimension };

    /**
      * \tparam P   Parameter class, wich fulfills the DirichletConstraintsParameters interface
      * \tparam IG  intersection geometry
      * \tparam LFS local function space
      * \tparam T   TransformationType
      */
    template<typename P, typename IG, typename LFS, typename T>
    void boundary(const P& param, const IG& ig, const LFS& lfs, T& trafo) const
    {
        if (GET_PROP_VALUE(TypeTag, FixVelocityConstraints))
        {
            typedef Dune::FiniteElementInterfaceSwitch<typename LFS::Traits::FiniteElementType> FESwitch;
            typedef Dune::FieldVector<typename IG::ctype, IG::dimension-1> FaceCoord;

            const int face = ig.indexInInside();

            // find all local indices of this face
            Dune::GeometryType gt = ig.inside().type();
            const Dune::ReferenceElement<Scalar,dim>& refelem = Dune::ReferenceElements<Scalar,dim>::general(gt);

            const Dune::ReferenceElement<Scalar,dim-1> &
                face_refelem = Dune::ReferenceElements<Scalar,dim-1>::general(ig.geometry().type());

            // empty map means Dirichlet constraint
            typename T::RowType empty;

            const FaceCoord testpoint = face_refelem.position(0,0);

            // Abort if this isn't a Wall, Inflow or Symmetry boundary
            if (!(param.isWall(ig,testpoint)
                  || param.isInflow(ig,testpoint)
                  || param.isSymmetry(ig,testpoint)))
            {
                return;
            }

            for (std::size_t i = 0;
                i < std::size_t(FESwitch::coefficients(lfs.finiteElement()).size());
                i++)
            {
                // The codim to which this dof is attached to
                unsigned int codim = FESwitch::coefficients(lfs.finiteElement()).localKey(i).codim();

                if (codim == 0)
                {
                    continue;
                }

                for (int j=0; j<refelem.size(face,1,codim); j++)
                {
                    if (static_cast<int>(FESwitch::coefficients(lfs.finiteElement()).
                                          localKey(i).subEntity())
                        == refelem.subEntity(face, 1, j, codim))
                    {
                        trafo[lfs.dofIndex(i)] = empty;
                    }
                }
            }
        }
    }
};
}

#endif
