// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines default properties for the eddy viscosity staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_EDDYVISCOSITY_PROPERTY_DEFAULTS_HH
#define DUMUX_EDDYVISCOSITY_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokespropertydefaults.hh>

#include"eddyviscosityproperties.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Use full Navier-Stokes equation
SET_BOOL_PROP(StaggeredGridEddyViscosity, ProblemEnableNavierStokes, true);

//! The Van Karman constant
SET_SCALAR_PROP(StaggeredGridEddyViscosity, KarmanConstant, 0.41);

//! The axis to calculate wall distances (negative means determine automatically)
SET_INT_PROP(StaggeredGridEddyViscosity, ProblemWallNormalAxis, 1);

//! The main flow axis for calculating the eddy viscosity (negative means determine automatically)
SET_INT_PROP(StaggeredGridEddyViscosity, ProblemFlowNormalAxis, 0);

//!< Only do an update of the stored variables at the beginning of each time step
SET_BOOL_PROP(StaggeredGridEddyViscosity, NewtonUpdateStoredVariables, false);

//! Factor, which is added to the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(StaggeredGridEddyViscosity, TimeManagerTimeStepIncrementFactor, 5.0/6.0);
}
}

#endif // DUMUX_EDDYVISCOSITY_PROPERTY_DEFAULTS_HH
