/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup EddyViscosityStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modelled with eddy viscosity models.
 *
 * Mass balance:
 * \f[
 *    \frac{\partial \varrho_\alpha}{\partial t}
 *    + \nabla \cdot \left( \varrho_\alpha v_\alpha \right)
 *    - q_{\varrho}
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial \left( \varrho_\alpha v_\alpha \right)}{\partial t}
 *    + \nabla \cdot \left( p_\alpha I
 *      - \varrho_\alpha \nu_\alpha
 *        \left( \nabla v_\alpha \right)
 *      - \varrho_\alpha \nu_{\alpha,\textrm{t}}
 *        \left( \nabla v_\alpha \right)
 *      \right)
 *    - \varrho_\alpha g
 *    - q_{\varrho v}
 *    = 0
 * \f]
 *
 * \todo Add possibility to include transposed part of the viscous term
 *
 * The eddy viscosity depends on the chosen model, this could be:
 * - 0-Equation or algebraic models (like Prandtl, Baldwin-Lomax)
 * - 1-Equation models (which are not part of dumux yet)
 * - 2-Equation models (like \f$ k-\varepsilon \f$ or \f$ k-\omega \f$)
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the standard Navier-Stokes local operator.
 */

#ifndef DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH
#define DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fmatrix.hh>
#include<dune/common/fvector.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/basenavierstokesstaggeredgrid.hh>

#include"eddyviscositypropertydefaults.hh"
#if IS_STAGGERED_MULTIDOMAIN_MODEL
#include<appl/staggeredgrid/multidomain/common/subdomainpropertydefaults.hh>
#endif

namespace Dune
{
    /**
      * \brief Layout template for faces
      *
      * This layout template is for use in the MultipleCodimMultipleGeomTypeMapper.
      * It selects only etities with <tt>dim = dimgrid-1</tt>.
      *
      * \tparam dimgrid The dimension of the grid.
      */
    template<int dimgrid> struct MCMGIntersectionLayout
    {
      /**
      * \brief Test whether entities of the given geometry type should be
      * included in the map
      */
      bool contains(Dune::GeometryType gt)
      {
        return gt.dim() == (dimgrid - 1);
      }
    };

  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization for steady-state
     *        Navier-Stokes equation, modeled with eddy viscosity models.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class EddyViscosityStaggeredGrid
    : public BaseNavierStokesStaggeredGrid<TypeTag>
    {
    public:
      typedef BaseNavierStokesStaggeredGrid<TypeTag> ParentTypeMassMomentum;

      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGIntersectionLayout> MapperIntersection;

      //! type to store the velocities and coordinates
      typedef std::vector<typename GridView::IndexSet::IndexType> StoredGridViewID;
      typedef std::vector<bool> StoredBool;
      typedef std::vector<unsigned int> StoredUnsignedInt;
      typedef std::vector<double> StoredScalar;
      typedef std::vector<Dune::FieldVector<double, GridView::dimension> > StoredVector;
      typedef std::vector<Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> > StoredMatrix;

      std::vector<std::vector<std::vector<unsigned int>>> storedNeighborID;
      StoredMatrix storedVelocityGradientTensor;

      StoredBool storedElementHasWall;
      StoredVector storedCorrespondingWallGlobal;
      StoredGridViewID storedCorrespondingWallElementID;
      StoredGridViewID storedOppositeWallElementID;
      StoredUnsignedInt storedCorrespondingWallNormalAxis;
      StoredUnsignedInt storedCorrespondingFlowNormalAxis;
      StoredScalar storedDistanceToWall;
      StoredScalar storedVelocityInWallCoordinates;
      StoredScalar storedDistanceInWallCoordinates;
      StoredScalar storedRoughness;
      StoredScalar storedAdditionalRoughnessLength;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedVelocityMaximum;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedVelocityMinimum;

      //! Eddy viscosity models
      mutable StoredScalar storedKinematicEddyViscosity;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomainTypeTag;
      typedef typename GET_PROP_TYPE(MultiDomainTypeTag, Indices) MultiDomainIndices;
      enum { stokesSubDomainIdx = MultiDomainIndices::stokesSubDomainIdx };
#endif
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx };

      //! \brief Constructor
      EddyViscosityStaggeredGrid(const BC& bc_,
                                 const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                                 const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                                 const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                                 GridView gridView_)
        : ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_),
          mapperElement(gridView_), mapperIntersection(gridView_)
      {
        enableNavierStokes_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableNavierStokes);
        enableUnsymmetrizedVelocityGradient_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableUnsymmetrizedVelocityGradient);
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);
        wallNormalAxis_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, WallNormalAxis);
        flowNormalAxis_ = GET_PARAM_FROM_GROUP(TypeTag, int, Problem, FlowNormalAxis);

        initialize();
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_massmomentum(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                      std::vector<DimVector> velocityFaces, Scalar pressure,
                                      Scalar massMoleFrac, Scalar temperature) const
      {
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();

        // /////////////////////
        // geometry information

        // velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
          faceCentersGlobal[curFace] = eg.geometry().global(faceCentersLocal[curFace]);
        }

        // distance between two face mid points
        Dune::FieldVector<Scalar, dim> distancesFaceCenters(0.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          distancesFaceCenters[curDim] =
            std::abs(faceCentersGlobal[2*curDim+1][curDim] - faceCentersGlobal[2*curDim][curDim]);
        }

        // staggered face volume (goes through cell center) perpendicular to each direction
        Dune::FieldVector<Scalar, dim> orthogonalFaceVolumes(1.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          for (unsigned int normDim = 0; normDim < dim; ++normDim)
          {
            if (curDim != normDim)
            {
              orthogonalFaceVolumes[curDim] *= distancesFaceCenters[normDim];
            }
          }
        }

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate fluid properties
        const Scalar density = asImp_().density(pressure, temperature, massMoleFrac);
        const Scalar kinematicViscosity = asImp_().kinematicViscosity(pressure, temperature, massMoleFrac);

        //! \note storedKinematicEddyViscosity[elementID] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID = mapperElement.index(eg.entity());
        Scalar kinematicEddyViscosity = asImp_().storedKinematicEddyViscosity[elementID];

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        /**
          * (1) \b Eddy term of \b momentum balance equation
          *
          * \f[
          *    - \boldsymbol{\tau}_\textrm{t}
          *    = - \nu_\textrm{t} \rho \nabla v
          *    \Rightarrow \int_\gamma - \boldsymbol{\tau} \cdot n
          *    = \int_\gamma - \nu_\textrm{t} \rho \nabla v \cdot n
          * \f]
          * parallel case for all coordinate axes
          * \f[
          *    \alpha_\textrm{self}
          *    = - |\gamma| \left( \boldsymbol{\tau} \cdot n \right) \cdot n
          *    = - |\gamma| \varrho \nu_\textrm{t}
          *      \frac{\textrm{d} v_\textrm{curDim}}{\textrm{d} x_\textrm{curDim}}
          *    = - |\gamma| \varrho \nu_\textrm{t}
          *      \frac{v_{\textrm{right,curDim}} - v_{\textrm{left,curDim}}}
          *           {x_{\textrm{right,curDim}} - x_{\textrm{left,curDim}}}
          * \f]
          */
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (enableNavierStokes_)
          {
#ifndef DisableVEddyViscosity
            r.accumulate(lfsu_v, 2*curDim,
                        // normal is always positive
                        -1.0 * kinematicEddyViscosity * density
                        * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                        / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                        * orthogonalFaceVolumes[curDim]); // face volume
            r.accumulate(lfsu_v, 2*curDim+1,
                        // normal is always negative
                        1.0 * kinematicEddyViscosity * density
                        * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                        / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                        * orthogonalFaceVolumes[curDim]); // face volume

            // include the symmetrized velocity gradient for the viscous term term
            if (!enableUnsymmetrizedVelocityGradient_)
            {
              r.accumulate(lfsu_v, 2*curDim,
                          // normal is always positive
                          -1.0 * (kinematicEddyViscosity) * density
                          * asImp_().storedVelocityGradientTensor[elementID][curDim][curDim]
                          * orthogonalFaceVolumes[curDim]); // face volume
              r.accumulate(lfsu_v, 2*curDim+1,
                          // normal is always negative
                          1.0 * (kinematicEddyViscosity) * density
                          * asImp_().storedVelocityGradientTensor[elementID][curDim][curDim]
                          * orthogonalFaceVolumes[curDim]); // face volume
            }
#endif
          }

          // include the symmetrized velocity gradient for the viscous term term
          if (!enableUnsymmetrizedVelocityGradient_)
          {
            r.accumulate(lfsu_v, 2*curDim,
                        // normal is always positive
                        -1.0 * (kinematicViscosity) * density
                        * asImp_().storedVelocityGradientTensor[elementID][curDim][curDim]
                        * orthogonalFaceVolumes[curDim]); // face volume
            r.accumulate(lfsu_v, 2*curDim+1,
                        // normal is always negative
                        1.0 * (kinematicViscosity) * density
                        * asImp_().storedVelocityGradientTensor[elementID][curDim][curDim]
                        * orthogonalFaceVolumes[curDim]); // face volume
          }
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_massmomentum(const IG& ig,
                                        const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                        const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                        R& r_s, R& r_n,
                                        std::vector<DimVector> velocities_s, Scalar pressure_s,
                                        Scalar massMoleFrac_s, Scalar temperature_s,
                                        std::vector<DimVector> velocities_n, Scalar pressure_n,
                                        Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        const LFSU_V& lfsu_v_n = lfsu_n.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // /////////////////////
        // geometry information


        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = std::abs(faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);
        Scalar distanceOutsideToFace = std::abs(outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim]);

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_n = asImp_().density(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar kinematicViscosity_n = asImp_().kinematicViscosity(pressure_n, temperature_n, massMoleFrac_n);

        //! \note storedKinematicEddyViscosity[elementID] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID_s = mapperElement.index(ig.inside());
        const typename GridView::IndexSet::IndexType elementID_n = mapperElement.index(ig.outside());
        Scalar kinematicEddyViscosity_s = asImp_().storedKinematicEddyViscosity[elementID_s];
        Scalar kinematicEddyViscosity_n = asImp_().storedKinematicEddyViscosity[elementID_n];
        Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> velocityGradients_s = asImp_().storedVelocityGradientTensor[elementID_s];
        Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> velocityGradients_n = asImp_().storedVelocityGradientTensor[elementID_n];

        // upwinding (from self to neighbor)
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar density_up = density_s;
        std::vector<RangeVelocity> velocities_up(velocities_s);
        if (velocityNormal < 0)
        {
          velocities_up = velocities_n;
          density_up = density_n;
        }

        // averaging: distance weighted average for diffusion term
        Scalar density_avg = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                             / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n
                                          + distanceOutsideToFace * kinematicViscosity_s)
                                        / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicEddyViscosity_avg = (distanceInsideToFace * kinematicEddyViscosity_n
                                              + distanceOutsideToFace * kinematicEddyViscosity_s)
                                            / (distanceInsideToFace + distanceOutsideToFace);
        Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> velocityGradients_avg(0.0);


        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            if (enableDiffusionHarmonic_)
            {
              velocityGradients_avg[i][j] = (2.0 * velocityGradients_n[i][j] * velocityGradients_s[i][j])
                                          / (velocityGradients_n[i][j] + velocityGradients_s[i][j]);
            }
            else
            {
              velocityGradients_avg[i][j] = (distanceInsideToFace * velocityGradients_n[i][j]
                                              + distanceOutsideToFace * velocityGradients_s[i][j])
                                            / (distanceInsideToFace + distanceOutsideToFace);
            }
          }
        }

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for diffusion term
          density_avg = (2.0 * density_n * density_s) / (density_n + density_s);
          kinematicViscosity_avg = (2.0 * kinematicViscosity_n * kinematicViscosity_s)
                                   / (kinematicViscosity_n + kinematicViscosity_s);
          kinematicEddyViscosity_avg = (2.0 * kinematicEddyViscosity_n * kinematicEddyViscosity_s)
                                       / (kinematicEddyViscosity_n + kinematicEddyViscosity_s);
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call

        /**
          * (1) \b Eddy term of \b momentum balance equation
          *
          * \f[
          *    - \boldsymbol{\tau}_\textrm{t}
          *    = - \nu_\textrm{t} \rho \nabla v
          *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
          *    = - \int_\gamma \nu_\textrm{t} \rho \left( \nabla v \cdot n \right)
          * \f]
          * Tangential cases for all coordinate axes (given for \b 2-D,
          * \b rectangular grids and \f$n=(0,1)^T\f$ which means balancing
          * momentum for \f$v_\textrm{0}\f$)<br>
          * \f[
          *    \alpha_\textrm{self}
          *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
          *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
          *      \frac{\partial v_{0}}{\partial x_{1}}
          * \f]
          * Tangential case, for: \f$\frac{\partial v_{0}}{\partial x_{1}}\f$,
          * right and left means along \f$x_1\f$ axis, \f$n\f$ means 1st entry,
          * \f$t\f$ means 0th entry
          * \f[
          *    A
          *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
          *      \frac{\partial v_{0}}{\partial x_{1}}
          *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{t,avg}
          *      \left( 0.5 \left[\frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
          *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
          *                \right]_\textrm{curDim}
          *           + 0.5 \left[ \frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
          *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
          *                \right]_\textrm{curDim+1}
          *      \right)
          * \f]
          *
          * The default procedure for averaging
          * \f$\varrho_\textrm{avg}\f$, \f$\nu_\textrm{t,avg}\f$ is a distance
          * weighted average, by using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt>
          * one can do an harmonic averaging instead.
          */
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          // only tangential case, exclude parallel case
          if (curDim != normDim)
          {
            unsigned int tangDim = curDim;
            if(enableNavierStokes_)
            {
#ifndef DisableVEddyViscosity
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                            -0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);
              r_n.accumulate(lfsu_v_n, 2*tangDim,
                            0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);

              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                            -0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);
              r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                            0.5 * density_avg * kinematicEddyViscosity_avg
                            * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                              / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume);
              if (!enableUnsymmetrizedVelocityGradient_)
              {
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                                -0.5 * density_avg * (kinematicEddyViscosity_avg)
                                * velocityGradients_avg[normDim][tangDim]
                                * faceUnitOuterNormal[normDim] * faceVolume);
                r_n.accumulate(lfsu_v_n, 2*tangDim,
                                0.5 * density_avg * (kinematicEddyViscosity_avg)
                                * velocityGradients_avg[normDim][tangDim]
                                * faceUnitOuterNormal[normDim] * faceVolume);

                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                                -0.5 * density_avg * (kinematicEddyViscosity_avg)
                                * velocityGradients_avg[normDim][tangDim]
                                * faceUnitOuterNormal[normDim] * faceVolume);
                r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                                0.5 * density_avg * (kinematicEddyViscosity_avg)
                                * velocityGradients_avg[normDim][tangDim]
                                * faceUnitOuterNormal[normDim] * faceVolume);
              }
#endif
            }

            if (!enableUnsymmetrizedVelocityGradient_)
            {
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                              -0.5 * density_avg * (kinematicViscosity_avg)
                              * velocityGradients_avg[normDim][tangDim]
                              * faceUnitOuterNormal[normDim] * faceVolume);
              r_n.accumulate(lfsu_v_n, 2*tangDim,
                              0.5 * density_avg * (kinematicViscosity_avg)
                              * velocityGradients_avg[normDim][tangDim]
                              * faceUnitOuterNormal[normDim] * faceVolume);

              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                              -0.5 * density_avg * (kinematicViscosity_avg)
                              * velocityGradients_avg[normDim][tangDim]
                              * faceUnitOuterNormal[normDim] * faceVolume);
              r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                              0.5 * density_avg * (kinematicViscosity_avg)
                              * velocityGradients_avg[normDim][tangDim]
                              * faceUnitOuterNormal[normDim] * faceVolume);
            }
          }
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_massmomentum(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                        std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                        Scalar massMoleFrac_s, Scalar temperature_s,
                                        Scalar pressure_boundary,
                                        Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);

        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

        // center in face's reference element
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face properties (coordinates and normal)
        const Dune::FieldVector<Scalar, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);
        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // cell center in reference element
        const Dune::FieldVector<Scalar,dim>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);

        //! \note storedKinematicEddyViscosity[elementID_s] has to be set by the child function
        const typename GridView::IndexSet::IndexType elementID_s = mapperElement.index(ig.inside());
        Scalar kinematicEddyViscosity_s = asImp_().storedKinematicEddyViscosity[elementID_s];
        Dune::FieldMatrix<double, GridView::dimension, GridView::dimension> velocityGradients_s = asImp_().storedVelocityGradientTensor[elementID_s];

        // Wall or Inflow boundary for velocity
        int elementID = mapperElement.index(ig.inside());
        if ((bcVelocity.isWall(ig, faceCenterLocal) && !asImp_().useWallFunctionMomentum(elementID))
            || bcVelocity.isInflow(ig, faceCenterLocal))
        {
          /**
            * Dirichlet boundary handling for velocity/ momentum balance<br>
            * (1) \b Eddy term of \b momentum balance equation
            *
            * \f[
            *    - \boldsymbol{\tau}_\textrm{t}
            *    = - \nu_\textrm{t} \rho \left( \nabla v \right)
            *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
            *    = - \int_\gamma \nu_\textrm{t} \rho \nabla v \cdot n
            * \f]
            * Only the tangential case is regarded here, as if there is a Dirichlet value
            *  for the velocity in face normal direction, it is automatically fixed.
            * \f[
            *    \alpha_\textrm{self}
            *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
            *    = - |\gamma| \varrho \nu_\textrm{t}
            *      \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}}
            * \f]
            * The first tangential case \f$\left( \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}} \right)\f$,
            * is calculated from the velocities of the perpendicular faces of the
            * inside element and the dirichlet value for the normal velocity.<br>
            * Currently no averaging is done for kinematic viscosity and density.
            */
          for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
          {
            // only tangential case, exclude parallel case
            if (tangDim != normDim)
            {
              // evaluate boundary condition functions
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCenter;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner0;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner1;
              Dune::FieldVector<Scalar, dim> insideFaceCornerLocal0(insideFaceCenterLocal);
              Dune::FieldVector<Scalar, dim> insideFaceCornerLocal1(insideFaceCenterLocal);
              insideFaceCornerLocal0[tangDim] = 0.0;
              insideFaceCornerLocal1[tangDim] = 1.0;

              dirichletVelocity.evaluate(ig.inside(), insideFaceCenterLocal, dirichletVelocityCenter);
              dirichletVelocity.evaluate(ig.inside(), insideFaceCornerLocal0, dirichletVelocityCorner0);
              dirichletVelocity.evaluate(ig.inside(), insideFaceCornerLocal1, dirichletVelocityCorner1);

              if(enableNavierStokes_)
              {
#ifndef DisableVEddyViscosity
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                                -0.5 * density_s * kinematicEddyViscosity_s
                                * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                                  / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);

                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                                -0.5 * density_s * kinematicEddyViscosity_s
                                * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                                  / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);
                if (!enableUnsymmetrizedVelocityGradient_)
                {
                  r_s.accumulate(lfsu_v_s, 2*tangDim,
                                  -0.5 * density_s * kinematicEddyViscosity_s
                                  * velocityGradients_s[normDim][tangDim]
                                  * faceUnitOuterNormal[normDim]
                                  * faceVolume);

                  r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                                  -0.5 * density_s * kinematicEddyViscosity_s
                                  * velocityGradients_s[normDim][tangDim]
                                  * faceUnitOuterNormal[normDim]
                                  * faceVolume);
                }
#endif
              }

              if (!enableUnsymmetrizedVelocityGradient_)
              {
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                                -0.5 * density_s * kinematicViscosity_s
                                * velocityGradients_s[normDim][tangDim]
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);

                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                                -0.5 * density_s * kinematicViscosity_s
                                * velocityGradients_s[normDim][tangDim]
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);
              }
            }
          }
        }
        else if (bcVelocity.isWall(ig, faceCenterLocal) && asImp_().useWallFunctionMomentum(elementID))
        {
          //! Do nothing for matching points in the k-epsilon model
        }
        // Outflow boundary for velocity
        else if (bcVelocity.isOutflow(ig, faceCenterLocal))
        {
          //! Nothing has to be done here.
        }
        // Symmetry boundary for velocity
        else if (bcVelocity.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        // Coupling boundary for velocity
        else if (bcVelocity.isCoupling(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Coupling Condition for velocity.
          //! Do nothing for matching points in the k-epsilon model
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for velocity.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * This function calls the ParentType and additionally
       * Traverses the grid and initialize values, needed for the eddy viscosity models.
       */
      void initialize()
      {
        ParentTypeMassMomentum::initialize();

        if (enableNavierStokes_ == false)
        {
          std::cout << "Navier Stokes is not enabled, but eddy viscosity should be used. This does not make sense!\n";
        }

        std::cout << "EnableUnsymmetrizedVelocityGradient = " << enableUnsymmetrizedVelocityGradient_ << std::endl;
        std::cout << "WallNormalAxis_ = " << wallNormalAxis_ << std::endl;
        std::cout << "FlowNormalAxis_ = " << flowNormalAxis_ << std::endl;

        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

        // velocities and coordinates stored at faces
        storedVelocityGradientTensor.resize(mapperElement.size());
        storedCorrespondingWallElementID.resize(mapperElement.size());
        storedOppositeWallElementID.resize(mapperElement.size());
        storedElementHasWall.resize(mapperElement.size());
        storedCorrespondingWallGlobal.resize(mapperElement.size());
        storedCorrespondingWallNormalAxis.resize(mapperElement.size());
        storedCorrespondingFlowNormalAxis.resize(mapperElement.size());
        storedDistanceToWall.resize(mapperElement.size());
        storedKinematicEddyViscosity.resize(mapperElement.size());
        storedVelocityInWallCoordinates.resize(mapperElement.size());
        storedDistanceInWallCoordinates.resize(mapperElement.size());
        storedRoughness.resize(mapperElement.size());
        storedAdditionalRoughnessLength.resize(mapperElement.size());
        storedVelocityMaximum.resize(mapperElement.size());
        storedVelocityMinimum.resize(mapperElement.size());
        storedNeighborID.resize(mapperElement.size());

        // initialize some of the values
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedNeighborID[i].resize(dim);
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedNeighborID[i][j].resize(2);
            for (unsigned int k = 0; k < 2; ++k)
            {
              storedNeighborID[i][j][k] = 0;
            }
          }
          storedElementHasWall[i] = false;
          storedCorrespondingWallNormalAxis[i] = 0;
          storedKinematicEddyViscosity[i] = 0.0;
          storedVelocityInWallCoordinates[i] = 0.0;
          storedDistanceInWallCoordinates[i] = std::numeric_limits<Scalar>::max();
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedVelocityMaximum[i][j] = 0.0;
            storedVelocityMinimum[i][j] = 0.0;
          }
          storedRoughness[i] = 0.0;
          storedAdditionalRoughnessLength[i] = 0.0;
        }

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<Scalar, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

          // store default case
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            for (unsigned int curElem = 0; curElem < 2; ++curElem)
            {
              storedNeighborID[elementInsideID][curDim][curElem] = elementInsideID;
            }
          }

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {

            if (!is->boundary()
#if IS_STAGGERED_MULTIDOMAIN_MODEL
                && gridView.indexSet().contains(stokesSubDomainIdx, is->inside())
                && gridView.indexSet().contains(stokesSubDomainIdx, is->outside())
#endif
                )
            {
              const typename GridView::IndexSet::IndexType neighborElementID = mapperElement.index(is->outside());
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(cellCenterGlobal[curDim]
                             - asImp_().storedElementCentersGlobal[neighborElementID][curDim]) > 1e-8)
                {
                  if (cellCenterGlobal[curDim] > asImp_().storedElementCentersGlobal[neighborElementID][curDim])
                    storedNeighborID[elementInsideID][curDim][0] = neighborElementID;
                  else
                    storedNeighborID[elementInsideID][curDim][1] = neighborElementID;
                }
              }
            }
          }
        }

        // vector containing element id's of boundary elements
        std::vector<typename GridView::IndexSet::IndexType> wallElementIDs(0);
        std::vector<Dune::FieldVector<Scalar, dim>> wallGlobals(0);
        std::vector<unsigned int> wallNormalAxis(0);
        wallElementIDs.clear();
        wallGlobals.clear();
        wallNormalAxis.clear();

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          storedCorrespondingWallGlobal[elementInsideID] = 0.0;

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          // element is not located in Stokes domain
          if (!gridView.indexSet().contains(stokesSubDomainIdx, eit))
              continue;
#endif

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(is->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = is->geometry().global(faceCenterLocal);

            if ((is->boundary()
#if IS_STAGGERED_MULTIDOMAIN_MODEL
                || (gridView.indexSet().contains(stokesSubDomainIdx, is->inside())
                    != gridView.indexSet().contains(stokesSubDomainIdx, is->outside()))
#endif
                )
                && (bcVelocity.isWall(*is, faceCenterLocal)
                    || bcVelocity.isCoupling(*is, faceCenterLocal)))
            {
              if (wallNormalAxis_ < 0) // no wallNormalAxis defined (auto-determine)
              {
                for (unsigned int curDim = 0; curDim < dim; ++curDim)
                {
                  if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
                  {
                    asImp_().storedElementHasWall[elementInsideID] = true;
                    wallElementIDs.push_back(elementInsideID);
                    wallGlobals.push_back(faceCenterGlobal);
                    wallNormalAxis.push_back(curDim);
                  }
                }
              }
              else // useWallNormalAxis_
              {
                if (std::abs(is->centerUnitOuterNormal()[wallNormalAxis_]) > 1e-10)
                {
                  asImp_().storedElementHasWall[elementInsideID] = true;
                  wallElementIDs.push_back(elementInsideID);
                  wallGlobals.push_back(faceCenterGlobal);
                  wallNormalAxis.push_back(wallNormalAxis_);
                }
              }
            }
          }
        }

        // stored velocities are only needed for tangential case of viscous term
        // which is only the case for dim > 1
        if (dim == 1)
        {
          return;
        }

        if (wallGlobals.size() == 0 && (wallNormalAxis_ < 0 || flowNormalAxis_ < 0))
        {
          std::cout << "ATTENTION: wallGlobals.size() is zero." << std::endl
                    << "Probably the problem has no walls and neither wallNormalAxis_ or flowNormalAxis_ axis are set." << std::endl
                    << "Setting the values has no influence but leads to segmentation faults and wrong outputs." << std::endl
                    << std::endl;
        }

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          Scalar distanceToWall = std::numeric_limits<Scalar>::max();
          Dune::FieldVector<Scalar, dim> temp;
          for (unsigned int i = 0; i < wallGlobals.size(); ++i)
          {
            temp = eit->geometry().center();
            temp -= wallGlobals[i];

            // discard elements which are not located above the wall when going along
            // a predefined wallNormalAxis
            if (!(wallNormalAxis_ < 0))
            {
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (curDim != wallNormalAxis_
                    && std::abs(wallGlobals[i][curDim] - eit->geometry().center()[curDim]) > 1e-10)
                  temp = 1e10;
              }
            }

            // the convention is to take the highest dim if the distance is the same
            if (temp.two_norm() < distanceToWall - 1e-10
                || (temp.two_norm() < distanceToWall + 1e-10
                    && storedCorrespondingWallNormalAxis[elementInsideID] <= wallNormalAxis[i]))
            {
              distanceToWall = temp.two_norm();
              storedCorrespondingWallElementID[elementInsideID] = wallElementIDs[i];
              storedCorrespondingWallGlobal[elementInsideID] = wallGlobals[i];
              storedCorrespondingWallNormalAxis[elementInsideID] = wallNormalAxis[i];
            }
          }

          storedDistanceToWall[elementInsideID] = distanceToWall;

          storedOppositeWallElementID[elementInsideID] = elementInsideID;
          for (unsigned int i = 0; i < wallGlobals.size(); ++i)
          {
            temp = eit->geometry().center();
            temp -= wallGlobals[i];
            Scalar otherAxisDifference = 0.0;
            unsigned int normDim = storedCorrespondingWallNormalAxis[elementInsideID];
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              if (curDim != normDim)
              {
                otherAxisDifference = temp[curDim];
              }
            }

            if (otherAxisDifference < 1e-10
                && temp[normDim] > storedDistanceToWall[elementInsideID])
            {
              storedOppositeWallElementID[elementInsideID] = wallElementIDs[i];
            }
          }

          // store variables for wall functions
          dirichletVelocity.wallNormalAxis_[elementInsideID] = storedCorrespondingWallNormalAxis[elementInsideID];
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesSubDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        ParentTypeMassMomentum::template updateStoredValues
                                <SubDomainGridView, MDGFS, X, stokesSubDomainIdx>
                                (sdgv, mdgfs, lastSolution);

        // grid function sub spaces
        using SubGfsVelocity = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesSubDomainIdx, velocityIdx> >;
        SubGfsVelocity subGfsVelocity(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);

        // grid function sub spaces
        using SubGfsVelocity = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<velocityIdx> >;
        SubGfsVelocity subGfsVelocity(gfs);
#endif

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // discrete function objects
        using DgfVelocity = Dune::PDELab::DiscreteGridFunction<SubGfsVelocity, X>;
        DgfVelocity dgfVelocity(subGfsVelocity, lastSolution);

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // NOTE use eit also for multidomain models
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          if (!gridView.indexSet().contains(stokesSubDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif

          // parallel case
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            Dune::FieldVector<Scalar, dim> faceCenterLocalLeft(0.5), faceCenterLocalRight(0.5);
            faceCenterLocalLeft[curDim] = 0.0;
            faceCenterLocalRight[curDim] = 1.0;

            Dune::FieldVector<Scalar, dim> faceCenterGlobalLeft(0.0), faceCenterGlobalRight(0.0);
            faceCenterGlobalLeft = eit->geometry().global(faceCenterLocalLeft);
            faceCenterGlobalRight = eit->geometry().global(faceCenterLocalRight);

            Dune::FieldVector<Scalar, dim> vLeft(0.0);
            Dune::FieldVector<Scalar, dim> vRight(0.0);
            dgfVelocity.evaluate(element, faceCenterLocalLeft, vLeft);
            dgfVelocity.evaluate(element, faceCenterLocalRight, vRight);

            asImp_().storedVelocityGradientTensor[elementIdInside][curDim][curDim]
              = (vRight[curDim] - vLeft[curDim]) / (faceCenterGlobalRight[curDim] - faceCenterGlobalLeft[curDim]);
          }
        }

        ///////////
        // loop for tangential storedVelocityGradientTensor
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.index(*eit);

          // tangential case
          if (dim > 1)
          {
            for (unsigned int vDim = 0; vDim < dim; ++vDim)
            {
              for (unsigned int xDim = 0; xDim < dim; ++xDim)
              {
                if (xDim != vDim)
                {
                  unsigned int leftID = storedNeighborID[elementIdInside][xDim][0];
                  unsigned int rightID = storedNeighborID[elementIdInside][xDim][1];

                  Dune::FieldVector<Scalar, dim> cellCenterGlobalLeft = asImp_().storedElementCentersGlobal[leftID];
                  Dune::FieldVector<Scalar, dim> cellCenterGlobalRight = asImp_().storedElementCentersGlobal[rightID];

                  Dune::FieldVector<Scalar, dim> vLeft = asImp_().storedVelocitiesAtElementCenter[leftID];
                  Dune::FieldVector<Scalar, dim> vRight = asImp_().storedVelocitiesAtElementCenter[rightID];

                  Scalar dist = cellCenterGlobalRight[xDim] - cellCenterGlobalLeft[xDim];
                  Scalar velDiff = vRight[vDim] - vLeft[vDim];

                  asImp_().storedVelocityGradientTensor[elementIdInside][vDim][xDim] = velDiff / dist;
                }
              }
            }
          }
        }

        ///////////
        // loop for tangential storedVelocityGradientTensor WITH inflow/wall BC
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementIdInside = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          // element is not located in Stokes domain
          if (!gridView.indexSet().contains(stokesSubDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif

          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(is->geometry().type()).position(0, 0);

            typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
            const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

            /**
             * \note for outflow and symmetry boundaries only the gradient from
              *       the domain-side is used
             */
            if ((is->boundary()
#if IS_STAGGERED_MULTIDOMAIN_MODEL
                || (gridView.indexSet().contains(stokesSubDomainIdx, is->inside())
                    != gridView.indexSet().contains(stokesSubDomainIdx, is->outside()))
#endif
                )
                 && (bcVelocity.isWall(*is, faceCenterLocal)
                    || bcVelocity.isInflow(*is, faceCenterLocal)
                    || bcVelocity.isCoupling(*is, faceCenterLocal)))
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curNormDim]) > 1e-10)
                {
                  normDim = curNormDim;
                }
              }

              const Dune::FieldVector<Scalar, dim>& insideFaceCenterLocal =
                is->geometryInInside().global(faceCenterLocal);
              Dune::FieldVector<Scalar, dim> faceCenterGlobal =
                is->geometry().global(faceCenterLocal);

              // evaluate boundary condition functions
              // NOTE: Do not evaluate the dgf (discrete grid function), really the value on the boundaries are needed
              typename DirichletVelocity::Traits::RangeType dirichletVelocityFace;
              dirichletVelocity.evaluate(*eit, insideFaceCenterLocal, dirichletVelocityFace);

              // update ALL cases
              // xDim is used because the normDim is the velocityDim here, too
              for (unsigned int xDim = 0; xDim < dim; ++xDim)
              {
                // update normal case
                if (normDim == xDim)
                {
                    Dune::FieldVector<Scalar, dim> faceCenterLocalLeft(0.5), faceCenterLocalRight(0.5);
                    faceCenterLocalLeft[normDim] = 0.0;
                    faceCenterLocalRight[normDim] = 1.0;

                    Dune::FieldVector<Scalar, dim> faceCenterGlobalLeft(0.0), faceCenterGlobalRight(0.0);
                    faceCenterGlobalLeft = eit->geometry().global(faceCenterLocalLeft);
                    faceCenterGlobalRight = eit->geometry().global(faceCenterLocalRight);

                    Dune::FieldVector<Scalar, dim> velocityOpposing(0.0);
                    Scalar velDiff = 0.0;
                    Scalar dist = 0.0;
                    if (faceCenterGlobal[normDim] < faceCenterGlobalRight[normDim] + 1e-10
                        && faceCenterGlobal[normDim] > faceCenterGlobalRight[normDim] - 1e-10)
                    {
                        dgfVelocity.evaluate(element, faceCenterLocalLeft, velocityOpposing);
                        velDiff = velocityOpposing[xDim] - dirichletVelocityFace[xDim];
                        dist = faceCenterGlobalLeft[normDim] - faceCenterGlobal[normDim];
                    }
                    else
                    {
                        dgfVelocity.evaluate(element, faceCenterLocalRight, velocityOpposing);
                        velDiff = velocityOpposing[xDim] - dirichletVelocityFace[xDim];
                        dist = faceCenterGlobalRight[normDim] - faceCenterGlobal[normDim];
                    }
                    asImp_().storedVelocityGradientTensor[elementIdInside][xDim][normDim] = velDiff / dist;
                }
                // WARNING sieht nicht aus als wäre das notwendig
//                 else
//                 {
//                     const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
//                       Dune::ReferenceElements<Scalar, dim>::general(is->inside().type()).position(0, 0);
//                     const Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
//                       is->inside().geometry().global(insideCellCenterLocal);
//
//                     Dune::FieldVector<Scalar, dim> adjacentFaceCenterLocalLeft(0.5), adjacentFaceCenterLocalRight(0.5);
//                     adjacentFaceCenterLocalLeft[xDim] = 0.0;
//                     adjacentFaceCenterLocalRight[xDim] = 1.0;
//
//                     Dune::FieldVector<Scalar, dim> insideFaceLocalLeft = insideFaceCenterLocal;
//                     Dune::FieldVector<Scalar, dim> insideFaceLocalRight = insideFaceCenterLocal;
//                     insideFaceLocalLeft[xDim] = 0.0;
//                     insideFaceLocalRight[xDim] = 0.0;
//
//                     Dune::FieldVector<Scalar, dim> velocityLeft(0.0);
//                     Dune::FieldVector<Scalar, dim> velocityRight(0.0);
//                     typename DirichletVelocity::Traits::RangeType faceVelocityLeft;
//                     typename DirichletVelocity::Traits::RangeType faceVelocityRight;
//                     dirichletVelocity.evaluate(*eit, insideFaceLocalLeft, faceVelocityLeft);
//                     dirichletVelocity.evaluate(*eit, insideFaceLocalRight, faceVelocityRight);
//
//                     dgfVelocity.evaluate(*eit, adjacentFaceCenterLocalLeft, velocityLeft);
//                     dgfVelocity.evaluate(*eit, adjacentFaceCenterLocalRight, velocityRight);
//                     Scalar velDiff = 0.5 * (velocityLeft[xDim] + velocityRight[xDim])
//                                      - 0.5 * (faceVelocityLeft[xDim] + faceVelocityRight[xDim]);
//                     Scalar dist = insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];
//
// //                     asImp_().storedVelocityGradientTensor[elementIdInside][xDim][normDim] += velDiff / dist;
//                 }
              }
            }
          }
        }

        // loop for storedCorrespondingFlowNormalAxis
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {

          if (dim > 1 && flowNormalAxis_ < 0)
          {
            storedCorrespondingFlowNormalAxis[mapperElement.index(*eit)] = 0;
            const typename GridView::IndexSet::IndexType wallElementID = storedCorrespondingWallElementID[mapperElement.index(*eit)];
            unsigned int wallNormalAxis = storedCorrespondingWallNormalAxis[mapperElement.index(*eit)];

            Scalar velocityGradient = 0.0;
            for (unsigned int vDim = 0; vDim < dim; ++vDim)
            {
              if (std::abs(velocityGradient) <
                  std::abs(storedVelocityGradientTensor[wallElementID][vDim][wallNormalAxis]))
              {
                velocityGradient = asImp_().storedVelocityGradientTensor[wallElementID][vDim][wallNormalAxis];
                storedCorrespondingFlowNormalAxis[mapperElement.index(*eit)] = vDim;
              }
            }
          }
        }

        ///////////
        // loop for normal storedWallValues and dirichletVelocity.Values
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          if (dim > 1)
          {
            const typename GridView::IndexSet::IndexType wallElementID = storedCorrespondingWallElementID[mapperElement.index(*eit)];
            unsigned int wallNormalAxis = storedCorrespondingWallNormalAxis[mapperElement.index(*eit)];
            unsigned int flowNormalAxis = storedCorrespondingFlowNormalAxis[mapperElement.index(*eit)]; // the flowNormalAxis of the wall decide which is the main flow axis above

            // zero valued velocityGradients may lead to inf values in the vtk output
            Scalar velocityGradient = std::max(1e-10, std::abs(storedVelocityGradientTensor[wallElementID][flowNormalAxis][wallNormalAxis]));

            storedVelocityInWallCoordinates[mapperElement.index(*eit)]
              = asImp_().storedVelocitiesAtElementCenter[mapperElement.index(*eit)][flowNormalAxis]
                / std::sqrt(velocityGradient * asImp_().storedKinematicViscosity[wallElementID]);
            storedDistanceInWallCoordinates[mapperElement.index(*eit)]
              = storedDistanceToWall[mapperElement.index(*eit)]
                * std::sqrt(asImp_().storedKinematicViscosity[wallElementID] * velocityGradient)
                / asImp_().storedKinematicViscosity[wallElementID];

            // Store variable for wall functions
            dirichletVelocity.storedVelocityGradientTensor_[mapperElement.index(*eit)] = asImp_().storedVelocityGradientTensor[mapperElement.index(*eit)];
          }
        }

        // loop for maximum velocities
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          for (unsigned int j = 0; j < dim; ++j)
          {
            storedVelocityMaximum[elementInsideID][j] = std::numeric_limits<Scalar>::min();
            storedVelocityMinimum[elementInsideID][j] = std::numeric_limits<Scalar>::max();
          }

          // get maximum and minimum velocities
          for (ElementIterator eit2 = gridView.template begin<0>();
               eit2 != gridView.template end<0>(); ++eit2)
          {
            const typename GridView::IndexSet::IndexType elementInsideID2 = mapperElement.index(eit2);
            const typename GridView::IndexSet::IndexType wallElementID2 = storedCorrespondingWallElementID[mapperElement.index(eit2)];

            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              // maximum velocity
              if (storedVelocityMaximum[wallElementID2][curDim] < asImp_().storedVelocitiesAtElementCenter[elementInsideID2][curDim])
              {
                storedVelocityMaximum[wallElementID2][curDim] = asImp_().storedVelocitiesAtElementCenter[elementInsideID2][curDim];
              }

              // minimum velocity
              if (storedVelocityMinimum[wallElementID2][curDim] > asImp_().storedVelocitiesAtElementCenter[elementInsideID2][curDim])
              {
                storedVelocityMinimum[wallElementID2][curDim] = asImp_().storedVelocitiesAtElementCenter[elementInsideID2][curDim];
              }
            }

            // ensure non-zero values
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
            {
              // maximum velocity
              if (storedVelocityMaximum[wallElementID2][curDim] < 1e-10
                  && storedVelocityMaximum[wallElementID2][curDim] > -1e-10)
              {
                storedVelocityMaximum[wallElementID2][curDim] = 1e-10;
              }
            }
          }
        }
      }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;

      MapperElement mapperElement;
      MapperIntersection mapperIntersection;

      // properties
      bool enableNavierStokes_;
      bool enableUnsymmetrizedVelocityGradient_;
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;
      int wallNormalAxis_;
      int flowNormalAxis_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_EDDY_VISCOSITY_STAGGERED_GRID_HH
