/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup SpalartAllmarasStaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization
 *        for the Spalart-Allmaras turbulence model.
 */

#ifndef DUMUX_BASE_SPALARTALLMARAS_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_BASE_SPALARTALLMARAS_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"spalartallmaraspropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the viscosityTilde equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseSpalartAllmarasTransientStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, TransientLocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { viscosityTildeIdx = Indices::viscosityTildeIdx };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      BaseSpalartAllmarasTransientStaggeredGrid(GridView gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \copydoc BaseNavierStokesTransientStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_spalartallmaras(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                        std::vector<DimVector> velocityFaces, Scalar pressure,
                                        Scalar massMoleFrac, Scalar temperature) const
      {
        typedef typename LFSU::template Child<viscosityTildeIdx>::Type LFSU_N;
        const LFSU_N& lfsu_n = lfsu.template child<viscosityTildeIdx>();

        const Scalar viscosityTilde = asImp_().viscosityTilde(eg, lfsu, x);
        const Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Storage term of <b> viscosityTilde </b> balance equation
          * \f[
          *    \frac{\partial}{\partial t} \tilde{\nu}
          * \f].
          */
        r.accumulate(lfsu_n, 0,
                     viscosityTilde * elementVolume);
      }

      /**
       * \brief Returns the viscosityTilde for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar viscosityTilde(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<viscosityTildeIdx>::Type LFSU_N;
        const LFSU_N& lfsu_n = lfsu.template child<viscosityTildeIdx>();
        return x(lfsu_n, 0);
      }

private:
      //! Instationary variables
      double time;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_BASE_SPALARTALLMARAS_TRANSIENT_STAGGERED_GRID_HH
