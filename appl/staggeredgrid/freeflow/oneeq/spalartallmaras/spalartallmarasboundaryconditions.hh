// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for Spalart-Allmaras boundary conditions
 */
#ifndef DUMUX_SPALARTALLMARAS_BOUNDARY_CONDITIONS_HH
#define DUMUX_SPALARTALLMARAS_BOUNDARY_CONDITIONS_HH

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesboundaryconditions.hh>

#include"spalartallmarasproperties.hh"
#include"spalartallmaraspropertydefaults.hh"

namespace Dumux
{
/**
 * \brief Boundary condition function for the component viscosityTilde.
 */
template<class TypeTag>
class BCViscosityTilde
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCViscosityTilde (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Wall Boundary for viscosityTilde
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcViscosityTildeIsWall(global);
  }

  //! \brief Return whether Intersection is Inflow Boundary for viscosityTilde
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcViscosityTildeIsInflow(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for viscosityTilde
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcViscosityTildeIsOutflow(global);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for viscosityTilde
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcViscosityTildeIsSymmetry(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for viscosityTilde Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletViscosityTilde
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    DirichletViscosityTilde<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletViscosityTilde<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  DirichletViscosityTilde (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluate(const typename Traits::ElementType& e,
                       const typename Traits::DomainType& x,
                       typename Traits::RangeType& y) const
  {
    const typename Traits::DomainType& global = e.geometry().global(x);
    y = problem_().dirichletViscosityTildeAtPos(e, global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for viscosityTilde Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannViscosityTilde
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    NeumannViscosityTilde<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannViscosityTilde<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannViscosityTilde (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannViscosityTildeAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Source term function for the viscosityTilde balance.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceViscosityTildeBalance
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    SourceViscosityTildeBalance<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceViscosityTildeBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceViscosityTildeBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceViscosityTildeBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

} // end namespace Dumux

#endif
