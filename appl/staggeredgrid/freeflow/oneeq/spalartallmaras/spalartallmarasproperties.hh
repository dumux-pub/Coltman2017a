// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 *
 * \file
 *
 * \brief Defines the properties required for the Spalart-Allmaras staggered grid model.
 */

#ifndef DUMUX_SPALARTALLMARAS_PROPERTIES_HH
#define DUMUX_SPALARTALLMARAS_PROPERTIES_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosityproperties.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the eddy viscosity problems
NEW_TYPE_TAG(StaggeredGridSpalartAllmaras, INHERITS_FROM(StaggeredGridEddyViscosity));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(BCViscosityTilde); //!< Property tag for the used ViscosityTilde boundary condition
NEW_PROP_TAG(SourceViscosityTildeBalance); //!< Property tag for the SourceViscosityTildeBalance
NEW_PROP_TAG(DirichletViscosityTilde); //!< Property tag for the DirichletViscosityTilde
NEW_PROP_TAG(NeumannViscosityTilde); //!< Property tag for the NeumannViscosityTilde

NEW_PROP_TAG(SpalartAllmarasModelConstants); //!< Type of the used set of constants for the Spalart-Allmaras model
NEW_PROP_TAG(SpalartAllmarasWallFunctionModel); //!< Type of the used Spalart-Allmaras wall function
NEW_PROP_TAG(SpalartAllmarasEnableKinematicViscosity); //!< Type of the used Spalart-Allmaras diffusion model
}
}

#endif // DUMUX_SPALARTALLMARAS_PROPERTIES_HH
