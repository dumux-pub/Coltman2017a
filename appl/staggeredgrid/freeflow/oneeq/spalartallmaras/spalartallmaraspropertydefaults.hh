// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 *
 * \file
 *
 * \brief Defines default properties for the Spalart-Allmaras staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_SPALARTALLMARAS_PROPERTY_DEFAULTS_HH
#define DUMUX_SPALARTALLMARAS_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscositypropertydefaults.hh>

#include"spalartallmarasboundaryconditions.hh"
#include"spalartallmarasproperties.hh"
#include"spalartallmarasindices.hh"

namespace Dumux
{

template<class TypeTag> class BCViscosityTilde;
template<class TypeTag, typename GridView, typename Scalar> class DirichletViscosityTilde;
template<class TypeTag, typename GridView, typename Scalar> class NeumannViscosityTilde;
template<class TypeTag, typename GridView, typename Scalar> class SourceViscosityTildeBalance;

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridSpalartAllmaras, Indices,
              StaggeredGridSpalartAllmarasCommonIndices<TypeTag>);

//! Set property value for the viscosityTilde boundary condition
SET_TYPE_PROP(StaggeredGridSpalartAllmaras, BCViscosityTilde, BCViscosityTilde<TypeTag>);

//! Set property value for BCType
SET_PROP(StaggeredGridSpalartAllmaras, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, BCViscosityTilde) BCViscosityTilde;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure,
                                                         BCViscosityTilde> type;
};

//! Set property value for DirichletViscosityTilde
SET_PROP(StaggeredGridSpalartAllmaras, DirichletViscosityTilde)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletViscosityTilde<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannViscosityTilde
SET_PROP(StaggeredGridSpalartAllmaras, NeumannViscosityTilde)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannViscosityTilde<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceViscosityTildeBalance
SET_PROP(StaggeredGridSpalartAllmaras, SourceViscosityTildeBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceViscosityTildeBalance<TypeTag, GridView, Scalar> type;
};

//! The axis to calculate wall distances (negative means determine automatically)
SET_INT_PROP(StaggeredGridSpalartAllmaras, ProblemWallNormalAxis, -1);

//! The main flow axis for calculating the eddy viscosity (negative means determine automatically)
SET_INT_PROP(StaggeredGridSpalartAllmaras, ProblemFlowNormalAxis, -1);

//! Factor, which is added to the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(StaggeredGridSpalartAllmaras, TimeManagerTimeStepIncrementFactor, 5.0/6.0);
}
}

#endif // DUMUX_SPALARTALLMARAS_PROPERTY_DEFAULTS_HH
