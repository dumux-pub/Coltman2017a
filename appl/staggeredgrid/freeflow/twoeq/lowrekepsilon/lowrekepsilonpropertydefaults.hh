// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 *
 * \file
 *
 * \brief Defines default properties for the low-re kepsilon staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_LOWREKEPSILON_PROPERTY_DEFAULTS_HH
#define DUMUX_LOWREKEPSILON_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscositypropertydefaults.hh>

#include"lowrekepsilonboundaryconditions.hh"
#include"lowrekepsilonproperties.hh"
#include"lowrekepsilonindices.hh"

namespace Dumux
{

template<class TypeTag> class BCTurbulentKineticEnergy;
template<class TypeTag> class BCDissipation;
template<class TypeTag, typename GridView, typename Scalar> class DirichletTurbulentKineticEnergy;
template<class TypeTag, typename GridView, typename Scalar> class DirichletDissipation;
template<class TypeTag, typename GridView, typename Scalar> class NeumannTurbulentKineticEnergy;
template<class TypeTag, typename GridView, typename Scalar> class NeumannDissipation;
template<class TypeTag, typename GridView, typename Scalar> class SourceTurbulentKineticEnergyBalance;
template<class TypeTag, typename GridView, typename Scalar> class SourceDissipationBalance;

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridLowReKEpsilon, Indices,
              StaggeredGridLowReKEpsilonCommonIndices<TypeTag>);

//! Set property value for the turbulentKineticEnergy boundary condition
SET_TYPE_PROP(StaggeredGridLowReKEpsilon, BCTurbulentKineticEnergy, BCTurbulentKineticEnergy<TypeTag>);

//! Set property value for the dissipation boundary condition
SET_TYPE_PROP(StaggeredGridLowReKEpsilon, BCDissipation, BCDissipation<TypeTag>);

//! Set property value for BCType
SET_PROP(StaggeredGridLowReKEpsilon, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, BCTurbulentKineticEnergy) BCTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, BCDissipation) BCDissipation;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure,
                                                         BCTurbulentKineticEnergy, BCDissipation> type;
};

//! Set property value for DirichletTurbulentKineticEnergy
SET_PROP(StaggeredGridLowReKEpsilon, DirichletTurbulentKineticEnergy)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletTurbulentKineticEnergy<TypeTag, GridView, Scalar> type;
};

//! Set property value for DirichletDissipation
SET_PROP(StaggeredGridLowReKEpsilon, DirichletDissipation)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletDissipation<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannTurbulentKineticEnergy
SET_PROP(StaggeredGridLowReKEpsilon, NeumannTurbulentKineticEnergy)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannTurbulentKineticEnergy<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannDissipation
SET_PROP(StaggeredGridLowReKEpsilon, NeumannDissipation)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannDissipation<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceTurbulentKineticEnergyBalance
SET_PROP(StaggeredGridLowReKEpsilon, SourceTurbulentKineticEnergyBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceTurbulentKineticEnergyBalance<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceDissipationBalance
SET_PROP(StaggeredGridLowReKEpsilon, SourceDissipationBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceDissipationBalance<TypeTag, GridView, Scalar> type;
};

//! The axis to calculate wall distances (negative means determine automatically)
SET_INT_PROP(StaggeredGridLowReKEpsilon, ProblemWallNormalAxis, -1);

//! The main flow axis for calculating the eddy viscosity (negative means determine automatically)
SET_INT_PROP(StaggeredGridLowReKEpsilon, ProblemFlowNormalAxis, -1);

//! Use Chien model by default
SET_INT_PROP(StaggeredGridLowReKEpsilon, LowReKEpsilonModel, 1);

//! Factor, which is added to the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(StaggeredGridLowReKEpsilon, TimeManagerTimeStepIncrementFactor, 0.5);
}
}

#endif // DUMUX_LOWREKEPSILON_PROPERTY_DEFAULTS_HH
