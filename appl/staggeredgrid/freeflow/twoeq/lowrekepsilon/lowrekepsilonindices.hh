// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Defines the indices required for the low-re kepsilon staggered grid model.
 */
#ifndef DUMUX_LOWREKEPSILON_INDICES_HH
#define DUMUX_LOWREKEPSILON_INDICES_HH

#include<dune/common/exceptions.hh>
#include "lowrekepsilonproperties.hh"

namespace Dumux
{

/*!
 * \brief The common indices for the low-re kepsilon staggered grid model.
 */
template <class TypeTag>
struct StaggeredGridLowReKEpsilonCommonIndices
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    //! Index of the fluid phase set by default to nPhase
    static const int phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);

    // Primary variable indices
    static const int numEq = 4;
    static const int velocityIdx = 0; //!< Index of the velocity
    static const int pressureIdx = 1; //!< Index of the pressure
    static const int turbulentKineticEnergyIdx = 2; //!< Index of the turbulent kinetic energy
    static const int dissipationIdx = 3; //!< Index of the dissipation

    // LowReKEpsilon models
    static const int standardHighReKEpsilon = 0;
    static const int chien = 1;
    static const int lamBremhorst = 2;
};
} // end namespace

#endif // DUMUX_LOWREKEPSILON_INDICES_HH
