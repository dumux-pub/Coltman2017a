/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        Navier-Stokes equation, modeled with low-re \f$ k-\varepsilon \f$ - turbulence
 *        models
 *
 * \copydoc BaseNavierStokesTransientStaggeredGrid
 * \copydoc EddyViscosityStaggeredGrid
 * \copydoc BaseLowReLowReKEpsilonStaggeredGrid
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_LOWREKEPSILON_STAGGERED_GRID_HH
#define DUMUX_LOWREKEPSILON_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/onecomponentfluid.hh>
#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscositystaggeredgrid.hh>

#include"baselowrekepsilonstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state low-re kepsilon and Navier-Stokes equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class LowReKEpsilonStaggeredGrid
    : public NumericalJacobianApplyVolume<LowReKEpsilonStaggeredGrid<TypeTag> >,
      public NumericalJacobianVolume<LowReKEpsilonStaggeredGrid<TypeTag> >,
      public NumericalJacobianSkeleton<LowReKEpsilonStaggeredGrid<TypeTag> >,
      public NumericalJacobianBoundary<LowReKEpsilonStaggeredGrid<TypeTag> >,
      public FullVolumePattern,
      public FullSkeletonPattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<double>,
      public EddyViscosityStaggeredGrid<TypeTag>,
      public BaseLowReKEpsilonStaggeredGrid<TypeTag>,
      public Dumux::OneComponentFluid<TypeTag>
    {
    public:
      typedef EddyViscosityStaggeredGrid<TypeTag> ParentTypeMassMomentum;
      typedef BaseLowReKEpsilonStaggeredGrid<TypeTag> ParentTypeKEpsilon;
      typedef Dumux::OneComponentFluid<TypeTag> BaseFluid;

      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceTurbulentKineticEnergyBalance) SourceTurbulentKineticEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceDissipationBalance) SourceDissipationBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTurbulentKineticEnergy) DirichletTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletDissipation) DirichletDissipation;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTurbulentKineticEnergy) NeumannTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannDissipation) NeumannDissipation;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      enum { dim = GridView::dimension };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      LowReKEpsilonStaggeredGrid(const BC& bc_,
        const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
        const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance_, const SourceDissipationBalance& sourceDissipationBalance_,
        const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
        const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy_, const DirichletDissipation& dirichletDissipation_,
        const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
        const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy_, const NeumannDissipation& neumannDissipation_,
        GridView gridView_, Problem& problem)
        : ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          ParentTypeKEpsilon(bc_,
              sourceTurbulentKineticEnergyBalance_, sourceDissipationBalance_,
              dirichletTurbulentKineticEnergy_, dirichletDissipation_,
              neumannTurbulentKineticEnergy_, neumannDissipation_,
              gridView_, problem),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          sourceTurbulentKineticEnergyBalance(sourceTurbulentKineticEnergyBalance_), sourceDissipationBalance(sourceDissipationBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          dirichletTurbulentKineticEnergy(dirichletTurbulentKineticEnergy_), dirichletDissipation(dirichletDissipation_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          neumannTurbulentKineticEnergy(neumannTurbulentKineticEnergy_), neumannDissipation(neumannDissipation_),
          gridView(gridView_), mapperElement(gridView_), problemPtr_(0)
      {
        problemPtr_ = &problem;
        initialize();
      }

      /**
       * \copydoc BaseLowReKEpsilonStaggeredGrid::alpha_volume
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> cellCenterGlobal = eg.geometry().global(cellCenterLocal);

        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(eg, lfsu, x);
        Scalar pressure = ParentTypeMassMomentum::pressure(eg, lfsu, x);
        Scalar massMoleFrac = problem_().massMoleFracAtPos(cellCenterGlobal);
        Scalar temperature = problem_().temperatureAtPos(cellCenterGlobal);

        ParentTypeKEpsilon::alpha_volume_kepsilon(eg, lfsu, x, lfsv, r,
                                                  velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
      }


      /**
       * \copydoc BaseLowReKEpsilonStaggeredGrid::alpha_skeleton
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);

        std::vector<DimVector> velocities_s = ParentTypeMassMomentum::velocity(ig.inside(), lfsu_s, x_s);
        std::vector<DimVector> velocities_n = ParentTypeMassMomentum::velocity(ig.outside(), lfsu_n, x_n);
        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        Scalar pressure_n = ParentTypeMassMomentum::pressure(ig.outside(), lfsu_n, x_n);
        Scalar massMoleFrac_s = problem_().massMoleFracAtPos(insideCellCenterGlobal);
        Scalar massMoleFrac_n = problem_().massMoleFracAtPos(outsideCellCenterGlobal);
        Scalar temperature_s = problem_().temperatureAtPos(insideCellCenterGlobal);
        Scalar temperature_n = problem_().temperatureAtPos(outsideCellCenterGlobal);
        ParentTypeKEpsilon::alpha_skeleton_kepsilon(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                    velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                    velocities_n, pressure_n, massMoleFrac_n, temperature_n);
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseLowReKEpsilonStaggeredGrid::alpha_boundary
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        // inside element coordinates
        const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> cellCenterGlobal = ig.inside().geometry().global(cellCenterLocal);
        // boundary face coordinates
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal = ig.geometry().global(faceCenterLocal);

        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(ig.inside(), lfsu_s, x_s);

        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        typename DirichletPressure::Traits::RangeType pressure_boundary(0.0);
        dirichletPressure.evaluateGlobal(faceCenterGlobal, pressure_boundary);

        Scalar massMoleFrac_s = problem_().massMoleFracAtPos(cellCenterGlobal);
        Scalar massMoleFrac_boundary = problem_().massMoleFracAtPos(faceCenterGlobal);
        Scalar temperature_s = problem_().temperatureAtPos(cellCenterGlobal);
        Scalar temperature_boundary = problem_().temperatureAtPos(faceCenterGlobal);
        ParentTypeKEpsilon::alpha_boundary_kepsilon(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                    velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                    pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);
      }

      /**
       * \copydoc BaseLowReKEpsilonStaggeredGrid::initialize
       * \copydoc BaseNavierStokesStaggeredGrid::initialize
       */
      void initialize()
      {
        ParentTypeMassMomentum::initialize();
        ParentTypeKEpsilon::initialize();
      }

      /**
       * \copydoc BaseLowReKEpsilonStaggeredGrid::updateStoredValues
       * \copydoc BaseNavierStokesStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
#endif
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
              Dune::ReferenceElements<Scalar, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<Scalar, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

          asImp_().storedTemperature[elementInsideID] = problem_().temperatureAtPos(cellCenterGlobal);
          asImp_().storedMassMoleFrac[elementInsideID] = problem_().massMoleFracAtPos(cellCenterGlobal);
        }

#if IS_STAGGERED_MULTIDOMAIN_MODEL
        ParentTypeMassMomentum::template updateStoredValues
                                <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                                (sdgv, mdgfs, lastSolution);
        ParentTypeKEpsilon::template updateStoredValues
                            <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                            (sdgv, mdgfs, lastSolution);
#else
        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);
        ParentTypeKEpsilon::updateStoredValues(gfs, lastSolution);
#endif
      }

      //! \brief Only relevant for k-epsilon
      const bool useWallFunctionMomentum(unsigned int elementID) const
      { return false; }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance;
      const SourceDissipationBalance& sourceDissipationBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy;
      const DirichletDissipation& dirichletDissipation;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy;
      const NeumannDissipation& neumannDissipation;
      GridView gridView;
      MapperElement mapperElement;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_LOWREKEPSILON_STAGGERED_GRID_HH
