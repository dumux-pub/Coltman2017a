/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization for
 *        the component and heat transport equation with low-Re kepsilon.
 */

#ifndef DUMUX_LOWREKEPSILON_TWOCNI_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_LOWREKEPSILON_TWOCNI_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/twocomponentfluid.hh>
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/basenavierstokestransientstaggeredgrid.hh>
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/basecomponenttransientstaggeredgrid.hh>
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/baseenergytransientstaggeredgrid.hh>

#include"../lowrekepsilon/baselowrekepsilontransientstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the compositional non-isothermal Navier-Stokes equation
     *        with low-Re kepsilon
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class LowReKEpsilonTwoCNITransientStaggeredGrid
    : public NumericalJacobianVolume<LowReKEpsilonTwoCNITransientStaggeredGrid<TypeTag> >,
      public NumericalJacobianApplyVolume<LowReKEpsilonTwoCNITransientStaggeredGrid<TypeTag> >,
      public FullVolumePattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<double>,
      public BaseLowReKEpsilonTransientStaggeredGrid<TypeTag>,
      public BaseNavierStokesTransientStaggeredGrid<TypeTag>,
      public BaseComponentTransientStaggeredGrid<TypeTag>,
      public BaseEnergyTransientStaggeredGrid<TypeTag>,
      public Dumux::TwoComponentFluid<TypeTag>
    {
    public:
      typedef BaseLowReKEpsilonTransientStaggeredGrid<TypeTag> ParentTypeKEpsilon;
      typedef BaseNavierStokesTransientStaggeredGrid<TypeTag> ParentTypeMassMomentum;
      typedef BaseComponentTransientStaggeredGrid<TypeTag> ParentTypeComponent;
      typedef BaseEnergyTransientStaggeredGrid<TypeTag> ParentTypeEnergy;
      typedef Dumux::TwoComponentFluid<TypeTag> BaseFluid;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      enum { dim = GridView::dimension };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      LowReKEpsilonTwoCNITransientStaggeredGrid(GridView gridView_)
        : ParentTypeKEpsilon(gridView_),
          ParentTypeMassMomentum(gridView_),
          ParentTypeComponent(gridView_),
          ParentTypeEnergy(gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \brief Contribution to volume integral.
       *
       * This function just calls the ParentType.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(eg, lfsu, x);
        Scalar pressure = ParentTypeMassMomentum::pressure(eg, lfsu, x);
        Scalar massMoleFrac = ParentTypeComponent::massMoleFrac(eg, lfsu, x);
        Scalar temperature = ParentTypeEnergy::temperature(eg, lfsu, x);

        ParentTypeKEpsilon::alpha_volume_kepsilon(eg, lfsu, x, lfsv, r,
                                                  velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeComponent::alpha_volume_component(eg, lfsu, x, lfsv, r,
                                                    velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeEnergy::alpha_volume_energy(eg, lfsu, x, lfsv, r,
                                              velocityFaces, pressure, massMoleFrac, temperature);
      }

      //! \brief Returns the density [kg/m^3]
      const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::density(pressure, temperature, massMoleFrac); }

      //! \brief Returns the internal energy [J/kg]
      const Scalar internalEnergy(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::internalEnergy(pressure, temperature, massMoleFrac); }

private:
      //! Instationary variables
      double time;
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_LOWREKEPSILON_TWOCNI_TRANSIENT_STAGGERED_GRID_HH
