
install(FILES
        lowrekepsilon2cniindices.hh
        lowrekepsilon2cniproblem.hh
        lowrekepsilon2cniproperties.hh
        lowrekepsilon2cnipropertydefaults.hh
        lowrekepsilon2cnistaggeredgrid.hh
        lowrekepsilon2cnitransientstaggeredgrid.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni)
