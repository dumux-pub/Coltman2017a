// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines the properties required for the compositional non-isothermal
 *        kepsilon staggered grid model.
 */

#ifndef DUMUX_KEPSILON_TWOCNI_PROPERTIES_HH
#define DUMUX_KEPSILON_TWOCNI_PROPERTIES_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosity2cniproperties.hh>
#include"../kepsilon/kepsilonproperties.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the eddy viscosity problems
NEW_TYPE_TAG(StaggeredGridKEpsilon2cni, INHERITS_FROM(StaggeredGridKEpsilon, StaggeredGridEddyViscosity2cni));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
}
}

#endif // DUMUX_KEPSILON_TWOCNI_PROPERTIES_HH
