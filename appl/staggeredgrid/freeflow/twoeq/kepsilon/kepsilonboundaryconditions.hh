// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for kepsilon boundary conditions
 */
#ifndef DUMUX_KEPSILON_BOUNDARY_CONDITIONS_HH
#define DUMUX_KEPSILON_BOUNDARY_CONDITIONS_HH

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesboundaryconditions.hh>

#include"kepsilonproperties.hh"
#include"kepsilonpropertydefaults.hh"

namespace Dumux
{
/**
 * \brief Boundary condition function for the component turbulentKineticEnergy.
 */
template<class TypeTag>
class BCTurbulentKineticEnergy
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCTurbulentKineticEnergy (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Wall Boundary for turbulentKineticEnergy
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTurbulentKineticEnergyIsWall(global);
  }

  //! \brief Return whether Intersection is Inflow Boundary for turbulentKineticEnergy
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTurbulentKineticEnergyIsInflow(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for turbulentKineticEnergy
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTurbulentKineticEnergyIsOutflow(global);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for turbulentKineticEnergy
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcTurbulentKineticEnergyIsSymmetry(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Boundary condition function for the component dissipation.
 */
template<class TypeTag>
class BCDissipation
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCDissipation (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Wall Boundary for dissipation
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcDissipationIsWall(global);
  }

  //! \brief Return whether Intersection is Inflow Boundary for dissipation
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcDissipationIsInflow(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for dissipation
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcDissipationIsOutflow(global);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for dissipation
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcDissipationIsSymmetry(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for turbulentKineticEnergy Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletTurbulentKineticEnergy
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    DirichletTurbulentKineticEnergy<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletTurbulentKineticEnergy<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  DirichletTurbulentKineticEnergy (const GridView& gridView, Problem& problem)
  : BaseT(gridView)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function per element
  inline void evaluate(const typename Traits::ElementType& e,
                       const typename Traits::DomainType& x,
                       typename Traits::RangeType& y) const
  {
    const typename Traits::DomainType& global = e.geometry().global(x);
    y = problem_().dirichletTurbulentKineticEnergyAtPos(e, global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for dissipation Dirichlet boundary conditions and initialization.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletDissipation
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    DirichletDissipation<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletDissipation<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  DirichletDissipation (const GridView& gridView, Problem& problem)
  : BaseT(gridView)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function per element
  inline void evaluate(const typename Traits::ElementType& e,
                       const typename Traits::DomainType& x,
                       typename Traits::RangeType& y) const
  {
    const typename Traits::DomainType& global = e.geometry().global(x);
    y = problem_().dirichletDissipationAtPos(e, global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Function for turbulentKineticEnergy Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannTurbulentKineticEnergy
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    NeumannTurbulentKineticEnergy<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannTurbulentKineticEnergy<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannTurbulentKineticEnergy (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannTurbulentKineticEnergyAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for dissipation Neumann boundary conditions and initialization.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannDissipation
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    NeumannDissipation<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannDissipation<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannDissipation (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannDissipationAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Source term function for the component balance.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceTurbulentKineticEnergyBalance
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    SourceTurbulentKineticEnergyBalance<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceTurbulentKineticEnergyBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceTurbulentKineticEnergyBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceTurbulentKineticEnergyBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Source term function for the energy balance.
 *
 * \tparam GV GridView type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceDissipationBalance
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    SourceDissipationBalance<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar,1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceDissipationBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceDissipationBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceDissipationBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


} // end namespace Dumux

#endif
