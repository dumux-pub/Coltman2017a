/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup KEpsilonStaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization
 *        for the \f$ k-\varepsilon \f$ - turbulence models.
 *
 * \copydoc BaseKepsilonTransientStaggeredGrid
 */

#ifndef DUMUX_KEPSILON_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_KEPSILON_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/onecomponentfluid.hh>
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes/basenavierstokestransientstaggeredgrid.hh>

#include"basekepsilontransientstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the \f$ k-\varepsilon \f$  equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class KEpsilonTransientStaggeredGrid
    : public NumericalJacobianVolume<KEpsilonTransientStaggeredGrid<TypeTag> >,
      public NumericalJacobianApplyVolume<KEpsilonTransientStaggeredGrid<TypeTag> >,
      public FullVolumePattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<double>,
      public BaseNavierStokesTransientStaggeredGrid<TypeTag>,
      public BaseKEpsilonTransientStaggeredGrid<TypeTag>,
      public Dumux::OneComponentFluid<TypeTag>
    {
    public:
      typedef BaseNavierStokesTransientStaggeredGrid<TypeTag> ParentTypeMassMomentum;
      typedef BaseKEpsilonTransientStaggeredGrid<TypeTag> ParentTypeKEpsilon;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      enum { dim = GridView::dimension };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      KEpsilonTransientStaggeredGrid(GridView gridView_, Problem& problem)
        : ParentTypeMassMomentum(gridView_), ParentTypeKEpsilon(gridView_), problemPtr_(0)
      {
        problemPtr_ = &problem;
      }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \brief Contribution to volume integral.
       *
       * This function just calls the ParentType.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> cellCenterGlobal = eg.geometry().global(cellCenterLocal);

        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(eg, lfsu, x);
        Scalar pressure = ParentTypeMassMomentum::pressure(eg, lfsu, x);
        Scalar massMoleFrac = problem_().massMoleFracAtPos(cellCenterGlobal);
        Scalar temperature = problem_().temperatureAtPos(cellCenterGlobal);

        ParentTypeKEpsilon::alpha_volume_kepsilon(eg, lfsu, x, lfsv, r,
                                                  velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
      }

private:
      //! Instationary variables
      double time;

protected:
      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_KEPSILON_TRANSIENT_STAGGERED_GRID_HH
