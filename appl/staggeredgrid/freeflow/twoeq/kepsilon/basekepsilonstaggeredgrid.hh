/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup BaseKEpsilonStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with \f$ k-\varepsilon \f$ - turbulence
 * models
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * The eddy viscosity depends on values for \f$ k \f$ and \f$ \varepsilon \f$:
 * \f[
 *    \nu_{\alpha,\textrm{t}}
 *    = \nu_{\alpha,\textrm{t}} \left( k, \varepsilon \right)
 *    > 0
 * \f]
 *
 * Turbulent Kinetic Energy balance:
 * \f[
 *    \frac{\partial \left( k \right)}{\partial t}
 *    + \nabla \cdot \left( v_\alpha k \right)
 *    - \nabla \cdot \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right)
 *    - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
 *    + \varepsilon
 *    - q_{k}
 *    = 0
 * \f]
 * with \f$ S_{ij} = \frac{1}{2} \left[ \frac{\partial}{\partial x_i} v_j + \frac{\partial}{\partial x_j} v_i \right] \f$
 * and \f$ a_{ij} \cdot b_{ij} = \sum_{i,j} a_{ij} b_{ij} \f$.
 *
 * Dissipation balance:
 * \f[
 *    \frac{\partial \left( \varepsilon \right)}{\partial t}
 *    + \nabla \cdot \left( v_\alpha \varepsilon \right)
 *    - \nabla \cdot \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \right) \nabla \varepsilon \right)
 *    - C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
 *    + C_{2\varepsilon} \frac{\varepsilon^2}{k}
 *    - q_{\varepsilon}
 *    = 0
 * \f]
 *
 * \note The dilation term is not included in the \f$ k-\varepsilon \f$ model
 *
 * The use of the kinematic fluid viscosity for the diffusion term in the \f$ k \f$
 * and the \f$ \varepsilon \f$ equations can be disabled by setting the property
 * <tt> KEpsilonDisableKinematicViscosity</tt>.
 * \note The kinematic fluid viscosity is included in [Wilcox, 2006], [OpenFOAM]
 *       and CFD online. It is not included in [Versteeg and Malalasekra, 2009]
 *       and [Pope 2006].
 *
 * The kinematic eddy viscosity is calculated as follows:
 * \f[
 *    \nu_\textrm{t} = C_\mu \frac{k^2}{\varepsilon}
 * \f]
 *
 * The constants have the values
 * \f[
 *   \sigma_k = 1.00 \;,\;
 *   \sigma_\varepsilon =1.30 \;,\;
 *   C_{1\varepsilon} = 1.44 \;,\;
 *   C_{2\varepsilon} = 1.92 \;,\;
 *   C_\mu = 0.09
 * \f]
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_KEPSILON_STAGGERED_GRID_HH
#define DUMUX_BASE_KEPSILON_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

// #include<dune/istl/io.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"kepsilonpropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state kepsilon equations.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseKEpsilonStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceTurbulentKineticEnergyBalance) SourceTurbulentKineticEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceDissipationBalance) SourceDissipationBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTurbulentKineticEnergy) DirichletTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletDissipation) DirichletDissipation;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTurbulentKineticEnergy) NeumannTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannDissipation) NeumannDissipation;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             turbulentKineticEnergyIdx = Indices::turbulentKineticEnergyIdx,
             dissipationIdx = Indices::dissipationIdx };

      enum { phaseIdx = Indices::phaseIdx };

      //! element stored data
      typedef std::vector<double> StoredScalar;
      mutable StoredScalar storedTurbulentKineticEnergy;
      mutable StoredScalar storedDissipation;
      std::vector<unsigned int> storedMatchingPointID;
      std::vector<bool> storedWallHasMatchingPoint;
      std::vector<bool> storedElementIsMatchingPoint;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      BaseKEpsilonStaggeredGrid(const BC& bc_,
                          const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance_, const SourceDissipationBalance& sourceDissipationBalance_,
                          const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy_, const DirichletDissipation& dirichletDissipation_,
                          const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy_, const NeumannDissipation& neumannDissipation_,
                          GridView gridView_, Problem& problem)
        : bc(bc_),
          sourceTurbulentKineticEnergyBalance(sourceTurbulentKineticEnergyBalance_), sourceDissipationBalance(sourceDissipationBalance_),
          dirichletTurbulentKineticEnergy(dirichletTurbulentKineticEnergy_), dirichletDissipation(dirichletDissipation_),
          neumannTurbulentKineticEnergy(neumannTurbulentKineticEnergy_), neumannDissipation(neumannDissipation_),
          gridView(gridView_), mapperElement(gridView), problemPtr_(0)
      {
        // properties
        useStoredEddyViscosity_ = GET_PROP_VALUE(TypeTag, KEpsilonUseStoredEddyViscosity);

        // parameter
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);
        kEpsilonEnableKinematicViscosity_= GET_PARAM_FROM_GROUP(TypeTag, bool, KEpsilon, EnableKinematicViscosity);
        kEpsilonModelConstants_ = GET_PARAM_FROM_GROUP(TypeTag, int, KEpsilon, ModelConstants);
        wallFunctionModel_ = GET_PARAM_FROM_GROUP(TypeTag, int, KEpsilon, WallFunctionModel);

        problemPtr_ = &problem;
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_kepsilon(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                 std::vector<DimVector> velocityFaces, Scalar pressure,
                                 Scalar massMoleFrac, Scalar temperature) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e = lfsu.template child<dissipationIdx>();

        Scalar elementVolume = eg.geometry().volume();

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        Scalar turbulentKineticEnergy = x(lfsu_k, 0);
        Scalar dissipation = x(lfsu_e, 0);
        if (!useStoredEddyViscosity_)
        {
          asImp_().storedKinematicEddyViscosity[mapperElement.index(eg.entity())]
              = calculateKinematicEddyViscosity(turbulentKineticEnergy, dissipation);
        }
        Scalar kinematicEddyViscosity = asImp_().storedKinematicEddyViscosity[mapperElement.index(eg.entity())];

        // scalar product of velocity gradient tensor is the stress tenosr S_ij
        Dune::FieldMatrix<Scalar, GridView::dimension, GridView::dimension> stressTensor(0.0);
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            stressTensor[i][j] = 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][i][j];
            stressTensor[i][j] += 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][j][i];

            // NOTE: this would include the dilation term
//            if (j == i)
//            {
//              stressTensor[i][j] -= 1.0 / 3.0 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][i][j];
//            }
          }
        }
        Scalar stressTensorScalarProduct = 0.0;
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            stressTensorScalarProduct += (stressTensor[i][j] * stressTensor[i][j]);
          }
        }

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension
        /**
          * (1) \b Production term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          *    \Rightarrow - \int_V 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          * \f]
          * \f[
          *    \alpha = - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} V_e
          * \f]
          */
#ifndef DisableKProduction
        if (!asImp_().useWallFunctionMomentum(mapperElement.index(eg.entity())))
        {
            r.accumulate(lfsu_k, 0,
                         -2.0 * kinematicEddyViscosity * stressTensorScalarProduct
                         * elementVolume);
        }
// else
// {
// FLUENT "SHORTCUT" G_k = epsilon
//           r.accumulate(lfsu_k, 0,
//                         -1.0 * dissipation * elementVolume);
//
// VERSTEEG
//         r.accumulate(lfsu_k, 0,
//                      -asImp_().storedDensity[mapperElement.index(eg.entity())]
//                      * std::pow(cMu(),0.75)
//                      * std::pow(std::max(
//                           asImp_().storedTurbulentKineticEnergy[mapperElement.index(eg.entity())]
//                               ,1e-8)
//                           ,0.5)
//                      * std::max(turbulentKineticEnergy,1e-8)
//                      * asImp_().storedVelocityInWallCoordinates[mapperElement.index(eg.entity())]
//                      / asImp_().storedDistanceToWall[mapperElement.index(eg.entity())]
//                      * elementVolume);
//
//         r.accumulate(lfsu_k, 0,
//                      asImp_().storedDensity[mapperElement.index(eg.entity())]
//                      * std::pow(cMu(),0.25)
//                      * std::pow(std::max(
//                           asImp_().storedTurbulentKineticEnergy[mapperElement.index(eg.entity())]
//                               ,1e-8)
//                           ,0.5)
//                      * asImp_().storedVelocitiesAtElementCenter[mapperElement.index(eg.entity())][0]
//                      * asImp_().storedVelocitiesAtElementCenter[mapperElement.index(eg.entity())][0]
//                      / asImp_().storedVelocityInWallCoordinates[mapperElement.index(eg.entity())]
//                      / asImp_().storedDistanceToWall[mapperElement.index(eg.entity())]
//                      * elementVolume);
// // FLUENT
//         r.accumulate(lfsu_k, 0,
//                      -problem_().kEpsilonWallFunctions().wallShearStressNominal(eg.entity())
//                      * problem_().kEpsilonWallFunctions().wallShearStressNominal(eg.entity())
//                      / GET_PROP_VALUE(TypeTag, KarmanConstant)
//                      / asImp_().storedDensity[mapperElement.index(eg.entity())]
//                      / std::pow(cMu(), 0.25)
//                      / std::sqrt(std::max(asImp_().storedTurbulentKineticEnergy[mapperElement.index(eg.entity())],1e-8))
//                      / asImp_().storedDistanceToWall[mapperElement.index(eg.entity())]
//                      * elementVolume);
// }
#endif

        /**
          * (2) \b Production term of \b dissipation balance equation<br>
          *
          * \f[
          *    - C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          *    \Rightarrow - \int_V C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          * \f]
          * \f[
          *    \alpha = - C_{1\varepsilon} \frac{\varepsilon}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} V_e
          * \f]
          */
#ifndef DisableEProduction
        r.accumulate(lfsu_e, 0,
                     -2.0 * kinematicEddyViscosity * stressTensorScalarProduct
                     * cOneEpsilon() * dissipation / turbulentKineticEnergy
                     * elementVolume);
#endif

        /**
          * (3) \b Destruction term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    \varepsilon
          *    \Rightarrow \int_V \varepsilon
          * \f]
          * \f[
          *    \alpha = \varepsilon V_e
          * \f]
          */
#ifndef DisableKDestruction
        if (!asImp_().useWallFunctionMomentum(mapperElement.index(eg.entity())))
        {
            r.accumulate(lfsu_k, 0,
                         1.0 * dissipation * elementVolume);
        }
#endif

        /**
          * (4) \b Destruction term of \b dissipation balance equation<br>
          *
          * \f[
          *    C_{2\varepsilon} \frac{\varepsilon^2}{k}
          *    \Rightarrow \int_V C_{2\varepsilon} \frac{\varepsilon^2}{k}
          * \f]
          * \f[
          *    \alpha = C_{2\varepsilon} \frac{\varepsilon^2}{k} V_e
          * \f]
          */
#ifndef DisableEDestruction
        r.accumulate(lfsu_e, 0,
                     1.0 * cTwoEpsilon() * dissipation * dissipation
                     / turbulentKineticEnergy * elementVolume);
#endif

        /**
          * (5) \b Source term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    - q_\textrm{k}
          *    \Rightarrow - \int_V q_\textrm{k}
          * \f]
          * \f[
          *    \alpha = - q_\textrm{k} V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt, qorder);
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceTurbulentKineticEnergyBalance::Traits::RangeType sourceTurbulentKineticEnergyBalanceValue;
          sourceTurbulentKineticEnergyBalance.evaluate(eg.entity(), it->position(), sourceTurbulentKineticEnergyBalanceValue);
          r.accumulate(lfsu_k, 0,
                       -1.0 * sourceTurbulentKineticEnergyBalanceValue * elementVolume * it->weight());
        }

        /**
          * (6) \b Source term of \b dissipation balance equation<br>
          *
          * \f[
          *    - q_{\varepsilon}
          *    \Rightarrow - \int_V q_{\varepsilon}
          * \f]
          * \f[
          *    \alpha = - q_{\varepsilon} V_e
          * \f]
          */
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceDissipationBalance::Traits::RangeType sourceDissipationBalanceValue;
          sourceDissipationBalance.evaluate(eg.entity(), it->position(), sourceDissipationBalanceValue);
          r.accumulate(lfsu_e, 0,
                       -1.0 * sourceDissipationBalanceValue * elementVolume * it->weight());
        }
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_kepsilon(const IG& ig,
                                   const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                   const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                   R& r_s, R& r_n,
                                   std::vector<DimVector> velocities_s, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   std::vector<DimVector> velocities_n, Scalar pressure_n,
                                   Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k_s = lfsu_s.template child<turbulentKineticEnergyIdx>();
        const LFSU_K& lfsu_k_n = lfsu_n.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e_s = lfsu_s.template child<dissipationIdx>();
        const LFSU_E& lfsu_e_n = lfsu_n.template child<dissipationIdx>();

        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        Scalar distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values and constants
        Scalar turbulentKineticEnergy_s = x_s(lfsu_k_s, 0);
        Scalar turbulentKineticEnergy_n = x_n(lfsu_k_n, 0);
        Scalar dissipation_s = x_s(lfsu_e_s, 0);
        Scalar dissipation_n = x_n(lfsu_e_n, 0);
        Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicViscosity_n = asImp_().kinematicViscosity(pressure_n, temperature_n, massMoleFrac_n);

        if (!useStoredEddyViscosity_)
        {
          asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.inside())]
              = calculateKinematicEddyViscosity(turbulentKineticEnergy_s, dissipation_s);
          asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.outside())]
              = calculateKinematicEddyViscosity(turbulentKineticEnergy_n, dissipation_n);
        }
        Scalar kinematicEddyViscosity_s = asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.inside())];
        Scalar kinematicEddyViscosity_n = asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.outside())];

        // averaging: distance weighted average for diffusion term
        Scalar kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n
                                      + distanceOutsideToFace * kinematicViscosity_s)
                                    / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicEddyViscosity_avg = (distanceInsideToFace * kinematicEddyViscosity_n
                                          + distanceOutsideToFace * kinematicEddyViscosity_s)
                                        / (distanceInsideToFace + distanceOutsideToFace);

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for diffusion term
          kinematicViscosity_avg = (2.0 * kinematicViscosity_n * kinematicViscosity_s)
                                  / (kinematicViscosity_n + kinematicViscosity_s);
          kinematicEddyViscosity_avg = (2.0 * kinematicEddyViscosity_n * kinematicEddyViscosity_s)
                                      / (kinematicEddyViscosity_n + kinematicEddyViscosity_s);
        }

        Scalar effectiveViscosityK_avg = 0.0;
#ifndef DisableKDiffusionTurb
        effectiveViscosityK_avg = kinematicEddyViscosity_avg / sigmaK();
#endif
        Scalar effectiveViscosityEpsilon_avg = 0.0;
#ifndef DisableEDiffusionTurb
        effectiveViscosityEpsilon_avg = kinematicEddyViscosity_avg / sigmaEpsilon();
#endif
        if (kEpsilonEnableKinematicViscosity_)
        {
#ifndef DisableKDiffusionMol
          effectiveViscosityK_avg += kinematicViscosity_avg;
#endif
#ifndef DisableEDiffusionMol
          effectiveViscosityEpsilon_avg += kinematicViscosity_avg;
#endif
        }

        // upwinding: advection term
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar turbulentKineticEnergy_up = turbulentKineticEnergy_s;
        Scalar dissipation_up = dissipation_s;
        if (velocityNormal < 0)
        {
          turbulentKineticEnergy_up = turbulentKineticEnergy_n;
          dissipation_up = dissipation_n;
        }

        if (enableAdvectionAveraging_)
        {
          turbulentKineticEnergy_up = (distanceInsideToFace * turbulentKineticEnergy_n
                                        + distanceOutsideToFace * turbulentKineticEnergy_s)
                                      / (distanceInsideToFace + distanceOutsideToFace);
          dissipation_up = (distanceInsideToFace * dissipation_n
                            + distanceOutsideToFace * dissipation_s)
                          / (distanceInsideToFace + distanceOutsideToFace);
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call

        /**
         * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    k v
         *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| k_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the turbulentKineticEnergy.
         */
#ifndef DisableKAdvection
        r_s.accumulate(lfsu_k_s, 0,
                       1.0 * turbulentKineticEnergy_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_k_n, 0,
                       -1.0 * turbulentKineticEnergy_up
                       * velocityNormal
                       * faceVolume);
#endif

        /**
         * (2) \b Flux term of \b dissipation balance equation
         *
         * \f[
         *    \varepsilon v
         *    \Rightarrow \int_\gamma \left( \varepsilon v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varepsilon_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the dissipation.
         */
#ifndef DisableEAdvection
        r_s.accumulate(lfsu_e_s, 0,
                       1.0 * dissipation_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_e_n, 0,
                       -1.0 * dissipation_up
                       * velocityNormal
                       * faceVolume);
#endif

        /**
         * (3) \b Diffusion term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \nabla k
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,avg}}{\sigma_\textrm{k,avg}} \right) \nabla k \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        if (wallFunctionModel_ != Indices::pope
            || !problem_().kEpsilonWallFunctions().useWallFunctionTurbulentKineticEnergy(ig.outside())
            || !problem_().kEpsilonWallFunctions().useWallFunctionTurbulentKineticEnergy(ig.inside()))
        {
          r_s.accumulate(lfsu_k_s, 0,
                         -1.0 * effectiveViscosityK_avg
                         * (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                         / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);
          r_n.accumulate(lfsu_k_n, 0,
                         1.0 * effectiveViscosityK_avg
                         * (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                         / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);
        }

        /**
         * (4) \b Diffusion term of \b dissipation balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \nabla \varepsilon
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \right) \nabla \varepsilon \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,avg}}{\sigma_{\varepsilon \textrm{,avg}}} \right) \nabla \varepsilon \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        r_s.accumulate(lfsu_e_s, 0,
                      -1.0 * effectiveViscosityEpsilon_avg
                       * (dissipation_n - dissipation_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_e_n, 0,
                      1.0 * effectiveViscosityEpsilon_avg
                       * (dissipation_n - dissipation_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_kepsilon(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                   std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   Scalar pressure_boundary,
                                   Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k_s = lfsu_s.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e_s = lfsu_s.template child<dissipationIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();

        // center in face's reference element
        const Dune::FieldVector<Scalar, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // center in inside element
        const Dune::FieldVector<Scalar, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar,IG::dimension-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside().geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<Scalar,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar,IG::dimension>::general(ig.inside().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // evaluation of cell values
        Scalar turbulentKineticEnergy_s = x_s(lfsu_k_s, 0);
        Scalar dissipation_s = x_s(lfsu_e_s, 0);

        // /////////////////////
        // call ParentType
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicViscosity_avg = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        if (!useStoredEddyViscosity_)
        {
          asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.inside())]
              = calculateKinematicEddyViscosity(turbulentKineticEnergy_s, dissipation_s);
        }
        Scalar kinematicEddyViscosity_avg = asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.inside())];

        // turbulentKineticEnergy at the boundary is given
        typename DirichletTurbulentKineticEnergy::Traits::RangeType turbulentKineticEnergy_boundary(0.0);
        dirichletTurbulentKineticEnergy.evaluate(ig.inside(), insideFaceCenterLocal, turbulentKineticEnergy_boundary);
        // dissipation at the boundary is given
        typename DirichletDissipation::Traits::RangeType  dissipation_boundary(0.0);
        dirichletDissipation.evaluate(ig.inside(), insideFaceCenterLocal, dissipation_boundary);
        Scalar effectiveViscosityK_avg = 0.0;
#ifndef DisableKDiffusionTurb
        effectiveViscosityK_avg = kinematicEddyViscosity_avg / sigmaK();
#endif
        Scalar effectiveViscosityEpsilon_avg = 0.0;
#ifndef DisableEDiffusionTurb
        effectiveViscosityEpsilon_avg = kinematicEddyViscosity_avg / sigmaEpsilon();
#endif

        if (kEpsilonEnableKinematicViscosity_)
        {
#ifndef DisableKDiffusionMol
          effectiveViscosityK_avg += kinematicViscosity_avg;
#endif
#ifndef DisableEDiffusionMol
          effectiveViscosityEpsilon_avg += kinematicViscosity_avg;
#endif
        }

        // Adapted Wall boundary for velocity
        unsigned int elementInsideID = mapperElement.index(ig.inside());
        unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[elementInsideID];
        if ((bcVelocity.isWall(ig, faceCenterLocal) || bcVelocity.isCoupling(ig, faceCenterLocal))
            && asImp_().useWallFunctionMomentum(elementInsideID))
        {
          for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
          {
            // flowNormalAxis only
            if (tangDim == asImp_().storedCorrespondingFlowNormalAxis[wallElementID])
            {
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                              0.5 * density_s * problem_().kEpsilonWallFunctions().wallShearStressNominal(ig.inside())
                              * faceVolume);

              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                              0.5 * density_s * problem_().kEpsilonWallFunctions().wallShearStressNominal(ig.inside())
                              * faceVolume);
            }
          }
        }

        // Inflow boundary for turbulentKineticEnergy
        if (bcTurbulentKineticEnergy.isInflow(ig, faceCenterLocal))
        {
          /**
           * Inflow boundary handling for turbulent kinetic energy balance<br>
           * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
           * \f[
           *    k v
           *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| k_\textrm{boundary} \left( v \cdot n \right)
           * \f]
           */
#ifndef DisableKAdvection
          r_s.accumulate(lfsu_k_s, 0,
                         1.0 * turbulentKineticEnergy_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
#endif

        /**
         * (2) \b Diffusion term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \nabla k
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,self}}{\sigma_\textrm{k,self}} \right) \nabla k \cdot n
         * \f]
         *
         * At the boundary the values from the inner cell are taken and <b>no averaging</b>
         * is performed.
         */
         // The default procedure for averaging is a distance weighted average, by
         // using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         // harmonic averaging instead.
        r_s.accumulate(lfsu_k_s, 0,
                      -1.0 * effectiveViscosityK_avg
                       * (turbulentKineticEnergy_boundary - turbulentKineticEnergy_s)
                       / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        }
        // Wall boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isWall(ig, faceCenterLocal))
        {
          //! Nothing has to be done here, no-flow boundary conditions are applied
        }
        // Outflow boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isOutflow(ig, faceCenterLocal))
        {
#ifndef DisableKAdvection
          /**
           * Outflow boundary handling for turbulent kinetic energy balance<br>
           * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
           * \f[
           *    k v
           *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| k_\textrm{self} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_k_s, 0,
                         1.0 * turbulentKineticEnergy_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
#endif
        }
        // Symmetry boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for turbulentKineticEnergy.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for turbulentKineticEnergy.");
        }


        // Inflow boundary for dissipation
        if (bcDissipation.isInflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for dissipation balance<br>
           * (1) \b Flux term of \b dissipation balance equation
           * \f[
           *    \varepsilon v
           *    \Rightarrow \int_\gamma \left( \varepsilon v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varepsilon_\textrm{boundary} \left( v \cdot n \right)
           * \f]
           */
#ifndef DisableEAdvection
          r_s.accumulate(lfsu_e_s, 0,
                         1.0 * dissipation_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
#endif

        /**
         * (2) \b Diffusion term of \b dissipation balance equation
         *
         * \f[
         *    - \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \nabla \varepsilon
         *    \Rightarrow - \int_\gamma \left( \frac{\nu_\textrm{t}}{\sigma_\varepsilon} \nabla \varepsilon \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \frac{\nu_\textrm{t,self}}{\sigma_{\varepsilon\textrm{,self}}} \nabla \varepsilon \cdot n
         * \f]
         *
         * At the boundary the values from the inner cell are taken and <b>no averaging</b>
         * is performed.
         */
         // The default procedure for averaging is a distance weighted average, by
         // using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         // harmonic averaging instead.
        r_s.accumulate(lfsu_e_s, 0,
                      -1.0 * effectiveViscosityEpsilon_avg
                       * (dissipation_boundary - dissipation_s)
                       / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        }
        // Wall boundary for dissipation
        else if (bcDissipation.isWall(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Wall Condition for dissipation.
          //! Dirichlet value will be set automatically.
        }
        // Outflow boundary for dissipation
        else if (bcDissipation.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for dissipation balance<br>
           * (1) \b Flux term of \b dissipation balance equation
           * \f[
           *    \varepsilon v
           *    \Rightarrow \int_\gamma \left( \varepsilon v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varepsilon_\textrm{self} \left( v \cdot n \right)
           * \f]
           */
#ifndef DisableEAdvection
          r_s.accumulate(lfsu_e_s, 0,
                         1.0 * dissipation_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
#endif
        }
        // Symmetry boundary for dissipation
        else if (bcDissipation.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for dissipation.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       */
      void initialize()
      {
        if (!kEpsilonEnableKinematicViscosity_)
        {
          std::cout << "kEpsilonEnableKinematicViscosity_ = false" << std::endl;
        }

        storedTurbulentKineticEnergy.resize(mapperElement.size());
        storedDissipation.resize(mapperElement.size());
        storedMatchingPointID.resize(mapperElement.size());
        storedWallHasMatchingPoint.resize(mapperElement.size());
        storedElementIsMatchingPoint.resize(mapperElement.size());

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // initialize some of the values
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          unsigned int elementInsideID = mapperElement.index(*eit);
          storedTurbulentKineticEnergy[elementInsideID] = 0.0;
          storedDissipation[elementInsideID] = 0.0;
          storedMatchingPointID[elementInsideID] = asImp_().storedCorrespondingWallElementID[elementInsideID];
          storedWallHasMatchingPoint[elementInsideID] = true;
          storedElementIsMatchingPoint[elementInsideID] = false;
        }

        // select the components from the subspaces
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();

        // loop over grid view to get elements to check boundary conditions
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCTurbulentKineticEnergy at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcTurbulentKineticEnergy.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcTurbulentKineticEnergy.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcTurbulentKineticEnergy.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for turbulentKineticEnergy at one point.");
            }

            // check for multiple defined boundary conditions
            numberOfBCTypesAtPos = 0;
            if (bcDissipation.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCDissipation at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcDissipation.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcDissipation.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcDissipation.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcDissipation.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for dissipation at one point.");
            }
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * This function just calls the ParentType.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsTurbulentKineticEnergy = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, turbulentKineticEnergyIdx> >;
        using SubGfsDissipation = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, dissipationIdx> >;
        SubGfsTurbulentKineticEnergy subGfsTurbulentKineticEnergy(mdgfs);
        SubGfsDissipation subGfsDissipation(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsTurbulentKineticEnergy = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<turbulentKineticEnergyIdx> >;
        using SubGfsDissipation = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<dissipationIdx> >;
        SubGfsTurbulentKineticEnergy subGfsTurbulentKineticEnergy(gfs);
        SubGfsDissipation subGfsDissipation(gfs);
#endif

        // discrete function objects
        using DgfTurbulentKineticEnergy = Dune::PDELab::DiscreteGridFunction<SubGfsTurbulentKineticEnergy, X>;
        using DgfDissipation = Dune::PDELab::DiscreteGridFunction<SubGfsDissipation, X>;
        DgfTurbulentKineticEnergy dgfTurbulentKineticEnergy(subGfsTurbulentKineticEnergy, lastSolution);
        DgfDissipation dgfDissipation(subGfsDissipation, lastSolution);

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // set k and epsilon from the "real" solution
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // NOTE use eit also for multidomain models
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          if (!gridView.indexSet().contains(stokesDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif

          // evaluate solutions
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);
          Dune::FieldVector<Scalar, 1> turbulentKineticEnergy(0.0);
          Dune::FieldVector<Scalar, 1> dissipation(0.0);
          dgfTurbulentKineticEnergy.evaluate(element, cellCenterLocal, turbulentKineticEnergy);
          dgfDissipation.evaluate(element, cellCenterLocal, dissipation);

          asImp_().storedTurbulentKineticEnergy[elementInsideID] = turbulentKineticEnergy;
          asImp_().storedDissipation[elementInsideID] = dissipation;
          asImp_().storedKinematicEddyViscosity[elementInsideID]
            = calculateKinematicEddyViscosity(turbulentKineticEnergy, dissipation);
        }

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[mapperElement.index(*eit)];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[mapperElement.index(*eit)];
          unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          problem_().kEpsilonWallFunctions().storedElementHasWall_[mapperElement.index(*eit)]
            = asImp_().storedElementHasWall[mapperElement.index(*eit)];
          problem_().kEpsilonWallFunctions().storedDistanceToWall_[mapperElement.index(*eit)]
            = asImp_().storedDistanceToWall[mapperElement.index(*eit)];
          problem_().kEpsilonWallFunctions().storedVelocityGradient_[mapperElement.index(*eit)]
            = asImp_().storedVelocityGradientTensor[mapperElement.index(*eit)][flowNormalAxis][wallNormalAxis];
          problem_().kEpsilonWallFunctions().storedKinematicViscosity_[mapperElement.index(*eit)]
            = asImp_().storedKinematicViscosity[mapperElement.index(*eit)];
          problem_().kEpsilonWallFunctions().storedDistanceInWallCoordinates_[mapperElement.index(*eit)]
            = asImp_().storedDistanceInWallCoordinates[mapperElement.index(*eit)];
          problem_().kEpsilonWallFunctions().storedVelocityInWallCoordinates_[mapperElement.index(*eit)]
            = asImp_().storedVelocityInWallCoordinates[mapperElement.index(*eit)];
          problem_().kEpsilonWallFunctions().storedTurbulentKineticEnergy_[mapperElement.index(*eit)]
            = asImp_().storedTurbulentKineticEnergy[mapperElement.index(*eit)];
          problem_().kEpsilonWallFunctions().storedVelocitiesAtElementCenter_[mapperElement.index(*eit)]
            = asImp_().storedVelocitiesAtElementCenter[mapperElement.index(*eit)][flowNormalAxis];
          problem_().kEpsilonWallFunctions().storedWallElementID_[mapperElement.index(*eit)]
            = wallElementID;
          unsigned int wallElementID2 = asImp_().storedNeighborID[wallElementID][wallNormalAxis][0];
          if (wallElementID == wallElementID2)
          {
            wallElementID2 = asImp_().storedNeighborID[wallElementID][wallNormalAxis][1];
          }
          problem_().kEpsilonWallFunctions().storedWallElementID2_[mapperElement.index(*eit)]
            = wallElementID2;
        }

        // set wall section to have no matching point on default
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          storedWallHasMatchingPoint[mapperElement.index(*eit)] = false;
          storedElementIsMatchingPoint[mapperElement.index(*eit)] = false;
        }

        // get matching point for k-epsilon wall function
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          unsigned int elementInsideID = mapperElement.index(*eit);
          unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int storedNeighborID0 = asImp_().storedNeighborID[elementInsideID][wallNormalAxis][0];
          unsigned int storedNeighborID1 = asImp_().storedNeighborID[elementInsideID][wallNormalAxis][1];
          if (problem_().kEpsilonWallFunctions().inWallFunctionRegion(elementInsideID)
              && (problem_().kEpsilonWallFunctions().inWallFunctionRegion(storedNeighborID0)
                  != problem_().kEpsilonWallFunctions().inWallFunctionRegion(storedNeighborID1)))
          {
            storedMatchingPointID[wallElementID] = elementInsideID;
            storedWallHasMatchingPoint[wallElementID] = true;
            problem_().kEpsilonWallFunctions().storedMatchingPointID_[wallElementID]
              = asImp_().storedMatchingPointID[wallElementID];
            problem_().kEpsilonWallFunctions().storedWallHasMatchingPoint_[wallElementID]
              = asImp_().storedWallHasMatchingPoint[wallElementID];
          }
        }

        // set wall section to have no matching point on default
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          unsigned int elementInsideID = mapperElement.index(*eit);
          unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[elementInsideID];
          if (asImp_().storedMatchingPointID[wallElementID] == elementInsideID
              && asImp_().storedWallHasMatchingPoint[wallElementID])
          {
            storedElementIsMatchingPoint[elementInsideID] = true;
          }
        }
      }

      /**
       * \brief Calculate eddy viscosity
       */
      double calculateKinematicEddyViscosity(const double turbulentKineticEnergy,
                                             const double dissipation) const
      {
        return cMu() * turbulentKineticEnergy * turbulentKineticEnergy / dissipation;
      }

      /**
       * \brief Returns the turbulentKineticEnergy for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar turbulentKineticEnergy(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        return x(lfsu_k, 0);
      }

      /**
       * \brief Returns the dissipation for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar dissipation(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_E;
        const LFSU_E& lfsu_e = lfsu.template child<dissipationIdx>();
        return x(lfsu_e, 0);
      }

    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar cMu() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 0.09;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 0.09;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar sigmaK() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.0;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 1.0;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar sigmaEpsilon() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.3;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 1.3;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_{1\varepsilon}  \$f constant
    const Scalar cOneEpsilon() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.44;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 1.44;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

    //! \brief Returns the \$f C_{2\varepsilon} \$f constant
    const Scalar cTwoEpsilon() const
    {
      if(kEpsilonModelConstants_ == Indices::launderSharma)
          return 1.92;
      else if (kEpsilonModelConstants_ == Indices::mohammadi)
          return 2.06;
      else
        DUNE_THROW(Dune::NotImplemented, "This set of kepsilon model constants is not implemented.");
    }

private:
      const BC& bc;
      const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance;
      const SourceDissipationBalance& sourceDissipationBalance;
      const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy;
      const DirichletDissipation& dirichletDissipation;
      const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy;
      const NeumannDissipation& neumannDissipation;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool useStoredEddyViscosity_;

      // parameter
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;
      bool kEpsilonEnableKinematicViscosity_;
      unsigned int kEpsilonModelConstants_;
      unsigned int wallFunctionModel_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_KEPSILON_STAGGERED_GRID_HH
