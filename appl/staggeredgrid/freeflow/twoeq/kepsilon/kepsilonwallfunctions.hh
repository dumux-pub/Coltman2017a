/** \file
  *  \ingroup StaggeredModel
  *
  * \brief This file contains different wall function approaches for
  *        \f$ k-\varepsilon \f$.
  *
  * The wall functions have to be called from the Dirichlet function of
  * the problem files.
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  */

#ifndef DUMUX_KEPSILON_WALLFUNCTIONS_HH
#define DUMUX_KEPSILON_WALLFUNCTIONS_HH

#include<dune/common/exceptions.hh>

#include"../kepsilon/kepsilonproperties.hh"
#include"../kepsilon/kepsilonproperties.hh"

namespace Dumux {
/**
  * \brief Returns wall functions values for \f$ k-\varepsilon \f$ equation.
  *
  * This class contains different wall function approaches and has to be called
  * from the Dirichlet function of the problem files.
  * The term "stored" in front of the variable name indicates, that this
  * variable is updated in the updateStoredValues function.
  *
  * \tparam TypeTag TypeTag of the problem
  */
template<class TypeTag>
class KEpsilonWallFunctions
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
  typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
  enum { dim = GridView::dimension };

  typedef typename GridView::template Codim<0>::Entity Element;

  //! \brief Constructor
  KEpsilonWallFunctions(const GridView& gv, Problem& problem)
  : mapperElement_(gv), problemPtr_(0)
  {
    karmanConstant_ = GET_PROP_VALUE(TypeTag, KarmanConstant);
    storedElementHasWall_.resize(mapperElement_.size());
    storedDistanceToWall_.resize(mapperElement_.size());
    storedVelocityGradient_.resize(mapperElement_.size());
    storedKinematicViscosity_.resize(mapperElement_.size());
    storedDistanceInWallCoordinates_.resize(mapperElement_.size());
    storedVelocityInWallCoordinates_.resize(mapperElement_.size());
    storedTurbulentKineticEnergy_.resize(mapperElement_.size());
    storedVelocitiesAtElementCenter_.resize(mapperElement_.size());
    storedWallElementID_.resize(mapperElement_.size());
    storedWallElementID2_.resize(mapperElement_.size());
    storedMatchingPointID_.resize(mapperElement_.size());
    storedWallHasMatchingPoint_.resize(mapperElement_.size());
    for (unsigned int i = 0; i < mapperElement_.size(); ++i)
    {
      // these values will be overwritten in the localoperator, variables are mutable
      storedElementHasWall_[i] = false;
      storedDistanceToWall_[i] = std::numeric_limits<Scalar>::max();
      storedVelocityGradient_[i] = std::numeric_limits<Scalar>::min();
      storedKinematicViscosity_[i] = std::numeric_limits<Scalar>::min();
      storedDistanceInWallCoordinates_[i] = std::numeric_limits<Scalar>::max();
      storedVelocityInWallCoordinates_[i] = std::numeric_limits<Scalar>::max();
      storedTurbulentKineticEnergy_[i] = std::numeric_limits<Scalar>::max();
      storedVelocitiesAtElementCenter_[i] = std::numeric_limits<Scalar>::max();
      storedWallElementID_[i] = 0;
      storedWallElementID2_[i] = 0;
      storedMatchingPointID_[i] = 0;
      storedWallHasMatchingPoint_[i] = false;
    }

    yPlusThreshold_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, KEpsilon, WallFunctionYPlusThreshold);
    uPlusThreshold_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, KEpsilon, WallFunctionUPlusThreshold);
    wallFunctionModel_ = GET_PARAM_FROM_GROUP(TypeTag, unsigned int, KEpsilon, WallFunctionModel);

    std::cout << "Used wall function model = ";
    switch (wallFunctionModel_)
    {
      case Indices::noWallFunction: // 0
        std::cout << "no wall function" << std::endl;
        break;
      case Indices::constantValue: // 1
        std::cout << "constant value" << std::endl;
        break;
      case Indices::pope: // 2
        std::cout << "standard pope" << std::endl;
        break;
      case Indices::wilcox: // 3
        std::cout << "standard wilcox" << std::endl;
        DUNE_THROW(Dune::NotImplemented, "The wilcox wall function model does not produce correct results.");
        break;
      default:
        DUNE_THROW(Dune::NotImplemented, "This wall function model is not implemented.");
    }
    problemPtr_ = &problem;
  }

  //! \brief Returns friction velocity
  double frictionVelocity(const Element& e) const
  {
    return std::sqrt(storedKinematicViscosity_[mapperElement_.index(e)]
                     * std::abs(storedVelocityGradient_[mapperElement_.index(e)]));
  }

  //! \brief Returns friction velocity
  double frictionVelocityWall(const Element& e) const
  {
    unsigned int wallElementID = storedWallElementID_[mapperElement_.index(e)];
    return std::sqrt(storedKinematicViscosity_[wallElementID]
                     * std::abs(storedVelocityGradient_[wallElementID]));
  }

  //! \brief Returns friction velocity
  double frictionVelocityLogRegion(const Element& e) const
  {
    unsigned int wallElementID1 = storedWallElementID_[mapperElement_.index(e)];
    unsigned int wallElementID2 = storedWallElementID2_[mapperElement_.index(e)];
    Scalar distanceRatio = storedDistanceToWall_[wallElementID1] / storedDistanceToWall_[wallElementID2];
    return (storedVelocitiesAtElementCenter_[wallElementID1] - storedVelocitiesAtElementCenter_[wallElementID2])
           / (1.0 / karmanConstant_ * std::log(distanceRatio));
  }

  //! \brief Returns whether an element is located in the wall function region
  bool inWallFunctionRegion(const Element& e) const
  {
    return inWallFunctionRegion(mapperElement_.index(e));
  }

  //! \brief Returns whether a wall function turbulent kinetic energy should be used
  bool useWallFunctionTurbulentKineticEnergy(const Element& e) const
  {
    return useWallFunctionTurbulentKineticEnergy(mapperElement_.index(e));
  }

  //! \brief Returns whether a wall function for dissipation should be used
  bool useWallFunctionDissipation(const Element& e) const
  {
    return useWallFunctionDissipation(mapperElement_.index(e));
  }

  //! \brief Returns whether an element is located in the wall function region
  bool inWallFunctionRegion(const unsigned int elementID) const
  {
    if (wallFunctionModel_ == Indices::noWallFunction || wallFunctionModel_ == Indices::constantValue)
      return false;

    return storedElementHasWall_[elementID]
           || (std::abs(storedDistanceInWallCoordinates_[elementID]) < yPlusThreshold_
               && storedVelocityInWallCoordinates_[elementID] < uPlusThreshold_);
  }

  //! \brief Returns whether a wall function turbulent kinetic energy should be used
  bool useWallFunctionTurbulentKineticEnergy(const unsigned int elementID) const
  {
    if (wallFunctionModel_ == Indices::constantValue)
    {
      return storedElementHasWall_[elementID];
    }
    else if (wallFunctionModel_ == Indices::pope)
    {
      return !storedWallHasMatchingPoint_[storedWallElementID_[elementID]]
             || (storedMatchingPointID_[storedWallElementID_[elementID]] != elementID
                 && inWallFunctionRegion(elementID));
    }
    else if (wallFunctionModel_ == Indices::wilcox)
    {
      return inWallFunctionRegion(elementID);
    }

    return false; // Indices::noWallFunction
  }

  //! \brief Returns whether a wall function for dissipation should be used
  bool useWallFunctionDissipation(const unsigned int elementID) const
  {
    if (wallFunctionModel_ == Indices::constantValue)
    {
      return storedElementHasWall_[elementID];
    }
    else if (wallFunctionModel_ == Indices::pope || wallFunctionModel_ == Indices::wilcox)
    {
      return inWallFunctionRegion(elementID);
    }

    return false; // Indices::noWallFunction
  }

  //! \brief Returns whether a wall function for momentum should be used
  bool useWallFunctionMomentum(const unsigned int elementID) const
  {
    if (wallFunctionModel_ == Indices::pope)
    {
      return true;
    }

    return false;
  }

  //! \brief Call wall function for turbulentKineticEnergy
  double wallFunctionTurbulentKineticEnergy(const Element& e) const
  {
    if (wallFunctionModel_ == Indices::constantValue)
    {
      return problem_().constantWallFunctionTurbulentKineticEnergy();
    }
    else if (wallFunctionModel_ == Indices::pope)
    {
      unsigned int elementID = mapperElement_.index(e);
      return storedTurbulentKineticEnergy_[elementID];
    }
    else if (wallFunctionModel_ == Indices::wilcox)
    {
//       Scalar frictionVel = frictionVelocity(e);
//       Scalar frictionVel = frictionVelocityWall(e);
      Scalar frictionVel = frictionVelocityLogRegion(e);
      return frictionVel * frictionVel
             / std::sqrt(problem_().cMu());
    }
    else
    {
        DUNE_THROW(Dune::NotImplemented, "This turbulent kinetic energy wall function method is not implemented.");
    }
  }
  //! \brief Call wall function for dissipation
  double wallFunctionDissipation(const Element& e) const
  {
    if (wallFunctionModel_ == Indices::constantValue)
    {
      return problem_().constantWallFunctionDissipation();
    }
    else if (wallFunctionModel_ == Indices::pope)
    {
      return uStarNominal(e) * uStarNominal(e) * uStarNominal(e)
             / karmanConstant_ / storedDistanceToWall_[mapperElement_.index(e)];
    }
    else if (wallFunctionModel_ == Indices::wilcox)
    {
      return std::pow(problem_().cMu(), 0.75)
             * std::pow(wallFunctionTurbulentKineticEnergy(e), 1.5)
             / karmanConstant_ / storedDistanceToWall_[mapperElement_.index(e)];
    }
    else
    {
      DUNE_THROW(Dune::NotImplemented, "This dissipation wall function method is not implemented.");
    }
  }

  //! \brief Returns the element mapper for "convenience" to make the multidomain work at all
  MapperElement mapperElement() const
  {
    return mapperElement_;
  }

  //! \brief Returns the nominal wall shear stress (accounts for poor approximation of viscous sublayer)
  Scalar wallShearStressNominal(const Element& e) const
  {
    unsigned int wallElementID = storedWallElementID_[mapperElement_.index(e)];
    unsigned int matchingPointID = storedMatchingPointID_[wallElementID];
    Scalar yPlusNominal = storedDistanceToWall_[matchingPointID] * uStarNominal(e)
                          / storedKinematicViscosity_[wallElementID];
    Scalar uPlusNominal = uStarNominal(e) * (1.0 / karmanConstant_ * std::log(yPlusNominal) + 5.0);
    return uStarNominal(e) * uStarNominal(e)
           * storedVelocitiesAtElementCenter_[matchingPointID]
           / uPlusNominal;
  }

  //! \brief Returns the nominal wall shear stress velocity (accounts for poor approximation of viscous sublayer)
  Scalar uStarNominal(const Element& e) const
  {
    unsigned int wallElementID = storedWallElementID_[mapperElement_.index(e)];
    unsigned int matchingPointID = storedMatchingPointID_[wallElementID];
    return std::pow(problem_().cMu(), 0.25)
           * std::sqrt(std::max(storedTurbulentKineticEnergy_[matchingPointID],1e-8));
  }

  MapperElement mapperElement_;
  mutable std::vector<double> storedElementHasWall_;
  mutable std::vector<double> storedDistanceToWall_;
  mutable std::vector<double> storedVelocityGradient_;
  mutable std::vector<double> storedKinematicViscosity_;
  mutable std::vector<double> storedDistanceInWallCoordinates_;
  mutable std::vector<double> storedVelocityInWallCoordinates_;
  mutable std::vector<double> storedTurbulentKineticEnergy_;
  mutable std::vector<double> storedVelocitiesAtElementCenter_;
  mutable std::vector<unsigned int> storedWallElementID_;
  mutable std::vector<unsigned int> storedWallElementID2_;
  mutable std::vector<unsigned int> storedMatchingPointID_;
  mutable std::vector<bool> storedWallHasMatchingPoint_;
  unsigned int wallFunctionModel_;
  Scalar yPlusThreshold_;
  Scalar uPlusThreshold_;
  Scalar karmanConstant_;

protected:
  Problem &problem_()
  { return *problemPtr_; }
  const Problem &problem_() const
  { return *problemPtr_; }

  Problem *problemPtr_;
};

} // end namespace Dumux

#endif // DUMUX_KEPSILON_WALLFUNCTIONS_HH
