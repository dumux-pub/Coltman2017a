// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 *
 * \file
 *
 * \brief Defines the properties required for the K-Omega staggered grid model.
 */

#ifndef DUMUX_KOMEGA_PROPERTIES_HH
#define DUMUX_KOMEGA_PROPERTIES_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosityproperties.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(BCTurbulentKineticEnergy); //!< Property tag for the used turbulentKineticEnergy boundary condition
NEW_PROP_TAG(BCDissipation); //!< Property tag for the used dissipation boundary condition
NEW_PROP_TAG(SourceTurbulentKineticEnergyBalance); //!< Property tag for the SourceTurbulentKineticEnergyBalance
NEW_PROP_TAG(SourceDissipationBalance); //!< Property tag for the SourceDissipationBalance
NEW_PROP_TAG(DirichletTurbulentKineticEnergy); //!< Property tag for the DirichletTurbulentKineticEnergy
NEW_PROP_TAG(DirichletDissipation); //!< Property tag for the DirichletDissipation
NEW_PROP_TAG(NeumannTurbulentKineticEnergy); //!< Property tag for the NeumannTurbulentKineticEnergy
NEW_PROP_TAG(NeumannDissipation); //!< Property tag for the NeumannDissipation
NEW_PROP_TAG(FixDissipationConstraints); //!< Property tag to constrain the dissipation value

//! The type tag for the eddy viscosity problems
NEW_TYPE_TAG(StaggeredGridKOmega, INHERITS_FROM(StaggeredGridEddyViscosity));
NEW_PROP_TAG(KOmegaEnableKinematicViscosity);

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(KOmegaModel); //!< Type of the used KOmega model
}
}

#endif // DUMUX_KOMEGA_PROPERTIES_HH
