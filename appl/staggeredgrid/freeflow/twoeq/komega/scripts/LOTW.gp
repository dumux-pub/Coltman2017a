reset
set terminal pngcairo size 1250,940
set datafile separator ','
DATA='./'
set output 'LOTW.png'
set log x
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'y^+'
set ylabel 'u^+'
set arrow from 30,0 to 30,25 nohead lt 0 lc 8 lw 2
set xrange [.7:1300]
set yrange [0:25]
set key font ",20"
set key right bottom samplen 1
set label "Viscous Laminar\nSublayer" at 4,23 center font ",20"
set label "(Buffer Transition \nSublayer)" at 15,3 center font ",20"
set label "Fully Turbulent \nSublayer" at 150,23 center font ",20"
set title font ",20"
set title 'The Law of the Wall: K-Omega Model'
  A(x)=x
  B(x)=2.5*log(x)+5.5
plot \
  'test_references/Laufer_u+y+_50k.csv'              using 6:7 w p ps 2 lc 8 pt 7 t 'Laufer Pipe Experiment', \
  'test_references/GridConverge/lauferpipe_135.csv'    u 22:21 w lp lw 4 lc 4 t 'K-Omega', \
  A(x)                             w lp lw 2 lt 1 lc rgb 'gray40' t 'Laminar Solution', \
  B(x)                             w l lw 4 lt 1 lc rgb 'gray70' t 'Turbulent Solution'

reset
set terminal pngcairo size 1250,940
set datafile separator ','
DATA='./'
set output 'LOTW_ModelCompare.png'
set log x
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'y^+'
set ylabel 'u^+'
set arrow from 30,0 to 30,25 nohead lt 0 lc 8 lw 2
set xrange [.7:1300]
set yrange [0:25]
set key font ",20"
set key right bottom samplen 1
set label "Viscous Laminar\nSublayer" at 4,23 center font ",20"
set label "(Buffer Transition \n Sublayer)" at 15,3 center font ",20"
set label "Fully Turbulent \nSublayer" at 150,23 center font ",20"
set title font ",20"
set title 'The Law of the Wall: Turbulence Models Compared'
  A(x)=x
  B(x)=2.5*log(x)+5.5
plot \
  'test_references/Laufer_u+y+_50k.csv'           using 6:7  w p ps 2 lc 8 pt 7 t 'Laufer Pipe Experiment', \
  'test_references/ZeroEq.csv'                       u 22:21 w lp lw 4 lc 1 t 'Zero Eq', \
  'test_references/kepsilon.csv'                     u 34:33 w lp lw 4 lc 2 t 'K-Epsilon', \
  'test_references/lowrekepsilon_135.csv'            u 22:21 w lp lw 4 lc 3 t 'Low Re K-Epsilon', \
  'test_references/lauferpipe_1_35_08_winflow.csv'   u 22:21 w lp lw 4 lc 4 t 'K-Omega', \
  A(x)                           w lp lw 2 lt 1 lc rgb 'gray40' t 'Laminar Solution', \
  B(x)                            w l lw 4 lt 1 lc rgb 'gray70' t 'Turbulent Solution'

reset
set terminal pngcairo size 1250,940
set datafile separator ','
DATA='./'
set output 'LOTW_Grid.png'
set log x
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'y^+'
set ylabel 'u^+'
set arrow from 30,0 to 30,25 nohead lt 0 lc 8 lw 2
set xrange [.7:1300]
set yrange [0:25]
set key font ",20"
set key right bottom samplen 1
set label "Viscous Laminar\nSublayer" at 4,23 center font ",20"
set label "(Buffer Transition \nSublayer)" at 15,3 center font ",20"
set label "Fully Turbulent \nSublayer" at 150,23 center font ",20"
set title font ",20"
set title 'The Law of the Wall: Grid Convergence'
  A(x)=x
  B(x)=2.5*log(x)+5.5
plot \
  'test_references/Laufer_u+y+_50k.csv'              using 6:7 w p ps 2 lc 8 pt 7 t 'Laufer Pipe Experiment', \
  'test_references/GridConverge/lauferpipe_105.csv'    u 22:21 w lp lw 4 lc 1 t 'Grading = 1.05', \
  'test_references/GridConverge/lauferpipe_110.csv'    u 22:21 w lp lw 4 lc 2 t 'Grading = 1.10', \
  'test_references/GridConverge/lauferpipe_115.csv'    u 22:21 w lp lw 4 lc 3 t 'Grading = 1.15', \
  'test_references/GridConverge/lauferpipe_120.csv'    u 22:21 w lp lw 4 lc 4 t 'Grading = 1.20', \
  'test_references/GridConverge/lauferpipe_125.csv'    u 22:21 w lp lw 4 lc 5 t 'Grading = 1.25', \
  'test_references/GridConverge/lauferpipe_130.csv'    u 22:21 w lp lw 4 lc 6 t 'Grading = 1.30', \
  'test_references/GridConverge/lauferpipe_135.csv'    u 22:21 w lp lw 4 lc 7 t 'Grading = 1.35', \
  'test_references/GridConverge/lauferpipe_140.csv'    u 22:21 w lp lw 4 lc 8 t 'Grading = 1.40', \
  A(x)                           w lp lw 2 lt 1 lc rgb 'gray40' t 'Laminar Solution', \
  B(x)                            w l lw 4 lt 1 lc rgb 'gray70' t 'Turbulent Solution'
