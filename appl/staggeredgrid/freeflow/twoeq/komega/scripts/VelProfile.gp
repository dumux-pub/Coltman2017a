reset
set terminal pngcairo size 1250,940 solid
set datafile separator ','
DATA='./'
set output 'VelProfile_Prime.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'u / u_{max}'
set ylabel 'y / h'
set xrange [0:1.05]
set yrange [0:0.5]
set key font ",20"
set key left Left reverse center samplen 1
set title font ",20"
set title 'Velocity Distribution: Laufer Pipe Experiment'
plot \
  'test_references/GridConverge/lauferpipe_135.csv'    u ($5):($27/0.2469) w l lw 4 lc 4 t 'k-Omega Laufer Pipe Simulation', \
  'test_references/WilcoxModel.csv'                    using 3:4           w l lw 5 lc 4 lt 0 t 'Wilcox k-Omega Model', \
  'test_references/Laufer_PipeRe50k.csv'               using 8:7 w p ps 2 lc 8 pt 7 t 'Laufer Pipe Experiment'

reset
set terminal pngcairo size 1250,940 solid
set datafile separator ','
DATA='./'
set output 'VelProfile_ModelCompare.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'u / u_{max}'
set ylabel 'y / h'
set xrange [0:1.05]
set yrange [0:0.5]
set key font ",20"
set key left Left reverse center samplen 1
set title font ",20"
set title 'Velocity Distribution: Turbulence Models Compared'
plot \
   'test_references/ZeroEq.csv'                      u ($5):($26/0.2469) w l lw 4 lc 1 t 'Zero Eq', \
   'test_references/kepsilon.csv'                    u ($5):($40/0.2469) w l lw 4 lc 2 t 'k-Epsilon', \
   'test_references/lowrekepsilon_135.csv'           u ($5):($26/0.2469) w l lw 4 lc 3 t 'Low Re k-Epsilon', \
   'test_references/lauferpipe_1_35_08_winflow.csv'  u ($5):($26/0.2469) w l lw 4 lc 4 t 'k-Omega Laufer Pipe Simulation', \
  'test_references/WilcoxModel.csv'                    using 3:4           w l lw 5 lc 4 lt 0 t 'Wilcox k-Omega Model', \
  'test_references/Laufer_PipeRe50k.csv'               using 8:7 w p ps 2 lc 8 pt 7 t 'Laufer Pipe Experiment'

   reset
set terminal pngcairo size 1250,940 solid
set datafile separator ','
DATA='./'
set output 'VelProfile_Grid.png'
set xlabel font ",20"
set ylabel font ",20"
set xlabel 'u / u_{max}'
set ylabel 'y / h'
set xrange [0:1.05]
set yrange [0:0.5]
set key font ",20"
set key left Left reverse center samplen 1
set title font ",20"
set title 'Velocity Distribution: Grid Convergence'
plot \
  'test_references/GridConverge/lauferpipe_105.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.05', \
  'test_references/GridConverge/lauferpipe_110.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.10', \
  'test_references/GridConverge/lauferpipe_115.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.15', \
  'test_references/GridConverge/lauferpipe_120.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.20', \
  'test_references/GridConverge/lauferpipe_125.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.25', \
  'test_references/GridConverge/lauferpipe_130.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.30', \
  'test_references/GridConverge/lauferpipe_135.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.35', \
  'test_references/GridConverge/lauferpipe_140.csv'    u ($5):($27/0.2469) w l lw 4 t 'Grid Grading = 1.40', \
  'test_references/Laufer_PipeRe50k.csv'               using 8:7 w p ps 2 lc 8 pt 7 t 'Laufer Pipe Experiment'
