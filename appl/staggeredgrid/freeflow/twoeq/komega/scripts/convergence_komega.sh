DATA="."
SCRIPT="/var/run/media/nedc/workspace10/MastersThesis/dumux/dumux/bin/postprocessing/extractlinedata.py"
#rm logfile.out lauferpipe_*.csv

runSim () {
./test_lauferpipe_komega $INPUT -Grid.Cells1 "$CELLS" -Grid.Grading1 "$1" -Problem.Name "$2" | tee -a logfile.out
input=`ls -ltr laufer*Secondary*vtu | tail -n 1 | awk '{print $9}'`
echo "$input" -> "$2" | tee -a logfile.out
pvpython $SCRIPT -f $input -o $DATA -of $2 -p1 $P1 -p2 $P2 -v 2 -r 10000 | tee -a logfile.out

mv *_105*.vtu *_105*.pvd *_105.csv GridConvergence/05/
mv *_110*.vtu *_110*.pvd *_110.csv GridConvergence/10/
mv *_115*.vtu *_115*.pvd *_115.csv GridConvergence/15/
mv *_120*.vtu *_120*.pvd *_120.csv GridConvergence/20/
mv *_125*.vtu *_125*.pvd *_125.csv GridConvergence/25/
mv *_130*.vtu *_130*.pvd *_130.csv GridConvergence/30/
mv *_135*.vtu *_135*.pvd *_135.csv GridConvergence/35/
mv *_140*.vtu *_140*.pvd *_140.csv GridConvergence/40/
}

### lauferpipe
P1="8.0 0.0 0.0"; P2="8.0 0.2469 0.0"
CELLS="25 25"
INPUT=test_lauferpipe_komega.input
runSim '1.05 -1.05' lauferpipe_105
runSim '1.10 -1.10' lauferpipe_110
runSim '1.15 -1.15' lauferpipe_115
runSim '1.20 -1.20' lauferpipe_120
runSim '1.25 -1.25' lauferpipe_125
runSim '1.30 -1.30' lauferpipe_130
runSim '1.35 -1.35' lauferpipe_135
runSim '1.40 -1.40' lauferpipe_140
# ZEROEQ=`ls -ltr lauferpipe_zeroeq*Secondary*vtu | tail -n 1 | awk '{print $9}'`
# pvpython $SCRIPT -f $ZEROEQ -of zeroeq -p1 $P1 -p2 $P2 -r 10000


