/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with \f$ k-\omega \f$ - turbulence
 * models
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * Turbulent Kinetic Energy balance:
 * \f[
 * \frac{\partial \left( k \right)}{\partial t}
 * + \nabla \cdot \left( v_\alpha k \right)
 * - \nabla \cdot \left[ \left( \nu +  \sigma_\textrm{k} \nu_t \right) \nabla k \right]
 * - P
 * + \beta_k^{*} k \omega
 * - q_{k}
 * = 0
 * \f]
 * with \f$ S_{ij} = \frac{1}{2} \left[ \frac{\partial}{\partial x_i} v_j + \frac{\partial}{\partial x_j} v_i \right] \f$
 * and \f$ a_{ij} \cdot b_{ij} = \sum_{i,j} a_{ij} b_{ij} \f$.
 *
 * Dissipation balance:
 * \f[
 * \frac{\partial \left( \omega \right)}{\partial t}
 * + \nabla \cdot \left( v_\alpha \omega \right)
 * - \nabla \cdot \left[ \left( \nu + \sigma_{\omega} \nu_t \right) \nabla \omega \right]
 * - \frac{\alpha}{\nu_t}P
 * + \beta_{\omega} \omega^2
 * - \frac{\sigma_d}{\omega} \nabla k \nabla \omega
 * - q_{\omega}
 * = 0
 * \f]
 *
 * The kinematic eddy viscosity \f$ \nu_\textrm{t} \f$ is calculated as follows:
 * \f[ \nu_\textrm{t} = \frac{k}{\omega}} \f]
 *
 * The turbulence Reynolds number \f$ Re_\textrm{T} \f$ is given as
 * \f[ Re_\textrm{T} = \frac{k}{\nu \omega} \f]
 *
 * The wall distance Reynolds number \f$ Re_\textrm{y} \f$ is given as
 * \f[ Re_\textrm{y} = \frac{\sqrt{k} y}{\nu} \f]
 *
 * The term \f$ \sigma_d \f$ is calculated as follows:
 * \f[ If
 *              \nabla K * \nabla \omega > 0
 *     Then
 *              \sigma_d = 1/8,
 *     Else
 *              \sigma_d = 0 \f]
 *
 *
 * The following models are available and can be chosen via setting <tt>Komega.Model</tt>,
 * for detailed description have look at the individual functions in the local operator.<br>
 * (0) Wilcox 88 [Wilcox, 1998]<br>
 * (1) Wilxox 08 [Wilcox, 2008]<br>
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_KOMEGA_STAGGERED_GRID_HH
#define DUMUX_BASE_KOMEGA_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"komegapropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state K-Omega equations.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseKOmegaStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceTurbulentKineticEnergyBalance) SourceTurbulentKineticEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceDissipationBalance) SourceDissipationBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTurbulentKineticEnergy) DirichletTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletDissipation) DirichletDissipation;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTurbulentKineticEnergy) NeumannTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannDissipation) NeumannDissipation;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { turbulentKineticEnergyIdx = Indices::turbulentKineticEnergyIdx,
             dissipationIdx = Indices::dissipationIdx };

      enum { phaseIdx = Indices::phaseIdx };

      //! element stored data
      typedef std::vector<double> StoredScalar;
      mutable StoredScalar storedTurbulentKineticEnergy;
      mutable StoredScalar storedDissipation;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedKinematicTurbulentEnergyGradient;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedDissipationGradient;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      BaseKOmegaStaggeredGrid(const BC& bc_,
        const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance_, const SourceDissipationBalance& sourceDissipationBalance_,
        const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy_, const DirichletDissipation& dirichletDissipation_,
        const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy_, const NeumannDissipation& neumannDissipation_,
        GridView gridView_, Problem& problem)
        : bc(bc_),
          sourceTurbulentKineticEnergyBalance(sourceTurbulentKineticEnergyBalance_), sourceDissipationBalance(sourceDissipationBalance_),
          dirichletTurbulentKineticEnergy(dirichletTurbulentKineticEnergy_), dirichletDissipation(dirichletDissipation_),
          neumannTurbulentKineticEnergy(neumannTurbulentKineticEnergy_), neumannDissipation(neumannDissipation_),
          gridView(gridView_), mapperElement(gridView), problemPtr_(0)
      {
        // properties
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);
        KOmegaEnableKinematicViscosity_= GET_PARAM_FROM_GROUP(TypeTag, bool, KOmega, EnableKinematicViscosity);
        KOmegaModel_ = GET_PARAM_FROM_GROUP(TypeTag, int, KOmega, Model);
        problemPtr_ = &problem;
        initialize();
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_komega(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                 std::vector<DimVector> velocityFaces, Scalar pressure,
                                 Scalar massMoleFrac, Scalar temperature) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_W;
        const LFSU_W& lfsu_w = lfsu.template child<dissipationIdx>();

        Scalar elementVolume = eg.geometry().volume();

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        Scalar turbulentKineticEnergy = x(lfsu_k, 0);
        Scalar dissipation = x(lfsu_w, 0);
        Scalar kinematicEddyViscosity = calculateKinematicEddyViscosity(turbulentKineticEnergy, dissipation);
        asImp_().storedKinematicEddyViscosity[mapperElement.index(eg.entity())] = kinematicEddyViscosity;

        // scalar product of velocity gradient tensor is the stress tenosr S_ij
        Dune::FieldMatrix<Scalar, GridView::dimension, GridView::dimension> stressTensor(0.0);
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            stressTensor[i][j] = 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][i][j];
            stressTensor[i][j] += 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][j][i];

//          // NOTE: this would include the dilation term
//            if (j == i)
//            {
//              stressTensor[i][j] -= 1.0 / 3.0 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][i][j];
//            }
          }
        }
        Scalar stressTensorScalarProduct = 0.0;
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            stressTensorScalarProduct += stressTensor[i][j] * stressTensor[i][j];
          }
        }

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension
        /**
          * (1) \b Production term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *     -P = - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} \Rightarrow - \int_V 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          * \f]
          * \f[
          *     \alpha = - 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} V_e
          * \f]
          */

        // plus 2006 K-limiter via P term
        Scalar Porig= 2.0 * kinematicEddyViscosity * stressTensorScalarProduct;
        Scalar Paltr= 20.0 * betaK() * turbulentKineticEnergy * dissipation;
        Scalar P_min= std::min(Porig,Paltr);
        r.accumulate(lfsu_k, 0,
                     -1.0 * P_min * elementVolume);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
          * (2) \b Production term of \b dissipation balance equation<br>
          *
          *  \f[  -\alpha \frac{\omega}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          *       \Rightarrow
          *       -\int_V \alpha \frac{\omega}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij}
          *  \f]
          *
          *  \f[  accumulate = - \alpha \frac{\omega}{k} 2 \nu_\textrm{t} S_{ij} \cdot S_{ij} V_e
          *  \f]
          *
          *  The kinematic eddy viscosity $ \nu_\textrm{t} $ is calculated as follows:
          *  \f[ \nu_\textrm{t} =  \frac{k}{\omega} \f]
          */
        r.accumulate(lfsu_w, 0,
                     alpha() *
                     (dissipation / turbulentKineticEnergy) *
                     -1.0 * P_min * elementVolume);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
          * (3) \b Destruction term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[  \beta_k^* k \omega
          *      \Rightarrow
          *      \int_V \beta_k^* k \omega
          * \f]
          *
          * \f[  \alpha = \beta_k^* k \omega V_e
          * \f]
          */
        r.accumulate(lfsu_k, 0,
                     1.0 * betaK() * turbulentKineticEnergy * dissipation * elementVolume);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
          * (4) \b Destruction term of \b dissipation balance equation<br>
          *

          * \f[  \beta_{omega} \omega \omega
          *      \Rightarrow
          *      \int_V \beta_{\omega} \omega \omega
          * \f]
          *
          * \f[  \alpha = \beta_{\omega} \omega \omega V_e
          * \f]
          */
        r.accumulate(lfsu_w, 0,
                     1.0 * betaOmega() * dissipation * dissipation * elementVolume);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /**
          * (6) \b Source term of <b> turbulent kinetic energy </b> balance equation<br>
          *
          * \f[
          *    - q_\textrm{k}
          *    \Rightarrow - \int_V q_\textrm{k}
          * \f]
          * \f[
          *    \alpha = - q_\textrm{k} V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt, qorder);
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceTurbulentKineticEnergyBalance::Traits::RangeType sourceTurbulentKineticEnergyBalanceValue;
          sourceTurbulentKineticEnergyBalance.evaluate(eg.entity(), it->position(), sourceTurbulentKineticEnergyBalanceValue);
          r.accumulate(lfsu_k, 0,
                       -1.0 * sourceTurbulentKineticEnergyBalanceValue * elementVolume * it->weight());
        }

        /**
          * (7) \b Source term of \b dissipation balance equation<br>
          *
          * \f[
          *    - q_{\omega}
          *    \Rightarrow - \int_V q_{\omega}
          * \f]
          * \f[
          *    \alpha = - q_{\omega} V_e
          * \f]
          */
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceDissipationBalance::Traits::RangeType sourceDissipationBalanceValue;
          sourceDissipationBalance.evaluate(eg.entity(), it->position(), sourceDissipationBalanceValue);
          r.accumulate(lfsu_w, 0,
                       -1.0 * sourceDissipationBalanceValue * elementVolume * it->weight());
        }
      }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_komega(const IG& ig,
                                   const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                   const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                   R& r_s, R& r_n,
                                   std::vector<DimVector> velocities_s, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   std::vector<DimVector> velocities_n, Scalar pressure_n,
                                   Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k_s = lfsu_s.template child<turbulentKineticEnergyIdx>();
        const LFSU_K& lfsu_k_n = lfsu_n.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_W;
        const LFSU_W& lfsu_w_s = lfsu_s.template child<dissipationIdx>();
        const LFSU_W& lfsu_w_n = lfsu_n.template child<dissipationIdx>();

        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        Scalar distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values and constants
        Scalar turbulentKineticEnergy_s = x_s(lfsu_k_s, 0);
        Scalar turbulentKineticEnergy_n = x_n(lfsu_k_n, 0);
        Scalar dissipation_s = x_s(lfsu_w_s, 0);
        Scalar dissipation_n = x_n(lfsu_w_n, 0);
        Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicViscosity_n = asImp_().kinematicViscosity(pressure_n, temperature_n, massMoleFrac_n);
        Scalar kinematicEddyViscosity_s
                = calculateKinematicEddyViscosity(turbulentKineticEnergy_s, dissipation_s);
        Scalar kinematicEddyViscosity_n
                = calculateKinematicEddyViscosity(turbulentKineticEnergy_n, dissipation_n);
        asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.inside())] = kinematicEddyViscosity_s;
        asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.outside())] = kinematicEddyViscosity_n;

        // averaging: distance weighted average for diffusion term
        Scalar kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n
                                      + distanceOutsideToFace * kinematicViscosity_s)
                                    / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicEddyViscosity_avg = (distanceInsideToFace * kinematicEddyViscosity_n
                                          + distanceOutsideToFace * kinematicEddyViscosity_s)
                                        / (distanceInsideToFace + distanceOutsideToFace);

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for diffusion term
          kinematicViscosity_avg = (2.0 * kinematicViscosity_n * kinematicViscosity_s)
                                  / (kinematicViscosity_n + kinematicViscosity_s);
          kinematicEddyViscosity_avg = (2.0 * kinematicEddyViscosity_n * kinematicEddyViscosity_s)
                                      / (kinematicEddyViscosity_n + kinematicEddyViscosity_s);
        }

        Scalar effectiveViscosityK_avg = kinematicEddyViscosity_avg * sigmaK();
        Scalar effectiveViscosityOmega_avg = kinematicEddyViscosity_avg * sigmaOmega();

        if (KOmegaEnableKinematicViscosity_)
        {
          effectiveViscosityK_avg += kinematicViscosity_avg;
          effectiveViscosityOmega_avg += kinematicViscosity_avg;
        }

        // upwinding: advection term
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar turbulentKineticEnergy_up = turbulentKineticEnergy_s;
        Scalar dissipation_up = dissipation_s;
        if (velocityNormal < 0)
        {
          turbulentKineticEnergy_up = turbulentKineticEnergy_n;
          dissipation_up = dissipation_n;
        }

        if (enableAdvectionAveraging_)
        {
          turbulentKineticEnergy_up = (distanceInsideToFace * turbulentKineticEnergy_n
                                        + distanceOutsideToFace * turbulentKineticEnergy_s)
                                      / (distanceInsideToFace + distanceOutsideToFace);
          dissipation_up = (distanceInsideToFace * dissipation_n
                            + distanceOutsideToFace * dissipation_s)
                          / (distanceInsideToFace + distanceOutsideToFace);
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call

        /**
         * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    k v
         *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| k_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the turbulentKineticEnergy.
         */
        r_s.accumulate(lfsu_k_s, 0,
                       1.0 * turbulentKineticEnergy_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_k_n, 0,
                       -1.0 * turbulentKineticEnergy_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (2) \b Flux term of \b dissipation balance equation
         *
         * \f[
         *    \omega v
         *    \Rightarrow \int_\gamma \left( \omega v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \omega_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the dissipation.
         */
        r_s.accumulate(lfsu_w_s, 0,
                       1.0 * dissipation_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_w_n, 0,
                       -1.0 * dissipation_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (3) \b Diffusion term of <b> turbulent kinetic energy </b> balance equation
         *
         * \f[
         *    \nu + \nu_\textrm{t} \sigma_\textrm{k} \nabla k
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \nu_\textrm{t} \sigma_\textrm{k} \right) \nabla k \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \nu_\textrm{t,avg} \sigma_\textrm{k,avg} \right) \nabla k \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        r_s.accumulate(lfsu_k_s, 0,
                       -1.0 * effectiveViscosityK_avg
                       * (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_k_n, 0,
                       1.0 * effectiveViscosityK_avg
                       * (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        /**
         * (4) \b Diffusion term of \b dissipation balance equation
         *
         * \f[
         *    \nu + \nu_\textrm{t} \sigma_{\omega} \nabla \omega
         *    \Rightarrow - \int_\gamma \left( \left( \nu + \nu_\textrm{t} \sigma_{\omega} \right) \nabla \tilde{\omega} \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \left( \nu + \nu_\textrm{t,avg} \sigma_{\textrm{\omega,avg}} \right) \nabla \tilde{\omega} \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        r_s.accumulate(lfsu_w_s, 0,
                      -1.0 * effectiveViscosityOmega_avg
                       * (dissipation_n - dissipation_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_w_n, 0,
                      1.0 * effectiveViscosityOmega_avg
                       * (dissipation_n - dissipation_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

                /**
         * (5) \b Sigma_D term of \b dissipation balance equation
         *
         *  \f[
         *      - \frac{\sigma_d}{\omega} \nabla k \nabla \omega
         *  \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */
        Scalar NabKWProduct = 0.0;
        for (unsigned int j = 0; j < dim; ++j)
        {
          NabKWProduct +=  storedKinematicTurbulentEnergyGradient[mapperElement.index(ig.inside())][j]
                            * storedDissipationGradient[mapperElement.index(ig.inside())][j];
        }

       Scalar elementVolumeInside = ig.inside().geometry().volume();
       Scalar elementVolumeOutside = ig.outside().geometry().volume();
       r_s.accumulate(lfsu_w_s, 0,
                     -0.5 * sigma_d(NabKWProduct)
                     * NabKWProduct
                     / dissipation_up
                     * elementVolumeInside);
       r_n.accumulate(lfsu_w_n, 0,
                     0.5 * sigma_d(NabKWProduct)
                     * NabKWProduct
                     / dissipation_up
                     * elementVolumeOutside);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_komega(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                   std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   Scalar pressure_boundary,
                                   Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k_s = lfsu_s.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_W;
        const LFSU_W& lfsu_w_s = lfsu_s.template child<dissipationIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();

        // center in face's reference element
        const Dune::FieldVector<Scalar, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // center in inside element
        const Dune::FieldVector<Scalar, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar,IG::dimension-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside().geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<Scalar,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar,IG::dimension>::general(ig.inside().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // evaluation of cell values
        Scalar turbulentKineticEnergy_s = x_s(lfsu_k_s, 0);
        Scalar dissipation_s = x_s(lfsu_w_s, 0);

        // /////////////////////
        // call ParentType
        Scalar kinematicViscosity_avg = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicEddyViscosity_avg = calculateKinematicEddyViscosity(turbulentKineticEnergy_s, dissipation_s);
        // turbulentKineticEnergy at the boundary is given
        typename DirichletTurbulentKineticEnergy::Traits::RangeType  turbulentKineticEnergy_boundary(0.0);
        dirichletTurbulentKineticEnergy.evaluate(ig.inside(), insideFaceCenterLocal, turbulentKineticEnergy_boundary);
        // dissipation at the boundary is given
        typename DirichletDissipation::Traits::RangeType  dissipation_boundary(0.0);
        dirichletDissipation.evaluate(ig.inside(), insideFaceCenterLocal, dissipation_boundary);

        asImp_().storedKinematicEddyViscosity[mapperElement.index(ig.inside())] = kinematicEddyViscosity_avg;

        Scalar effectiveViscosityK_avg = kinematicEddyViscosity_avg * sigmaK() + kinematicViscosity_avg;
        Scalar effectiveViscosityOmega_avg = kinematicEddyViscosity_avg * sigmaOmega() + kinematicViscosity_avg;

        // Inflow boundary for turbulentKineticEnergy
        if (bcTurbulentKineticEnergy.isInflow(ig, faceCenterLocal) || bcTurbulentKineticEnergy.isWall(ig, faceCenterLocal))
        {
          /**
           * Inflow boundary handling for turbulent kinetic energy balance<br>
           * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
           * \f[
           *    k v
           *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| k_\textrm{boundary} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_k_s, 0,
                         1.0 * turbulentKineticEnergy_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);

          /**
           * (2) \b Diffusion term of <b> turbulent kinetic energy </b> balance equation
           *
           * \f[
           *    - \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \nabla k
           *    \Rightarrow - \int_\gamma \left( \left( \nu + \frac{\nu_\textrm{t}}{\sigma_\textrm{k}} \right) \nabla k \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = - |\gamma| \left( \nu + \frac{\nu_\textrm{t,self}}{\sigma_\textrm{k,self}} \right) \nabla k \cdot n
           * \f]
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          // The default procedure for averaging is a distance weighted average, by
          // using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
          // harmonic averaging instead.
          r_s.accumulate(lfsu_k_s, 0,
                        -1.0 * effectiveViscosityK_avg
                        * (turbulentKineticEnergy_boundary - turbulentKineticEnergy_s)
                        / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                        * faceUnitOuterNormal[normDim]
                        * faceVolume);
        }
        // Outflow boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for turbulent kinetic energy balance<br>
           * (1) \b Flux term of <b> turbulent kinetic energy </b> balance equation
           * \f[
           *    k v
           *    \Rightarrow \int_\gamma \left( k v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| k_\textrm{self} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_k_s, 0,
                         1.0 * turbulentKineticEnergy_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Symmetry boundary for turbulentKineticEnergy
        else if (bcTurbulentKineticEnergy.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for turbulentKineticEnergy.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for turbulentKineticEnergy.");
        }


        // Inflow boundary for dissipation
        if (bcDissipation.isInflow(ig, faceCenterLocal)
            || (bcDissipation.isWall(ig, faceCenterLocal))
           )
        {
          /**
           * Inflow boundary handling for dissipation balance<br>
           * (1) \b Flux term of \b dissipation balance equation
           * \f[
           *    \omega v
           *    \Rightarrow \int_\gamma \left( \omega v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \omega_{\textrm{boundary}} \left( v \cdot n \right)
           * \f]
           */
         r_s.accumulate(lfsu_w_s, 0,
                        1.0 * dissipation_boundary
                        * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                        * faceVolume);
          /**
           * (2) \b Diffusion term of \b dissipation balance equation
           *
           * \f[
           *    - \nu + \nu_\textrm{t} \sigma_{\omega} \nabla \omega
           *    \Rightarrow - \int_\gamma \left(\nu + \nu_\textrm{t} \sigma_{\omega} \omega \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = - |\gamma| \nu + \nu_\textrm{t,self} \sigma_{\omega\textrm{,self}} \nabla \omega \cdot n
           * \f]
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          // The default procedure for averaging is a distance weighted average, by
          // using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
          // harmonic averaging instead.
          r_s.accumulate(lfsu_w_s, 0,
                          -1.0 * effectiveViscosityOmega_avg
                          * (dissipation_boundary - dissipation_s)
                          / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);

        }
        // Outflow boundary for dissipation
        else if (bcDissipation.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for dissipation balance<br>
           * (1) \b Flux term of \b dissipation balance equation
           * \f[
           *    \omega v
           *    \Rightarrow \int_\gamma \left( \tilde{\varepsilon} v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \omega_\textrm{self} \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_w_s, 0,
                         1.0 * dissipation_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Symmetry boundary for dissipation
        else if (bcDissipation.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for dissipation.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for dissipation.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       */
      void initialize()
      {
        if (KOmegaModel_ == Indices::Wilcox88)
          std::cout << "Model_ = Wilcox88" << std::endl;
        else if (KOmegaModel_ == Indices::Wilcox08)
          std::cout << "Model_ = Wilcox08" << std::endl;
        else
          DUNE_THROW(Dune::NotImplemented, "This Model is not implemented.");

        storedTurbulentKineticEnergy.resize(mapperElement.size());
        storedDissipation.resize(mapperElement.size());
        storedKinematicTurbulentEnergyGradient.resize(mapperElement.size());
        storedDissipationGradient.resize(mapperElement.size());

        // initialize some of the values
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedTurbulentKineticEnergy[i] = 0.0;
          storedDissipation[i] = 0.0;
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedKinematicTurbulentEnergyGradient[i][j] = 0.0;
          }
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedDissipationGradient[i][j] = 0.0;
          }
        }

        // select the components from the subspaces
        typedef typename BC::template Child<turbulentKineticEnergyIdx>::Type BCTurbulentKineticEnergy;
        const BCTurbulentKineticEnergy& bcTurbulentKineticEnergy = bc.template child<turbulentKineticEnergyIdx>();
        typedef typename BC::template Child<dissipationIdx>::Type BCDissipation;
        const BCDissipation& bcDissipation = bc.template child<dissipationIdx>();

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTurbulentKineticEnergy.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCTurbulentKineticEnergy at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcTurbulentKineticEnergy.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcTurbulentKineticEnergy.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcTurbulentKineticEnergy.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcTurbulentKineticEnergy.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for turbulentKineticEnergy at one point.");
            }

            // check for multiple defined boundary conditions
            numberOfBCTypesAtPos = 0;
            if (bcDissipation.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcDissipation.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCDissipation at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcDissipation.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcDissipation.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcDissipation.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcDissipation.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for dissipation at one point.");
            }
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * This function just calls the ParentType.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsTurbulentKineticEnergy = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, turbulentKineticEnergyIdx> >;
        using SubGfsDissipation = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, dissipationIdx> >;
        SubGfsTurbulentKineticEnergy subGfsTurbulentKineticEnergy(mdgfs);
        SubGfsDissipation subGfsDissipation(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsTurbulentKineticEnergy = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<turbulentKineticEnergyIdx> >;
        using SubGfsDissipation = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<dissipationIdx> >;
        SubGfsTurbulentKineticEnergy subGfsTurbulentKineticEnergy(gfs);
        SubGfsDissipation subGfsDissipation(gfs);
#endif

        // discrete function objects
        using DgfTurbulentKineticEnergy = Dune::PDELab::DiscreteGridFunction<SubGfsTurbulentKineticEnergy, X>;
        using DgfDissipation = Dune::PDELab::DiscreteGridFunction<SubGfsDissipation, X>;
        DgfTurbulentKineticEnergy dgfTurbulentKineticEnergy(subGfsTurbulentKineticEnergy, lastSolution);
        DgfDissipation dgfDissipation(subGfsDissipation, lastSolution);

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // NOTE use eit also for multidomain models
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          if (!gridView.indexSet().contains(stokesDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif

          // evaluate solutions
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);
          Dune::FieldVector<Scalar, 1> turbulentKineticEnergy(0.0);
          Dune::FieldVector<Scalar, 1> dissipation(0.0);
          dgfTurbulentKineticEnergy.evaluate(element, cellCenterLocal, turbulentKineticEnergy);
          dgfDissipation.evaluate(element, cellCenterLocal, dissipation);

          storedTurbulentKineticEnergy[elementInsideID] = turbulentKineticEnergy;
          storedDissipation[elementInsideID] = dissipation;
          asImp_().storedKinematicEddyViscosity[elementInsideID]
            = calculateKinematicEddyViscosity(storedTurbulentKineticEnergy[elementInsideID],
                                              storedDissipation[elementInsideID]);

          for (unsigned int i = 0; i < dim; ++i)
          {
            storedKinematicTurbulentEnergyGradient[elementInsideID][i] = 0.0;
            storedDissipationGradient[elementInsideID][i] = 0.0;
          }
          typedef typename GridView::IntersectionIterator IntersectionIterator;
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            if (is->neighbor())
            {
              // local position of cell and face centers
              const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
                Dune::ReferenceElements<Scalar, dim>::general(is->inside().type()).position(0, 0);
              const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
                Dune::ReferenceElements<Scalar, dim>::general(is->outside().type()).position(0, 0);

              // global position of cell and face centers
              Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
                is->inside().geometry().global(insideCellCenterLocal);
              Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
                is->outside().geometry().global(outsideCellCenterLocal);


#if IS_STAGGERED_MULTIDOMAIN_MODEL
              if (!gridView.indexSet().contains(stokesDomainIdx, is->inside())
                  || !gridView.indexSet().contains(stokesDomainIdx, is->outside()))
                  continue;
              auto element_s = sdgv.grid().subDomainEntityPointer(is->inside());
              auto element_n = sdgv.grid().subDomainEntityPointer(is->outside());
#else
              auto element_s = is->inside();
              auto element_n = is->outside();
#endif
              // get the pressure in the cell centers
              Dune::FieldVector<Scalar, 1> turbulentKineticEnergy_s(0.0);
              Dune::FieldVector<Scalar, 1> turbulentKineticEnergy_n(0.0);
              Dune::FieldVector<Scalar, 1> dissipation_s(0.0);
              Dune::FieldVector<Scalar, 1> dissipation_n(0.0);
              dgfTurbulentKineticEnergy.evaluate(element_s, insideCellCenterLocal, turbulentKineticEnergy_s);
              dgfTurbulentKineticEnergy.evaluate(element_n, outsideCellCenterLocal, turbulentKineticEnergy_n);
              dgfDissipation.evaluate(element_s, insideCellCenterLocal, dissipation_s);
              dgfDissipation.evaluate(element_n, outsideCellCenterLocal, dissipation_n);

              storedKinematicTurbulentEnergyGradient[elementInsideID][dim] += (turbulentKineticEnergy_n - turbulentKineticEnergy_s)
                                                      / (insideCellCenterGlobal[dim] - outsideCellCenterGlobal[dim]);
              storedDissipationGradient[elementInsideID][dim] += (dissipation_n - dissipation_s)
                                                      / (insideCellCenterGlobal[dim] - outsideCellCenterGlobal[dim]);

            }
          }
        }
      }

      /**
       * \brief Returns the turbulentKineticEnergy for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar turbulentKineticEnergy(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        return x(lfsu_k, 0);
      }

      /**
       * \brief Returns the dissipation for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar dissipation(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_W;
        const LFSU_W& lfsu_w = lfsu.template child<dissipationIdx>();
        return x(lfsu_w, 0);
      }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
      * \brief Calculate eddy viscosity
      */
    const Scalar calculateKinematicEddyViscosity(Scalar turbulentKineticEnergy, Scalar dissipation) const
    {
      return turbulentKineticEnergy/dissipation;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f Re_T \$f value
    const Scalar reT(Scalar turbulentKineticEnergy, Scalar dissipation,
                     Scalar kinematicViscosity) const
    {
      return turbulentKineticEnergy * turbulentKineticEnergy
             / kinematicViscosity / dissipation;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f Re_y \$f value
    const Scalar reY(Scalar turbulentKineticEnergy, Scalar wallDistance,
                     Scalar kinematicViscosity) const
    {
      return std::sqrt(turbulentKineticEnergy) * wallDistance
             / kinematicViscosity;
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f \alpha \$f value
    const Scalar alpha() const
    {
          if(KOmegaModel_ == Indices::Wilcox88)
               return 0.55555555556;
          else if (KOmegaModel_ == Indices::Wilcox08)
               return 0.520;
          else
              DUNE_THROW(Dune::NotImplemented, "This Model is not implemented.");
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//! \brief Returns the \$f \sigma_k \$f constant
    const Scalar sigmaK() const
    {
          if(KOmegaModel_ == Indices::Wilcox88)
               return 0.50;
          else if (KOmegaModel_ == Indices::Wilcox08)
               return 0.60;
          else
              DUNE_THROW(Dune::NotImplemented, "This Model is not implemented.");
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f \sigma_{\omega} \$f constant
    const Scalar sigmaOmega() const
    {
          if(KOmegaModel_ == Indices::Wilcox88)
               return 0.50;
          else if (KOmegaModel_ == Indices::Wilcox08)
               return 0.50;
          else
              DUNE_THROW(Dune::NotImplemented, "This Model is not implemented.");
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f \beta_k \$f constant
    const Scalar betaK() const
    {
          if(KOmegaModel_ == Indices::Wilcox88)
               return 0.09;
          else if (KOmegaModel_ == Indices::Wilcox08)
               return 0.09;
          else
              DUNE_THROW(Dune::NotImplemented, "This Model is not implemented.");
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f \beta_{\omega} \$f constant
    const Scalar betaOmega() const
    {
          if(KOmegaModel_ == Indices::Wilcox88)
               return 0.0750;
          else if (KOmegaModel_ == Indices::Wilcox08)
               return 0.0708;
          else
              DUNE_THROW(Dune::NotImplemented, "This Model is not implemented.");
    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns if the product of \$f  \nabla K \$f and \$f  \nabla omega \$f is more than 0
    bool NabProductBool(Scalar NabProduct) const
    {
       bool status = false;
       if (NabProduct > 0)
          {status = true;}
       return status;
          }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //! \brief Returns the \$f \sigma_{d} \$f constant
    const Scalar sigma_d(Scalar NabProduct) const
    {
          if(NabProductBool(NabProduct) == 1)
               return 1/8;
          else
               return 0;
    }

private:
      const BC& bc;
      const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance;
      const SourceDissipationBalance& sourceDissipationBalance;
      const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy;
      const DirichletDissipation& dirichletDissipation;
      const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy;
      const NeumannDissipation& neumannDissipation;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;
      bool KOmegaEnableKinematicViscosity_;
      unsigned int KOmegaModel_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_KOMEGA_STAGGERED_GRID_HH
