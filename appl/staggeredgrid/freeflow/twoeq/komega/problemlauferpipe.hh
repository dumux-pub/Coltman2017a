// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for a pipe flow using the komega turbulence model
 */

#ifndef DUMUX_PROBLEM_PIPEFLOW_DUMUX_HH
#define DUMUX_PROBLEM_PIPEFLOW_DUMUX_HH

#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidsystems/gasphase.hh>

#include "komegaproblem.hh"
#include "komegapropertydefaults.hh"

namespace Dumux
{
template <class TypeTag>
class LauferPipeProblem;

namespace Properties
{
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridKOmega));

// Set the problem property
SET_TYPE_PROP(LauferPipeProblem, Problem,
              Dumux::LauferPipeProblem<TypeTag>);

// Set the used local operator
SET_TYPE_PROP(LauferPipeProblem, LocalOperator,
              Dune::PDELab::KOmegaStaggeredGrid<TypeTag>);

// Set the used transient local operator
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator,
              Dune::PDELab::KOmegaTransientStaggeredGrid<TypeTag>);

// Set the grid type
SET_TYPE_PROP(LauferPipeProblem, Grid,
              Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Disable gravity field
SET_BOOL_PROP(LauferPipeProblem, ProblemEnableGravity, false);

// Use air as fluid
SET_TYPE_PROP(LauferPipeProblem, Fluid,
              FluidSystems::GasPhase<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     Air<typename GET_PROP_TYPE(TypeTag, Scalar)> >);
}

template <class TypeTag>
class LauferPipeProblem : public KOmegaProblem<TypeTag>
{
    typedef KOmegaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;

public:
    LauferPipeProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Velocity);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, MassMoleFrac);
        smallestGridCellY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, SmallestGridCellY);
        FluidSystem::init();

        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        FluidState fluidState;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(temperature_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        kinematicViscosity_ = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        Dune::FieldVector<double, dim> halfWay;
        halfWay[0] = 0.5 * (this->bBoxMax()[0] - this->bBoxMin()[0]) + this->bBoxMin()[0];
        halfWay[1] = 0.5 * smallestGridCellY_;
        Dune::FieldVector<double, dim> end;
        end[0] = this->bBoxMax()[0];
        end[1] = 0.5 * smallestGridCellY_;
        turbulenceProperties.yPlusEstimation(velocity_, halfWay, kinematicViscosity_, density);
        turbulenceProperties.yPlusEstimation(velocity_, end, kinematicViscosity_, density);
        turbulenceProperties.entranceLength(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_);
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_, false);
        dissipationRate_ = turbulenceProperties.dissipationRate(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_,false);
        std::cout << std::endl;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return (!bcVelocityIsInflow(global) && !bcVelocityIsOutflow(global)); }
    bool bcVelocityIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcVelocityIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcVelocityIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global); }

    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return (!bcTurbulentKineticEnergyIsInflow(global) && !bcTurbulentKineticEnergyIsOutflow(global)); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return (!bcDissipationIsInflow(global) && !bcDissipationIsOutflow(global)); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcDissipationIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcDissipationIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (global[1] > this->bBoxMin()[1] && global[1] < this->bBoxMax()[1]

        )
        {
            y[0] = velocity_;
        }
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return 1e5; }
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
    }
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
        if (bcDissipationIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
                dofMapper(gridView_);
            return 6.0 * kinematicViscosity_
                   / (this->betaOmega() * std::pow(this->lop_.storedDistanceToWall[dofMapper.index(e)], 2));
        }
        return dissipationRate_;
    }

    //! \brief Neumann values
//     DimVector neumannVelocityAtPos(const DimVector& global) const
//     { return DimVector(0.0); }
//     Scalar neumannPressureAtPos(const DimVector& global) const
//     { return 0.0; }
//     Scalar neumannTurbulentKineticEnergyAtPos(const DimVector& global) const
//     { return 0.0; }
//     Scalar neumannDissipationAtPos(const DimVector& global) const
//     { return 0.0; }

    //! \brief Source term values
//     DimVector sourceMomentumBalanceAtPos(const DimVector& global) const
//     { return DimVector(0.0); }
//     Scalar sourceMassBalanceAtPos(const DimVector& global) const
//     { return 0.0; }
//     Scalar sourceTurbulentKineticEnergyBalanceAtPos(const DimVector& global) const
//     { return 0.0; }
//     Scalar sourceDissipationBalanceAtPos(const DimVector& global) const
//     { return 0.0; }

    //! \brief Return the temperature at a given position
    Scalar massMoleFracAtPos(const DimVector& global) const
    { return massMoleFrac_; }

    //! \brief Return the temperature at a given position
    Scalar temperatureAtPos(const DimVector& global) const
    { return temperature_; }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    Scalar velocity_;
    Scalar temperature_;
    Scalar massMoleFrac_;
    Scalar kinematicViscosity_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipationRate_;
    Scalar smallestGridCellY_;
};

} //end namespace


#endif // DUMUX_PROBLEM_PIPEFLOW_DUMUX_HH
