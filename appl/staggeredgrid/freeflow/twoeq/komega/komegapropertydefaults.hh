// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 *
 * \file
 *
 * \brief Defines default properties for the K-Omega staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_KOMEGA_PROPERTY_DEFAULTS_HH
#define DUMUX_KOMEGA_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscositypropertydefaults.hh>

#include"komegaboundaryconditions.hh"
#include"komegaproperties.hh"
#include"komegaindices.hh"

namespace Dumux
{

template<class TypeTag> class BCTurbulentKineticEnergy;
template<class TypeTag> class BCDissipation;
template<class TypeTag, typename GridView, typename Scalar> class DirichletTurbulentKineticEnergy;
template<class TypeTag, typename GridView, typename Scalar> class DirichletDissipation;
template<class TypeTag, typename GridView, typename Scalar> class NeumannTurbulentKineticEnergy;
template<class TypeTag, typename GridView, typename Scalar> class NeumannDissipation;
template<class TypeTag, typename GridView, typename Scalar> class SourceTurbulentKineticEnergyBalance;
template<class TypeTag, typename GridView, typename Scalar> class SourceDissipationBalance;

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridKOmega, Indices,
              StaggeredGridKOmegaCommonIndices<TypeTag>);

//! Set property value for the turbulentKineticEnergy boundary condition
SET_TYPE_PROP(StaggeredGridKOmega, BCTurbulentKineticEnergy, BCTurbulentKineticEnergy<TypeTag>);

//! Set property value for the dissipation boundary condition
SET_TYPE_PROP(StaggeredGridKOmega, BCDissipation, BCDissipation<TypeTag>);

// Do not use constraint values for dissipation on default
SET_BOOL_PROP(StaggeredGridKOmega, FixDissipationConstraints, true);

//! Set property value for BCType
SET_PROP(StaggeredGridKOmega, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, BCTurbulentKineticEnergy) BCTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, BCDissipation) BCDissipation;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure,
                                                         BCTurbulentKineticEnergy, BCDissipation> type;
};

//! Set property value for DirichletTurbulentKineticEnergy
SET_PROP(StaggeredGridKOmega, DirichletTurbulentKineticEnergy)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletTurbulentKineticEnergy<TypeTag, GridView, Scalar> type;
};

//! Set property value for DirichletDissipation
SET_PROP(StaggeredGridKOmega, DirichletDissipation)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletDissipation<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannTurbulentKineticEnergy
SET_PROP(StaggeredGridKOmega, NeumannTurbulentKineticEnergy)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannTurbulentKineticEnergy<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannDissipation
SET_PROP(StaggeredGridKOmega, NeumannDissipation)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannDissipation<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceTurbulentKineticEnergyBalance
SET_PROP(StaggeredGridKOmega, SourceTurbulentKineticEnergyBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceTurbulentKineticEnergyBalance<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceDissipationBalance
SET_PROP(StaggeredGridKOmega, SourceDissipationBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceDissipationBalance<TypeTag, GridView, Scalar> type;
};

//! The axis to calculate wall distances (negative means determine automatically)
SET_INT_PROP(StaggeredGridKOmega, ProblemWallNormalAxis, -1);

//! The main flow axis for calculating the eddy viscosity (negative means determine automatically)
SET_INT_PROP(StaggeredGridKOmega, ProblemFlowNormalAxis, -1);

//! Use Wilcox08 model by default
SET_INT_PROP(StaggeredGridKOmega, KOmegaModel, 1);

//! Use kinematic viscosity for diffusion term of the komega equation
SET_BOOL_PROP(StaggeredGridKOmega, KOmegaEnableKinematicViscosity, true);

//! Factor, which is added to the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(StaggeredGridKOmega, TimeManagerTimeStepIncrementFactor, 0.5);
}
}

#endif // DUMUX_KOMEGA_PROPERTY_DEFAULTS_HH
