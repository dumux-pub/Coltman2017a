// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for K-Omega problems
 */
#ifndef DUMUX_KOMEGA_PROBLEM_HH
#define DUMUX_KOMEGA_PROBLEM_HH

#include <dumux/common/timemanager.hh>
#include <dumux/implicit/problem.hh>

// copied includes from the old *.cc file
#include<cmath>
#include<iostream>
#include<vector>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/io.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/preconditioners.hh>

#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/gridfunctionspace/subspace.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/gridoperator/onestep.hh>
#include<dune/pdelab/instationary/onestep.hh>

#include<appl/staggeredgrid/common/fixpressureconstraints.hh>
#include<appl/staggeredgrid/common/fixvelocityconstraints.hh>
#include<appl/staggeredgrid/common/l2interpolationerror.hh>
#include<appl/staggeredgrid/common/newton.hh>
#include<appl/staggeredgrid/localfunctions/staggeredq0fem.hh>

#include"fixdissipationconstraints.hh"
#include"komegastaggeredgrid.hh"
#include"komegatransientstaggeredgrid.hh"

namespace Dumux
{
/*!
 * \brief Base class for problems
 */
template<class TypeTag>
class KOmegaProblem
{
    // the following properties are also required by start.hh, so they are
    // contained in the coupled TypeTag
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, BCType) BCType;
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
    typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
    typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;
    typedef typename GET_PROP_TYPE(TypeTag, BCTurbulentKineticEnergy) BCTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, DirichletTurbulentKineticEnergy) DirichletTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, NeumannTurbulentKineticEnergy) NeumannTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, SourceTurbulentKineticEnergyBalance) SourceTurbulentKineticEnergyBalance;
    typedef typename GET_PROP_TYPE(TypeTag, BCDissipation) BCDissipation;
    typedef typename GET_PROP_TYPE(TypeTag, DirichletDissipation) DirichletDissipation;
    typedef typename GET_PROP_TYPE(TypeTag, NeumannDissipation) NeumannDissipation;
    typedef typename GET_PROP_TYPE(TypeTag, SourceDissipationBalance) SourceDissipationBalance;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;

    typedef Dune::PDELab::StaggeredQ0LocalFiniteElementMap<Scalar, Scalar, dim> VFEM;
    typedef Dune::PDELab::P0LocalFiniteElementMap<Scalar, Scalar, dim> PFEM;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { velocityIdx = Indices::velocityIdx,
           pressureIdx = Indices::pressureIdx,
           turbulentKineticEnergyIdx = Indices::turbulentKineticEnergyIdx,
           dissipationIdx = Indices::dissipationIdx };

    typedef Dune::PDELab::ISTLVectorBackend<> VectorBackend;
    typedef Dumux::FixPressureConstraints<TypeTag> PressureConstraints;
    typedef Dune::PDELab::GridFunctionSpace<GridView, PFEM, PressureConstraints, VectorBackend> P0GFS_P;
    typedef Dumux::FixVelocityConstraints<TypeTag> VelocityConstraints;
    typedef Dune::PDELab::GridFunctionSpace<GridView, VFEM, VelocityConstraints, VectorBackend> StaggeredQ0GFS;
    typedef Dune::PDELab::GridFunctionSpace<GridView, PFEM, Dune::PDELab::NoConstraints, VectorBackend> P0GFS_K;
    typedef Dumux::FixDissipationConstraints<TypeTag> DissipationConstraints;
    typedef Dune::PDELab::GridFunctionSpace<GridView, PFEM, DissipationConstraints, VectorBackend> P0GFS_W;
    typedef Dune::PDELab::CompositeGridFunctionSpace<VectorBackend,
        Dune::PDELab::LexicographicOrderingTag, StaggeredQ0GFS, P0GFS_P, P0GFS_K, P0GFS_W> MGFS;
    typedef typename MGFS::template ConstraintsContainer<Scalar>::Type ConstraintsContainer;

    typedef Dune::PDELab::CompositeGridFunction<DirichletVelocity, DirichletPressure,
                                                DirichletTurbulentKineticEnergy, DirichletDissipation> DirichletComposed;
    typedef Dune::PDELab::KOmegaStaggeredGrid<TypeTag> LOP;
    typedef Dune::PDELab::KOmegaTransientStaggeredGrid<TypeTag> TransientLocalOperator;
    typedef Dune::PDELab::ISTLMatrixBackend MatrixBackend;
    typedef Dune::PDELab::GridOperator<MGFS,MGFS,LOP,MatrixBackend,
      double,double,double,ConstraintsContainer,ConstraintsContainer> GO0;
    typedef Dune::PDELab::GridOperator<MGFS,MGFS,TransientLocalOperator,MatrixBackend,
      double,double,double,ConstraintsContainer,ConstraintsContainer> GO1;
    typedef Dune::PDELab::OneStepGridOperator<GO0,GO1> IGO;
    typedef typename IGO::Traits::Domain X;

    typedef typename GET_PROP_TYPE(TypeTag, LinearSolver) LinearSolver;
    typedef Dune::PDELab::GridFunctionSubSpace<MGFS,Dune::TypeTree::TreePath<velocityIdx> > VSUB;
    typedef Dune::PDELab::GridFunctionSubSpace<MGFS,Dune::TypeTree::TreePath<pressureIdx> > PSUB;
    typedef Dune::PDELab::GridFunctionSubSpace<MGFS,Dune::TypeTree::TreePath<turbulentKineticEnergyIdx> > CSUB;
    typedef Dune::PDELab::GridFunctionSubSpace<MGFS,Dune::TypeTree::TreePath<dissipationIdx> > TSUB;
    typedef Dune::PDELab::DiscreteGridFunction<VSUB, X> StaggeredQ0DGF;
    typedef Dune::PDELab::DiscreteGridFunction<PSUB, X> P0DGF_P;
    typedef Dune::PDELab::DiscreteGridFunction<CSUB, X> P0DGF_K;
    typedef Dune::PDELab::DiscreteGridFunction<TSUB, X> P0DGF_W;

    // copying a problem is not a good idea
    KOmegaProblem(const KOmegaProblem &);

public:
    /*!
     * \brief Constructor
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    KOmegaProblem(TimeManager &timeManager, const GridView &gridView)
    : timeManager_(&timeManager), gridView_(gridView),
      bBoxMin_(getBBoxMin()),
      bBoxMax_(getBBoxMax()),
      timeVector_(0.0),
      // Dirichlet values
      dirichletVelocity_(gridView_, asImp_()), dirichletPressure_(gridView_, asImp_()),
      dirichletTurbulentKineticEnergy_(gridView_, asImp_()), dirichletDissipation_(gridView_, asImp_()),
      dirichletComposed_(dirichletVelocity_, dirichletPressure_,
                         dirichletTurbulentKineticEnergy_, dirichletDissipation_),
      // Neumann and Source Values
      neumannVelocity_(gridView_, asImp_()), neumannPressure_(gridView_, asImp_()),
      neumannTurbulentKineticEnergy_(gridView_, asImp_()), neumannDissipation_(gridView_, asImp_()),
      sourceMomentumBalance_(gridView_, asImp_()), sourceMassBalance_(gridView_, asImp_()),
      sourceTurbulentKineticEnergyBalance_(gridView_, asImp_()), sourceDissipationBalance_(gridView_, asImp_()),
      // bc, maps and function spaces
      pfem_(Dune::GeometryType(Dune::GeometryType::cube, dim)),
      bcVelocity_(asImp_()),
      bcPressure_(asImp_()),
      bcTurbulentKineticEnergy_(asImp_()),
      bcDissipation_(asImp_()),
      bc_(bcVelocity_, bcPressure_, bcTurbulentKineticEnergy_, bcDissipation_),
      // constraints
      pressureConstraints_(gridView_, asImp_()),
      p0gfs_p_(gridView_, pfem_, pressureConstraints_),
      staggeredQ0Gfs_(gridView_, vfem_, velocityConstraints_),
      p0gfs_k_(gridView_, pfem_),
      dissipationConstraints_(gridView_, asImp_()),
      p0gfs_w_(gridView_, pfem_, dissipationConstraints_),
      mgfs_(staggeredQ0Gfs_, p0gfs_p_, p0gfs_k_, p0gfs_w_),
      vsub_(mgfs_), psub_(mgfs_),
      ksub_(mgfs_), esub_(mgfs_),
      // solution vectors
      xOld_(mgfs_, 0.0), xNew_(mgfs_, 0.0),
      // local and grid operators
      lop_(bc_, sourceMomentumBalance_, sourceMassBalance_,
           sourceTurbulentKineticEnergyBalance_, sourceDissipationBalance_,
           dirichletVelocity_, dirichletPressure_,
           dirichletTurbulentKineticEnergy_, dirichletDissipation_,
           neumannVelocity_, neumannPressure_,
           neumannTurbulentKineticEnergy_, neumannDissipation_,
           gridView_, asImp_()),
      transientLocalOperator_(gridView_, asImp_()),
      go0_(mgfs_, constraintsContainer_, mgfs_, constraintsContainer_, lop_),
      go1_(mgfs_, constraintsContainer_, mgfs_, constraintsContainer_, transientLocalOperator_),
      igo_(go0_, go1_),
      // solvers
      ls_(false), newton_(igo_, ls_), osm_(method_, igo_, newton_)
    {
        // the constructor should not be used as this leads to problems with the
        // initialization of the dirichlet values (inital conditions) from input files
    }

    /*!
     * \brief Function to get the bBoxMin, is needed to initalize the values
     *        before the local operator is called for the first time.
     */
    DimVector getBBoxMin()
    {
        std::fill(bBoxMin_.begin(), bBoxMin_.end(), std::numeric_limits<Scalar>::max());
        // calculate the bounding box of the local partition of the grid view
        VertexIterator vIt = gridView_.template begin<dim>();
        const VertexIterator vEndIt = gridView_.template end<dim>();
        for (; vIt!=vEndIt; ++vIt) {
            for (int i=0; i<dim; i++) {
                bBoxMin_[i] = std::min(bBoxMin_[i], vIt->geometry().corner(0)[i]);
            }
        }

        // communicate to get the bounding box of the whole domain
        if (gridView_.comm().size() > 1) {
            for (int i = 0; i < dim; ++i) {
                bBoxMin_[i] = gridView_.comm().min(bBoxMin_[i]);
            }
        }
        return bBoxMin_;
    }

    /*!
     * \brief Function to get the bBoxMin, is needed to initalize the values
     *        before the local operator is called for the first time.
     */
    DimVector getBBoxMax()
    {
        std::fill(bBoxMax_.begin(), bBoxMax_.end(), -std::numeric_limits<Scalar>::max());
        // calculate the bounding box of the local partition of the grid view
        VertexIterator vIt = gridView_.template begin<dim>();
        const VertexIterator vEndIt = gridView_.template end<dim>();
        for (; vIt!=vEndIt; ++vIt) {
            for (int i=0; i<dim; i++) {
                bBoxMax_[i] = std::max(bBoxMax_[i], vIt->geometry().corner(0)[i]);
            }
        }

        // communicate to get the bounding box of the whole domain
        if (gridView_.comm().size() > 1) {
            for (int i = 0; i < dim; ++i) {
                bBoxMax_[i] = gridView_.comm().max(bBoxMax_[i]);
            }
        }
        return bBoxMax_;
    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem and the sub-problems.
     *
     * If you overload this method don't forget to call
     * ParentType::init()
     */
    void init()
    {
        // setting some basic properties
        numVtkFiles_ = 0;
        outputVtkFrequency_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, VtkFrequency);

        // names
        staggeredQ0Gfs_.name("velocity");
        p0gfs_p_.name("pressure");
        p0gfs_k_.name("turbulentKineticEnergy");
        p0gfs_w_.name("dissipation");

        // fill constraints container
        constraintsContainer_.clear();
        Dune::PDELab::constraints(bc_, mgfs_, constraintsContainer_);
        // do interpolation for BOTH old and new solution
        Dune::PDELab::interpolate(dirichletComposed_, mgfs_, xOld_);
        Dune::PDELab::interpolate(dirichletComposed_, mgfs_, xNew_);

        std::cout << "interpolation_ff: " << xOld_.N()
                  << " dof total, " << constraintsContainer_.size()
                  << " dof constrained" << std::endl;

//         for (unsigned int i = 0; i < xOld_.base().size(); ++i)
//         {
//           std::cout << "xOld_[" << i << "]: " << xOld_.base()[i] << std::endl;
//         }

        // make grid function operator
        igo_.divideMassTermByDeltaT();

        // newton properties
        newton_.setVerbosityLevel(2);
        newton_.setMaxIterations(GET_PARAM_FROM_GROUP(TypeTag, int, Newton, MaxSteps));
        newton_.setReduction(GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, ResidualReduction));
        newton_.setAbsoluteLimit(GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, AbsoluteLimit));
        newton_.setLineSearchStrategy(newton_.noLineSearch);
        newton_.setMaxRelShift(GET_PARAM_FROM_GROUP(TypeTag, Scalar, Newton, MaxRelativeShift));
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, UpdateStoredVariables))
        {
          auto updateStoredValues = [&](X solutionVector)
          {
              lop_.updateStoredValues(mgfs_, solutionVector);
          };
          newton_.setUpdateStoredVariablesFunction(updateStoredValues);
        }
        osm_.setVerbosityLevel(0);
        targetSteps_ = GET_PARAM_FROM_GROUP(TypeTag, int, Newton, TargetSteps);

        // starting the problem
        std::cout << "Initial time step size = "
                  << GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitial)
                  << std::endl;
        lop_.updateStoredValues(mgfs_, xNew_);

        writerStokes = std::make_shared<Dumux::VtkMultiWriter<GridView>>(gridView_, asImp_().name() + std::string("-ffSecondary"));
    }

    /*!
     * \brief This method writes the complete state of the simulation
     *        to the harddisk.
     *
     * The file will start with the prefix returned by the name()
     * method, has the current time of the simulation clock in it's
     * name and uses the extension <tt>.drs</tt>. (Dumux ReStart
     * file.)  See Dumux::Restart for details.
     */
    void serialize()
    {}

    /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()
    {
        // this is necessary for correctly applying the wall functions depending on a y^+ value
        constraintsContainer_.clear();
        Dune::PDELab::constraints(bc_, mgfs_, constraintsContainer_);

        std::cout << "interpolation: " << xOld_.N()
                  << " dof total, " << constraintsContainer_.size()
                  << " dof constrained" << std::endl;
    }

    /*!
     * \brief Called by Dumux::TimeManager in order to do a time
     *        integration on the model.
     */
    void timeIntegration()
    {
        const int maxFails = GET_PARAM_FROM_GROUP(TypeTag, int, Newton, MaxTimeStepDivisions);
        int i = 0;
        for (; i < maxFails; ++i)
        {
            try
            {
                osm_.apply(timeManager().time(), timeManager().timeStepSize(),
                           xOld_, dirichletComposed_, xNew_);
                return;
            }
            catch (Dune::PDELab::NewtonNotConverged &e) {
                // update failed
                timeManager().setTimeStepSize(timeManager().timeStepSize() / 2);

                std::cout << "Newton solver did not converge after "
                          << newton_.result().iterations << " steps. Retrying with time step of "
                          << timeManager().timeStepSize() << "sec\n";
            }
        }

        DUNE_THROW(Dune::MathError,
                  "Newton solver didn't converge after "
                  << maxFails
                  << " timestep divisions. dt="
                  << timeManager().timeStepSize());
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        xOld_ = xNew_;
        lop_.updateStoredValues(mgfs_, xNew_);
    }

    /*!
     * \brief Called by Dumux::TimeManager whenever a solution for a
     *        timestep has been computed and the simulation time has
     *        been updated.
     */
    Scalar nextTimeStepSize(const Scalar dt)
    {
        return suggestTimeStepSize(dt);
    }

    /*!
     * \brief Returns true if the current solution should be written to
     *        disk (i.e. as a VTK file)
     */
    bool shouldWriteOutput() const
    { return true /* advanced handling can be found in writeOutput() */; }

    /*!
     * \brief Returns true if the current state of the simulation
     * should be written to disk
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    {
        std::cerr << "The end of an episode is reached, but the problem "
                  << "does not override the episodeEnd() method. "
                  << "Doing nothing!\n";
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     * It could be either overwritten by the problem files, or simply
     * declared over the setName() function in the application file.
     */
    const char *name() const
    {
        return "komega";
    }

    /*!
     * \brief Called by the time manager after everything which can be
     *        done about the current time step is finished and the
     *        model should be prepared to do the next time integration.
     */
    void advanceTimeLevel()
    {}

    /*!
     * \brief Write the relevant quantities of the current solution into
     * an VTK output file.
     */
    void writeOutput()
    {
        if (outputVtkFrequency_
            && (this->timeManager().episodeWillBeFinished() || this->timeManager().willBeFinished()
                || this->timeManager().timeStepIndex() % outputVtkFrequency_ == 0))
        {
            std::cout << "Writing vtk result file for \"" << asImp_().name() << "\"\n";

            char fname[255];
            // plot result as VTK
            StaggeredQ0DGF staggeredQ0DGF(vsub_, xNew_);
            P0DGF_P p0dgf_p(psub_, xNew_);
            P0DGF_K p0dgf_k(ksub_, xNew_);
            P0DGF_W p0dgf_w(esub_, xNew_);
            Dune::SubsamplingVTKWriter<GridView> vtkwriter(gridView_, 1);
            vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<P0DGF_P> >(p0dgf_p, "pressure"));
            vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<StaggeredQ0DGF> >(staggeredQ0DGF, "velocity"));
            vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<P0DGF_K> >(p0dgf_k, "turbulentKineticEnergy"));
            vtkwriter.addCellData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<P0DGF_W> >(p0dgf_w, "dissipation"));
            sprintf(fname, "%s-%04d", asImp_().name(), numVtkFiles_);
            vtkwriter.write(fname, Dune::VTK::ascii);
            timeVector_.push_back(timeManager().time()+timeManager().timeStepSize());
            ++numVtkFiles_;

            // write pvd file
            sprintf(fname, "%s.pvd", asImp_().name());
            std::string pvdname = fname;
            std::ofstream pvd(pvdname.c_str());
            assert(pvd.is_open());
            pvd << std::fixed;
            pvd << "<?xml version=\"1.0\"?>\n"
                << "<VTKFile type=\"Collection\" version=\"0.1\">\n"
                << "<Collection>\n";
            for (unsigned int i = 0; i < numVtkFiles_; ++i)
                {
                    sprintf(fname, "%s-%04d.vtu", asImp_().name(), i);
                    pvd << "  <DataSet"
                        << " index=\"" << i << "\""
                        << " timestep=\"" << timeVector_[i] << "\""
                        << " file=\"" << fname << "\"/>\n";
                }
            pvd << "</Collection>\n"
                << "</VTKFile>\n";
            pvd.close();

            // Write output for secondary variables of the Stokes domain
            using ScalarField = Dune::BlockVector<Dune::FieldVector<Scalar, 1> >;
            using VectorField = Dune::BlockVector<Dune::FieldVector<Scalar, dim> >;
            unsigned numDofs = gridView_.size(0);

            writerStokes->beginWrite(timeManager().time()+timeManager().timeStepSize());

            // create the required scalar fields
            VectorField *velocity = writerStokes->template allocateManagedBuffer<Scalar, dim>(numDofs);
            VectorField *relVelocity = writerStokes->template allocateManagedBuffer<Scalar, dim>(numDofs);
            VectorField *velocityGradients0 = writerStokes->template allocateManagedBuffer<Scalar, dim>(numDofs);
            VectorField *velocityGradients1 = writerStokes->template allocateManagedBuffer<Scalar, dim>(numDofs);
            VectorField *velocityGradients2 = writerStokes->template allocateManagedBuffer<Scalar, dim>(numDofs);
            ScalarField *pressure = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *relPressure = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *massMoleFrac = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *temperature = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *density = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *viscosity = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *eddyViscosity = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *distanceToWall = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *uPlus = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *yPlus = writerStokes->allocateManagedBuffer(numDofs);
            ScalarField *MaxVelocity0 = writerStokes->allocateManagedBuffer(numDofs);

            // use the
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
                dofMapper(gridView_);
            for (const auto& element : Dune::elements(gridView_))
            {
                int dofIdx = dofMapper.index(element);
                int wallElementIdx = lop_.storedCorrespondingWallElementID[dofIdx];

                (*velocity)[dofIdx] = lop_.storedVelocitiesAtElementCenter[dofIdx];
                for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
                    (*relVelocity)[dofIdx][dimIdx] = lop_.storedVelocitiesAtElementCenter[dofIdx][dimIdx]
                                                     / lop_.storedVelocityMaximum[wallElementIdx][dimIdx];
                (*velocityGradients0)[dofIdx] = lop_.storedVelocityGradientTensor[dofIdx][0];
                if (dim > 1)
                    (*velocityGradients1)[dofIdx] = lop_.storedVelocityGradientTensor[dofIdx][1];
                if (dim > 2)
                    (*velocityGradients2)[dofIdx] = lop_.storedVelocityGradientTensor[dofIdx][2];
                (*pressure)[dofIdx] = lop_.storedPressure[dofIdx];
                (*relPressure)[dofIdx] = lop_.storedPressure[dofIdx] - 1e5;
                (*massMoleFrac)[dofIdx] = lop_.storedMassMoleFrac[dofIdx];
                (*temperature)[dofIdx] = lop_.storedTemperature[dofIdx];
                (*viscosity)[dofIdx] = lop_.storedKinematicViscosity[dofIdx];
                (*density)[dofIdx] = lop_.storedDensity[dofIdx];
                (*eddyViscosity)[dofIdx] = lop_.storedKinematicEddyViscosity[dofIdx];
                (*distanceToWall)[dofIdx] = lop_.storedDistanceToWall[dofIdx];
                (*uPlus)[dofIdx] = lop_.storedVelocityInWallCoordinates[dofIdx];
                (*yPlus)[dofIdx] = lop_.storedDistanceInWallCoordinates[dofIdx];
                (*MaxVelocity0)[dofIdx][0] = lop_.storedVelocityMaximum[wallElementIdx][0];
            } // loop over elements

            writerStokes->attachDofData(*velocity, "velocityAtElementCenters", false, dim);
            writerStokes->attachDofData(*relVelocity, "relVelocity", false, dim);
            writerStokes->attachDofData(*velocityGradients0, "velocityGradients0", false, dim);
            if (dim > 1)
                writerStokes->attachDofData(*velocityGradients1, "velocityGradients1", false, dim);
            if (dim > 2)
                writerStokes->attachDofData(*velocityGradients2, "velocityGradients2", false, dim);
            writerStokes->attachDofData(*pressure, "pressure", false);
            writerStokes->attachDofData(*relPressure, "pn_rel", false);
            writerStokes->attachDofData(*massMoleFrac, "massMoleFrac", false);
            writerStokes->attachDofData(*temperature, "temperature", false);
            writerStokes->attachDofData(*viscosity, "viscosity", false);
            writerStokes->attachDofData(*density, "density", false);
            writerStokes->attachDofData(*eddyViscosity, "eddyViscosity", false);
            writerStokes->attachDofData(*distanceToWall, "distanceToWall", false);
            writerStokes->attachDofData(*uPlus, "u^+", false);
            writerStokes->attachDofData(*yPlus, "y^+", false);
            writerStokes->attachDofData(*MaxVelocity0, "MaximumVelocity_X", false);
            writerStokes->endWrite();
        }

        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, ErrorConvergence))
        {
            // compute L2 error if analytical solution is available
            std::cout.precision(8);
            std::cout << "L2 error for "
                      << std::setw(6) << gridView_.size(0)
                      << " elements. pressure: "
                      << std::scientific
                      << l2interpolationerror(dirichletPressure_, psub_, xNew_, 8)
                      << " velocity: "
                      << std::scientific
                      << l2interpolationerror(dirichletVelocity_, vsub_, xNew_, 8)
                      << " turbulentKineticEnergy: "
                      << std::scientific
                      << l2interpolationerror(dirichletTurbulentKineticEnergy_, ksub_, xNew_, 8)
                      << " dissipation: "
                      << std::scientific
                      << l2interpolationerror(dirichletDissipation_, esub_, xNew_, 8)
                      << std::endl;
        }
    }

    /*!
     * \brief Load a previously saved state of the whole simulation
     *        from disk.
     *
     * \param tRestart The simulation time on which the program was
     *                 written to disk.
     */
    void restart(const Scalar tRestart)
    {}

    /*!
     * \brief Suggest a new time-step size based on the old time-step
     *        size.
     *
     * The default behavior is to suggest the old time-step size
     * scaled by the ratio between the target iterations and the
     * iterations required to actually solve the last time-step.
     */
    Scalar suggestTimeStepSize(Scalar oldTimeStep) const
    {
        // Be aggressive reducing the time-step size but  conservative when increasing it.
        // The rationale is that we want to avoid failing in the next Newton
        // iteration which would require another linearization of the problem.
        unsigned int numNewtonIterations = newton_.result().iterations;
        if (numNewtonIterations > targetSteps_) {
            Scalar percent = Scalar(numNewtonIterations - targetSteps_)/targetSteps_;
            return oldTimeStep/(1.0 + percent);
        }

        Scalar percent = Scalar(targetSteps_ - numNewtonIterations)/targetSteps_;
        return oldTimeStep*(1.0 + percent * GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, TimeStepIncrementFactor));
    }

    /*!
     * \brief Returns the user specified maximum time step size
     *
     * Overload in problem for custom needs.
     */
    Scalar maxTimeStepSize() const
    {
        return std::numeric_limits<Scalar>::max();
    }


    //! \brief Return whether there is a Wall condition for velocity (default: false)
    bool bcVelocityIsWall(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is an Inflow condition for velocity (default: false)
    bool bcVelocityIsInflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Outflow condition for velocity (default: false)
    bool bcVelocityIsOutflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Symmetry condition for velocity (default: false)
    bool bcVelocityIsSymmetry(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Coupling condition for velocity (default: false)
    bool bcVelocityIsCoupling(const DimVector& global) const
    {
        return false;
    }


    //! \brief Return whether there is Dirichlet condition for pressure (default: false)
    bool bcPressureIsDirichlet(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Outflow condition for pressure (default: false)
    bool bcPressureIsOutflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Coupling condition for pressure (default: false)
    bool bcPressureIsCoupling(const DimVector& global) const
    {
        return false;
    }


    //! \brief Return whether there is a Wall condition for turbulentKineticEnergy (default: false)
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is an Inflow condition for turbulentKineticEnergy (default: false)
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Outflow condition for turbulentKineticEnergy (default: false)
    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Symmetry condition for turbulentKineticEnergy (default: false)
    bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
    {
        return false;
    }


    //! \brief Return whether there is a Wall condition for dissipation (default: false)
    bool bcDissipationIsWall(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is an Inflow condition for dissipation (default: false)
    bool bcDissipationIsInflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Outflow condition for dissipation (default: false)
    bool bcDissipationIsOutflow(const DimVector& global) const
    {
        return false;
    }

    //! \brief Return whether there is Symmetry condition for dissipation (default: false)
    bool bcDissipationIsSymmetry(const DimVector& global) const
    {
        return false;
    }


    //! \brief Return the velocity Dirichlet value at a given position
    DimVector dirichletVelocityAtPos(const DimVector& global) const
    {
        return DimVector(0.0);
    }

    //! \brief Return the pressure Dirichlet value at a given position
    Scalar dirichletPressureAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the turbulentKineticEnergy Dirichlet value at a given position
    Scalar dirichletTurbulentKineticEnergyAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the dissipation Dirichlet value at a given position
    Scalar dirichletDissipationAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the velocity Neumann value at a given position
    DimVector neumannVelocityAtPos(const DimVector& global) const
    {
        return DimVector(0.0);
    }

    //! \brief Return the pressure Neumann value at a given position
    Scalar neumannPressureAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the turbulentKineticEnergy Neumann value at a given position
    Scalar neumannTurbulentKineticEnergyAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the dissipation Neumann value at a given position
    Scalar neumannDissipationAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the momentum Source value at a given position
    DimVector sourceMomentumBalanceAtPos(const DimVector& global) const
    {
        return DimVector(0.0);
    }

    //! \brief Return the mass Source value at a given position
    Scalar sourceMassBalanceAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the component Source value at a given position
    Scalar sourceTurbulentKineticEnergyBalanceAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the energy Source value at a given position
    Scalar sourceDissipationBalanceAtPos(const DimVector& global) const
    {
        return 0.0;
    }

    //! \brief Return the temperature at a given position
    Scalar temperatureAtPos(const DimVector& global) const
    {
        DUNE_THROW(Dune::NotImplemented, "temperatureAtPos() method not implemented by the actual problem");
    }

    //! \brief Return the mass or mole fraction at a given position
    Scalar massMoleFracAtPos(const DimVector& global) const
    {
        DUNE_THROW(Dune::NotImplemented, "massMoleFracAtPos() method not implemented by the actual problem");
    }

    /*!
     * \brief The coordinate of the corner of the GridView's bounding
     *        box with the smallest values.
     */
    const DimVector &bBoxMin() const
    { return bBoxMin_; }

    /*!
     * \brief The coordinate of the corner of the GridView's bounding
     *        box with the largest values.
     */
    const DimVector &bBoxMax() const
    { return bBoxMax_; }

    bool onLeftBoundary_(const DimVector &globalPos) const
    { return globalPos[0] < bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const DimVector &globalPos) const
    { return globalPos[0] > bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const DimVector &globalPos) const
    { return globalPos[1] < bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const DimVector &globalPos) const
    { return globalPos[1] > bBoxMax()[1] - eps_; }

    /*!
     * \brief Returns TimeManager object used by the simulation
     */
    TimeManager &timeManager()
    { return *timeManager_; }

    //! \copydoc Dumux::NavierStokesProblem::timeManager()
    const TimeManager &timeManager() const
    { return *timeManager_; }

    //! \brief Returns the DirichletTurbulentKineticEnergy object used by the simulation
    const DirichletTurbulentKineticEnergy dirichletTurbulentKineticEnergy() const
    {return dirichletTurbulentKineticEnergy_; }

    //! \brief Returns the DirichletDissipation object used by the simulation
    const DirichletDissipation dirichletDissipation() const
    {return dirichletDissipation_; }

    //! \brief Returns the \$f C_\mu \$f constant
    const Scalar cMu() const
    {
      return lop_.cMu();
    }

    //! \brief Returns the \$f \beta_\omega \$f constant
    const Scalar betaOmega() const
    {
      return lop_.betaOmega();
    }

    /*!
    * \brief Pass turbulent variables from the StokesLocalOperator
    */
    void passAdditionalVariables(std::function<Scalar (unsigned int)> wallDistance,
                                 std::function<Scalar (unsigned int)> elementHasWall)
    {
        wallDistance_ = wallDistance;
        elementHasWall_ = elementHasWall;
    }

protected:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

    static constexpr Scalar eps_ = 1e-6;

    TimeManager *timeManager_;
    Scalar targetSteps_;
    const GridView gridView_;
    DimVector bBoxMin_;
    DimVector bBoxMax_;

    std::vector<double> timeVector_;
    unsigned int numVtkFiles_;
    unsigned int outputVtkFrequency_;

    // Dirichlet values
    DirichletVelocity dirichletVelocity_;
    DirichletPressure dirichletPressure_;
    DirichletTurbulentKineticEnergy dirichletTurbulentKineticEnergy_;
    DirichletDissipation dirichletDissipation_;
    DirichletComposed dirichletComposed_;
    // Neumann and Source Values
    NeumannVelocity neumannVelocity_;
    NeumannPressure neumannPressure_;
    NeumannTurbulentKineticEnergy neumannTurbulentKineticEnergy_;
    NeumannDissipation neumannDissipation_;
    SourceMomentumBalance sourceMomentumBalance_;
    SourceMassBalance sourceMassBalance_;
    SourceTurbulentKineticEnergyBalance sourceTurbulentKineticEnergyBalance_;
    SourceDissipationBalance sourceDissipationBalance_;

    // bc, maps and function spaces
    VFEM vfem_;
    PFEM pfem_;
    BCVelocity bcVelocity_;
    BCPressure bcPressure_;
    BCTurbulentKineticEnergy bcTurbulentKineticEnergy_;
    BCDissipation bcDissipation_;
    BCType bc_;
    VelocityConstraints velocityConstraints_;
    PressureConstraints pressureConstraints_;
    P0GFS_P p0gfs_p_;
    StaggeredQ0GFS staggeredQ0Gfs_;
    P0GFS_K p0gfs_k_;
    DissipationConstraints dissipationConstraints_;
    P0GFS_W p0gfs_w_;

    ConstraintsContainer constraintsContainer_;
    MGFS mgfs_;
    VSUB vsub_;
    PSUB psub_;
    CSUB ksub_;
    TSUB esub_;

    // solution vectors
    X xOld_;
    X xNew_;

    // local and grid operators
    LOP lop_;
    TransientLocalOperator transientLocalOperator_;
    GO0 go0_;
    GO1 go1_;
    IGO igo_;

    // solvers
    Dune::PDELab::ImplicitEulerParameter<double> method_;          // defines coefficients
    LinearSolver ls_;
    Dune::PDELab::StaggeredGridNewton<IGO, LinearSolver, X> newton_;
    Dune::PDELab::OneStepMethod<double, IGO, Dune::PDELab::StaggeredGridNewton<IGO, LinearSolver, X>, X, X> osm_;

    std::shared_ptr<Dumux::VtkMultiWriter<GridView>> writerStokes;

    std::function<Scalar (unsigned int)> wallDistance_;
    std::function<Scalar (unsigned int)> elementHasWall_;
};
} // end namespace Dumux

#endif
