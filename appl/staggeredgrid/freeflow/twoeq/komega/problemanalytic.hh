// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A analytic 1-D test problem for a mixing layer flow using the komega turbulence model
 * values for analytic test problem.
 *
 * The 1-D analytic solution is given by
 * \f[ v_\text{x} = 2 \cdot x^3 \f]
 * \f[ p = 2 - 2 \cdot x \f]
 * \f[ k = 1 + x^2 \f]
 * \f[ \omega = 2 - x^2\f]
 *
 * The 2-D analytic solution is given by
 * \f[ v_\text{x} = 2 \cdot x^3 \f]
 * \f[ v_\text{y} = 2 - 2 \cdot y^3 \f]
 * \f[ p = 2 - 2 \cdot x y \f]
 * \f[ k = 1 + x^2 y^2 \f]
 * \f[ \omega = 2 - x^2 y^2 \f]
 */

#ifndef DUMUX_PROBLEM_ANALYTIC_HH
#define DUMUX_PROBLEM_ANALYTIC_HH

// #define DisableKAdvection 1
// #define DisableWAdvection 1
// #define DisableVEddyViscosity 1
// #define DisableKDiffusionTurb 1
// #define DisableWDiffusionTurb 1
// #define DisableKDiffusionMol 1
// #define DisableWDiffusionMol 1
// #define DisableKProduction 1
// #define DisableWProduction 1
// #define DisableKDestruction 1
// #define DisableWDestruction 1
// #define DisableWCrossDiffusion 1

#include "komegaproblem.hh"
#include "komegapropertydefaults.hh"

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/gasphase.hh>

#include <dune/grid/io/file/dgfparser.hh>

namespace Dumux
{
template <class TypeTag>
class AnalyticProblem;

namespace Properties
{
NEW_TYPE_TAG(AnalyticProblem, INHERITS_FROM(StaggeredGridKOmega));

// Set the problem property
SET_TYPE_PROP(AnalyticProblem, Problem,
              Dumux::AnalyticProblem<TypeTag>);

// Set the used local operator
SET_TYPE_PROP(AnalyticProblem, LocalOperator,
              Dune::PDELab::KOmegaStaggeredGrid<TypeTag>);

// Set the used transient local operator
SET_TYPE_PROP(AnalyticProblem, TransientLocalOperator,
              Dune::PDELab::KOmegaTransientStaggeredGrid<TypeTag>);

// Use constrained pressure
SET_BOOL_PROP(AnalyticProblem, FixPressureConstraints, true);

// Use constrained dissipation
SET_BOOL_PROP(AnalyticProblem, FixDissipationConstraints, true);

// Disable gravity field
SET_BOOL_PROP(AnalyticProblem, ProblemEnableGravity, false);

// Enable symmetrized velocity gradient
SET_BOOL_PROP(AnalyticProblem, ProblemEnableUnsymmetrizedVelocityGradient, false);

// Enable error convergence output
SET_BOOL_PROP(AnalyticProblem, OutputErrorConvergence, true);

// Recalculate the eddy viscosity in each Newton step
//SET_BOOL_PROP(AnalyticProblem, KOmegaUseStoredEddyViscosity, false);

// Set property value for the grid
#ifndef DIMENSION
SET_TYPE_PROP(AnalyticProblem, Grid, Dune::YaspGrid<1>);
#else
SET_TYPE_PROP(AnalyticProblem, Grid, Dune::YaspGrid<2>);
#endif

// Use constant fluid properties
SET_TYPE_PROP(AnalyticProblem, Fluid,
              FluidSystems::GasPhase<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     Constant<TypeTag, typename GET_PROP_TYPE(TypeTag, Scalar)> >);
}

template <class TypeTag>
class AnalyticProblem : public KOmegaProblem<TypeTag>
{
    typedef KOmegaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
    typedef typename Dune::FieldVector<DimVector, dim> DimMatrix;

public:
    AnalyticProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        FluidSystem::init();
        density_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, GasDensity);
        kinematicViscosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, GasKinematicViscosity);
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return true; }
//     bool bcVelocityIsInflow(const DimVector& global) const
//     { return true; }
//     bool bcVelocityIsOutflow(const DimVector& global) const
//     { return this->onRightBoundary_(global); }
//     bool bcVelocityIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return true; }
//     bool bcPressureIsOutflow(const DimVector& global) const
//     { return !bcPressureIsDirichlet(global); }

    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return true; }
//     bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
//     { return true; }
//     bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
//     { return !bcVelocityIsInflow(global); }
//     bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return true; }
//     bool bcDissipationIsInflow(const DimVector& global) const
//     { return true; }
//     bool bcDissipationIsOutflow(const DimVector& global) const
//     { return !bcVelocityIsInflow(global); }
//     bool bcDissipationIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Dirichlet values
    // analytic velocity
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    { return velocity(global); }
    DimVector velocity(const DimVector& global) const
    {
        DimVector y(0.0);
        y[0] = 2.0 * global[0] * global[0] * global[0];
        if (dim > 1)
        {
          y[1] = 2.0 - 2.0 * global[1] * global[1] * global[1];
        }
        return y;
    }

    // analytic pressure
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return pressure(global); }
    Scalar pressure(const DimVector& global) const
    {
      if (dim > 1)
        return 2.0 - 2.0 * global[0] * global[1];
      else
        return 2.0 - 2.0 * global[0];
    }

    // analytic turbulentKineticEnergy
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    { return turbulentKineticEnergy(global); }
    Scalar turbulentKineticEnergy(const DimVector& global) const
    {
      if (dim > 1)
        return 1.0 + global[0] * global[0] * global[1] * global[1];
      else
        return 1.0 + global[0] * global[0];
    }

    // analytic dissipation
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    { return dissipation(global); }
    Scalar dissipation(const DimVector& global) const
    {
      if (dim > 1)
        return 2.0 - global[0] * global[0] * global[1] * global[1];
      else
        return 2.0 - global[0] * global[0];
    }

    //! \brief The gradients
    const DimMatrix dvdx(const DimVector& global) const
    {
        DimMatrix dvdx;
        dvdx[0][0] = 6.0 * global[0] * global[0];
        if (dim > 1)
        {
            dvdx[0][1] = 0.0;
            dvdx[1][0] = 0.0;
            dvdx[1][1] = -6.0 * global[1] * global[1];
        }
        return dvdx;
    }
    const DimMatrix dv2dx(const DimVector& global) const
    {
        DimMatrix dv2dx;
        // (using product rule) -> nothing to do here
        for (unsigned int velIdx = 0; velIdx < dim; ++velIdx)
        {
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                dv2dx[velIdx][dimIdx] = dvdx(global)[velIdx][dimIdx] * velocity(global)[dimIdx]
                                        + dvdx(global)[dimIdx][dimIdx] * velocity(global)[velIdx];
            }
        }
        return dv2dx;
    }
    const DimMatrix dvdx2(const DimVector& global) const
    {
        DimMatrix dvdx2;
        dvdx2[0][0] = 12.0 * global[0];
        if (dim > 1)
        {
            dvdx2[0][1] = 0.0;
            dvdx2[1][0] = 0.0;
            dvdx2[1][1] = -12.0 * global[1];
        }
        return dvdx2;
    }
    DimVector dpdx(const DimVector& global) const
    {
        DimVector dpdx(0.0);
        dpdx[0] = -2.0;
        if (dim > 1)
        {
          dpdx[0] = -2.0 * global[1];
          dpdx[1] = -2.0 * global[0];
        }
        return dpdx;
    }
    DimVector dkdx(const DimVector& global) const
    {
        DimVector dkdx(0.0);
        dkdx[0] = 2.0 * global[0];
        if (dim > 1)
        {
          dkdx[0] = 2.0 * global[0] * global[1] * global[1];
          dkdx[1] = 2.0 * global[1] * global[0] * global[0];
        }
        return dkdx;
    }
    DimVector dkdx2(const DimVector& global) const
    {
        DimVector dkdx2(0.0);
        dkdx2[0] = 2.0;
        if (dim > 1)
        {
          dkdx2[0] = 2.0 * global[1] * global[1];
          dkdx2[1] = 2.0 * global[0] * global[0];
        }
        return dkdx2;
    }
    DimVector dwdx(const DimVector& global) const
    {
        DimVector dwdx(0.0);
        dwdx[0] = -2.0 * global[0];
        if (dim > 1)
        {
          dwdx[0] = -2.0 * global[0] * global[1] * global[1];
          dwdx[1] = -2.0 * global[0] * global[0] * global[1];
        }
        return dwdx;
    }
    DimVector dwdx2(const DimVector& global) const
    {
        DimVector dwdx2(0.0);
        dwdx2[0] = -2.0;
        if (dim > 1)
        {
          dwdx2[0] = -2.0 * global[1] * global[1];
          dwdx2[1] = -2.0 * global[0] * global[0];
        }
        return dwdx2;
    }

    // eddy viscosity and its gradient
    Scalar nut(const DimVector& global) const
    { return turbulentKineticEnergy(global) / dissipation(global); }
    DimVector dnutdx(const DimVector& global) const
    {
        DimVector x(global);
        DimVector y(0.0);
        y[0] = 6*x[0]
               /( (2-(x[0]*x[0]))
                 *(2-(x[0]*x[0]))
                );
        if (dim > 1)
        {
          y[0] = 6*x[0]*x[1]*x[1]
                 / ( (2-(x[0]*x[0]*x[1]*x[1]))
                    *(2-(x[0]*x[0]*x[1]*x[1]))
                   );
          y[1] = 6*x[0]*x[0]*x[1]
                 / ( (2-(x[0]*x[0]*x[1]*x[1]))
                    *(2-(x[0]*x[0]*x[1]*x[1]))
                   );
        }
        return y;
    }

    // shearStressTensorProduct
    Scalar shearStressTensorProduct(const DimVector& global) const
    {
        Scalar shearStressTensorProduct = 0.0;
        for (unsigned int velIdx = 0; velIdx < dim; ++velIdx)
        {
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                shearStressTensorProduct += 0.5 * (dvdx(global)[velIdx][dimIdx] + dvdx(global)[dimIdx][velIdx])
                                            * 0.5 * (dvdx(global)[velIdx][dimIdx] + dvdx(global)[dimIdx][velIdx]);
            }
        }
        return shearStressTensorProduct;
    }

    //! \brief Source term values
    DimVector sourceMomentumBalanceAtPos(const DimVector& global) const
    {
        DimVector y(0.0);
        DimVector gravity(0.0);
        gravity[dim-1] = -9.81;
        for (unsigned int velIdx = 0; velIdx < dim; ++velIdx)
        {
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                // inertia term
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableNavierStokes))
                  y[velIdx] += density_ * dv2dx(global)[velIdx][dimIdx];

                // viscous term (molecular)
                y[velIdx] -= density_ * kinematicViscosity_* dvdx2(global)[velIdx][dimIdx];
                if (!GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableUnsymmetrizedVelocityGradient))
                  y[velIdx] -= density_ * kinematicViscosity_* dvdx2(global)[dimIdx][velIdx];

                // viscous term (turbulent) (using product rule)
#ifndef DisableVEddyViscosity
                y[velIdx] -= density_ * (dnutdx(global)[velIdx] * dvdx(global)[velIdx][dimIdx]
                                          + nut(global) * dvdx2(global)[velIdx][dimIdx]);
                if (!GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableUnsymmetrizedVelocityGradient))
                  y[velIdx] -= density_ * (dnutdx(global)[dimIdx] * dvdx(global)[velIdx][dimIdx]
                                            + nut(global) * dvdx2(global)[dimIdx][velIdx]);
#endif
            }
            // pressure term
            y[velIdx] += dpdx(global)[velIdx];

            // gravity term
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            {
                y[velIdx] -= density_ * gravity[velIdx];
            }
        }
        return y;
    }
//Source Mass Balance
    Scalar sourceMassBalanceAtPos(const DimVector& global) const
    {
        // term div(rho*v)
        Scalar y(0.0);
        for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
        {
            y += dvdx(global)[dimIdx][dimIdx];
        }
        return density_ * y;
    }
//Source Turbulent Kinetic Energy Balance
    Scalar sourceTurbulentKineticEnergyBalanceAtPos(const DimVector& global) const
    {
        Scalar y = 0.0;
        //+Advection & Diffusion terms
        for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
        {
            // advection term (using product rule)
#ifndef DisableKAdvection
            y += ( (dvdx(global)[dimIdx][dimIdx] * turbulentKineticEnergy(global))
                  +(velocity(global)[dimIdx] * dkdx(global)[dimIdx])
                 );
#endif
            // Molecular diffusion term (added to eq when KV is enabled)
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, KOmega, EnableKinematicViscosity))
            {
#ifndef DisableKDiffusionMol
                y -= kinematicViscosity_ * dkdx2(global)[dimIdx];
#endif
            }
            // Turbulent diffusion term (using product rule)
#ifndef DisableKDiffusionTurb
            y -= this->lop_.sigmaK() *
                   (  (dnutdx(global)[dimIdx] * dkdx(global)[dimIdx])
                    + (nut(global) * dkdx2(global)[dimIdx])
                   );
#endif
        }
        // + Production term
#ifndef DisableKProduction
        // plus 2006 K-limiter via P term
        Scalar KPorig= 2.0 * nut(global) * shearStressTensorProduct(global);
        Scalar KPaltr= 20.0 * this->lop_.betaK() * turbulentKineticEnergy(global) * dissipation(global);
        Scalar KP_min= std::min(KPorig,KPaltr);
        y -= KP_min;
#endif
        // + Destruction term
#ifndef DisableKDestruction
        y += this->lop_.betaK() * turbulentKineticEnergy(global) * dissipation(global);
#endif
        return y;
    }
//Source Dissipation Balance
    Scalar sourceDissipationBalanceAtPos(const DimVector& global) const
    {
        Scalar y = 0.0;
        // +Advection & Diffusion
        for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
        {
            // Advection term (using product rule)
#ifndef DisableWAdvection
            y += dvdx(global)[dimIdx][dimIdx] * dissipation(global)
                 + velocity(global)[dimIdx] * dwdx(global)[dimIdx];
#endif
            // Molecular diffusion term (added to eq when KV is enabled)
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, KOmega, EnableKinematicViscosity))
            {
#ifndef DisableWDiffusionMol
                y -= kinematicViscosity_ * dwdx2(global)[dimIdx];
#endif
            }
            // Turbulent diffusion term (using product rule)
#ifndef DisableWDiffusionTurb
            y -= this->lop_.sigmaOmega()
                 * ( (dnutdx(global)[dimIdx] * dwdx(global)[dimIdx])
                    +(nut(global) * dwdx2(global)[dimIdx])
                   );
#endif
        }
        //+CrossDiffusion term
#ifndef DisableWCrossDiffusion
    //! \brief Returns the \$f \sigma_{d} \$f constant
        Scalar kwProduct = 0.0;
        for (unsigned int j = 0; j < dim; ++j)
        {
          kwProduct +=  dkdx(global)[j] * dwdx(global)[j];
        }
      //crossdiffusion term
    y -= sigma_d(kwProduct) * (kwProduct) / dissipation(global);
#endif

        //+Production term
#ifndef DisableWProduction
        Scalar WPorig= 2.0 * nut(global) * shearStressTensorProduct(global);
        Scalar WPaltr= 20.0 * this->lop_.betaK() * turbulentKineticEnergy(global) * dissipation(global);
        Scalar WP_min= std::min(WPorig,WPaltr);
        y -= (this->lop_.alpha()
           * (dissipation(global) / turbulentKineticEnergy(global))
           * WP_min);
#endif
        //+Destruction term
#ifndef DisableWDestruction
        y += this->lop_.betaOmega()
             * dissipation(global) * dissipation(global);
#endif
        return y;
        }
//! \brief Returns if the product of \$f  \nabla K \$f and \$f  \nabla omega \$f is more than 0
    bool NabProductBool(Scalar NabProduct) const
    {
       bool status = false;
       if (NabProduct > 0)
          {status = true;}
       return status;
          }
//! \brief Returns the \$f \sigma_{d} \$f constant
    const Scalar sigma_d(Scalar NabProduct) const
    {
          if(NabProductBool(NabProduct) == 1)
               return 1/8;
          else
               return 0;
    }
//! \brief Return the temperature at a given position
    Scalar massMoleFracAtPos(const DimVector& global) const
    { return 0.0; }

//! \brief Return the temperature at a given position
    Scalar temperatureAtPos(const DimVector& global) const
    { return 298; }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    Scalar density_;
    Scalar kinematicViscosity_;
};

} //end namespace


#endif // DUMUX_PROBLEM_ANALYTIC_HH
