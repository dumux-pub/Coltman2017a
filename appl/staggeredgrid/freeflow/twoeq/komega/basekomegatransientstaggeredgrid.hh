/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization
 *        for \f$ k-\omega \f$ - turbulence  models.
 *
 * Turbulent Kinetic Energy balance:
 * \f[
 *    \frac{\partial}{\partial t} k
 *    + \dots
 *    = 0
 * \f]
 *
 * Dissipation balance:
 * \f[
 *    \frac{\partial}{\partial t}  \omega
 *    + \dots
 *    = 0
 * \f]
 */

#ifndef DUMUX_BASE_KOMEGA_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_BASE_KOMEGA_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"komegapropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the \f$ k-\omega \f$  equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseKOmegaTransientStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, TransientLocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { turbulentKineticEnergyIdx = Indices::turbulentKineticEnergyIdx,
             dissipationIdx = Indices::dissipationIdx };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      BaseKOmegaTransientStaggeredGrid(GridView gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \copydoc BaseNavierStokesTransientStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_komega (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                  std::vector<DimVector> velocityFaces, Scalar pressure,
                                  Scalar massMoleFrac, Scalar temperature) const
      {
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_W;
        const LFSU_W& lfsu_w = lfsu.template child<dissipationIdx>();

        const Scalar turbulentKineticEnergy = asImp_().turbulentKineticEnergy(eg, lfsu, x);
        const Scalar dissipation = asImp_().dissipation(eg, lfsu, x);
        const Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Storage term of <b> turbulent kinetic energy </b> balance equation
          * \f[
          *    \frac{\partial}{\partial t} k
          * \f].
          */
        r.accumulate(lfsu_k, 0,
                     turbulentKineticEnergy * elementVolume);

        /**
          * (2) \b Storage term of \b dissipation balance equation
          * \f[
          *    \frac{\partial}{\partial t} \omgea
          * \f].
          */
        r.accumulate(lfsu_w, 0,
                     dissipation * elementVolume);
      }

      /**
       * \brief Returns the turbulent kinetic energy for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar turbulentKineticEnergy(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<turbulentKineticEnergyIdx>::Type LFSU_K;
        const LFSU_K& lfsu_k = lfsu.template child<turbulentKineticEnergyIdx>();
        return x(lfsu_k, 0);
      }

      /**
       * \brief Returns the dissipation for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar dissipation(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<dissipationIdx>::Type LFSU_W;
        const LFSU_W& lfsu_w = lfsu.template child<dissipationIdx>();
        return x(lfsu_w, 0);
      }

private:
      //! Instationary variables
      double time;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_BASE_KOMEGA_TRANSIENT_STAGGERED_GRID_HH
