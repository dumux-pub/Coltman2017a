/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        compositional non-isothermal komega equation.
 *
 * \copydoc BaseKOmegaTransientStaggeredGrid
 * \copydoc BaseNavierStokesTransientStaggeredGrid
 * \copydoc BaseComponentStaggeredGrid
 * \copydoc BaseEnergyStaggeredGrid
 */

#ifndef DUMUX_KOMEGA_TWOCNI_STAGGERED_GRID_HH
#define DUMUX_KOMEGA_TWOCNI_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/twocomponentfluid.hh>
#include<appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/basezeroeqcomponentstaggeredgrid.hh>
#include<appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/basezeroeqenergystaggeredgrid.hh>
#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscositystaggeredgrid.hh>

#include"komega2cnipropertydefaults.hh"
#include"../komega/komegastaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization for steady-state
     *        compositional non-isothermal komega equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class KOmegaTwoCNIStaggeredGrid
    : public NumericalJacobianApplyVolume<KOmegaTwoCNIStaggeredGrid<TypeTag>>,
      public NumericalJacobianVolume<KOmegaTwoCNIStaggeredGrid<TypeTag>>,
      public NumericalJacobianSkeleton<KOmegaTwoCNIStaggeredGrid<TypeTag>>,
      public NumericalJacobianBoundary<KOmegaTwoCNIStaggeredGrid<TypeTag>>,
      public FullVolumePattern,
      public FullSkeletonPattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<>,
      public BaseKOmegaStaggeredGrid<TypeTag>,
      public EddyViscosityStaggeredGrid<TypeTag>,
      public BaseZeroEqComponentStaggeredGrid<TypeTag>,
      public BaseZeroEqEnergyStaggeredGrid<TypeTag>,
      public Dumux::TwoComponentFluid<TypeTag>
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef BaseKOmegaStaggeredGrid<TypeTag> ParentTypeKOmega;
      typedef EddyViscosityStaggeredGrid<TypeTag> ParentTypeMassMomentum;
      typedef BaseZeroEqComponentStaggeredGrid<TypeTag> ParentTypeComponent;
      typedef BaseZeroEqEnergyStaggeredGrid<TypeTag> ParentTypeEnergy;
      typedef Dumux::TwoComponentFluid<TypeTag> BaseFluid;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceComponentBalance) SourceComponentBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceEnergyBalance) SourceEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceTurbulentKineticEnergyBalance) SourceTurbulentKineticEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceDissipationBalance) SourceDissipationBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletMassMoleFrac) DirichletMassMoleFrac;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTemperature) DirichletTemperature;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTurbulentKineticEnergy) DirichletTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletDissipation) DirichletDissipation;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannMassMoleFrac) NeumannMassMoleFrac;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTemperature) NeumannTemperature;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTurbulentKineticEnergy) NeumannTurbulentKineticEnergy;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannDissipation) NeumannDissipation;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      enum { dim = GridView::dimension };

      //! \brief Index of unknowns
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx,
             massMoleFracIdx = Indices::massMoleFracIdx,
             temperatureIdx = Indices::temperatureIdx,
             turbulentKineticEnergyIdx = Indices::turbulentKineticEnergyIdx,
             dissipationIdx = Indices::dissipationIdx };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      KOmegaTwoCNIStaggeredGrid(const BC& bc_,
                                const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                                const SourceComponentBalance& sourceComponentBalance_, const SourceEnergyBalance& sourceEnergyBalance_,
                                const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance_, const SourceDissipationBalance& sourceDissipationBalance_,
                                const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                                const DirichletMassMoleFrac& dirichletMassMoleFrac_, const DirichletTemperature& dirichletTemperature_,
                                const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy_, const DirichletDissipation& dirichletDissipation_,
                                const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                                const NeumannMassMoleFrac& neumannMassMoleFrac_, const NeumannTemperature& neumannTemperature_,
                                const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy_, const NeumannDissipation& neumannDissipation_,
                                GridView gridView_, Problem& problem)
        : ParentTypeKOmega(bc_,
              sourceTurbulentKineticEnergyBalance_, sourceDissipationBalance_,
              dirichletTurbulentKineticEnergy_, dirichletDissipation_,
              neumannTurbulentKineticEnergy_, neumannDissipation_,
              gridView_, problem),
          ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          ParentTypeComponent(bc_,
              sourceComponentBalance_, dirichletMassMoleFrac_,
              neumannMassMoleFrac_, gridView_),
          ParentTypeEnergy(bc_,
              sourceEnergyBalance_, dirichletTemperature_,
              neumannTemperature_, gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          sourceComponentBalance(sourceComponentBalance_), sourceEnergyBalance(sourceEnergyBalance_),
          sourceTurbulentKineticEnergyBalance(sourceTurbulentKineticEnergyBalance_), sourceDissipationBalance(sourceDissipationBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          dirichletMassMoleFrac(dirichletMassMoleFrac_), dirichletTemperature(dirichletTemperature_),
          dirichletTurbulentKineticEnergy(dirichletTurbulentKineticEnergy_), dirichletDissipation(dirichletDissipation_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          neumannMassMoleFrac(neumannMassMoleFrac_), neumannTemperature(neumannTemperature_),
          neumannTurbulentKineticEnergy(neumannTurbulentKineticEnergy_), neumannDissipation(neumannDissipation_),
          gridView(gridView_), mapperElement(gridView_), problemPtr_(0)
      {
        problemPtr_ = &problem;
        useMoles_ = GET_PROP_VALUE(TypeTag, UseMoles);
        initialize();
      }

      /**
       * \copydoc BaseKOmegaStaggeredGrid::alpha_volume
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       * \copydoc BaseComponentStaggeredGrid::alpha_volume
       * \copydoc BaseEnergyStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(eg, lfsu, x);
        Scalar pressure = ParentTypeMassMomentum::pressure(eg, lfsu, x);
        Scalar massMoleFrac = ParentTypeComponent::massMoleFrac(eg, lfsu, x);
        Scalar temperature = ParentTypeEnergy::temperature(eg, lfsu, x);

        ParentTypeKOmega::alpha_volume_komega(eg, lfsu, x, lfsv, r,
                                                  velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeComponent::alpha_volume_component(eg, lfsu, x, lfsv, r,
                                                    velocityFaces, pressure, massMoleFrac, temperature);
        ParentTypeEnergy::alpha_volume_energy(eg, lfsu, x, lfsv, r,
                                              velocityFaces, pressure, massMoleFrac, temperature);
      }

      /**
       * \copydoc BaseKOmegaStaggeredGrid::alpha_skeleton
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       * \copydoc BaseComponentStaggeredGrid::alpha_skeleton
       * \copydoc BaseEnergyStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        std::vector<DimVector> velocities_s = ParentTypeMassMomentum::velocity(ig.inside(), lfsu_s, x_s);
        std::vector<DimVector> velocities_n = ParentTypeMassMomentum::velocity(ig.outside(), lfsu_n, x_n);
        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        Scalar pressure_n = ParentTypeMassMomentum::pressure(ig.outside(), lfsu_n, x_n);
        Scalar massMoleFrac_s = ParentTypeComponent::massMoleFrac(ig.inside(), lfsu_s, x_s);
        Scalar massMoleFrac_n = ParentTypeComponent::massMoleFrac(ig.outside(), lfsu_n, x_n);
        Scalar temperature_s = ParentTypeEnergy::temperature(ig.inside(), lfsu_s, x_s);
        Scalar temperature_n = ParentTypeEnergy::temperature(ig.outside(), lfsu_n, x_n);
        ParentTypeKOmega::alpha_skeleton_komega(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                    velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                    velocities_n, pressure_n, massMoleFrac_n, temperature_n);
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);
        ParentTypeComponent::alpha_skeleton_component(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                      velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                      velocities_n, pressure_n, massMoleFrac_n, temperature_n);
        ParentTypeEnergy::alpha_skeleton_energy(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseKOmegaStaggeredGrid::alpha_boundary
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       * \copydoc BaseComponentStaggeredGrid::alpha_boundary
       * \copydoc BaseEnergyStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        // boundary face coordinates
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal = ig.geometry().global(faceCenterLocal);

        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(ig.inside(), lfsu_s, x_s);

        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        typename DirichletPressure::Traits::RangeType pressure_boundary(0.0);
        dirichletPressure.evaluateGlobal(faceCenterGlobal, pressure_boundary);

        Scalar massMoleFrac_s = ParentTypeComponent::massMoleFrac(ig.inside(), lfsu_s, x_s);
        typename DirichletMassMoleFrac::Traits::RangeType massMoleFrac_boundary(0.0);
        dirichletMassMoleFrac.evaluateGlobal(faceCenterGlobal, massMoleFrac_boundary);

        Scalar temperature_s = ParentTypeEnergy::temperature(ig.inside(), lfsu_s, x_s);
        typename DirichletPressure::Traits::RangeType temperature_boundary(0.0);
        dirichletTemperature.evaluateGlobal(faceCenterGlobal, temperature_boundary);

        ParentTypeKOmega::alpha_boundary_komega(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                    velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                    pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        ParentTypeComponent::alpha_boundary_component(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                      velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                      pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        ParentTypeEnergy::alpha_boundary_energy(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                pressure_boundary, massMoleFrac_boundary, temperature_boundary);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::initialize
       * \copydoc BaseComponentStaggeredGrid::initialize
       * \copydoc BaseEnergyStaggeredGrid::initialize
       */
      void initialize()
      {
        ParentTypeMassMomentum::initialize();
        ParentTypeComponent::initialize();
        ParentTypeEnergy::initialize();
        ParentTypeKOmega::initialize();
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::updateStoredValues
       * \copydoc BaseComponentStaggeredGrid::updateStoredValues
       * \copydoc BaseEnergyStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsMassMoleFrac = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, massMoleFracIdx> >;
        using SubGfsTemperature = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, temperatureIdx> >;
        SubGfsMassMoleFrac subGfsMassMoleFrac(mdgfs);
        SubGfsTemperature subGfsTemperature(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsMassMoleFrac = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<massMoleFracIdx> >;
        using SubGfsTemperature = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<temperatureIdx> >;
        SubGfsMassMoleFrac subGfsMassMoleFrac(gfs);
        SubGfsTemperature subGfsTemperature(gfs);
#endif

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // discrete function objects
        using DgfMassMoleFrac = Dune::PDELab::DiscreteGridFunction<SubGfsMassMoleFrac, X>;
        using DgfTemperature = Dune::PDELab::DiscreteGridFunction<SubGfsTemperature, X>;
        DgfMassMoleFrac dgfMassMoleFrac(subGfsMassMoleFrac, lastSolution);
        DgfTemperature dgfTemperature(subGfsTemperature, lastSolution);

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // NOTE use eit also for multidomain models
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          if (!gridView.indexSet().contains(stokesDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif

          // evaluate solution bound to grid function space for cell mid-points
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);
          Dune::FieldVector<Scalar, 1> massMoleFrac(0.0);
          Dune::FieldVector<Scalar, 1> temperature(0.0);
          dgfMassMoleFrac.evaluate(element, cellCenterLocal, massMoleFrac);
          dgfTemperature.evaluate(element, cellCenterLocal, temperature);
          asImp_().storedMassMoleFrac[elementInsideID] = massMoleFrac;
          asImp_().storedTemperature[elementInsideID] = temperature;
        }

#if IS_STAGGERED_MULTIDOMAIN_MODEL
        ParentTypeMassMomentum::template updateStoredValues
                                <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                                (sdgv, mdgfs, lastSolution);
        ParentTypeComponent::template updateStoredValues
                             <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                             (sdgv, mdgfs, lastSolution);
        ParentTypeEnergy::template updateStoredValues
                          <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                          (sdgv, mdgfs, lastSolution);
        ParentTypeKOmega::template updateStoredValues
                            <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                            (sdgv, mdgfs, lastSolution);
#else
        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);
        ParentTypeComponent::updateStoredValues(gfs, lastSolution);
        ParentTypeEnergy::updateStoredValues(gfs, lastSolution);
        ParentTypeKOmega::updateStoredValues(gfs, lastSolution);
#endif
      }

      //! \brief Returns the density [kg/m^3]
      const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::density(pressure, temperature, massMoleFrac); }

      //! \brief Returns the density [mol/m^3]
      const Scalar molarDensity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::molarDensity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the dynamic viscosity [kg/(m s)]
      const Scalar dynamicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::dynamicViscosity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the kinematic viscosity [m^2/s]
      const Scalar kinematicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::kinematicViscosity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the thermal conductivity [W/(m*K)]
      const Scalar thermalConductivity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::thermalConductivity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the heat capacity [J/(kg*K)]
      const Scalar heatCapacity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::heatCapacity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the enthalpy of a component [J/kg]
      const Scalar enthalpyComponent(Scalar pressure, Scalar temperature, unsigned int compIdx) const
      { return BaseFluid::enthalpyComponent(pressure, temperature, compIdx); }

      //! \brief Returns the enthalpy of the phase [J/kg]
      const Scalar enthalpyPhase(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::enthalpyPhase(pressure, temperature, massMoleFrac); }

      //! \brief Returns the binary diffusion coefficient [m^2/s]
      const Scalar diffusionCoefficient(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::diffusionCoefficient(pressure, temperature, massMoleFrac); }

      //! \brief Returns the mass of the component [kg/mol]
      const Scalar molarMassComponent(unsigned int compIdx) const
      { return BaseFluid::molarMassComponent(compIdx); }

      //! \brief Returns the molar mass of the phase [kg/mol]
      const Scalar molarMassPhase(Scalar massMoleFrac) const
      { return BaseFluid::molarMassPhase(massMoleFrac); }

      //! \brief Returns the molar fraction [-]
      const Scalar convertToMoleFrac(Scalar massMoleFrac) const
      { return BaseFluid::convertToMoleFrac(massMoleFrac); }

      //! \brief Only relevant for K-epsilon
      const bool useWallFunctionMomentum(unsigned int elementID) const
      { return false; }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const SourceComponentBalance& sourceComponentBalance;
      const SourceEnergyBalance& sourceEnergyBalance;
      const SourceTurbulentKineticEnergyBalance& sourceTurbulentKineticEnergyBalance;
      const SourceDissipationBalance& sourceDissipationBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const DirichletMassMoleFrac& dirichletMassMoleFrac;
      const DirichletTemperature& dirichletTemperature;
      const DirichletTurbulentKineticEnergy& dirichletTurbulentKineticEnergy;
      const DirichletDissipation& dirichletDissipation;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      const NeumannMassMoleFrac& neumannMassMoleFrac;
      const NeumannTemperature& neumannTemperature;
      const NeumannTurbulentKineticEnergy& neumannTurbulentKineticEnergy;
      const NeumannDissipation& neumannDissipation;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool useMoles_;

protected:
    //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_KOMEGA_TWOCNI_STAGGERED_GRID_HH
