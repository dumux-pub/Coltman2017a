// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for a channel flow using the compositional non-isothermal
 *        kOmega equation
 */

#ifndef DUMUX_PROBLEM_PIPE_FLOW_HH
#define DUMUX_PROBLEM_PIPE_FLOW_HH

#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

#include "komega2cniproblem.hh"
#include "komega2cnipropertydefaults.hh"

namespace Dumux
{
template <class TypeTag>
class PipeFlowProblem;

namespace Properties
{
NEW_TYPE_TAG(PipeFlowProblem, INHERITS_FROM(StaggeredGridKOmega2cni));

// Set the problem property
SET_TYPE_PROP(PipeFlowProblem, Problem,
              Dumux::PipeFlowProblem<TypeTag>);

// Set the used local operator
SET_TYPE_PROP(PipeFlowProblem, LocalOperator,
              Dune::PDELab::KOmegaTwoCNIStaggeredGrid<TypeTag>);

// Set the used transient local operator
SET_TYPE_PROP(PipeFlowProblem, TransientLocalOperator,
              Dune::PDELab::KOmegaTwoCNITransientStaggeredGrid<TypeTag>);

// Set the grid type
SET_TYPE_PROP(PipeFlowProblem, Grid,
              Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Disable gravity field
SET_BOOL_PROP(PipeFlowProblem, ProblemEnableGravity, false);

// Select the fluid system
SET_TYPE_PROP(PipeFlowProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);
}

template <class TypeTag>
class PipeFlowProblem : public KOmegaTwoCNIProblem<TypeTag>
{
    typedef KOmegaTwoCNIProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };


    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;

public:
    PipeFlowProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Velocity);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, MassMoleFrac);
        FluidSystem::init();

        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        FluidState fluidState;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(temperature_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        Scalar kinematicViscosity = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity, false);
        dissipationRate_ = turbulenceProperties.dissipationRate(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity,false);
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return !bcVelocityIsOutflow(global) && !bcVelocityIsInflow(global); }
    bool bcVelocityIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcVelocityIsOutflow(const DimVector& global) const
    { return this->onLowerBoundary_(global) && global[0] > 0.9 * this->bBoxMax()[0]; }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global); }

    //! \brief MassMoleFrac boundary condition types
    bool bcMassMoleFracIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcMassMoleFracIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcMassMoleFracIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }

    //! \brief Temperature boundary condition types
    bool bcTemperatureIsWall(const DimVector& global) const
    { return !bcTemperatureIsOutflow(global) && !bcTemperatureIsInflow(global); }
    bool bcTemperatureIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global)
             || (this->onLowerBoundary_(global) && !bcVelocityIsOutflow(global)); }
    bool bcTemperatureIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }

    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) && this->onBoundary(global); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) && this->onBoundary(global); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcDissipationIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (!this->onUpperBoundary_(global)
            && !this->onLowerBoundary_(global)
            && !this->onRightBoundary_(global))
        {
            y[0] = velocity_;
        }
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return 1e5; }
    Scalar dirichletMassMoleFracAtPos(const DimVector& global) const
    { return 0.01; }
    Scalar dirichletTemperatureAtPos(const DimVector& global) const
    {
        if (this->onLowerBoundary_(global))
        {
            return 0.99*temperature_;
        }
        return temperature_;
    }
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
    }
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
        if (bcDissipationIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
                dofMapper(gridView_);
            return 6.0 * 15e-6 / (0.075 * std::pow(this->lop_.storedDistanceToWall[dofMapper.index(e)], 2));
        }
        return dissipationRate_;
    }

    Scalar sourceComponentBalanceAtPos(const DimVector& global) const
    {
      Scalar y = 0.0;
      if (global[0] > 0.3*this->bBoxMax()[0] && global[0] < 0.4*this->bBoxMax()[0]
          && global[1] > 0.4*this->bBoxMax()[1] && global[1] < 0.5*this->bBoxMax()[1])
      {
        y = 0.01;
      }
      return y;
    }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    Scalar velocity_;
    Scalar temperature_;
    Scalar massMoleFrac_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipationRate_;
};

} //end namespace


#endif // DUMUX_PROBLEM_PIPE_FLOW_HH
