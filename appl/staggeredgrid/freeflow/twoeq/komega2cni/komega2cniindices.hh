// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Defines the indices required for the compositional non-isothermal
 *        komega staggered grid model.
 */
#ifndef DUMUX_KOMEGA_TWOCNI_INDICES_HH
#define DUMUX_KOMEGA_TWOCNI_INDICES_HH

#include<appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cniindices.hh>

#include"komega2cniproperties.hh"

namespace Dumux
{
// \{

/*!
 * \brief The common indices for the compositional non-isothermal
 *        kepsilon staggered grid model.
 */
template <class TypeTag>
struct StaggeredGridKOmega2cniCommonIndices
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    //! Number of components in employed fluidsystem
    static const int numComponents = FluidSystem::numComponents;

    //! Index of the fluid phase set by default to nPhase
    static const int phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);

    //! The index of the main component of the considered phase
    static const int phaseCompIdx = phaseIdx;
    //! The index of the first transported component; ASSUMES phase indices of 0 and 1
    static const int transportCompIdx = (unsigned int)(1-phaseIdx);

    // Primary variable indices
    static const int numEq = 6;
    static const int velocityIdx = 0; //!< Index of the velocity
    static const int pressureIdx = 1; //!< Index of the pressure
    static const int massMoleFracIdx = 2; //!< Index of the massMoleFrac
    static const int temperatureIdx = 3; //!< Index of the temperature
    static const int turbulentKineticEnergyIdx = 4; //!< Index of the turbulentKineticEnergy
    static const int dissipationIdx = 5; //!< Index of the dissipation
    // Komega Models
    static const int Wilcox88 = 0;
    static const int Wilcox08 = 1;
    static const int MenterSST = 2;

};
} // end namespace

#endif // DUMUX_KOMEGA_TWOCNI_INDICES_HH
