/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup EnergyStaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization
 *        for heat transport models.
 *
 * The heat transport equation:<br>
 *
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho u \right)
 *    + \dots
 *    = 0
 * \f]
 */

#ifndef DUMUX_BASE_ENERGY_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_BASE_ENERGY_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"navierstokes2cniproperties.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the heat transport equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseEnergyTransientStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, TransientLocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { temperatureIdx = Indices::temperatureIdx };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      BaseEnergyTransientStaggeredGrid(GridView gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \copydoc BaseNavierStokesTransientStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_energy (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                std::vector<DimVector> velocityFaces, Scalar pressure,
                                Scalar massMoleFrac, Scalar temperature) const
      {
        typedef typename LFSU::template Child<temperatureIdx>::Type LFSU_T;
        const LFSU_T& lfsu_t = lfsu.template child<temperatureIdx>();

        const Scalar density = asImp_().density(pressure, temperature, massMoleFrac);
        const Scalar internalEnergy = asImp_().internalEnergy(pressure, temperature, massMoleFrac);
        const Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Storage term of \b temperature balance equation
          * \f[
          *    \frac{\partial}{\partial t} \varrho u
          * \f]
          */
        r.accumulate(lfsu_t, 0,
                     density * internalEnergy * elementVolume);
      }

      /**
       * \brief Returns the temperature for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar temperature(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<temperatureIdx>::Type LFSU_T;
        const LFSU_T& lfsu_t = lfsu.template child<temperatureIdx>();
        return x(lfsu_t, 0);
      }

private:
      //! Instationary variables
      double time;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_BASE_ENERGY_TRANSIENT_STAGGERED_GRID_HH
