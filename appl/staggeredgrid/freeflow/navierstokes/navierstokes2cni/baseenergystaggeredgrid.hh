/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup BaseEnergyStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        heat transport equation
 *
 * \copydoc NavierStokesStaggeredGrid
 *
 * The heat transport equation:<br>
 *
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho u \right)
 *    + \nabla \cdot \left( \varrho v h \right)
 *    - \nabla \cdot \left( \sum_\kappa \varrho D^\kappa h^\kappa \nabla x^\kappa \right)
 *    - \nabla \cdot \left( \lambda \nabla T \right)
 *    - q_\textrm{T}
 *    = 0
 * \f]
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASEENERGY_STAGGERED_GRID_HH
#define DUMUX_BASEENERGY_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"navierstokes2cniproperties.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the steady-state energy transport equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseEnergyStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceEnergyBalance) SourceEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTemperature) DirichletTemperature;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTemperature) NeumannTemperature;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { temperatureIdx = Indices::temperatureIdx };
      enum { numComponents = Indices::numComponents,
             transportCompIdx = Indices::transportCompIdx,
             phaseCompIdx = Indices::phaseCompIdx  };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      // Types to store the velocities and coordinates
      typedef std::vector<double> StoredScalar;
      StoredScalar storedThermalConductivity;
      StoredScalar storedSpecificHeatCapacity;
      StoredScalar storedEnthalpyPhase;

      //! \brief Constructor
      BaseEnergyStaggeredGrid(const BC& bc_,
        const SourceEnergyBalance& sourceEnergyBalance_,
        const DirichletTemperature& dirichletTemperature_,
        const NeumannTemperature& neumannTemperature_,
        GridView gridView_)
        : bc(bc_),
          sourceEnergyBalance(sourceEnergyBalance_),
          dirichletTemperature(dirichletTemperature_),
          neumannTemperature(neumannTemperature_),
          gridView(gridView_), mapperElement(gridView_)
      {
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);

        useMoles_ = GET_PROP_VALUE(TypeTag, UseMoles);
        if (useMoles_)
        {
          DUNE_THROW(NotImplemented, "The component transport is not implemented for mole fraction formulation");
        }

        initialize();
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_energy(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                               std::vector<DimVector> velocityFaces, Scalar pressure,
                               Scalar massMoleFrac, Scalar temperature) const
      {
        typedef typename LFSU::template Child<temperatureIdx>::Type LFSU_T;
        const LFSU_T& lfsu_t = lfsu.template child<temperatureIdx>();

        // /////////////////////
        // geometry information

        Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Source term of \b heat balance equation<br>
          *
          * \f[
          *    - q_\textrm{T}
          *    \Rightarrow - \int_V q_\textrm{T}
          * \f]
          * \f[
          *    \alpha = - q_\textrm{T} V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt, qorder);
        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceEnergyBalance::Traits::RangeType sourceEnergyBalanceValue;
          sourceEnergyBalance.evaluate(eg.entity(), it->position(), sourceEnergyBalanceValue);
          r.accumulate(lfsu_t, 0,
                       -1.0 * sourceEnergyBalanceValue * elementVolume * it->weight());
        }
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_energy(const IG& ig,
                                 const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                 const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                 R& r_s, R& r_n,
                                 std::vector<DimVector> velocities_s, Scalar pressure_s,
                                 Scalar massMoleFrac_s, Scalar temperature_s,
                                 std::vector<DimVector> velocities_n, Scalar pressure_n,
                                 Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<temperatureIdx>::Type LFSU_T;
        const LFSU_T& lfsu_t_s = lfsu_s.template child<temperatureIdx>();
        const LFSU_T& lfsu_t_n = lfsu_n.template child<temperatureIdx>();

        // domain and range field type
        typedef typename LFSU_T::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;

        // /////////////////////
        // geometry information


        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        Scalar distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_n = asImp_().density(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar molarDensity_s = asImp_().molarDensity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar molarDensity_n = asImp_().molarDensity(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar diffusionCoefficient_s = asImp_().diffusionCoefficient(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar diffusionCoefficient_n = asImp_().diffusionCoefficient(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar eddyDiffusivity_s = asImp_().eddyDiffusivity(ig.inside(), lfsu_s, x_s);
        const Scalar eddyDiffusivity_n = asImp_().eddyDiffusivity(ig.outside(), lfsu_n, x_n);
        const Scalar thermalConductivity_s = asImp_().thermalConductivity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar thermalConductivity_n = asImp_().thermalConductivity(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar thermalEddyConductivity_s = asImp_().thermalEddyConductivity(ig.inside(), lfsu_s, x_s);
        const Scalar thermalEddyConductivity_n = asImp_().thermalEddyConductivity(ig.outside(), lfsu_n, x_n);
        const Scalar enthalpyPhase_s = asImp_().enthalpyPhase(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar enthalpyPhase_n = asImp_().enthalpyPhase(pressure_n, temperature_n, massMoleFrac_n);

        // averaging: distance weighted average for conduction term
        Scalar density_avg = (distanceInsideToFace * density_n
                           + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
        Scalar molarDensity_avg = (distanceInsideToFace * molarDensity_n
                                + distanceOutsideToFace * molarDensity_s)
                              / (distanceInsideToFace + distanceOutsideToFace);
        Scalar diffusionCoefficient_avg = (distanceInsideToFace * diffusionCoefficient_n
                                        + distanceOutsideToFace * diffusionCoefficient_s)
                                      / (distanceInsideToFace + distanceOutsideToFace);
        Scalar eddyDiffusivity_avg = (distanceInsideToFace * eddyDiffusivity_n
                                   + distanceOutsideToFace * eddyDiffusivity_s)
                                 / (distanceInsideToFace + distanceOutsideToFace);
        Scalar thermalConductivity_avg = (distanceInsideToFace * thermalConductivity_n
                                       + distanceOutsideToFace * thermalConductivity_s)
                                     / (distanceInsideToFace + distanceOutsideToFace);
        Scalar thermalEddyConductivity_avg = (distanceInsideToFace * thermalEddyConductivity_n
                                           + distanceOutsideToFace * thermalEddyConductivity_s)
                                         / (distanceInsideToFace + distanceOutsideToFace);

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for conduction term
          density_avg = (2.0 * density_n * density_s)
                        / (density_n + density_s);
          molarDensity_avg = (2.0 * molarDensity_n * molarDensity_s)
                             / (molarDensity_n + molarDensity_s);
          diffusionCoefficient_avg = (2.0 * diffusionCoefficient_n * diffusionCoefficient_s)
                                     / (diffusionCoefficient_n + diffusionCoefficient_s);
          eddyDiffusivity_avg = (2.0 * eddyDiffusivity_n * eddyDiffusivity_s)
                                / (eddyDiffusivity_n + eddyDiffusivity_s);
          thermalConductivity_avg = (2.0 * thermalConductivity_n * thermalConductivity_s)
                                    / (thermalConductivity_n + thermalConductivity_s);
          thermalEddyConductivity_avg = (2.0 * thermalEddyConductivity_n * thermalEddyConductivity_s)
                                        / (thermalEddyConductivity_n + thermalEddyConductivity_s);
        }

        // upwinding: advection term
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar density_up = density_s;
        Scalar enthalpyPhase_up = enthalpyPhase_s;
        if (velocityNormal < 0)
        {
          density_up = density_n;
          enthalpyPhase_up = enthalpyPhase_n;
        }

        if (enableAdvectionAveraging_)
        {
            // distance weighted average mean
            density_up = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
            enthalpyPhase_up = (distanceInsideToFace * enthalpyPhase_n + distanceOutsideToFace * enthalpyPhase_s)
                               / (distanceInsideToFace + distanceOutsideToFace);
        }

        /**
         * (1) \b Flux term of \b heat balance equation
         *
         * In mole formulation:
         * \f[
         *    \varrho h v
         *    \Rightarrow \int_\gamma \left( \varrho h v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varrho_{\textrm{up}} h_{\textrm{up}} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the heat transport.
         */
        r_s.accumulate(lfsu_t_s, 0,
                       1.0 * enthalpyPhase_up
                       * density_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_t_n, 0,
                       -1.0 * enthalpyPhase_up
                       * density_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (2) \b Diffusion term of \b heat balance equation
         *
         * \f[
         *    - \nabla \cdot \left( \varrho D^\kappa h^\kappa \nabla x^\kappa \right)
         * \f]
         */
        Scalar sumDiffusiveFluxes = 0.0;
        for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
          // the transported components of the phase
          if (compIdx != phaseCompIdx)
          {
            Scalar moleFraction_s = asImp_().convertToMoleFrac(massMoleFrac_s);
            Scalar moleFraction_n = asImp_().convertToMoleFrac(massMoleFrac_n);

            Scalar enthalpyComponent_s = asImp_().enthalpyComponent(pressure_s, temperature_s, compIdx);
            Scalar enthalpyComponent_n = asImp_().enthalpyComponent(pressure_n, temperature_n, compIdx);
            Scalar enthalpyComponent_avg = (distanceInsideToFace * enthalpyComponent_n + distanceOutsideToFace * enthalpyComponent_s)
                                           / (distanceInsideToFace + distanceOutsideToFace);

            Scalar diffusiveFlux = (moleFraction_n - moleFraction_s)
                                    / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                                    * faceUnitOuterNormal[normDim]
                                    * (diffusionCoefficient_avg + eddyDiffusivity_avg)
                                    * molarDensity_avg;
            sumDiffusiveFluxes += diffusiveFlux;

            r_s.accumulate(lfsu_t_s, 0,
                           -1.0 * diffusiveFlux
                           * enthalpyComponent_avg
                           * asImp_().molarMassComponent(compIdx)
                           * faceVolume);
            r_n.accumulate(lfsu_t_n, 0,
                           1.0 * diffusiveFlux
                           * enthalpyComponent_avg
                           * asImp_().molarMassComponent(compIdx)
                           * faceVolume);
          }
          // the main component of the phase
          Scalar enthalpyComponent_s = asImp_().enthalpyComponent(pressure_s, temperature_s, phaseCompIdx);
          Scalar enthalpyComponent_n = asImp_().enthalpyComponent(pressure_n, temperature_n, phaseCompIdx);
          Scalar enthalpyComponent_avg = (distanceInsideToFace * enthalpyComponent_n + distanceOutsideToFace * enthalpyComponent_s)
                                         / (distanceInsideToFace + distanceOutsideToFace);
          r_s.accumulate(lfsu_t_s, 0,
                         -1.0 * -sumDiffusiveFluxes
                         * enthalpyComponent_avg
                         * asImp_().molarMassComponent(phaseCompIdx)
                         * faceVolume);
          r_n.accumulate(lfsu_t_n, 0,
                         1.0 * -sumDiffusiveFluxes
                         * enthalpyComponent_avg
                         * asImp_().molarMassComponent(phaseCompIdx)
                         * faceVolume);
        }

        /**
         * (3) \b Conduction term of \b heat balance equation
         *
         * In mole formulation
         * \f[
         *    - \lambda \nabla T
         *    \Rightarrow - \int_\gamma \left( \lambda \nabla T \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| \lambda_{\textrm{avg}} \nabla T \cdot n
         * \f]
         *
         * The default procedure for averaging is a distance weighted average, by
         * using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt> you can do an
         * harmonic averaging instead.
         */

        r_s.accumulate(lfsu_t_s, 0,
                       -1.0 * (thermalConductivity_avg + thermalEddyConductivity_avg)
                       * (temperature_n - temperature_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_t_n, 0,
                       1.0 * (thermalConductivity_avg + thermalEddyConductivity_avg)
                       * (temperature_n - temperature_s)
                       / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_energy(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                 std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                 Scalar massMoleFrac_s, Scalar temperature_s,
                                 Scalar pressure_boundary,
                                 Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<temperatureIdx>::Type LFSU_T;
        const LFSU_T& lfsu_t_s = lfsu_s.template child<temperatureIdx>();
        typedef typename BC::template Child<temperatureIdx>::Type BCTemperature;
        const BCTemperature& bcTemperature = bc.template child<temperatureIdx>();

        // domain and range field type
        typedef typename LFSU_T::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;


        // center in face's reference element
        const Dune::FieldVector<Scalar, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
          * Dune::ReferenceElements<Scalar,IG::dimension-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside().geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<Scalar,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar,IG::dimension>::general(ig.inside().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);

        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // evaluation of cell values and constants
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_boundary = asImp_().density(pressure_boundary, temperature_boundary, massMoleFrac_boundary);
        const Scalar thermalConductivity_s = asImp_().thermalConductivity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar thermalEddyConductivity_s = asImp_().thermalEddyConductivity(ig.inside(), lfsu_s, x_s);
        const Scalar enthalpyPhase_s = asImp_().enthalpyPhase(pressure_s, temperature_s, massMoleFrac_s);

        //! \note  An inflow BC should also be used for a wall with a constant temperature is achieved.
        //!        A Wall BC for temperature means an isolated wall and Neumann no-flow.
        if (bcTemperature.isInflow(ig, faceCenterLocal))
        {
          const Scalar enthalpyPhase_boundary = asImp_().enthalpyPhase(pressure_boundary, temperature_boundary, massMoleFrac_boundary);

          /**
           * Inflow boundary handling for heat balance<br>
           * (1) \b Flux term of \b heat balance equation
           *
           * In mole formulation:
           * \f[
           *    \varrho h v
           *    \Rightarrow \int_\gamma \left( \varrho h v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho_{textrm{s}} h_{\textrm{boundary}} \left( v \cdot n \right)
           * \f]
           *
           * \note upwinding is assumed by default, because it is inflow
           */
          r_s.accumulate(lfsu_t_s, 0,
                         1.0 * enthalpyPhase_boundary
                         * density_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);

          /**
          * (2) \b Diffusion term of \b heat balance equation
          *
          * \f[
          *    - \nabla \cdot \left( \varrho D^\kappa h^\kappa \nabla x^\kappa \right)
          * \f]
          */
          Scalar sumDiffusiveFluxes = 0.0;
          for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
          {
            // the transported components of the phase
            if (compIdx != phaseCompIdx)
            {
              Scalar moleFraction_boundary = asImp_().convertToMoleFrac(massMoleFrac_boundary);
              Scalar moleFraction_s = asImp_().convertToMoleFrac(massMoleFrac_s);
              Scalar molarDensity_s = asImp_().molarDensity(pressure_s, temperature_s, massMoleFrac_s);
              Scalar diffusionCoefficient_boundary = asImp_().diffusionCoefficient(pressure_boundary, temperature_boundary, massMoleFrac_boundary);
              Scalar eddyDiffusivity_s = asImp_().eddyDiffusivity(ig.inside(), lfsu_s, x_s);

              Scalar diffusiveFlux = (moleFraction_boundary - moleFraction_s)
                                     / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                                     * faceUnitOuterNormal[normDim]
                                     * (diffusionCoefficient_boundary + eddyDiffusivity_s)
                                     * molarDensity_s;
              sumDiffusiveFluxes += diffusiveFlux;
              r_s.accumulate(lfsu_t_s, 0,
                             -1.0 * diffusiveFlux
                             * asImp_().enthalpyComponent(pressure_boundary, temperature_boundary, compIdx)
                             * asImp_().molarMassComponent(compIdx)
                             * faceVolume);
            }
            // the main component of the phase
            r_s.accumulate(lfsu_t_s, 0,
                           sumDiffusiveFluxes
                           * asImp_().enthalpyComponent(pressure_boundary, temperature_boundary, phaseCompIdx)
                           * asImp_().molarMassComponent(phaseCompIdx)
                           * faceVolume);
          }

          /**
          * (3) \b Conduction term of \b heat balance equation
          *
          * \f[
          *    - \lambda \nabla T
          *    \Rightarrow - \int_\gamma \left( \lambda \nabla T \right) \cdot n
          * \f]
          * \f[
          *    \alpha_\textrm{self}
          *    = - |\gamma| \lambda \nabla T \cdot n
          * \f]
          *
          * At the boundary the values from the inner cell are taken and <b>no averaging</b>
          * is performed.
          */
          r_s.accumulate(lfsu_t_s, 0,
                         -1.0 * (thermalConductivity_s + thermalEddyConductivity_s)
                         * (temperature_boundary - temperature_s)
                         / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);
        }
        // Wall boundary for temperature
        else if (bcTemperature.isWall(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Wall Condition for temperature.
          //! \note Wall BC for temperature means an isolated wall and Neumann no-flow.
          //!       A wall with a constant temperature is achieved by a Dirichlet (Inflow) BC.
        }
        // Outflow boundary for temperature
        else if (bcTemperature.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for heat balance<br>
           * (1) \b Flux term of \b heat balance equation
           *
           * \f[
           *    \varrho_h v
           *    \Rightarrow \int_\gamma \left( \varrho h v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho_{\textrm{self}} h{\textrm{self}} \left( v \cdot n \right)
           * \f]
           *
           * \note upwinding is assumed by default, because it is inflow
           */
          r_s.accumulate(lfsu_t_s, 0,
                         1.0 * enthalpyPhase_s
                         * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Symmetry boundary for temperature
        else if (bcTemperature.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        // Coupling boundary for temperature
        else if (bcTemperature.isCoupling(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Coupling Condition for temperature.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for temperature.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       */
      void initialize()
      {
        storedThermalConductivity.resize(mapperElement.size());
        storedSpecificHeatCapacity.resize(mapperElement.size());
        storedEnthalpyPhase.resize(mapperElement.size());
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedThermalConductivity[i] = 0.0;
          storedSpecificHeatCapacity[i] = 0.0;
          storedEnthalpyPhase[i] = 0.0;
        }

        typedef typename BC::template Child<temperatureIdx>::Type BCTemperature;
        const BCTemperature& bcTemperature = bc.template child<temperatureIdx>();

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            if (!ig->boundary())
            {
              continue;
            }

            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcTemperature.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTemperature.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTemperature.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTemperature.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcTemperature.isCoupling(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos != 1)
            {
              std::cout << "BCTemperature at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcTemperature.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcTemperature.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcTemperature.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcTemperature.isSymmetry(*ig, faceCenterLocal) << std::endl;
              std::cout << "Coupling  " << bcTemperature.isCoupling(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple or no boundary conditions for temperature at one point.");
            }
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * Travers grid and store data, this is only needed for visualization.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
#endif
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          storedThermalConductivity[elementInsideID] = asImp_().thermalConductivity(asImp_().storedPressure[elementInsideID],
                                                                                    asImp_().storedTemperature[elementInsideID],
                                                                                    asImp_().storedMassMoleFrac[elementInsideID]);

          storedSpecificHeatCapacity[elementInsideID] = asImp_().heatCapacity(asImp_().storedPressure[elementInsideID],
                                                                              asImp_().storedTemperature[elementInsideID],
                                                                              asImp_().storedMassMoleFrac[elementInsideID]);

          storedEnthalpyPhase[elementInsideID] = asImp_().enthalpyPhase(asImp_().storedPressure[elementInsideID],
                                                                        asImp_().storedTemperature[elementInsideID],
                                                                        asImp_().storedMassMoleFrac[elementInsideID]);
        }
      }

      /**
       * \brief Returns the temperature for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar temperature(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<temperatureIdx>::Type LFSU_T;
        const LFSU_T& lfsu_t = lfsu.template child<temperatureIdx>();
        return x(lfsu_t, 0);
      }

private:
      const BC& bc;
      const SourceEnergyBalance& sourceEnergyBalance;
      const DirichletTemperature& dirichletTemperature;
      const NeumannTemperature& neumannTemperature;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;
      bool useMoles_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASEENERGY_STAGGERED_GRID_HH
