/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation.
 *
 * \copydoc BaseNavierStokesStaggeredGrid
 */

#ifndef DUMUX_NAVIER_STOKES_STAGGERED_GRID_DUMUX_HH
#define DUMUX_NAVIER_STOKES_STAGGERED_GRID_DUMUX_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/onecomponentfluid.hh>

#include"basenavierstokesstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state Navier-Stokes equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class NavierStokesStaggeredGrid
    : public NumericalJacobianApplyVolume<NavierStokesStaggeredGrid<TypeTag>>,
      public NumericalJacobianVolume<NavierStokesStaggeredGrid<TypeTag>>,
      public NumericalJacobianSkeleton<NavierStokesStaggeredGrid<TypeTag>>,
      public NumericalJacobianBoundary<NavierStokesStaggeredGrid<TypeTag>>,
      public FullVolumePattern,
      public FullSkeletonPattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<>,
      public BaseNavierStokesStaggeredGrid<TypeTag>
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;

      typedef BaseNavierStokesStaggeredGrid<TypeTag> ParentTypeMassMomentum;
      //! \todo sollte umgestellt werden, dass BaseFluid auch ein PROP_TYPE ist
      typedef Dumux::OneComponentFluid<TypeTag> BaseFluid;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      enum { dim = GridView::dimension };
      enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      NavierStokesStaggeredGrid(const BC& bc_,
                          const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                          const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                          const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                          GridView gridView_, Problem& problem)
        : ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_), gridView(gridView_),
          mapperElement(gridView_), problemPtr_(0)
      {
        problemPtr_ = &problem;
        initialize();
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> cellCenterGlobal = eg.geometry().global(cellCenterLocal);

        /** \todo it would be much nicer to get all the unknowns from convenience functions, which look
         *        all the same. This would be advantegeous, as then these functions can be called from
         *        all base classes with the asImp_() function.
         *
         *        for example:
         *        ParentTypeMassMomentum::pressure(eg, lfsu, x);
         *        -> asImp_().pressure(eg, lfsu, x);
         *
         *        and
         *        problem_().temperatureAtPos(cellCenterGlobal);
         *        -> asImp_().temperatureAtPos(eg, lfsu, x);
         *        -> which then calls problem_().temperatureAtPos(cellCenterGlobal);
         *
         *        but then the function call for temperature would be always the same. If
         *        energy the balance is implemented or not.
         */
        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(eg, lfsu, x);
        Scalar pressure = ParentTypeMassMomentum::pressure(eg, lfsu, x);
        Scalar massMoleFrac = problem_().massMoleFracAtPos(cellCenterGlobal);
        Scalar temperature = problem_().temperatureAtPos(cellCenterGlobal);
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);

        std::vector<DimVector> velocities_s = ParentTypeMassMomentum::velocity(ig.inside(), lfsu_s, x_s);
        std::vector<DimVector> velocities_n = ParentTypeMassMomentum::velocity(ig.outside(), lfsu_n, x_n);
        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        Scalar pressure_n = ParentTypeMassMomentum::pressure(ig.outside(), lfsu_n, x_n);
        Scalar massMoleFrac_s = problem_().massMoleFracAtPos(insideCellCenterGlobal);
        Scalar massMoleFrac_n = problem_().massMoleFracAtPos(outsideCellCenterGlobal);
        Scalar temperature_s = problem_().temperatureAtPos(insideCellCenterGlobal);
        Scalar temperature_n = problem_().temperatureAtPos(outsideCellCenterGlobal);
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        // inside element coordinates
        const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> cellCenterGlobal = ig.inside().geometry().global(cellCenterLocal);
        // boundary face coordinates
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal = ig.geometry().global(faceCenterLocal);

        std::vector<DimVector> velocityFaces = ParentTypeMassMomentum::velocity(ig.inside(), lfsu_s, x_s);
        Scalar pressure_s = ParentTypeMassMomentum::pressure(ig.inside(), lfsu_s, x_s);
        typename DirichletPressure::Traits::RangeType  pressure_boundary(0.0);
        dirichletPressure.evaluateGlobal(faceCenterGlobal, pressure_boundary);
        Scalar massMoleFrac_s = problem_().massMoleFracAtPos(cellCenterGlobal);
        Scalar massMoleFrac_boundary = problem_().massMoleFracAtPos(faceCenterGlobal);
        Scalar temperature_s = problem_().temperatureAtPos(cellCenterGlobal);
        Scalar temperature_boundary = problem_().temperatureAtPos(faceCenterGlobal);
        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);
        }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::initialize
       */
      void initialize()
      {
        ParentTypeMassMomentum::initialize();
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
#endif
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
              Dune::ReferenceElements<Scalar, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<Scalar, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);

          asImp_().storedTemperature[elementInsideID] = problem_().temperatureAtPos(cellCenterGlobal);
          asImp_().storedMassMoleFrac[elementInsideID] = problem_().massMoleFracAtPos(cellCenterGlobal);
        }


#if IS_STAGGERED_MULTIDOMAIN_MODEL
        ParentTypeMassMomentum::template updateStoredValues
                                <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                                (sdgv, mdgfs, lastSolution);
#else
        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);
#endif
      }

      //! \brief Returns Returns the density [kg/m^3]
      const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::density(pressure, temperature, massMoleFrac); }

      //! \brief Returns the dynamic viscosity [kg/(m s)]
      const Scalar dynamicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::dynamicViscosity(pressure, temperature, massMoleFrac); }

      //! \brief Returns the kinematic viscosity [m^2/s]
      const Scalar kinematicViscosity(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::kinematicViscosity(pressure, temperature, massMoleFrac); }

      //! \brief Only relevant for k-epsilon
      const bool useWallFunctionMomentum(unsigned int elementID) const
      { return false; }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
      MapperElement mapperElement;

protected:
    //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_NAVIER_STOKES_STAGGERED_GRID_DUMUX_HH
