/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Storage term / transient part of staggered grid discretization for Navier-Stokes equation.
 *
 * \copydoc BaseNavierStokesTransientStaggeredGrid
 */

#ifndef DUMUX_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_DUMUX_HH
#define DUMUX_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_DUMUX_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/onecomponentfluid.hh>

#include"basenavierstokestransientstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the transient part of the Navier-Stokes equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class NavierStokesTransientStaggeredGrid
    : public NumericalJacobianVolume<NavierStokesTransientStaggeredGrid<TypeTag> >,
      public NumericalJacobianApplyVolume<NavierStokesTransientStaggeredGrid<TypeTag> >,
      public FullVolumePattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<double>,
      public BaseNavierStokesTransientStaggeredGrid<TypeTag>
    {
    public:
      typedef BaseNavierStokesTransientStaggeredGrid<TypeTag> ParentType;
      typedef Dumux::OneComponentFluid<TypeTag> BaseFluid;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      enum { dim = GridView::dimension };

      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Constructor
      NavierStokesTransientStaggeredGrid(GridView gridView_, Problem& problem)
        : ParentType(gridView_), problemPtr_(0)
      {
        problemPtr_ = &problem;
      }

      /**
       * \brief set time for subsequent evaluation
       */
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \brief Contribution to volume integral.
       *
       * This function just calls the ParentType.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> cellCenterGlobal = eg.geometry().global(cellCenterLocal);

        std::vector<DimVector> velocityFaces = ParentType::velocity(eg, lfsu, x);
        Scalar pressure = ParentType::pressure(eg, lfsu, x);
        Scalar massMoleFrac = problem_().massMoleFracAtPos(cellCenterGlobal);
        Scalar temperature = problem_().temperatureAtPos(cellCenterGlobal);
        ParentType::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                              velocityFaces, pressure, massMoleFrac, temperature);
      }

      //! \brief Returns the density [kg/m^3]
      const Scalar density(Scalar pressure, Scalar temperature, Scalar massMoleFrac) const
      { return BaseFluid::density(pressure, temperature, massMoleFrac); }

private:
      //! Instationary variables
      double time;

protected:
      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_DUMUX_HH
