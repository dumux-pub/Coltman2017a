/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Local operator base class for staggered grid discretization for steady-state
 * Navier-Stokes equation.
 *
 * The Navier-Stokes-Equations:<br>
 *
 * Mass balance:
 * \f[
 *    \nabla \cdot \left( \varrho_\alpha v_\alpha \right)
 *    - q_{\varrho}
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \nabla \cdot \left( p_\alpha I
 *      - \varrho_\alpha \nu_\alpha \nabla v_\alpha
 *      \right)
 *    - \varrho_\alpha g
 *    - q_{\varrho v}
 *    = 0
 * \f]
 *
 * \note The transposed part of the viscous term is not included in the
 *       equations yet.
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_NAVIER_STOKES_STAGGERED_GRID_DUMUX_HH
#define DUMUX_BASE_NAVIER_STOKES_STAGGERED_GRID_DUMUX_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"navierstokesproperties.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator base class for staggered grid discretization solving
     * the steady-state Navier-Stokes equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseNavierStokesStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      //! \brief Index of unknowns
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      // Types to store the velocities and coordinates
      //! \todo most of the stored variables can be removed, they are only for output issues
      typedef std::vector<double> StoredScalar;
      typedef std::vector<Dune::FieldVector<double, dim> > StoredVector;

      StoredVector storedElementCentersGlobal;
      std::vector<std::vector<Dune::FieldVector<double, dim> > > storedElementFacesGlobal;
      StoredVector storedVelocitiesAtElementCenter;
      std::vector<std::vector<Dune::FieldVector<double, dim> > > storedVelocitiesAtElementFaces;
      StoredScalar storedPressure;
      StoredScalar storedTemperature;
      StoredScalar storedMassMoleFrac;
      StoredScalar storedKinematicViscosity;
      StoredScalar storedDensity;
      std::vector<double> storedValueIsInStokesDomain;

      //! \brief Constructor
      BaseNavierStokesStaggeredGrid(const BC& bc_,
        const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
        const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
        const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
        GridView gridView_)
        : bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_), mapperElement(gridView_)
      {
        enableNavierStokes_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableNavierStokes);
        enableGravity_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity);
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableDiffusionHarmonic_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusionHarmonic);

        gravity_ = -9.81;
        gravityDim_ = dim - 1;

        initialize();
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        // This function is empty on purpose. From the child classes this function
        // has to call the respective CNI version below.
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       * \param velocity phase velocity vector of the velocity on the inside the element faces
       * \param pressure phase pressure inside the element
       * \param massMoleFrac phase composition inside the element
       * \param temperature phase temperature inside the element
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_massmomentum(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                     std::vector<DimVector> velocityFaces, Scalar pressure,
                                     Scalar massMoleFrac, Scalar temperature) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();

        // /////////////////////
        // geometry information

        // velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
          faceCentersGlobal[curFace] = eg.geometry().global(faceCentersLocal[curFace]);
        }

        // distance between two face mid points
        Dune::FieldVector<Scalar, dim> distancesFaceCenters(0.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          distancesFaceCenters[curDim] =
            std::abs(faceCentersGlobal[2*curDim+1][curDim] - faceCentersGlobal[2*curDim][curDim]);
        }

        // staggered face volume (goes through cell center) perpendicular to each direction
        Dune::FieldVector<Scalar, dim> orthogonalFaceVolumes(1.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          for (unsigned int normDim = 0; normDim < dim; ++normDim)
          {
            if (curDim != normDim)
            {
              orthogonalFaceVolumes[curDim] *= distancesFaceCenters[normDim];
            }
          }
        }

        // /////////////////////
        // evaluation of upwinding and averaging

        // evaluate fluid properties
        const Scalar density = asImp_().density(pressure, temperature, massMoleFrac);
        const Scalar kinematicViscosity = asImp_().kinematicViscosity(pressure, temperature, massMoleFrac);

        // distance weighted average quantities on staggered intersection
        std::vector<Scalar> velocity_avg(dim);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          velocity_avg[curDim] = 0.5 *
            (velocityFaces[curDim*2][curDim] + velocityFaces[curDim*2+1][curDim]);
        }

        // upwinding on staggered intersection
        std::vector<Scalar> velocity_up(dim);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          velocity_up[curDim] = velocityFaces[curDim*2][curDim];
          if (velocity_up[curDim] < 0)
          {
            velocity_up[curDim] = velocityFaces[curDim*2+1][curDim];
          }
        }

        if (enableAdvectionAveraging_)
        {
          velocity_up = velocity_avg;
        }

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
            * Contribution to the different balance equations. All formulas for the
            * \b momentum balance equation are given for the left degree of
            * freedom <tt>curDim</tt>.<br>
            *
            * <br>
            * (1) \b Inertia term of \b momentum balance equation
            * \f[
            *    \varrho v v^T
            *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
            * \f]
            * parallel case for all coordinate axes
            * \f[
            *    \alpha_\textrm{left}
            *    = |\gamma| \varrho \left[ \left( v v^T \right) \cdot n \right] \cdot n
            *    = |\gamma| \varrho v_\textrm{up,curDim} v_\textrm{avg,curDim}
            * \f]
            * with
            * \f[
            *    v_\textrm{avg} = \frac{v_\textrm{right} \Delta x_\textrm{left}
            *                          + v_\textrm{left} \Delta x_\textrm{right}}
            *                    {\Delta x_\textrm{right} + \Delta x_\textrm{left}}
            * \f]
            *
            * The default value is \b upwinding for the advective part, by
            * using the property <tt>EnableAdvectionAveraging</tt> you can do an
            * averaging instead of upwinding for \b one velocity component.
            */
          if (enableNavierStokes_)
          {
              r.accumulate(lfsu_v, 2*curDim,
                          // normal is always positive
                          1.0 * density
                          * velocity_up[curDim]
                          * velocity_avg[curDim]
                          * orthogonalFaceVolumes[curDim]); // staggered face volume
              r.accumulate(lfsu_v, 2*curDim+1,
                          // normal is always negative
                          -1.0 * density
                          * velocity_up[curDim]
                          * velocity_avg[curDim]
                          * orthogonalFaceVolumes[curDim]); // staggered face volume
          }

          /**
           * (2) \b Viscous term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}
           *    = - \nu \rho \nabla v
           *    \Rightarrow \int_\gamma - \boldsymbol{\tau} \cdot n
           *    = \int_\gamma - \nu \rho \nabla v \cdot n
           * \f]
           * parallel case for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = - |\gamma| \left( \boldsymbol{\tau} \cdot n \right) \cdot n
           *    = - |\gamma| \varrho \nu
           *      \frac{\textrm{d} v_\textrm{curDim}}{\textrm{d} x_\textrm{curDim}}
           *    = - |\gamma| \varrho \nu
           *      \frac{v_{\textrm{right,curDim}} - v_{\textrm{left,curDim}}}
           *           {x_{\textrm{right,curDim}} - x_{\textrm{left,curDim}}}
           * \f]
           */
          r.accumulate(lfsu_v, 2*curDim,
                       // normal is always positive
                       -1.0 * kinematicViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]); // face volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       // normal is always negative
                       1.0 * kinematicViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]); // face volume

          /**
           * (3) \b Pressure term of \b momentum balance equation (with Gauss theorem)
           *
           * \f[
           *    \nabla p
           *    \Rightarrow \int_\gamma p
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = p \gamma
           * \f]
           */
          r.accumulate(lfsu_v, 2*curDim,
                       // normal is always positive
                       1.0 * pressure
                       * orthogonalFaceVolumes[curDim]); // staggered face volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       // normal is always negative
                       -1.0 * pressure
                       * orthogonalFaceVolumes[curDim]); // staggered face volume


          /**
            * (4) \b Gravity term of \b momentum balance equation
            *
            * \f[
            *    \varrho g_i
            *    \Rightarrow \int_V \varrho g_i
            * \f]
            * \f[
            *    \alpha_\textrm{i} = -0.5 \varrho g_i V_e
            * \f]
            */
          if (enableGravity_ && curDim == gravityDim_)
          {
            Scalar elementVolume = eg.geometry().volume();
            r.accumulate(lfsu_v, 2*curDim,
                         -0.5 * density
                         * gravity_
                         * elementVolume);
            r.accumulate(lfsu_v, 2*curDim+1,
                         -0.5 * density
                         * gravity_
                         * elementVolume);
          }

          /**
           * (5) \b Source term of \b momentum balance equation<br>
           *
           * \f[
           *    - q_{\varrho v}
           *    \Rightarrow - \int_V q_{\varrho v}
           * \f]
           * \f[
           *    \alpha_\textrm{curDim} = -0.5 q_{\varrho v\textrm{,curDim}} V_e
           * \f]
           */
          Scalar elementVolume = eg.geometry().volume();

          typename SourceMomentumBalance::Traits::RangeType sourceMomentumBalanceValue_s;
          typename SourceMomentumBalance::Traits::RangeType sourceMomentumBalanceValue_n;
          Dune::GeometryType gt = eg.geometry().type();
          const int qorder = 4;
          const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt,qorder);

          // loop over quadrature points
          for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            Dune::FieldVector<double, dim> pos_s = it->position();
            Dune::FieldVector<double, dim> pos_n = it->position();
            pos_s[curDim] *= 0.5;
            pos_n[curDim] *= 0.5;
            pos_n[curDim] += 0.5;
            sourceMomentumBalance.evaluate(eg.entity(), pos_s, sourceMomentumBalanceValue_s);
            sourceMomentumBalance.evaluate(eg.entity(), pos_n, sourceMomentumBalanceValue_n);
            r.accumulate(lfsu_v, 2*curDim,
                          -0.5 * sourceMomentumBalanceValue_s[curDim]
                          * elementVolume * it->weight());
            r.accumulate(lfsu_v, 2*curDim+1,
                          -0.5 * sourceMomentumBalanceValue_n[curDim]
                          * elementVolume * it->weight());
          }
        }
        /**
          * (6) \b Source term of \b mass balance equation<br>
          *
          * \f[
          *    - q_{\varrho}
          *    \Rightarrow - \int_V q_{\varrho}
          * \f]
          * \f[
          *    \alpha = - q_{\varrho} V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt,qorder);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceMassBalance::Traits::RangeType sourceMassBalanceValue;
          sourceMassBalance.evaluate(eg.entity(), it->position(), sourceMassBalanceValue);
          Scalar elementVolume = eg.geometry().volume();
          r.accumulate(lfsu_p, 0,
                       -1.0 * sourceMassBalanceValue * elementVolume * it->weight());
        }
      }


      /**
       * \brief Skeleton integral depending on test and ansatz functions.
       *
       * Contribution of flux over interface. Each face is only visited once!
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions of self/inside
       * \param x_s coefficient vector of self/inside
       * \param lfsv_s local function space for test functions of self/inside
       * \param lfsu_n local functions space for ansatz functions of neighbor/outside
       * \param x_n coefficient vector of neighbor/outside
       * \param lfsv_n local function space for test functions of neighbor/outside
       * \param r_s residual vector of self/inside
       * \param r_n residual vector of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        // This function is empty on purpose. From the child classes this function
        // has to call the respective CNI version below.
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       * \param massMoleFrac_s phase composition of self/inside
       * \param temperature_s phase composition of neighbor/outside
       * \param massMoleFrac_n phase temperature of self/inside
       * \param temperature_n phase temperature of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_massmomentum(const IG& ig,
                                       const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                       const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                       R& r_s, R& r_n,
                                       std::vector<DimVector> velocities_s, Scalar pressure_s,
                                       Scalar massMoleFrac_s, Scalar temperature_s,
                                       std::vector<DimVector> velocities_n, Scalar pressure_n,
                                       Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        const LFSU_V& lfsu_v_n = lfsu_n.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        const LFSU_P& lfsu_p_n = lfsu_n.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // /////////////////////
        // geometry information

        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);


        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = std::abs(faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);
        Scalar distanceOutsideToFace = std::abs(outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim]);

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_n = asImp_().density(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar kinematicViscosity_n = asImp_().kinematicViscosity(pressure_n, temperature_n, massMoleFrac_n);

        // normal velocity
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);

        // averaging: distance weighted average quantities on intersection
        std::vector<RangeVelocity> velocities_avg(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            velocities_avg[curFace][curDim] = (distanceInsideToFace * velocities_n[curFace][curDim] + distanceOutsideToFace * velocities_s[curFace][curDim])
                / (distanceInsideToFace + distanceOutsideToFace);
          }
        }
        Scalar density_avg = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
        Scalar kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n + distanceOutsideToFace * kinematicViscosity_s)
                                    / (distanceInsideToFace + distanceOutsideToFace);

        if (enableDiffusionHarmonic_)
        {
          // averaging: harmonic averages for diffusion term
          density_avg = (2.0 * density_n * density_s)
                        / (density_n + density_s);
          kinematicViscosity_avg = (2.0 * kinematicViscosity_n * kinematicViscosity_s)
                                   / (kinematicViscosity_n + kinematicViscosity_s);
        }

        // upwinding (from self to neighbor)
        Scalar density_up = density_s;
        std::vector<RangeVelocity> velocities_up(numFaces);
        velocities_up = velocities_s;
        if (velocityNormal < 0)
        {
          density_up = density_n;
          velocities_up = velocities_n;
        }

        if (enableAdvectionAveraging_)
        {
            // distance weighted average mean
            density_up = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
            velocities_up = velocities_avg;
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call
        Dune::FieldVector<Scalar, dim> ones(1.0);

        /**
         * Contribution to the different balance equations. All formulas are given for
         * the <tt>self</tt> element.<br>
         * <br>
         * (1) \b Flux term of \b mass balance equation
         *
         * \f[
         *    \varrho v
         *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the property <tt>EnableAdvectionAveraging</tt> you can do an
         * averaging instead of upwinding for \b the density.
         */
        r_s.accumulate(lfsu_p_s, 0,
                       1.0 * density_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_p_n, 0,
                       -1.0 * density_up
                       * velocityNormal
                       * faceVolume);

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (2) \b Inertia term of \b momentum balance equation
           *
           * \f[
           *    \varrho v v^T
           *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
           * \f]
           * Tangential cases for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \left[ \left( \varrho v v^T \right) \cdot n \right] \cdot t
           *    = |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *      \left(v_\textrm{aou} \cdot t \right)
           *    = 0.5 |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *        v_\textrm{aou,left,t}
           *      + 0.5 |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *        v_\textrm{aou,right,t}
           * \f]
           * or shorter
           * \f[
           *    \alpha_\textrm{self}
           *    = 0.5 |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *       \left[ v_\textrm{aou,left,t} + v_\textrm{aou,right,t} \right]
           * \f]
           *
           * The default value is \b upwinding for the advective part, by
           * using the property <tt>EnableAdvectionAveraging</tt> you can do an
           * averaging instead of upwinding for the tangential velocity component
           * \b and the density.
           */
          if (enableNavierStokes_)
          {
              // only tangential case, exclude parallel case
              if (curDim != normDim)
              {
                unsigned int tangDim = curDim;
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                               0.5 * density_up
                               * velocityNormal
                               * velocities_up[2*tangDim][tangDim]
                               * faceVolume);
                r_n.accumulate(lfsu_v_n, 2*tangDim,
                               -0.5 * density_up
                               * velocityNormal
                               * velocities_up[2*tangDim][tangDim]
                               * faceVolume);
                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                               0.5 * density_up
                               * velocityNormal
                               * velocities_up[2*tangDim+1][tangDim]
                               * faceVolume);
                r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                               -0.5 * density_up
                               * velocityNormal
                               * velocities_up[2*tangDim+1][tangDim]
                               * faceVolume);
              }
          }

          /**
           * (3) \b Viscous term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}
           *    = - \nu \rho \nabla v
           *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
           *    = - \int_\gamma \nu \rho \left( \nabla v \cdot n \right)
           * \f]
           * Tangential cases for all coordinate axes (given for \b 2-D,
           * \b rectangular grids and \f$n=(0,1)^T\f$ which means balancing
           * momentum for \f$v_\textrm{0}\f$)<br>
           * \f[
           *    \alpha_\textrm{self}
           *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           * \f]
           * Tangential case, for: \f$\frac{\partial v_{0}}{\partial x_{1}}\f$,
           * right and left means along \f$x_1\f$ axis, \f$n\f$ means 1st entry,
           * \f$t\f$ means 0th entry
           * \f[
           *    A
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \left( 0.5 \left[\frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim}
           *           + 0.5 \left[ \frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim+1}
           *      \right)
           * \f]
           *
           * The default procedure for averaging
           * \f$\varrho_\textrm{avg}\f$, \f$\nu_\textrm{avg}\f$ is a distance weighted
           * average, by using the property <tt>EnableDiffusionHarmonic</tt>
           * one can do an harmonic averaging instead.
           */
          // only tangential case, exclude parallel case
          if (curDim != normDim)
          {
            unsigned int tangDim = curDim;
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                           -0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim,
                           0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                           -0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                           0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
          }
        }
      }

      /**
       * \brief Boundary integral depending on test and ansatz functions
       *
       * We put the Dirchlet evaluation also in the alpha term to save
       * some geometry evaluations.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions
       * \param x_s coefficient vector
       * \param lfsv_s local function space for test functions
       * \param r_s residual vector
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        // This function is empty on purpose. From the child classes this function
        // has to call the respective CNI version below.
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       * \param velocity phase velocity vector of the velocity on the inside the element faces
       * \param pressure_s phase pressure inside the element
       * \param massMoleFrac_s phase composition inside the element
       * \param temperature_s phase temperature inside the element
       * \param pressure_boundary phase pressure at the boundary face
       * \param massMoleFrac_boundary phase composition at the boundary face
       * \param temperature_boundary phase temperature at the boundary face
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_massmomentum(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                       std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                       Scalar massMoleFrac_s, Scalar temperature_s,
                                       Scalar pressure_boundary,
                                       Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();
        typedef typename BC::template Child<pressureIdx>::Type BCPressure;
        const BCPressure& bcPressure = bc.template child<pressureIdx>();

        // center in face's reference element
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face properties (coordinates and normal)
        const Dune::FieldVector<Scalar, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);
        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
          * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // cell center in reference element
        const Dune::FieldVector<Scalar,dim>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);

        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
//         const Scalar density_boundary = asImp_().density(pressure_boundary, temperature_boundary);
//         const Scalar kinematicViscosity_boundary = asImp_().kinematicViscosity(pressure_boundary, temperature_boundary);

        // Dirichlet boundary for pressure
        if (bcPressure.isDirichlet(ig, faceCenterLocal))
        {
          /*!
           * Dirichlet boundary handling for pressure / mass balance<br>
           * (1) \b Flux term of \b mass balance equation
           * \f[
           *    \varrho v
           *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho \left( v \cdot n \right)
           * \f]
           */
          // TODO check whether we should use density_boundary here
          r_s.accumulate(lfsu_p_s, 0,
                         1.0 * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        else if (bcPressure.isOutflow(ig, faceCenterLocal))
        {
          /*!
           * Outflow boundary handling for pressure / mass balance<br>
           * (1) \b Flux term of \b mass balance equation
           * \f[
           *    \varrho v
           *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_p_s, 0,
                         1.0 * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Coupling boundary for pressure
        else if (bcPressure.isCoupling(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Coupling Condition for pressure.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for pressure.");
        }

        // Wall or Inflow boundary for velocity
        int elementID = mapperElement.index(ig.inside());
        if ((bcVelocity.isWall(ig, faceCenterLocal) && !asImp_().useWallFunctionMomentum(elementID))
            || bcVelocity.isInflow(ig, faceCenterLocal))
        {
          for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
          {
            // tangential cases only
            if (tangDim != normDim)
            {
              // evaluate boundary condition functions
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCenter;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner0;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner1;
              Dune::FieldVector<Scalar, dim> insideFaceCornerLocal0(insideFaceCenterLocal);
              Dune::FieldVector<Scalar, dim> insideFaceCornerLocal1(insideFaceCenterLocal);
              insideFaceCornerLocal0[tangDim] = 0.0;
              insideFaceCornerLocal1[tangDim] = 1.0;

              dirichletVelocity.evaluate(ig.inside(), insideFaceCenterLocal, dirichletVelocityCenter);
              dirichletVelocity.evaluate(ig.inside(), insideFaceCornerLocal0, dirichletVelocityCorner0);
              dirichletVelocity.evaluate(ig.inside(), insideFaceCornerLocal1, dirichletVelocityCorner1);

              /**
               * Dirichlet boundary handling for velocity/ momentum balance<br>
               * (1) \b Inertia term of \b momentum balance equation
               *
               * \f[
               *    \varrho v v^T
               *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
               * \f]
               * Only the tangential case is regarded here, as if there is a Dirichlet value
               * for the velocity in face normal direction, it is automatically fixed.
               * \f[
               *    \alpha_\textrm{self}
               *    = |\gamma| \left[ \left( \varrho v v^T \right) \cdot n \right] \cdot t
               *    = |\gamma| \varrho \left( v \cdot n \right)
               *      \left(v \cdot t \right)
               *    = 0.5 |\gamma| \varrho \left( v \cdot n \right)
               *        v_\textrm{left,t}
               *      + 0.5 |\gamma| \varrho \left( v \cdot n \right)
               *        v_\textrm{right,t}
               * \f]
               */
              if (enableNavierStokes_)
              {
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                                0.5 * density_s
                                * dirichletVelocityCorner0[normDim]
                                * dirichletVelocityCorner0[tangDim]
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);
                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                                0.5 * density_s
                                * dirichletVelocityCorner1[normDim]
                                * dirichletVelocityCorner1[tangDim]
                                * faceUnitOuterNormal[normDim]
                                * faceVolume);
              }

              /**
               * (2) \b Viscous term of \b momentum balance equation
               *
               * \f[
               *    - \boldsymbol{\tau}
               *    = - \nu \rho \left( \nabla v \right)
               *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
               *    = - \int_\gamma \nu \rho \nabla v \cdot n
               * \f]
               * First the tangential case is regarded.
               * \f[
               *    \alpha_\textrm{self}
               *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
               *    = - |\gamma| \varrho \nu
               *      \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}}
               * \f]
               * The first tangential case \f$\left( \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}} \right)\f$,
               * is calculated from the velocities of the perpendicular faces of the
               * inside element and the Dirichlet value for the normal velocity.<br>
               */
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                              -0.5 * density_s * kinematicViscosity_s
                              * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                                / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                              * faceUnitOuterNormal[normDim]
                              * faceVolume);

              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                              -0.5 * density_s * kinematicViscosity_s
                              * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                                / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                              * faceUnitOuterNormal[normDim]
                              * faceVolume);
            }
            // parallel case
            else if (!GET_PROP_VALUE(TypeTag, FixVelocityConstraints))
            {
              // evaluate boundary condition functions
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCenter;
              dirichletVelocity.evaluate(ig.inside(), insideFaceCenterLocal, dirichletVelocityCenter);

              /**
               * If there is a Dirichlet value for the velocity in face normal direction, it is either
               * fixed via a constraint or is imposed by a penalty term.
               * \f[
               *    \alpha_\textrm{self}
               *    = 10^6 \left( v - v_\textrm{Dirichlet} \right) |\gamma|
               * \f]
               *
               * \todo How to choose the penalty value?
               */
              r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                             1e6 * (velocityFaces[ig.indexInInside()][normDim] - dirichletVelocityCenter[normDim])
                             * faceVolume);
            }
          }
        }
        else if (bcVelocity.isWall(ig, faceCenterLocal) && asImp_().useWallFunctionMomentum(elementID))
        {
          //! Do nothing for matching points in the k-epsilon model
        }
        // Outflow boundary for velocity
        else if (bcVelocity.isOutflow(ig, faceCenterLocal))
        {
          if (!bcPressure.isDirichlet(ig, faceCenterLocal))
          {
            std::cout << "At faceCenterGlobal " << faceCenterGlobal << "." << std::endl;
            std::cout.flush();
            DUNE_THROW(Dune::Exception, "Pressure has to be Dirichlet, when velocity is Outflow.");
          }
          /**
           * Outflow boundary handling for velocity / momentum balance<br>
           * (1) \b Inertia term of \b momentum balance equation
           *
           * \f[
           *    \varrho v v^T
           *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
           * \f]
           * parallel case
           * \f[
           *    \alpha_\textrm{left}
           *    = |\gamma| \varrho \left[ \left( v v \right) \cdot n \right] \cdot n
           *    = |\gamma| \varrho v v n
           * \f]
           */
          if (enableNavierStokes_)
          {
            // parallel case
            r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                          1.0 * density_s
                          * velocityFaces[ig.indexInInside()][normDim]
                          * velocityFaces[ig.indexInInside()][normDim]
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);

            for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
            {
              // only tangential cases
              if (tangDim != normDim)
              {
                r_s.accumulate(lfsu_v_s, 2*tangDim,
                              0.5 * density_s
                              * velocityFaces[ig.indexInInside()][normDim]
                              * velocityFaces[2*tangDim][tangDim] // may be use different (then stored) velocity here
                              * faceUnitOuterNormal[normDim]
                              * faceVolume);
                r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                              0.5 * density_s
                              * velocityFaces[ig.indexInInside()][normDim]
                              * velocityFaces[2*tangDim+1][tangDim] // may be use different (then stored) velocity here
                              * faceUnitOuterNormal[normDim]
                              * faceVolume);
              }
            }
          }

          /**
           * Outflow boundary handling for velocity / momentum balance<br>
           * (2) \b Pressure term of \b momentum balance equation
           *
           * \f[
           *    \nabla p
           *    \Rightarrow \int_\gamma p
           * \f]
           * parallel case
           * \f[
           *    \alpha_\textrm{left}
           *    = |\gamma| p
           * \f]
           */
          r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                          1.0 * pressure_boundary
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);
        }
        // Symmetry boundary for velocity
        else if (bcVelocity.isSymmetry(ig, faceCenterLocal))
        {
          if (velocityFaces[ig.indexInInside()][normDim] > 1e-6)
          {
            std::cout << "At faceCenterGlobal " << faceCenterGlobal << "." << std::endl;
            std::cout.flush();
            DUNE_THROW(Dune::Exception, "Normal velocity is not 0 at a Symmetry boundary.");
          }
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! But ensure the normal velocity is 0.
        }
        // Coupling boundary for velocity
        else if (bcVelocity.isCoupling(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Coupling Condition for velocity.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for velocity.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * Checks if fundamental assumptions for staggered are valid:
       *  - non-3D
       *  - <tt>curDim</tt>=0 is always the x-axis
       *  - <tt>curDim+1</tt> \f$ > \f$ <tt>curDim</tt> for all elements (checked
       *    by checking all face centers)
       *  - opposing face centers have the same coordinate in tangential direction
       *
       * Travers grid and store value, which are constant during simulation (like global positions)
       *
       * \tparam GridView GridView type
       */
      void initialize()
      {
        static const double epsilon = 1e-6;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();

        if (enableAdvectionAveraging_) // check if upwinding or averaging is used
        {
          std::cout << "EnableAdvectionAveraging = true" << std::endl;
        }

        if (enableDiffusionHarmonic_) // check if harmonic mean is used for diffusion term
        {
          std::cout << "EnableDiffusionHarmonic = true" << std::endl;
        }

        // ensure sane dimension
        if ((dim < 1 || dim > 3))
        {
          DUNE_THROW(Dune::NotImplemented,
                     "Staggered grid is only implemented for 1d, 2d, and 3d cases.");
        }

        // check element plausibality
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // global positions
          const unsigned int numControlVolumeFaces =
            Dune::ReferenceElements<double, dim>::general(eit->geometry().type()).size(1);
          std::vector<Dune::FieldVector<double, dim> > faceCentersGlobal(numControlVolumeFaces);
          for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
          {
            Dune::FieldVector<double, dim> faceCentersLocal;
            std::fill(faceCentersLocal.begin(), faceCentersLocal.end(), 0.5);
            faceCentersLocal[curFace/2] = curFace % 2;
            faceCentersGlobal[curFace] = eit->geometry().global(faceCentersLocal);
          }

          // iterate normal coordinate axis over all dimensions
          for (unsigned int normDim = 0; normDim < dim; ++normDim)
          {
            // check if the coordinate in normal direction of normDim+1 is always
            // greater than the one of normDim
            if (!(faceCentersGlobal[normDim*2+1][normDim]
                  > faceCentersGlobal[normDim*2][normDim]))
            {
              std::cout << "error in element: normDim+1 is not greater than normDim:" <<
                           "measured with epsilon=" << epsilon << "." << std::endl <<
              " the normal coordinate axis is " << normDim <<
              " faceCentersGlobal[normDim*2] " << faceCentersGlobal[normDim*2] <<
              " faceCentersGlobal[normDim*2+1] " << faceCentersGlobal[normDim*2+1] <<
              std::endl <<
              " in " <<  __FILE__ " line " << __LINE__ << std::endl;
              exit(1);
            }

            // iterate tangential coordinate axis over all dimensions expect for normDim
            for (unsigned int tangDim = 0; normDim < dim; ++normDim)
            {
              if (tangDim != normDim)
              {
                // check for normDim always x/y/z-axis
                if (std::abs(faceCentersGlobal[normDim*2+1][tangDim]
                    - faceCentersGlobal[normDim*2][tangDim]) > epsilon)
                {
                  std::cout << "error in element: tangDim coordinate from faceCenters differ, " <<
                    "measured with epsilon=" << epsilon << "." << std::endl <<
                    "the normal coordinate axis is " << normDim << std::endl <<
                    "the tangential coordinate axis is " << tangDim << std::endl <<
                    "faceCentersGlobal[normDim*2] " << faceCentersGlobal[normDim*2] <<
                    " faceCentersGlobal[normDim*2+1] " << faceCentersGlobal[normDim*2+1]
                    << std::endl <<
                    " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                  exit(3);
                }

                // check if faces in subsequent order have the same coordinate in tangential direction
                // and/or if opposing faces have the same coordinate in tangential direction
                if ((faceCentersGlobal[normDim*2][tangDim]
                    > faceCentersGlobal[normDim*2+1][tangDim] + epsilon)
                    || (faceCentersGlobal[normDim*2][tangDim]
                        < faceCentersGlobal[normDim*2+1][tangDim] - epsilon))
                {
                  std::cout << "error in element: two subsequent numbered faces do not have the" <<
                              "same coordinate in tangential direction," <<
                              "measured with epsilon=" << epsilon << "." << std::endl <<
                  " the normal coordinate axis is " << normDim <<
                  " faceCentersGlobal[normDim*2] " << faceCentersGlobal[normDim*2] <<
                  " faceCentersGlobal[normDim*2+1] " << faceCentersGlobal[normDim*2+1] <<
                  std::endl <<
                  "this could have two reasons:" << std::endl <<
                  " (1) two subsequent numbered faces are not opposing" << std::endl <<
                  " (2) two subsequent numbered faces are shifted tangentially" <<
                  std::endl <<
                  " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                  exit(2);
                }
              }
            }
          }
        } // loop over all elements

        // select the two components from the subspaces
        typedef typename BC::template Child<pressureIdx>::Type BCPressure;
        const BCPressure& bcPressure = bc.template child<pressureIdx>();

        // loop over grid view to get elemets with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            if (!ig->boundary())
            {
              continue;
            }

            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcPressure.isDirichlet(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcPressure.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcPressure.isCoupling(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos != 1)
            {
              std::cout << "BCPressure at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Dirichlet " << bcPressure.isDirichlet(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcPressure.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Coupling  " << bcPressure.isCoupling(*ig, faceCenterLocal) << std::endl;
              std::cout.flush();
              DUNE_THROW(Dune::Exception, "Multiple or no boundary conditions for pressure at one point.");
            }

            // check for multiple defined boundary conditions
            numberOfBCTypesAtPos = 0;
            if (bcVelocity.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isCoupling(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos != 1)
            {
              std::cout << "BCVelocity at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcVelocity.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcVelocity.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcVelocity.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcVelocity.isSymmetry(*ig, faceCenterLocal) << std::endl;
              std::cout << "Coupling  " << bcVelocity.isCoupling(*ig, faceCenterLocal) << std::endl;
              std::cout.flush();
              DUNE_THROW(Dune::Exception, "Multiple or no boundary conditions for velocity at one point.");
            }
          }
        }

        storedElementCentersGlobal.resize(mapperElement.size());
        storedElementFacesGlobal.resize(mapperElement.size());
        storedVelocitiesAtElementCenter.resize(mapperElement.size());
        storedVelocitiesAtElementFaces.resize(mapperElement.size());
        storedPressure.resize(mapperElement.size());
        storedTemperature.resize(mapperElement.size());
        storedMassMoleFrac.resize(mapperElement.size());
        storedKinematicViscosity.resize(mapperElement.size());
        storedDensity.resize(mapperElement.size());
        storedValueIsInStokesDomain.resize(mapperElement.size());

        // initialize some of the values
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedElementFacesGlobal[i].resize(2*dim);
          storedVelocitiesAtElementFaces[i].resize(2*dim);
          for (unsigned int k = 0; k < dim; ++k)
          {
            storedElementCentersGlobal[i][k] = 0.0;
            storedVelocitiesAtElementCenter[i][k] = 0.0;
            for (unsigned int j = 0; j < 2*dim; ++j)
            {
              storedElementFacesGlobal[i][j][k] = 0.0;
              storedVelocitiesAtElementFaces[i][j][k] = 0.0;
            }
          }
          storedPressure[i] = 0.0;
          storedTemperature[i] = 0.0;
          storedMassMoleFrac[i] = 0.0;
          storedKinematicViscosity[i] = 0.0;
          storedDensity[i] = 0.0;
          storedValueIsInStokesDomain[i] = false;
        }

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const Dune::FieldVector<Scalar, dim>& cellCenterLocal =
            Dune::ReferenceElements<Scalar, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<Scalar, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);
          storedElementCentersGlobal[elementInsideID] = cellCenterGlobal;

          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            Dune::FieldVector<Scalar, dim> faceLocalLeft(0.5), faceLocalRight(0.5);
            faceLocalLeft[curDim] = 0.0;
            faceLocalRight[curDim] = 1.0;

            Dune::FieldVector<Scalar, dim> faceGlobalLeft(0.0), faceGlobalRight(0.0);
            faceGlobalLeft = eit->geometry().global(faceLocalLeft);
            faceGlobalRight = eit->geometry().global(faceLocalRight);

            storedElementFacesGlobal[elementInsideID][2*curDim] = faceGlobalLeft;
            storedElementFacesGlobal[elementInsideID][2*curDim+1] = faceGlobalRight;
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * Travers grid and store velocites from last Newton step or time step
       * The velocities are copied across each intersection to be available on the other
       * side adjacent face. This is only necessary for the tangential case of the viscous
       * term, which means <tt>dim > 1</tt>.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsVelocity = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, velocityIdx> >;
        using SubGfsPressure = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, pressureIdx> >;
        SubGfsVelocity subGfsVelocity(mdgfs);
        SubGfsPressure subGfsPressure(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsVelocity = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<velocityIdx> >;
        using SubGfsPressure = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<pressureIdx> >;
        SubGfsVelocity subGfsVelocity(gfs);
        SubGfsPressure subGfsPressure(gfs);
#endif

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // discrete function objects
        using DgfVelocity = Dune::PDELab::DiscreteGridFunction<SubGfsVelocity, X>;
        using DgfPressure = Dune::PDELab::DiscreteGridFunction<SubGfsPressure, X>;
        DgfVelocity dgfVelocity(subGfsVelocity, lastSolution);
        DgfPressure dgfPressure(subGfsPressure, lastSolution);

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // NOTE use eit also for multidomain models
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          if (!gridView.indexSet().contains(stokesDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif
          storedValueIsInStokesDomain[elementInsideID] = true;

          // evaluate solution
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);
          Dune::FieldVector<Scalar, 1> pressure(0.0);
          dgfPressure.evaluate(element, cellCenterLocal, pressure);

          storedPressure[elementInsideID] = pressure;
          Scalar temperature = storedTemperature[elementInsideID];
          Scalar massMoleFrac = storedMassMoleFrac[elementInsideID];
          storedDensity[elementInsideID] = asImp_().density(pressure, temperature, massMoleFrac);
          storedKinematicViscosity[elementInsideID] = asImp_().kinematicViscosity(pressure, temperature, massMoleFrac);

          // parallel case
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            Dune::FieldVector<Scalar, dim> faceLocalLeft(0.5), faceLocalRight(0.5);
            faceLocalLeft[curDim] = 0.0;
            faceLocalRight[curDim] = 1.0;

            // evaluate solution
            Dune::FieldVector<Scalar, dim> vLeft(0.0);
            Dune::FieldVector<Scalar, dim> vRight(0.0);
            dgfVelocity.evaluate(element, faceLocalLeft, vLeft);
            dgfVelocity.evaluate(element, faceLocalRight, vRight);

            // this only works if the velocity entry in curDim is used instead of the complete vector
            storedVelocitiesAtElementFaces[elementInsideID][2*curDim][curDim] = vLeft[curDim];
            storedVelocitiesAtElementFaces[elementInsideID][2*curDim+1][curDim] = vRight[curDim];
            storedVelocitiesAtElementCenter[elementInsideID][curDim] = (vRight[curDim] + vLeft[curDim]) / 2.0;
          }
        }
      }


      /**
       * \brief Returns the velocities for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const std::vector<DimVector> velocity (const EG& eg, const LFSU& lfsu, const X& x) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
        }

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v.size());
          lfsu_v.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // evaluate velocity on intersection
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v.size(); i++)
          {
            velocityFaces[curFace].axpy(x(lfsu_v, i), velocityBasis[curFace][i]);
          }
        }

        return velocityFaces;
      }

      /**
       * \brief Returns the pressure for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar pressure(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();
        return x(lfsu_p, 0);
      }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;

      MapperElement mapperElement;

      // properties
      bool enableNavierStokes_;
      bool enableGravity_;
      bool enableAdvectionAveraging_;
      bool enableDiffusionHarmonic_;

      Scalar gravity_;
      unsigned int gravityDim_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_NAVIER_STOKES_STAGGERED_GRID_DUMUX_HH
