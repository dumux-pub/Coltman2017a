// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for Navier-Stokes boundary conditions
 */
#ifndef DUMUX_NAVIERSTOKES_BOUNDARY_CONDITIONS_HH
#define DUMUX_NAVIERSTOKES_BOUNDARY_CONDITIONS_HH

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

#include<appl/staggeredgrid/common/velocitywallfunctions.hh>

#include "navierstokesproperties.hh"

namespace Dumux
{
/**
 * \brief Boundary condition function for the velocity components.
 */
template<class TypeTag>
class BCVelocity
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCVelocity (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Wall Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isWall(const I& intersection,
              const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcVelocityIsWall(global);
  }

  //! \brief Return whether Intersection is Inflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isInflow(const I& intersection,
                const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcVelocityIsInflow(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcVelocityIsOutflow(global);
  }

  //! \brief Return whether Intersection is Symmetry Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isSymmetry(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcVelocityIsSymmetry(global);
  }

  //! \brief Return whether Intersection is Coupling Boundary for velocity
  //! \tparam I Intersection type
  template<typename I>
  bool isCoupling(const I& intersection,
                  const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcVelocityIsCoupling(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Boundary condition function for the pressure component.
 */
template<class TypeTag>
class BCPressure
: public Dune::PDELab::DirichletConstraintsParameters
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

  //! \brief Constructor
  BCPressure (Problem& problem)
  : problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Return whether Intersection is Dirichlet Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isDirichlet(const I& intersection,
                   const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcPressureIsDirichlet(global);
  }

  //! \brief Return whether Intersection is Outflow Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isOutflow(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcPressureIsOutflow(global);
  }

  //! \brief Return whether Intersection is Coupling Boundary for pressure
  //! \tparam I Intersection type
  template<typename I>
  bool isCoupling(const I& intersection,
                 const Dune::FieldVector<typename I::ctype, I::dimension-1>& coord) const
  {
    Dune::FieldVector<typename I::ctype, I::dimension>
      global = intersection.geometry().global(coord);
    return problem_().bcPressureIsCoupling(global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Function for velocity Dirichlet boundary conditions and initialization.
 *
 * \tparam GridView GridView type
 * \tparam Scalar Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, GridView::dimension>,
    DirichletVelocity<TypeTag,GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults,
  public Dumux::VelocityWallFunctions<TypeTag>
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, GridView::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletVelocity<TypeTag, GridView, Scalar> > BaseT;
  typedef Dumux::VelocityWallFunctions<TypeTag> VelocityWallFunctions;

  //! \brief Constructor
  DirichletVelocity (const GridView& gridView, Problem& problem)
  : BaseT(gridView), VelocityWallFunctions(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function per element
  inline void evaluate(const typename Traits::ElementType& e,
                       const typename Traits::DomainType& x,
                       typename Traits::RangeType& y) const
  {
    const typename Traits::DomainType& global = e.geometry().global(x);
    y = problem_().dirichletVelocityAtPos(e, global);
  }

  Scalar roughness(const typename Traits::ElementType& e,
                   const typename Traits::DomainType& x) const
  {
    const typename Traits::DomainType& global = e.geometry().global(x);
    return problem_().roughness(e, global);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for pressure Dirichlet boundary conditions and initialization.
 *
 * \tparam GridView GridView type
 * \tparam Scalar Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class DirichletPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    DirichletPressure<TypeTag, GridView, Scalar> >,
  public Dune::PDELab::InstationaryFunctionDefaults
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, DirichletPressure<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  DirichletPressure (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().dirichletPressureAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Function for velocity Neumann boundary conditions and initialization.
 *
 * \tparam GridView GridView type
 * \tparam Scalar Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannVelocity
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, GridView::dimension>,
    NeumannVelocity<TypeTag, GridView, Scalar> >
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, GridView::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannVelocity<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannVelocity (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannVelocityAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Function for pressure Neumann boundary conditions and initialization.
 *
 * \tparam GridView GridView type
 * \tparam Scalar Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class NeumannPressure
: public Dune::PDELab::AnalyticGridFunctionBase<
    Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
    NeumannPressure<TypeTag, GridView, Scalar> >
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, NeumannPressure<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  NeumannPressure (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().neumannPressureAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};


/**
 * \brief Source term function for the momentum balance.
 *
 * \tparam GridView GridView type
 * \tparam Scalar Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceMomentumBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, GridView::dimension>,
      SourceMomentumBalance<TypeTag, GridView, Scalar> >
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, GridView::dimension> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMomentumBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceMomentumBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceMomentumBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

/**
 * \brief Source term function for the mass balance.
 *
 * \tparam GridView GridView type
 * \tparam Scalar Range type
 */
template<class TypeTag, typename GridView, typename Scalar>
class SourceMassBalance
  : public Dune::PDELab::AnalyticGridFunctionBase<
      Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1>,
      SourceMassBalance<TypeTag, GridView, Scalar> >
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef Dune::PDELab::AnalyticGridFunctionTraits<GridView, Scalar, 1> Traits;
  typedef Dune::PDELab::AnalyticGridFunctionBase<Traits, SourceMassBalance<TypeTag, GridView, Scalar> > BaseT;

  //! \brief Constructor
  SourceMassBalance (const GridView& gridView, Problem& problem)
  : BaseT(gridView), problemPtr_(0)
  {
    problemPtr_ = &problem;
  }

  //! \brief Evaluate the function globally
  inline void evaluateGlobal(const typename Traits::DomainType& x,
                             typename Traits::RangeType& y) const
  {
    y = problem_().sourceMassBalanceAtPos(x);
  }

protected:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    Problem *problemPtr_;
};

} // end namespace Dumux

#endif
