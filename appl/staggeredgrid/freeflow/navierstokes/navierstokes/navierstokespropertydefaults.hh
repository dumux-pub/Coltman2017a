// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines default properties for the Navier-Stokes staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_NAVIERSSTOKES_PROPERTY_DEFAULTS_HH
#define DUMUX_NAVIERSSTOKES_PROPERTY_DEFAULTS_HH

#include<dune/grid/common/mcmgmapper.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/superlu.hh>
#include<dune/pdelab/constraints/common/constraints.hh>

#include <dumux/common/intersectionmapper.hh>
#include <dumux/material/fluidstates/immiscible.hh>
#include <dumux/material/fluidsystems/1p.hh>
#include <dumux/material/components/nullcomponent.hh>

#include <appl/staggeredgrid/common/fixpressureconstraints.hh>
#include <appl/staggeredgrid/common/fixvelocityconstraints.hh>

#include "navierstokesboundaryconditions.hh"
#include "navierstokesindices.hh"
#include "navierstokesproperties.hh"

namespace Dumux
{

template<class TypeTag> class BCVelocity;
template<class TypeTag> class BCPressure;
template<class TypeTag, typename GridView, typename Scalar> class DirichletVelocity;
template<class TypeTag, typename GridView, typename Scalar> class DirichletPressure;
template<class TypeTag, typename GridView, typename Scalar> class NeumannVelocity;
template<class TypeTag, typename GridView, typename Scalar> class NeumannPressure;
template<class TypeTag, typename GridView, typename Scalar> class SourceMomentumBalance;
template<class TypeTag, typename GridView, typename Scalar> class SourceMassBalance;

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set property value for the time manager
SET_TYPE_PROP(StaggeredGridNavierStokes, TimeManager, Dumux::TimeManager<TypeTag>);

//! Set property value for the grid
SET_TYPE_PROP(StaggeredGridNavierStokes, Grid,
              Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridNavierStokes, Indices,
              StaggeredGridNavierStokesCommonIndices<TypeTag>);

//! Set property value for the DimVector
SET_PROP(StaggeredGridNavierStokes, DimVector)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef Dune::FieldVector<Scalar, GridView::dimension> type;
};

//! Set property value for the element mapper
SET_PROP(StaggeredGridNavierStokes, MapperElement)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> type;
};

//! Set property value for the velocity boundary condition
SET_TYPE_PROP(StaggeredGridNavierStokes, BCVelocity, BCVelocity<TypeTag>);

//! Set property value for the pressure boundary condition
SET_TYPE_PROP(StaggeredGridNavierStokes, BCPressure, BCPressure<TypeTag>);

//! Set the contraints types
SET_TYPE_PROP(StaggeredGridNavierStokes, PressureConstraints,
              Dumux::FixPressureConstraints<TypeTag>);
SET_TYPE_PROP(StaggeredGridNavierStokes, VelocityConstraints,
              Dumux::FixVelocityConstraints<TypeTag>);

//! Set property value for BCType
SET_PROP(StaggeredGridNavierStokes, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure> type;
};

//! Set property value for DirichletVelocity
SET_PROP(StaggeredGridNavierStokes, DirichletVelocity)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletVelocity<TypeTag, GridView, Scalar> type;
};

//! Set property value for DirichletPressure
SET_PROP(StaggeredGridNavierStokes, DirichletPressure)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletPressure<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannVelocity
SET_PROP(StaggeredGridNavierStokes, NeumannVelocity)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannVelocity<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannPressure
SET_PROP(StaggeredGridNavierStokes, NeumannPressure)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannPressure<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceMomentumBalance
SET_PROP(StaggeredGridNavierStokes, SourceMomentumBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceMomentumBalance<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceMassBalance
SET_PROP(StaggeredGridNavierStokes, SourceMassBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceMassBalance<TypeTag, GridView, Scalar> type;
};

//!< Property tag to constraint the velocity dof
SET_BOOL_PROP(StaggeredGridNavierStokes, FixVelocityConstraints, true);

//!< Property tag to constraint the pressure dof
SET_BOOL_PROP(StaggeredGridNavierStokes, FixPressureConstraints, false);

//! Set calculation to Navier-Stokes instead of Stokes
SET_BOOL_PROP(StaggeredGridNavierStokes, ProblemEnableNavierStokes, true);

//! Do not use the symmetrized velocity gradient for the Stokes equation
SET_BOOL_PROP(StaggeredGridNavierStokes, ProblemEnableUnsymmetrizedVelocityGradient, true);

//! Unset gravity in standard cases
SET_BOOL_PROP(StaggeredGridNavierStokes, ProblemEnableGravity, false);

//! Use an upwinding scheme for the advection terms
SET_BOOL_PROP(StaggeredGridNavierStokes, ProblemEnableAdvectionAveraging, false);

//! Use an arithmetic averaging scheme for the diffsion terms
SET_BOOL_PROP(StaggeredGridNavierStokes, ProblemEnableDiffusionHarmonic, false);


//! The fluid system to use by default
SET_PROP(StaggeredGridNavierStokes, FluidSystem)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Fluid) Fluid;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::OneP<Scalar, Fluid> type;
};

//! The fluid that is used in the single-phase FluidSystem
SET_PROP(StaggeredGridNavierStokes, Fluid)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::FluidSystems::LiquidPhase<Scalar, Dumux::NullComponent<Scalar> > type;
};

//! Choose the type of the employed fluid state.
SET_PROP(StaggeredGridNavierStokes, FluidState)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
public:
    typedef Dumux::ImmiscibleFluidState<Scalar, FluidSystem> type;
};

//! Set the phaseIndex per default to zero (important for two-phase fluidsystems).
SET_INT_PROP(StaggeredGridNavierStokes, PhaseIdx, 0);

//! Set number of maximum Newton steps to 8
SET_INT_PROP(StaggeredGridNavierStokes, NewtonMaxSteps, 8);

//! Set number of maximum timestep divisions to 10
SET_INT_PROP(StaggeredGridNavierStokes, NewtonMaxTimeStepDivisions, 10);

//! Set maximum relative shift criterion for convergence of Newton solver
SET_SCALAR_PROP(StaggeredGridNavierStokes, NewtonMaxRelativeShift, 1e-8);

//! Set reduction for convergence of Newton solver
SET_SCALAR_PROP(StaggeredGridNavierStokes, NewtonResidualReduction, 1e-5);

//! Set absolute limit for convergence of Newton solver
SET_SCALAR_PROP(StaggeredGridNavierStokes, NewtonAbsoluteLimit, 1e-11);

//! Set linear solver to be used
// SET_TYPE_PROP(StaggeredGridNavierStokes, LinearSolver, Dune::PDELab::ISTLBackend_SEQ_SuperLU);
SET_TYPE_PROP(StaggeredGridNavierStokes, LinearSolver, Dune::PDELab::ISTLBackend_SEQ_UMFPack);

//! Print always a vtk file
SET_INT_PROP(StaggeredGridNavierStokes, OutputVtkFrequency, 1);

//! Do not print the error convergence on default
SET_BOOL_PROP(StaggeredGridNavierStokes, OutputErrorConvergence, false);

//! set the number of equations
SET_PROP(StaggeredGridNavierStokes, NumEq)
{
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    static const int dim = Grid::dimension;
public:
    static constexpr int value = 1 + dim;
};
}
}

#endif // DUMUX_NAVIERSSTOKES_PROPERTY_DEFAULTS_HH
