
install(FILES
        basezeroeqstaggeredgrid.hh
        zeroeqindices.hh
        zeroeqproblem.hh
        zeroeqproperties.hh
        zeroeqpropertydefaults.hh
        zeroeqstaggeredgrid.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/freeflow/zeroeq/zeroeq)
