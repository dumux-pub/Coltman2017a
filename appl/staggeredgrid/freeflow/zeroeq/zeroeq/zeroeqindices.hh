// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Defines the indices required for the zeroeq staggered grid model.
 */
#ifndef DUMUX_ZEROEQ_INDICES_HH
#define DUMUX_ZEROEQ_INDICES_HH

#include "zeroeqproperties.hh"

namespace Dumux
{

/*!
 * \brief The common indices for the zeroeq staggered grid model.
 */
template <class TypeTag>
struct StaggeredGridZeroEqCommonIndices
{

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    // Primary variable indices
    static const int numEq = 2;
    static const int velocityIdx = 0; //!< Index of the velocity
    static const int pressureIdx = 1; //!< Index of the pressure

    //! Index of the fluid phase set by default to nPhase
    static const int phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);
};
} // end namespace

#endif // DUMUX_ZEROEQ_INDICES_HH
