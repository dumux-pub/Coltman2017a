/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        energy transport zeroeq equation
 *
 * \copydoc BaseEnergyStaggeredGrid
 */

#ifndef DUMUX_BASE_ZEROEQ_TEMPERATURE_STAGGERED_GRID_HH
#define DUMUX_BASE_ZEROEQ_TEMPERATURE_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosity2cniindices.hh>
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/baseenergystaggeredgrid.hh>

#include"zeroeq2cnipropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the steady-state energy transport zeroeq equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseZeroEqEnergyStaggeredGrid
    : public BaseEnergyStaggeredGrid<TypeTag>
    {
    public:
      typedef BaseEnergyStaggeredGrid<TypeTag> ParentTypeEnergy;

      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceEnergyBalance) SourceEnergyBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletTemperature) DirichletTemperature;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannTemperature) NeumannTemperature;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { massMoleFracIdx = Indices::massMoleFracIdx };
      enum { numComponents = Indices::numComponents,
             phaseIdx = Indices::phaseIdx,
             transportCompIdx = Indices::transportCompIdx,
             phaseCompIdx = Indices::phaseCompIdx };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      // Types to store the velocities and coordinates
      typedef std::vector<double> StoredScalar;
      StoredScalar storedThermalEddyConductivity;

      //! \brief Constructor
      BaseZeroEqEnergyStaggeredGrid(const BC& bc_,
        const SourceEnergyBalance& sourceEnergyBalance_,
        const DirichletTemperature& dirichletTemperature_,
        const NeumannTemperature& neumannTemperature_,
        GridView gridView_)
        : ParentTypeEnergy(bc_, sourceEnergyBalance_,
              dirichletTemperature_, neumannTemperature_,
              gridView_),
          bc(bc_),
          sourceEnergyBalance(sourceEnergyBalance_),
          dirichletTemperature(dirichletTemperature_),
          neumannTemperature(neumannTemperature_),
          gridView(gridView_), mapperElement(gridView_)
      {
        karmanConstant_ = GET_PROP_VALUE(TypeTag, KarmanConstant);
        eddyConductivityModel_ = GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyConductivityModel);
        turbulentPrandtlNumber_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, TurbulentPrandtlNumber);

        initialize();
      }

      /**
       * \copydoc BaseEnergyStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_energy(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                               std::vector<DimVector> velocityFaces, Scalar pressure,
                               Scalar massMoleFrac, Scalar temperature) const
      {
        ParentTypeEnergy::alpha_volume_energy(eg, lfsu, x, lfsv, r,
                                              velocityFaces, pressure, massMoleFrac, temperature);
      }


      /**
       * \copydoc BaseEnergyStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_energy(const IG& ig,
                                 const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                 const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                 R& r_s, R& r_n,
                                 std::vector<DimVector> velocities_s, Scalar pressure_s,
                                 Scalar massMoleFrac_s, Scalar temperature_s,
                                 std::vector<DimVector> velocities_n, Scalar pressure_n,
                                 Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        ParentTypeEnergy::alpha_skeleton_energy(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseEnergyStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_energy(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                 std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                 Scalar massMoleFrac_s, Scalar temperature_s,
                                 Scalar pressure_boundary,
                                 Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        ParentTypeEnergy::alpha_boundary_energy(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                pressure_boundary, massMoleFrac_boundary, temperature_boundary);
      }

      /**
       * \copydoc BaseEnergyStaggeredGrid::initialize
       */
      void initialize()
      {
        storedThermalEddyConductivity.resize(mapperElement.size());
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedThermalEddyConductivity[i] = 0.0;
        }

        ParentTypeEnergy::initialize();

        std::cout << "Used eddy conductivity model = ";
        switch (eddyConductivityModel_)
        {
          case EddyConductivityIndices::noEddyConductivityModel:
            std::cout << "no eddy conductivity model" << std::endl;
            break;
          case EddyConductivityIndices::reynoldsAnalogy:
            std::cout << "reynoldsAnalogy" << std::endl;
            break;
          case EddyConductivityIndices::modifiedVanDriest:
            std::cout << "modifiedVanDriest" << std::endl;
            break;
          case EddyConductivityIndices::deissler:
            std::cout << "deissler" << std::endl;
            break;
          case EddyConductivityIndices::meier:
            std::cout << "meier" << std::endl;
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy conductivity method is not implemented.");
        }
      }

      /**
       * \copydoc BaseEnergyStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        ParentTypeEnergy::template updateStoredValues
                          <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                          (sdgv, mdgfs, lastSolution);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        ParentTypeEnergy::updateStoredValues(gfs, lastSolution);
#endif
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        switch (eddyConductivityModel_)
        {
          case EddyConductivityIndices::noEddyConductivityModel:
            for (ElementIterator eit = gridView.template begin<0>();
                  eit != gridView.template end<0>(); ++eit)
            {
              const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
              asImp_().storedThermalEddyConductivity[elementInsideID] = 0.0;
            }
            break;
          case EddyConductivityIndices::reynoldsAnalogy: // 1
            reynoldsAnalogy();
            break;
          case EddyConductivityIndices::modifiedVanDriest: // 2
            modifiedVanDriest();
            break;
          case EddyConductivityIndices::deissler: // 3
            deissler();
            break;
          case EddyConductivityIndices::meier: // 4
            meier();
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy eddy conductivity method is not implemented.");
        }
      }

      /**
       * \brief Converts the eddy conductivity based on the kinematic eddy viscosity
       *
       * \f$ \lambda_\text{t} = \frac{\nu_\text{t} c_\textrm{p} \varrho}{\text{Pr}_\text{t}} \f$
       */
      void reynoldsAnalogy()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
              eit != gridView.template end<0>(); ++eit)
        {
          asImp_().storedThermalEddyConductivity[mapperElement.index(*eit)]
            = asImp_().storedKinematicEddyViscosity[mapperElement.index(*eit)]
              * asImp_().storedSpecificHeatCapacity[mapperElement.index(*eit)]
              * asImp_().storedDensity[mapperElement.index(*eit)]
              / turbulentPrandtlNumber_;
        }
      }

      /**
       * \brief Converts the eddy conductivity based on the kinematic eddy viscosity
       *
       * \f[ \lambda_\textrm{t} =  c_\textrm{p} \varrho l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \frac{1 - \exp \left( - y^+ / 26 \right)}{\sqrt{1.0 - \exp \left( -0.26 y^+ \right)}}] \f$
       * and \f$ \kappa = 0.4 \f$
       */
      void modifiedVanDriest()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double velocityGradient = asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis];
          // conversion to yPlusRough
          double yPlusRough = asImp_().storedDistanceInWallCoordinates[elementInsideID]
                              / asImp_().storedDistanceToWall[elementInsideID] * wallDistance;
          double mixingLength = 0.0;
          if (wallDistance > 0.0 && yPlusRough > 0.0)
            mixingLength = karmanConstant_ * wallDistance
                          * (1.0 - std::exp(-yPlusRough / 26.0 ))
                          / std::sqrt(1.0 - std::exp(-0.26 * yPlusRough));

          asImp_().storedThermalEddyConductivity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient)
              * asImp_().storedSpecificHeatCapacity[elementInsideID] * asImp_().storedDensity[elementInsideID];
        }
      }

      /**
       * \brief Calculates the  eddy conductivity based on Deissler's formula
       *
       * \f[ \lambda_\textrm{t} = c_\textrm{p} \varrho \beta \left( 1.0 - \exp \left( \nicefrac{-\beta}{\nu} \right) \right) \f]
       * with \f$ \beta =  0.124^2 v_\text{x} y \f$ and 0.124 as the Deissler constant
       */
      void deissler()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
              eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double deisslerConstant = 0.124;
          double beta = deisslerConstant * deisslerConstant
                        * asImp_().storedVelocitiesAtElementCenter[elementInsideID][flowNormalAxis]
                        * wallDistance;
          asImp_().storedThermalEddyConductivity[elementInsideID]
            = beta * (1.0 - exp(-beta / asImp_().storedKinematicViscosity[elementInsideID]))
              * asImp_().storedSpecificHeatCapacity[elementInsideID]
              * asImp_().storedDensity[elementInsideID];
        }
      }

      /**
       * \brief Calculates the eddy conductivity based on Meier's formula
       *
       * \f[ D_\textrm{t} = c_\textrm{p} \varrho l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = k_\text{m} y \left( 1.0 - \exp \left( \nicefrac{y^+}{32.45} \right) \right) \f$
       * and the Maier constant \f$ k_\text{m} = 0.44 \f$
       */
      void meier()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
              eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double velocityGradient = asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis];
          // conversion to yPlusRough
          double yPlusRough = asImp_().storedDistanceInWallCoordinates[elementInsideID]
                              / asImp_().storedDistanceToWall[elementInsideID] * wallDistance;
          double kConstantMeier = 0.44;
          double aPlusMeier = 32.45;
          double mixingLength = 0.0;
          if (wallDistance > 0.0 && yPlusRough > 0.0)
            mixingLength = kConstantMeier * wallDistance * (1.0 - exp(-yPlusRough / aPlusMeier));
          asImp_().storedThermalEddyConductivity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient)
              * asImp_().storedSpecificHeatCapacity[elementInsideID]
              * asImp_().storedDensity[elementInsideID];
        }
      }

      /**
       * \brief Returns the eddy thermal conductivity for a given element [W/(m*K)]
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar thermalEddyConductivity(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(eg);
        return asImp_().storedThermalEddyConductivity[elementInsideID];
      }

private:
      const BC& bc;
      const SourceEnergyBalance& sourceEnergyBalance;
      const DirichletTemperature& dirichletTemperature;
      const NeumannTemperature& neumannTemperature;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      unsigned int eddyConductivityModel_;
      Scalar turbulentPrandtlNumber_;
      Scalar karmanConstant_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_ZEROEQ_TEMPERATURE_STAGGERED_GRID_HH
