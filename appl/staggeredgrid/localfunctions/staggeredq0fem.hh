/** \file
 *  \ingroup StaggeredLocalfunctions
 *
 *  \brief Staggered Q0 finite elements
*/

#ifndef DUMUX_STAGGERED_Q0_FEM_HH
#define DUMUX_STAGGERED_Q0_FEM_HH

#include<dune/common/exceptions.hh>
#include<dune/pdelab/finiteelementmap/finiteelementmap.hh>

#include"staggeredq0.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Wrap up staggered Q0 finite elements from local functions
     *
     * \tparam DF Domain field data type
     * \tparam RF Range field data type
     * \tparam dim Dimension of the reference element
     */
    template<class DF, class RF, unsigned int dim>
    class StaggeredQ0LocalFiniteElementMap
      : public SimpleLocalFiniteElementMap<
        Dune::StaggeredQ0LocalFiniteElement<DF, RF, dim> >
    {
    public:
      //! \brief Returns true, if all geometry types have the same number of dof
      bool fixedSize() const
      {
        return false;
      }

      //! \brief Is called if fixedSize() is true, number of dofs in a geometry type
      std::size_t size(GeometryType gt) const
      {
        DUNE_THROW(Dune::NotImplemented, "fixedSize() returns false, thus not implemented");
      }

      //! \brief Maximum number of dofs per element
      std::size_t maxLocalSize() const
      {
        return 2 * dim;
      }

      //! \brief DOFs might be attached to the given codimension
      bool hasDOFs(int codim) const
      {
        return codim == 1;
      }
    };
  }
}

#endif // DUMUX_STAGGERED_Q0_FEM_HH
