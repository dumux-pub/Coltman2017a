
install(FILES
        staggeredq0fem.hh
        staggeredq0.hh
        staggeredq0localbasis.hh
        staggeredq0localcoefficients.hh
        staggeredq0localinterpolation.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/appl/staggeredgrid/localfunctions)
