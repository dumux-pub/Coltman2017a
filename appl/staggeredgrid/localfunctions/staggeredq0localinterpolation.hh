/** \file
 *  \ingroup StaggeredLocalfunctions
 *
 *  \brief The interpolation for staggered Q0 finite elements
*/

#ifndef DUMUX_STAGGERED_Q0_LOCALINTERPOLATION_HH
#define DUMUX_STAGGERED_Q0_LOCALINTERPOLATION_HH

#include <vector>
#include <dune/geometry/referenceelements.hh>

namespace Dune
{
  /**
   * \brief Interpolation operator for staggered Q0 finite elements.
   *
   * \tparam LB corresponding LocalBasis giving traits
   */
  template<class LB>
  class StaggeredQ0LocalInterpolation
  {
  public:
    //! \brief Constructor
    StaggeredQ0LocalInterpolation()
    {}

    /**
     * \brief Determine coefficients interpolating a given function
     *
     * \tparam F Function type
     * \tparam C Coefficent type
     */
    template<typename F, typename C>
    void interpolate (const F& f, std::vector<C>& out) const
    {
      typename LB::Traits::DomainType x;
      typename LB::Traits::RangeType y;
      const int dim = LB::Traits::dimDomain;

      out.resize(2 * dim);

      // create midpoint of local element
      x = typename LB::Traits::DomainType(0.5);

      // evaluate at each face midpoint
      for (int curDim = 0; curDim < dim; ++curDim)
      {
        // move toward one side in direction of current dimension
        x[curDim] = 0.0;
        f.evaluate(x, y);
        out[2*curDim] = y[curDim];

        // move toward other side
        x[curDim] = 1.0;
        f.evaluate(x, y);
        out[2*curDim + 1] = y[curDim];

        // reset value to midpoint
        x[curDim] = 0.5;
        }
    }
  };
}

#endif // DUMUX_STAGGERED_Q0_LOCALINTERPOLATION_HH
